import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PagesRoutes } from './pages.routing';
import { LoadingModule } from '../shared/loading/loading.module';

@NgModule({
	declarations: [],
	imports: [
		CommonModule,
		RouterModule.forChild(PagesRoutes),
		FormsModule,
		MaterialModule,
		ReactiveFormsModule,
		LoadingModule
	]
})

export class PagesModule { }
