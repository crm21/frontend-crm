import { Routes } from '@angular/router';

import { ListarAgendaComponent } from './listar-agenda/listar-agenda.component';
import { RegistrarAgendaClienteComponent } from './registrar-agenda-cliente/registrar-agenda-cliente.component';
import { AgendamientoComponent } from './agendamiento/agendamiento.component';
import { RegistrarAgendaTallerComponent } from './registrar-agenda-taller/registrar-agenda-taller.component';

export const AgendamientoPosventaRoutes: Routes = [
	{
		path: '',
		redirectTo: 'listar',
		pathMatch: 'full',
	},
	{
		path: '',
		children: [
			{
				path: 'listar',
				component: ListarAgendaComponent
			}, {
				path: 'agendar-cliente',
				component: RegistrarAgendaClienteComponent
			}, {
				path: 'agendamiento',
				component: AgendamientoComponent
			}, {
				path: 'agendar-taller',
				component: RegistrarAgendaTallerComponent
			}
		]
	}
];