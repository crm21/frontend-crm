import { Component, OnInit } from "@angular/core";
import { DatePipe } from "@angular/common";
import { Router } from "@angular/router";
import swal from "sweetalert2";
import { ReporteBdcService } from '../../services/reporte-bdc/reporte-bdc.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { TableroAsignacionService } from '../../services/tablero-asignacion/tablero-asignacion.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';

declare var $: any;
declare interface TableData {
	headerRow: string[];
	dataRows: string[][];
}


@Component({
	selector: 'app-atenciones-tipificacion',
	templateUrl: './atenciones-tipificacion.component.html',
	styleUrls: ['./atenciones-tipificacion.component.css']
})
export class AtencionesTipificacionComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	dataTable: TableData;
	reporteArray: any[] = [];
	tipificacionArray: any[] = [];
	textoExtendido: string = '';
	busquedaFiltros: any = {
		tipificacion: ''
	};
	tipificacionValor: string = '';
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;

	constructor(
		private _sweetAlertService: SweetAlertService,
		private _tableroAsignacionService: TableroAsignacionService,
		private _parametrizacionService: ParametrizacionService,
		private _router: Router
	) { this.datosTabla(); }

	ngOnInit() {
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			this.listarTipificacionesPorCliente('1');
		}
	}

	datosTabla() {
		if (localStorage.getItem('identity_crm')) {
			this.cargarInformacionSelect();
		}
		this.dataTable = {
			headerRow: [
				"#",
				"Documento Cliente",
				"Cliente",
				"Fecha Atención",
				"Asesor",
				"Observaciones",
				"Tipificación",
				"Acciones"
			],
			dataRows: []
		};
	}

	onSubmitBuscarReporte(page: string) {
		this.tipificacionValor = this.busquedaFiltros.tipificacion;
		//console.log(`${this.tipificacionValor}`);
		this.listarTipificacionesPorCliente(page);
	}

	async listarTipificacionesPorCliente(page: string) {
		this.loading = true;
		//console.log(`${this.tipificacionValor}, ${page}`);
		const responseClientesPorTipificacion = await this._tableroAsignacionService.listarClientesPorTipificacion(this.tipificacionValor, page);
		//console.log(responseClientesPorTipificacion);
		if (responseClientesPorTipificacion['status'] == "success" && responseClientesPorTipificacion['code'] == "200") {
			this.llenarDatosTabla(responseClientesPorTipificacion['data'], +page);
			this.loading = false;
		}
		else if (responseClientesPorTipificacion['status'] == "success" && responseClientesPorTipificacion['code'] == "300") {
			this.loading = false;
			this.dataTable.dataRows = [];
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseClientesPorTipificacion['message'] + '<b>', 2000);
		}
		else if (responseClientesPorTipificacion["status"] === "error" && responseClientesPorTipificacion["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseClientesPorTipificacion['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			swal('¡Error!', responseClientesPorTipificacion['message'], 'error');
		}
	}

	llenarDatosTabla(responseListado: any, page: number) {
		//console.log(page);
		this.reporteArray = responseListado['clientes'];
		this.dataTable.dataRows = [];
		this.total_item_count = responseListado['total_item_count'];
		this.page_actual = page;

		this.total_pages = responseListado['total_pages'];

		if (page >= 2) this.page_prev = page - 1;
		else this.page_prev = 1;

		if (page < responseListado['total_pages']) this.page_next = page + 1;
		else {
			if (responseListado['total_pages'] == 0) this.page_next = 1;
			else this.page_next = responseListado['total_pages'];
		}

		let JSONParam;

		this.reporteArray.forEach(element => {
			JSONParam = {
				id: element.id,
				tipoDocCliente: element.cliente.tipoDocumento.valor,
				documentoCliente: element.cliente.documento,
				nombresCliente: element.cliente.nombres + ' ' + element.cliente.apellidos,
				idAtencion: element.ultimaAtencion.id,
				fechaInicialAtencion: element.ultimaAtencion.fechaInicial,
				fechaFinalAtencion: element.ultimaAtencion.fechaFinal,
				asesor: element.ultimaAtencion.asesor.nombres + ' ' + element.ultimaAtencion.asesor.apellidos,
				observacion: element.ultimaAtencion.observacion,
				tipificacionAtencion: element.ultimaAtencion.tipificacion.valor,
				ingresoId: element.ultimaAtencion.ingreso.id
			};
			this.dataTable.dataRows.push(JSONParam);
		});
		//console.log(this.dataTable.dataRows);;
	}

	async cargarInformacionSelect() {
		this.tipificacionArray = [];
		const tipificacionParametros: any = await this._parametrizacionService.listarCategoriaParametrizacion('Tipificación Clientes');

		tipificacionParametros['data'].forEach((element) => {
			this.tipificacionArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
	}

	limpiarFiltros() {
		this.busquedaFiltros.tipificacion = '';
		this.tipificacionValor = '';
		(document.getElementById('tipificacion') as HTMLInputElement).value = '';

		this.listarTipificacionesPorCliente('1');
	}

	observarTexto(texto: any) {
		this.textoExtendido = texto;
		$("#modalObservacion").modal("show");
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	verFichaCliente(idIngreso: any) {
		this._router.navigate(["clientes/ficha-clientes/", idIngreso]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}