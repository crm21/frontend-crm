import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteClientesAnioComponent } from './reporte-clientes-anio.component';

describe('ReporteClientesAnioComponent', () => {
  let component: ReporteClientesAnioComponent;
  let fixture: ComponentFixture<ReporteClientesAnioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteClientesAnioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteClientesAnioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
