import { Routes } from '@angular/router';

import { TicketsListarComponent } from './tickets-listar/tickets-listar.component';
import { TicketsRevisionComponent } from './tickets-revision/tickets-revision.component';

export const TicketsRoutes: Routes = [
	{
		path: '',
		children: [
			{
				path: 'listar',
				component: TicketsListarComponent
			}, {
				path: 'revision/:idTicket',
				component: TicketsRevisionComponent
			}
		]
	}
];