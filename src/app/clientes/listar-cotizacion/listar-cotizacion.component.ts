import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from "@angular/router";
import swal from "sweetalert2";
import { ActualizarCotizacion } from '../../interfaces/interfaces';
import { ModificarCotizacion } from '../../models/cotizacion';
import { ClienteService } from '../../services/cliente/cliente.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare var $: any;

@Component({
	selector: 'app-listar-cotizacion',
	templateUrl: './listar-cotizacion.component.html',
	styleUrls: ['./listar-cotizacion.component.css']
})
export class ListarCotizacionComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	idIngreso: any = 0;
	ingresoFinalizado: any = 0;
	botonMostrar: any;
	actualizarCotizacionForm: FormGroup;
	listadoCotizacionesArray: any[] = [];
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;

	tipoVehiculoArray: any[] = [];
	metodoPagoArray: any[] = [];
	versionVehiculoArray: any[] = [];
	colorVehiculoArray: any[] = [];
	modeloArray: any[] = [];
	cuotaInicialArray: any[] = [];
	estadosNegocioArray: any[] = [];
	idCotizacion: any;
	ocultarBotonesCliente = '';

	datosCliente = {
		nombres: null,
		documento: null
	}

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _formBuilder: FormBuilder,
		private _clienteService: ClienteService,
		private _parametrizacionService: ParametrizacionService,
		private _sweetAlertService: SweetAlertService
	) {
		this.idIngreso = this._route.snapshot.paramMap.get('idIngreso');
		this.ingresoFinalizado = this._route.snapshot.paramMap.get('ingresoFinalizado');

		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			let permisoVerFicha = identityData['data'];
			this.ocultarBotonesCliente = permisoVerFicha.rol.valor;
			this.botonMostrar = this.ingresoFinalizado == 1 ? true : false;
		}
	}

	ngOnInit() {
		this.actualizarCotizacionForm = this._formBuilder.group({
			numCotizacion: [''],
			tipoVehiculo: ['', Validators.required],
			modelo: ['', Validators.required],
			cuotaInicial: ['', Validators.required],
			metodoPago: ['', Validators.required],
			versionId: ['', Validators.required],
			colorVehiculo: ['', Validators.required],
			valorVehiculo: ['', Validators.required],
			ultimoDigito: ['', [Validators.pattern("^[0-9-]{1,6}$")]],
			lugarMatricula: [''],
			estadoNegocio: [''],
		});
		if (localStorage.getItem('identity_crm')) {
			this.listarCotizaciones(1);
			this.cargarInformacionSelect();
		}
	}

	async listarCotizaciones(page: any) {
		this.loading = true;
		let responseListadoCotizacion = await this._clienteService.listarCotizacion(2, this.idIngreso, page);
		//console.log(responseListadoCotizacion);
		if (responseListadoCotizacion["status"] === "success" && responseListadoCotizacion["code"] === "200") {
			this.llenarDatosCliente(responseListadoCotizacion['data'].informacionCliente);
			this.llenarDatosCotizaciones(responseListadoCotizacion['data'].informacionCotizaciones, page);
			this.loading = false;
			$(".main-panel").scrollTop(0);
		}
		else if (responseListadoCotizacion["status"] === "success" && responseListadoCotizacion["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseListadoCotizacion["message"], 'warning')
				.then(result => {
					this._router.navigate(["clientes/buscar"]);
				}).catch(swal.noop);
		}
		else if (responseListadoCotizacion["status"] === "error" && responseListadoCotizacion["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseListadoCotizacion['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseListadoCotizacion["message"], 'error')
				.then(result => {
					this._router.navigate(["clientes/buscar"]);
				}).catch(swal.noop);
		}
	}

	llenarDatosCliente(data: any) {
		this.datosCliente.nombres = data.nombre;
		this.datosCliente.documento = data.documento;
	}

	llenarDatosCotizaciones(data: any, page: any) {
		if (data.cotizaciones) {
			//console.log(data.cotizaciones);
			this.listadoCotizacionesArray = [];
			let listadoCotizacionesParametros = data.cotizaciones;
			this.total_item_count = data.total_item_count;
			this.page_actual = page;

			this.total_pages = data.total_pages;

			if (page >= 2) this.page_prev = page - 1;
			else this.page_prev = 1;

			if (page < data.total_pages) this.page_next = page + 1;
			else {
				if (data.total_pages == 0) this.page_next = 1;
				else this.page_next = data.total_pages;
			}

			listadoCotizacionesParametros.forEach((element) => {
				this.listadoCotizacionesArray.push({
					id: element.id,
					numCotizacion: element.numCotizacion,
					fechaCotizacion: element.fechaCotizacion,
					fechaIngreso: element.ingresoConcesionario.fechaIngreso,
					fechaSalida: element.ingresoConcesionario.fechaSalida,
					metodoPago: element.metodoPago,
					metodoPagoNombre: element.metodoPago.nombre,
					cuotaInicial: element.cuotaInicial,
					vehiculo: element.vehiculo,
					vehiculoNombre: element.vehiculo.nombre,
					modelo: element.modelo,
					versionId: element.versionId,
					versionIdNombre: element.versionId.nombre,
					colorVehiculo: element.colorVehiculo,
					colorVehiculoNombre: element.colorVehiculo.nombre,
					valorVehiculo: element.valorVehiculo,
					ultimoDigito: element.ultimoDigito,
					lugarMatricula: element.lugarMatricula,
					estado: element.estado,
					colorEtiqueta: this.darColorEstadoCotizacion(element.estado.valor)
				});
			});
		} else {
			this.listadoCotizacionesArray = [];
		}

	}

	async cargarInformacionSelect() {
		this.loading = true;

		let tipoVehiculoParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo Vehículo');
		let metodoPagoParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Método de Pago');
		let versionVehiculoParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Versión Vehículo');
		let colorVehiculoParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Color Vehículo');
		let estadoCotizacionParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Estados de Negocio');

		tipoVehiculoParametros['data'].forEach((element) => {
			this.tipoVehiculoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		metodoPagoParametros['data'].forEach((element) => {
			this.metodoPagoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		versionVehiculoParametros['data'].forEach((element) => {
			this.versionVehiculoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		colorVehiculoParametros['data'].forEach((element) => {
			this.colorVehiculoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		estadoCotizacionParametros['data'].forEach((element) => {
			this.estadosNegocioArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});


		const anioActual = new Date();
		const rangoAnioActual: number = + anioActual.getFullYear();

		for (var i = rangoAnioActual + 2; i >= 1990; --i) {
			this.modeloArray.push({
				valor: '' + i,
				nombre: '' + i
			});
		}

		for (var i = 1; i < 11; ++i) {

			let valor1;
			let valo2 = (0 + i) * 10;

			if (i == 1) valor1 = 0;
			else valor1 = ((10 * i) + 1) - 10;

			this.cuotaInicialArray.push({
				valor: valor1 + '% - ' + valo2 + '%',
				nombre: 'Entre ' + valor1 + '% - ' + valo2 + '%'
			});
		}
		this.loading = false;
	}

	async onSubmitActualizarCotizacion() {
		this.loading = true;

		let cotizacion = new ModificarCotizacion(
			this.actualizarCotizacionForm.value.numCotizacion,
			this.actualizarCotizacionForm.value.tipoVehiculo,
			this.actualizarCotizacionForm.value.modelo,
			this.actualizarCotizacionForm.value.cuotaInicial,
			this.actualizarCotizacionForm.value.metodoPago,
			this.actualizarCotizacionForm.value.versionId,
			this.actualizarCotizacionForm.value.colorVehiculo,
			this.formatoValorEntero(this.actualizarCotizacionForm.value.valorVehiculo),
			'' + this.actualizarCotizacionForm.value.ultimoDigito,
			this.actualizarCotizacionForm.value.lugarMatricula,
			this.actualizarCotizacionForm.value.estadoNegocio,
			this.idIngreso
		); //console.log(cotizacion);

		const responseModificarCotizacion = await this._clienteService.actualizacionCotizacion(this.idCotizacion, cotizacion);
		//console.log(responseModificarCotizacion);
		if (responseModificarCotizacion["status"] === "success" && responseModificarCotizacion["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseModificarCotizacion["message"], 'success')
				.then(result => {
					this.actualizarCotizacionForm.reset();
					$("#modalFiltro").modal("hide");
					this.listarCotizaciones(1);
				}).catch(swal.noop);
		}
		else if (responseModificarCotizacion["status"] === "success" && responseModificarCotizacion["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseModificarCotizacion["message"], 'OK', 'warning');
		}
		else if (responseModificarCotizacion["status"] === "error" && responseModificarCotizacion["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseModificarCotizacion['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseModificarCotizacion["message"], 'OK', 'error');
		}
	}


	modificarCotizacion(cotizacion: any) {
		let rowCotizacion: ActualizarCotizacion = cotizacion;
		this.idCotizacion = cotizacion.id;

		this.actualizarCotizacionForm.setValue({
			numCotizacion: rowCotizacion.numCotizacion,
			tipoVehiculo: rowCotizacion.vehiculo.valor,
			modelo: rowCotizacion.modelo,
			cuotaInicial: rowCotizacion.cuotaInicial,
			metodoPago: rowCotizacion.metodoPago.valor,
			versionId: rowCotizacion.versionId.valor,
			colorVehiculo: rowCotizacion.colorVehiculo.valor,
			valorVehiculo: this.colocarFormatoMoneda(rowCotizacion.valorVehiculo),
			ultimoDigito: rowCotizacion.ultimoDigito,
			lugarMatricula: rowCotizacion.lugarMatricula,
			estadoNegocio: rowCotizacion.estado.valor,
		});
		$("#modalFiltro").modal("show");
	}

	crearCotizacion() {
		this._router.navigate(["clientes/solicitar-cotizacion/", this.idIngreso, this.ingresoFinalizado]);
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	formatoValorEntero(dato: any) {
		dato = dato.replace(/COP/gi, '');
		dato = dato.replace(/[-$+()\s.]/g, '');
		dato = dato.replace(',00', '');
		return dato;
	}

	colocarFormatoMoneda(numero: any) {
		if (numero) {
			return new Intl.NumberFormat("COP", { style: "currency", currency: "COP" }).format(numero);
		}
		return 0;
	}

	formatearMoneda(evento: any) {
		let numero: number = parseInt((document.getElementById('valorVehiculo') as HTMLInputElement).value);
		if (numero) {
			let formateado: any = new Intl.NumberFormat("COP", { style: "currency", currency: "COP" }).format(numero);
			(document.getElementById('valorVehiculo') as HTMLInputElement).value = formateado;
			//console.log(formateado);
		}
	}

	darColorEstadoCotizacion(estado: string): string {
		switch (estado) {
			case 'INC':
				return 'info';
				break;
			case 'EE':
				return 'warning';
				break;
			case 'EP':
				return 'success';
				break;
			case 'RC':
				return 'danger';
				break;
			case 'AB':
				return 'optimus-black';
				break;
			case 'CR':
				return 'default';
				break;
			default:
				return 'default';
				break;
		}
	}

	regresar() {
		this._router.navigate(["clientes/ficha-clientes/", this.idIngreso]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}
