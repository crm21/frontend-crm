import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { ProcesoVentasService } from '../../services/proceso-ventas/proceso-ventas.service';
import { TableroAsignacionService } from '../../services/tablero-asignacion/tablero-asignacion.service';
import { DatePipe } from "@angular/common";

declare var $: any;
declare interface TableData {
	headerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-listar-procesos-venta',
	templateUrl: './listar-procesos-venta.component.html',
	styleUrls: ['./listar-procesos-venta.component.css']
})
export class ListarProcesosVentaComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	idUsuario: any;
	dataTable: TableData;
	buscarUsuarioFormGroup: FormGroup;
	buscarFiltrosForm: FormGroup;
	reporteEstadisticaArray: any[] = [];
	todosAsesoresArray: any[] = [];
	total_item_count: number = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	consultaVaciaCargarSpinner: boolean = false;
	camposProcesoVentasChevy: boolean = false;
	ocultarCampoAsesor: boolean = true;
	procesoVentasDetalles = {
		id: null,
		cotizacionNum: null,
		idPersona: null,
		tipoDocPersona: null,
		documentoPersona: null,
		nombresPersona: null,
		apellidosPersona: null,
		asesor: null,
		fechaInicioProceso: null,
		fechaFacturacion: null,
		fechaEnvioMatricula: null,
		fechaRecepcionMatricula: null,
		fechaSoat: null,
		fechaActivacionChevy: null,
		fechaEntrega: null,
		fechaFirmaContrato: null,
		fechaConsignacion: null,
		precioFinalVenta: null,
		placa: null,
		vin: null,
		kilometraje: null,
		observacion: null,
		porcentaje: null,
		colorPorcentaje: null,
		negocioAbierto: null
	};
	identityUsuario: any;
	fechaIni: string = '';
	fechaFin: string = '';
	busquedaFiltros: any = {
		asesor: ''
	};
	documentoCliente: string = '';
	cambiarPaginador: boolean = true;

	constructor(
		private _router: Router,
		private _datepipe: DatePipe,
		private _formBuilder: FormBuilder,
		private _sweetAlertService: SweetAlertService,
		private _procesoVentasService: ProcesoVentasService,
		private _tableroAsignacionService: TableroAsignacionService,
	) {
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			let identityUsuario = identityData['data'];
			this.identityUsuario = identityData['data'];
			this.idUsuario = identityUsuario.sub;
			if (this.identityUsuario.rol.valor == 'S') {
				this.ocultarCampoAsesor = false;
			}

			this.listarProcesosDeVentas(1);
		}
		this.datosTabla();
	}

	ngOnInit() {
		this.buscarFiltrosForm = this._formBuilder.group({
			fechaInicial: ['', Validators.required],
			fechaFinal: ['', Validators.required]
		});
		this.buscarUsuarioFormGroup = this._formBuilder.group({
			filtroUsuario: ['', Validators.required]
		});
		if (localStorage.getItem('identity_crm')) {
			this.cargarDisponibilidadAsesores();
		}
	}

	datosTabla() {
		this.dataTable = {
			headerRow: [
				"#",
				"Documento Cliente",
				"Nombres Completos",
				"Fecha Inicio del Proceso",
				"Precio Final",
				"Asesor",
				"Porcentaje de Avance",
				"Mas Detalles",
				"Acciones"
			],
			dataRows: []
		};
	}

	/* Seccion onSubmit de las consultas */
	onSubmitBuscarReporte() {
		let _fechaIni: string, _fechaFin: string, asesor: string, financiera: string;
		let comprobarFecha: boolean = true;
		_fechaIni = this.buscarFiltrosForm.value.fechaInicial;
		_fechaFin = this.buscarFiltrosForm.value.fechaFinal;

		if (this.identityUsuario.rol.valor == 'S') this.busquedaFiltros.asesor = this.identityUsuario.sub;

		if (!_fechaIni || _fechaIni == '' || !_fechaFin || _fechaFin == '') {
			this.fechaIni = '';
			this.fechaFin = '';
		} else {
			this.fechaIni = this.fechasValidacion(this.buscarFiltrosForm.value.fechaInicial);
			this.fechaFin = this.fechasValidacion(this.buscarFiltrosForm.value.fechaFinal);
			if (this.validarFechasSuperior(this.fechaIni, this.fechaFin)) comprobarFecha = true;
			else comprobarFecha = false;
		}

		if (comprobarFecha) {
			//console.log(`${this.busquedaFiltros.asesor}, ${this.fechaIni}, ${this.fechaFin}`);
			this.listarProcesosDeVentas(1);
		} else {
			//console.log('La fecha inicial debe ser inferior a la fecha final.');
			this._sweetAlertService.showNotification('top', 'right', 'danger', 'warning', '<b>' + 'La fecha inicial debe ser inferior a la fecha final.' + '<b>', 2000);
		}
	}

	/* Llamado al metodo con los filtros de las consultas */
	async listarProcesosDeVentas(page: any) {
		//console.log(`${page}, ${this.busquedaFiltros.asesor}, ${this.fechaIni}, ${this.fechaFin}`);
		this.loading = true;
		const responseEstadisticas = await this._procesoVentasService.listarProcesoVentasFiltros(page, this.busquedaFiltros.asesor, this.fechaIni, this.fechaFin);
		//console.log(responseEstadisticas);
		if (responseEstadisticas["status"] === "success" && responseEstadisticas["code"] === "200") {
			this.loading = false;
			this.llenarDatosTabla(page, responseEstadisticas['data']);
		}
		else if (responseEstadisticas["status"] === "success" && responseEstadisticas["code"] === "300") {
			this.loading = false;
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseEstadisticas['message'] + '<b>', 2000);
		}
		else if (responseEstadisticas["status"] === "error" && responseEstadisticas["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseEstadisticas['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			this._sweetAlertService.showNotification('top', 'right', 'error', 'danger', '<b>' + responseEstadisticas['message'] + '<b>', 2000);
		}
	}

	llenarDatosTabla(page: any, responseEstadisticas: any) {
		//console.log(responseEstadisticas);
		this.reporteEstadisticaArray = responseEstadisticas['procesosVentas'];
		this.dataTable.dataRows = [];
		this.total_item_count = responseEstadisticas['total_item_count'];
		this.page_actual = page;

		this.total_pages = responseEstadisticas['total_pages'];

		if (page >= 2) this.page_prev = page - 1;
		else this.page_prev = 1;

		if (page < responseEstadisticas['total_pages']) this.page_next = page + 1;
		else {
			if (responseEstadisticas['total_pages'] == 0) this.page_next = 1;
			else this.page_next = responseEstadisticas['total_pages'];
		}

		let JSONParam: any;

		this.reporteEstadisticaArray.forEach(element => {
			let porcentajePV: number = element.porcentajeAvance;
			
			JSONParam = {
				id: element.proceso.id,
				cotizacionId: element.proceso.cotizacionId.id,
				cotizacionNum: element.proceso.cotizacionId.numCotizacion,
				idIngreso: element.proceso.cotizacionId.ingresoConcesionario.id,
				idPersona: element.proceso.cotizacionId.ingresoConcesionario.persona.id,
				tipoDocPersona: element.proceso.cotizacionId.ingresoConcesionario.persona.tipoDocumento.valor,
				documentoPersona: element.proceso.cotizacionId.ingresoConcesionario.persona.documento,
				nombresPersona: element.proceso.cotizacionId.ingresoConcesionario.persona.nombres,
				apellidosPersona: element.proceso.cotizacionId.ingresoConcesionario.persona.apellidos,
				negocioAbierto: element.proceso.cotizacionId.ingresoConcesionario.fechaSalida == '0000-00-00' ? true : false,
				asesor: element.proceso.cotizacionId.ingresoConcesionario.asesor.nombres + ' ' + element.proceso.cotizacionId.ingresoConcesionario.asesor.apellidos,
				fechaInicioProceso: element.proceso.fechaInicioProceso,
				fechaFacturacion: element.proceso.fechaFacturacion == null ? '- - -' : element.proceso.fechaFacturacion,
				fechaEnvioMatricula: element.proceso.fechaEnvioMatricula == null ? '- - -' : element.proceso.fechaEnvioMatricula,
				fechaRecepcionMatricula: element.proceso.fechaRecepcionMatricula == null ? '- - -' : element.proceso.fechaRecepcionMatricula,
				fechaSoat: element.proceso.fechaSoat == null ? '- - -' : element.proceso.fechaSoat,
				fechaActivacionChevy: element.proceso.fechaActivacionChevyOnStar == null ? '- - -' : element.proceso.fechaActivacionChevyOnStar,
				fechaEntrega: element.proceso.fechaEntrega == null ? '- - -' : element.proceso.fechaEntrega,
				fechaFirmaContrato: element.proceso.fechaFirmaContrato == null ? '- - -' : element.proceso.fechaFirmaContrato,
				fechaConsignacion: element.proceso.fechaConsignacion == null ? '- - -' : element.proceso.fechaConsignacion,
				precioFinalVenta: element.proceso.precioFinalVenta == null ? '- - -' : element.proceso.precioFinalVenta,
				placa: element.proceso.placa == '' ? '- - -' : element.proceso.placa,
				vin: element.proceso.vin == '' ? '- - -' : element.proceso.vin,
				kilometraje: element.proceso.kilometrajeInicial,
				observacion: element.proceso.observacion == null ? '- - -' : element.proceso.observacion,
				porcentajeAvance: porcentajePV,
				colorPorcentajeAvance: this.darColorPorcentajeAvance(porcentajePV),
			};
			this.dataTable.dataRows.push(JSONParam);
		});
		//console.log(this.usuarioArray);
		if (this.total_item_count || this.total_item_count == 0) {
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
		}
	}

	verProcesoVentas(rowProcesoVentas: any) {
		//console.log(rowProcesoVentas);
		let letraEmpresa = localStorage.getItem('business');
		switch (letraEmpresa) {
			case 'A':
				this.camposProcesoVentasChevy = true;
				break;
			case 'M':
				this.camposProcesoVentasChevy = false;
				break;
			case 'T':
				this.camposProcesoVentasChevy = false;
				break;
			default:
				this.camposProcesoVentasChevy = false;
				break;
		}

		this.procesoVentasDetalles.id = rowProcesoVentas.id;
		this.procesoVentasDetalles.cotizacionNum = rowProcesoVentas.cotizacionNum;
		this.procesoVentasDetalles.idPersona = rowProcesoVentas.idPersona;
		this.procesoVentasDetalles.tipoDocPersona = rowProcesoVentas.tipoDocPersona;
		this.procesoVentasDetalles.documentoPersona = rowProcesoVentas.documentoPersona;
		this.procesoVentasDetalles.nombresPersona = rowProcesoVentas.nombresPersona;
		this.procesoVentasDetalles.apellidosPersona = rowProcesoVentas.apellidosPersona;
		this.procesoVentasDetalles.asesor = rowProcesoVentas.asesor;
		this.procesoVentasDetalles.fechaInicioProceso = rowProcesoVentas.fechaInicioProceso;
		this.procesoVentasDetalles.fechaFacturacion = rowProcesoVentas.fechaFacturacion;
		this.procesoVentasDetalles.fechaEnvioMatricula = rowProcesoVentas.fechaEnvioMatricula;
		this.procesoVentasDetalles.fechaRecepcionMatricula = rowProcesoVentas.fechaRecepcionMatricula;
		this.procesoVentasDetalles.fechaSoat = rowProcesoVentas.fechaSoat;
		this.procesoVentasDetalles.fechaActivacionChevy = rowProcesoVentas.fechaActivacionChevy;
		this.procesoVentasDetalles.fechaEntrega = rowProcesoVentas.fechaEntrega;
		this.procesoVentasDetalles.fechaFirmaContrato = rowProcesoVentas.fechaFirmaContrato;
		this.procesoVentasDetalles.fechaConsignacion = rowProcesoVentas.fechaConsignacion;
		this.procesoVentasDetalles.precioFinalVenta = rowProcesoVentas.precioFinalVenta;
		this.procesoVentasDetalles.placa = rowProcesoVentas.placa;
		this.procesoVentasDetalles.vin = rowProcesoVentas.vin;
		this.procesoVentasDetalles.kilometraje = rowProcesoVentas.kilometraje;
		this.procesoVentasDetalles.observacion = rowProcesoVentas.observacion;
		this.procesoVentasDetalles.porcentaje = rowProcesoVentas.porcentajeAvance;
		this.procesoVentasDetalles.colorPorcentaje = rowProcesoVentas.colorPorcentajeAvance;
		this.procesoVentasDetalles.negocioAbierto = rowProcesoVentas.negocioAbierto;
		$("#modalInformacionDetallada").modal("show");
		//console.log(this.procesoVentasDetalles);
	}

	/* Filtros por documento */
	onSubmitClientePorDocumento(page: string) {
		this.documentoCliente = this.buscarUsuarioFormGroup.value.filtroUsuario;
		this.listarClientePorDocumento(page);
	}

	async listarClientePorDocumento(page: string) {
		//console.log(`${page}, ${this.documentoCliente}`);
		this.loading = true;
		const responseUsuarios = await this._procesoVentasService.listarProcesoVentasFiltrosDocumento(page, this.documentoCliente);
		//console.log(responseUsuarios);
		if (responseUsuarios["status"] === "success" && responseUsuarios["code"] === "200") {
			this.loading = false;
			this.cambiarPaginador = false;
			this.buscarUsuarioFormGroup.reset();
			this.llenarDatosTabla(page, responseUsuarios['data']);
		}
		else if (responseUsuarios["status"] === "success" && responseUsuarios["code"] === "300") {
			this.loading = false;
			this.cambiarPaginador = false;
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseUsuarios['message'] + '<b>', 2000);
		}
		else if (responseUsuarios["status"] === "error" && responseUsuarios["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseUsuarios['message']);
		}
		else {
			this.loading = false;
			this.cambiarPaginador = false;
			this._sweetAlertService.showNotification('top', 'right', 'error', 'warning', '<b>' + responseUsuarios['message'] + '<b>', 2000);
		}
	}

	/* Redireccionamiento a las diferntes pantallas */
	buscarIdCliente(rowId: any) {
		localStorage.setItem('rutaSN', 'seguimiento-del-negocio/procesos-de-venta');
		this._router.navigate(["clientes/ficha-clientes/", rowId]);
	}

	editarProcesoVenta(rowId: string, rowCotizacion: string) {
		localStorage.setItem('rutaSeguimiento', 'seguimiento-del-negocio/procesos-de-venta');
		this._router.navigate(["clientes/contestacion/", rowId, rowCotizacion, 'PV']);
	}

	/* Consuta para cargar los selects */
	async cargarDisponibilidadAsesores() {
		this.loading = true;
		const responseDisponibilidadAsesores = await this._tableroAsignacionService.disponibilidadAsesores();
		//console.log(responseDisponibilidadAsesores);
		if (responseDisponibilidadAsesores["status"] === "success" && responseDisponibilidadAsesores["code"] === "200") {
			this.loading = false;
			this.cargarInformacionSelect(responseDisponibilidadAsesores['data']);
		}
		else if (responseDisponibilidadAsesores["status"] === "success" && responseDisponibilidadAsesores["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseDisponibilidadAsesores["message"], 'OK', 'warning');
		}
		else if (responseDisponibilidadAsesores["status"] === "error" && responseDisponibilidadAsesores["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseDisponibilidadAsesores['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseDisponibilidadAsesores["message"], 'OK', 'error');
		}
	}

	async cargarInformacionSelect(asesores: any) {
		//console.log(asesores);
		let disponibilidadAsesorArray: any[] = [];
		this.todosAsesoresArray = [];

		if (asesores.asesoresDisponibles.length > 0) {
			disponibilidadAsesorArray = asesores.asesoresDisponibles;
		}
		if (asesores.asesoresOcupados.length >= 0) {
			this.todosAsesoresArray = disponibilidadAsesorArray.concat(asesores.asesoresOcupados);
		}
	}

	/* Limpiar filtros */
	limpiarFiltros() {
		this.cambiarPaginador = true;
		this.buscarFiltrosForm.reset();
		this.buscarUsuarioFormGroup.reset();
		this.buscarFiltrosForm.value.fechaInicial = null;
		this.buscarFiltrosForm.value.fechaFinal = null;
		this.fechaIni = '';
		this.fechaFin = '';
		if (this.identityUsuario.rol.valor != 'S') {
			this.busquedaFiltros.asesor = '';
			(document.getElementById('asesor') as HTMLInputElement).value = '';
		}
		this.listarProcesosDeVentas(1);
	}

	/* Validacion de fechas y selects */
	fechasValidacion(formatoFecha: any) {
		if (formatoFecha) {
			//Formatear fecha
			const fechaActualTem = this._datepipe
				.transform(formatoFecha, "yyyy/MM/dd")
				.replace(/\//g, "-");
			//console.log("fecha formato: "+fechaActualTem);
			return fechaActualTem;
		}
		return '';
	}

	validarFechasSuperior(fechaStart: any, fechaEnd: any) {
		const fechaStart1 = new Date(fechaStart);
		const fechaEnd1 = new Date(fechaEnd);

		const diaInicio = fechaStart1.getDate();
		const mesInicio = fechaStart1.getMonth() + 1;
		const anioInicio = fechaStart1.getFullYear();
		const fechaStart1Tem = diaInicio + "-" + mesInicio + "-" + anioInicio;

		const diaFin = fechaEnd1.getUTCDate();
		const mesFin = fechaEnd1.getUTCMonth() + 1;
		const anioFin = fechaEnd1.getUTCFullYear();
		const fechaEnd1Tem = diaFin + "-" + mesFin + "-" + anioFin;

		//console.log("fechaInicio: "+fechaStart1Tem);
		//console.log("fechaFinal: "+fechaEnd1Tem);

		if (!(anioFin > anioInicio)) {
			if (anioFin < anioInicio) return false;

			if (!(mesFin > mesInicio)) {
				if (mesFin < mesInicio) return false;

				if (!(diaFin > diaInicio)) return false;
				return true;
			}
			return true;
		}
		return true;
	}

	/* Colores del porcentaje */
	darColorPorcentajeAvance(estado: number): string {
		switch (true) {
			case (estado >= 0 && estado < 11):
				return 'danger';
				break;
			case (estado >= 11 && estado < 71):
				return 'warning';
				break;
			case (estado >= 71 && estado <= 100):
				return 'success';
				break;
			default:
				return 'default';
				break;
		}
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	regresar() {
		this._router.navigate(["dashboard"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}