import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresosClientesComponent } from './ingresos-clientes.component';

describe('IngresosClientesComponent', () => {
  let component: IngresosClientesComponent;
  let fixture: ComponentFixture<IngresosClientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngresosClientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngresosClientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
