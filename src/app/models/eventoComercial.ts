export class EventoComercial {
    constructor(
        public cliente: number,
        public titulo: string,
        public evento: string,
        public fechaEvento: string,
        public hora: string,
        public observacion: string,
        public confirmarEnvioCorreo: boolean,
        public correoEmpresa: string,
        public telefonoEmpresa: string
    ) { }
}