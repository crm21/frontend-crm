import { Component, OnInit, ElementRef, OnDestroy, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

declare var $: any;

@Component({
	selector: 'app-elegir-empresas',
	templateUrl: './elegir-empresas.component.html',
	styleUrls: ['./elegir-empresas.component.css']
})
export class ElegirEmpresasComponent implements OnInit, AfterViewInit {

	private toggleButton: any;
	private sidebarVisible: boolean;
	private nativeElement: Node;
	loading = false;
	test: Date = new Date();
	version = environment.version;

	constructor( private element: ElementRef, private _router: Router) {
		this.nativeElement = element.nativeElement;
		this.sidebarVisible = false;
	}

	ngOnInit() {
		var navbar: HTMLElement = this.element.nativeElement;
		this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
		const body = document.getElementsByTagName('body')[0];
		body.classList.add('login-page');
		body.classList.add('off-canvas-sidebar');
	}

	sidebarToggle() {
		var toggleButton = this.toggleButton;
		var body = document.getElementsByTagName('body')[0];
		var sidebar = document.getElementsByClassName('navbar-collapse')[0];
		if (this.sidebarVisible == false) {
			setTimeout(function () {
				toggleButton.classList.add('toggled');
			}, 500);
			body.classList.add('nav-open');
			this.sidebarVisible = true;
		} else {
			this.toggleButton.classList.remove('toggled');
			this.sidebarVisible = false;
			body.classList.remove('nav-open');
		}
	}

	ngOnDestroy() {
		const body = document.getElementsByTagName('body')[0];
		body.classList.remove('login-page');
		body.classList.remove('off-canvas-sidebar');
		localStorage.removeItem('identity_crm');
		localStorage.removeItem('token_crm');
		localStorage.removeItem('system');
	}

	ngAfterViewInit() {
		const breakCards = true;
		if (breakCards === true) {
			// We break the cards headers if there is too much stress on them :-)
			$('[data-header-animation="true"]').each(function () {
				const $fix_button = $(this);
				const $card = $(this).parent('.card');
				$card.find('.fix-broken-card').click(function () {
					const $header = $(this).parent().parent().siblings('.card-header, .card-image');
					$header.removeClass('hinge').addClass('fadeInDown');

					$card.attr('data-count', 0);

					setTimeout(function () {
						$header.removeClass('fadeInDown animate');
					}, 480);
				});
			});
		}
	}

	onClickSesionUsuario(numero: number) {
		switch (numero) {
			case 1:
				this._router.navigate(["autodenar"]);
				break;
			case 2:
				this._router.navigate(["motodenar"]);
				break;
			case 3:
				this._router.navigate(["automotora"]);
				break;
			default:
				this._router.navigate(["empresas"]);
				break;
		}
	}
}