import { Routes } from '@angular/router';
import { AsignacionAsesoresComponent } from './asignacion-asesores/asignacion-asesores.component';
import { AtencionesComponent } from './atenciones/atenciones.component';
import { ListarDisponibilidadAsesoresComponent } from './listar-disponibilidad-asesores/listar-disponibilidad-asesores.component';
import { ReasignarAsesorComponent } from './reasignar-asesor/reasignar-asesor.component';
import { AtencionesTipificacionComponent } from './atenciones-tipificacion/atenciones-tipificacion.component';

export const TableroAsignacionRoutes: Routes = [

	{
		path: '',
		children: [
			{
				path: 'listar',
				component: AtencionesComponent
			}, {
				path: 'asignar-asesor/:idIngreso/:idCliente/:opcionVentana',
				component: AsignacionAsesoresComponent
			}, {
				path: 'listar-disponibilidad',
				component: ListarDisponibilidadAsesoresComponent
			}, {
				path: 'reasignar-asesor/:idIngreso',
				component: ReasignarAsesorComponent
			}, {
				path: 'leads',
				component: AtencionesTipificacionComponent
			}
		]
	}
];