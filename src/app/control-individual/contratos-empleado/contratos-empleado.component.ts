import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Contratos } from '../../models/contratos';
import { ActualizarContratos } from '../../interfaces/interfaces';
import swal from 'sweetalert2';
import { DatePipe } from "@angular/common";
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { ControlIndividualService } from '../../services/control-individual/control-individual.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare var $: any;

@Component({
	selector: 'app-contratos-empleado',
	templateUrl: './contratos-empleado.component.html',
	styleUrls: ['./contratos-empleado.component.css']
})
export class ContratosEmpleadoComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	actualizarContratoForm: FormGroup;
	crearContratoForm: FormGroup;
	empleadoContrato: any[] = [];
	tipoDesvinculacionArray: any[] = [];
	cargoArray: any[] = [];
	tipoContratoArray: any[] = [];
	descripcionSalarioArray: string[] = ['Fijo', 'Variable'];
	filtroBusqueda: string = '';
	contratoEncontrado: boolean = false;
	empleado: any = {
		id: null,
		documento: null,
		nombres: null,
		fechaFirmaContrato: null,
		fechaIniciacionLabores: null,
		descripcionSalario: null,
		valorSalario: null,
		tipoContrato: null,
		fechaDesvinculacion: null,
		tipoDesvinculacion: null,
		observacion: null,
		cargo: null
	};
	contrato: any = {
		fechaFirmaContrato: null,
		fechaIniciacionLabores: null,
		descripcionSalario: null,
		valorSalario: null,
		tipoContrato: null,
		fechaDesvinculacion: null,
		tipoDesvinculacion: null,
		observacion: null,
		cargo: null
	};
	opcionVentana: any;
	ideEmpleado: any;
	ideContrato: any;
	mostrarFormulario: boolean = false;
	exiteContrato: boolean = true;

	constructor(
		private _router: Router,
		private _datepipe: DatePipe,
		private _formBuilder: FormBuilder,
		private _route: ActivatedRoute,
		private _parametrizacionService: ParametrizacionService,
		private _controlIndividualService: ControlIndividualService,
		private _sweetAlertService: SweetAlertService
	) {
		this.opcionVentana = this._route.snapshot.data.opc;
		this.ideEmpleado = this._route.snapshot.paramMap.get('idEmpleado');
		this.ideContrato = this._route.snapshot.paramMap.get('idContrato');
		if (localStorage.getItem('identity_crm')) {
			this.cargarInformacionSelect();

			if (this.opcionVentana == '1') {
				/* Ver contrato del empleado */
				this.exiteContrato = true;
				if (localStorage.getItem('empleadoID')) {
					this.mostrarFormulario = true;
					this.ideEmpleado = localStorage.getItem('empleadoID')
					this.listarContrato(2, this.ideContrato, 1);
				}
				else this.regresarPorDefecto();

			} else {
				/* Registrar contrato al empleado */
				this.exiteContrato = false;
			}
		}
	}

	ngOnInit() {
		this.actualizarContratoForm = this._formBuilder.group({
			fechaFirmaContrato: ['', [Validators.required]],
			fechaIniciacionLabores: ['', [Validators.required]],
			descripcionSalario: ['', [Validators.required]],
			valorSalario: ['', [Validators.required]],
			tipoContrato: ['', [Validators.required]],
			fechaDesvinculacion: [''],
			tipoDesvinculacion: [''],
			observacion: [''],
			cargo: ['', [Validators.required]]
		});
		this.crearContratoForm = this._formBuilder.group({
			fechaFirmaContrato: ['', [Validators.required]],
			fechaIniciacionLabores: ['', [Validators.required]],
			descripcionSalario: ['', [Validators.required]],
			valorSalario: ['', [Validators.required]],
			tipoContrato: ['', [Validators.required]],
			fechaDesvinculacion: [''],
			tipoDesvinculacion: [''],
			observacion: [''],
			cargo: ['', [Validators.required]]
		});
	}

	async listarContrato(opc: any, filtro: any, page: any) {
		this.loading = true;

		const responseContrato = await this._controlIndividualService.listarContratos(opc, filtro, page);
		//console.log(responseContrato);
		if (responseContrato["status"] === "success" && responseContrato["code"] === "200") {
			this.contratoEncontrado = true;
			this.llenarDatos(responseContrato['data']);
			//this._sweetAlertService.showNotification('top', 'right', 'success', 'success', '<b>' + 'Puede editar la información del contrato' + '<b>', 2000);
			this.loading = false;
		}
		else if (responseContrato["status"] === "success" && responseContrato["code"] === "300") {
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseContrato['message'] + '<b>', 2000);
			this.contratoEncontrado = false;
			this.loading = false;
		}
		else if (responseContrato["status"] === "error" && responseContrato["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseContrato['message']);
		}
		else {
			this._sweetAlertService.alertGeneral('¡Error!', responseContrato["message"], 'OK', 'error');
			this.contratoEncontrado = false;
			this.loading = false;
		}
	}

	llenarDatos(datosContrato: any) {
		this.empleado.id = datosContrato.persona.id;
		this.empleado.documento = datosContrato.persona.documento;
		this.empleado.nombres = datosContrato.persona.nombres + ' ' + datosContrato.persona.apellidos;
		this.empleado.fechaFirmaContrato = datosContrato.fechaFirmaContrato;
		this.empleado.fechaIniciacionLabores = datosContrato.fechaIniciacionLabores;
		this.empleado.descripcionSalario = datosContrato.descripcionSalario;
		this.empleado.valorSalario = datosContrato.valorSalario;
		this.empleado.tipoContrato = datosContrato.tipoContrato.nombre;
		this.empleado.fechaDesvinculacion = datosContrato.fechaDesvinculacion;
		this.empleado.tipoDesvinculacion = datosContrato.tipoDesvinculacion.nombre;
		this.empleado.observacion = datosContrato.observacion;
		this.empleado.cargo = datosContrato.cargo.nombre;

		this.contrato.fechaFirmaContrato = datosContrato.fechaFirmaContrato;
		this.contrato.fechaIniciacionLabores = datosContrato.fechaIniciacionLabores;
		this.contrato.descripcionSalario = datosContrato.descripcionSalario;
		this.contrato.valorSalario = datosContrato.valorSalario;
		this.contrato.tipoContrato = datosContrato.tipoContrato;
		this.contrato.fechaDesvinculacion = datosContrato.fechaDesvinculacion;
		this.contrato.tipoDesvinculacion = datosContrato.tipoDesvinculacion;
		this.contrato.observacion = datosContrato.observacion;
		this.contrato.cargo = {
			valor: '' + datosContrato.cargo.id,
			nombre: datosContrato.cargo.nombre
		};
		//console.log(this.empleado);
	}

	async cargarInformacionSelect() {
		this.loading = true;
		let tipoDesvinculacion = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo Desvinculación');
		let tipoContrato = await this._parametrizacionService.listarCategoriaParametrizacion('Clase de Contrato');

		tipoDesvinculacion['data'].forEach((element) => {
			this.tipoDesvinculacionArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		tipoContrato['data'].forEach((element) => {
			this.tipoContratoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		this.listarCargosContrato();
	}

	async listarCargosContrato() {
		const responseNombreCargo = await this._controlIndividualService.listarNombreCargo(4, '');
		//console.log(responseNombreCargo);
		if (responseNombreCargo["status"] === "success" && responseNombreCargo["code"] === "200") {
			let cargosLista = responseNombreCargo["data"];
			cargosLista.forEach((element) => {
				this.cargoArray.push({
					valor: '' + element.id,
					nombre: element.nombre
				});
			});
			this.loading = false;
		}
		else if (responseNombreCargo["status"] === "success" && responseNombreCargo["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseNombreCargo['message'] + '<b>', 2000);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseNombreCargo["message"], 'OK', 'error');
		}
	}

	modalActualizarContrato() {
		let contratoActualizar: ActualizarContratos = this.contrato;
		this.actualizarContratoForm.setValue({
			fechaFirmaContrato: contratoActualizar.fechaFirmaContrato,
			fechaIniciacionLabores: contratoActualizar.fechaIniciacionLabores,
			descripcionSalario: contratoActualizar.descripcionSalario,
			valorSalario: contratoActualizar.valorSalario,
			tipoContrato: contratoActualizar.tipoContrato.valor,
			fechaDesvinculacion: contratoActualizar.fechaDesvinculacion,
			tipoDesvinculacion: contratoActualizar.tipoDesvinculacion.valor,
			observacion: contratoActualizar.observacion,
			cargo: contratoActualizar.cargo.valor
		});
		$("#modalContrato").modal("show");
	}

	async onSubmitActualizarContrato() {
		this.loading = true;
		let contratoActualizar = new Contratos(
			this.fechasValidacion(this.actualizarContratoForm.value.fechaFirmaContrato),
			this.fechasValidacion(this.actualizarContratoForm.value.fechaDesvinculacion),
			this.actualizarContratoForm.value.descripcionSalario,
			this.actualizarContratoForm.value.valorSalario,
			this.fechasValidacion(this.actualizarContratoForm.value.fechaIniciacionLabores),
			this.actualizarContratoForm.value.observacion,
			this.actualizarContratoForm.value.tipoContrato,
			this.actualizarContratoForm.value.tipoDesvinculacion,
			this.actualizarContratoForm.value.cargo,
			this.ideEmpleado
		); //console.log(contratoActualizar);
		const responseActualizarContrato = await this._controlIndividualService.modificarContrato(this.ideContrato, contratoActualizar);
		//console.log(responseActualizarContrato);
		if (responseActualizarContrato["status"] === "success" && responseActualizarContrato["code"] === "200") {
			this.listarContrato(2, this.ideContrato, 1);
			this._sweetAlertService.showNotification('top', 'right', 'success', 'warning', '<b>' + responseActualizarContrato['message'] + '<b>', 2000);
			$("#modalContrato").modal("hide");
			this.loading = false;
		}
		else if (responseActualizarContrato["status"] === "success" && responseActualizarContrato["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseActualizarContrato['message'] + '<b>', 2000);
		}
		else if (responseActualizarContrato["status"] === "error" && responseActualizarContrato["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseActualizarContrato['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseActualizarContrato["message"], 'OK', 'error');
		}
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	fechasValidacion(formatoFecha) {
		if (formatoFecha) {
			//Formatear fecha
			const fechaActualTem = this._datepipe
				.transform(formatoFecha, "yyyy/MM/dd")
				.replace(/\//g, "-");
			//console.log("fecha formato: "+fechaActualTem);
			return fechaActualTem;
		}
		return '';
	}

	registrarAfiliacion() {
		this._router.navigate(["control-individual/listar-afiliacion", this.ideContrato]);
	}

	regresar() {
		localStorage.removeItem('empleadoID');
		this._router.navigate(["empleados/ficha-empleados/", this.ideEmpleado]);
	}

	regresarPorDefecto() {
		localStorage.removeItem('empleadoID');
		this._router.navigate(["empleados/listar"]);
	}
	/* Fin Seccion para mostrar contrato de empleado */

	/* Inicio Seccion para registrar contrato a empleado */
	async onSubmitCrearContrato() {
		let nuevoContrato = new Contratos(
			this.fechasValidacion(this.crearContratoForm.value.fechaFirmaContrato),
			this.fechasValidacion(this.crearContratoForm.value.fechaDesvinculacion),
			this.crearContratoForm.value.descripcionSalario,
			this.crearContratoForm.value.valorSalario,
			this.fechasValidacion(this.crearContratoForm.value.fechaIniciacionLabores),
			this.crearContratoForm.value.observacion,
			this.crearContratoForm.value.tipoContrato,
			this.crearContratoForm.value.tipoDesvinculacion,
			this.crearContratoForm.value.cargo,
			this.ideEmpleado,
		); console.log(nuevoContrato);

		const responseCrearContrato = await this._controlIndividualService.registrarContrato(nuevoContrato);
		//console.log(responseCrearContrato);
		if (responseCrearContrato["status"] === "success" && responseCrearContrato["code"] === "200") {
			let idContrato = responseCrearContrato["data"].id;
			$('.main-panel').scrollTop(0);
			this.crearContratoForm.reset();
			localStorage.setItem('empleadoID', this.ideEmpleado);
			this.loading = false;
			swal('¡Bien!', responseCrearContrato["message"], 'success')
				.then(result => {
					this._router.navigate(["control-individual/ver-contrato-empleado/", idContrato]);
				}).catch(swal.noop);
		}
		else if (responseCrearContrato["status"] === "success" && responseCrearContrato["code"] === "300") {
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseCrearContrato['message'] + '<b>', 2000);
			this.loading = false;
		}
		else if (responseCrearContrato["status"] === "error" && responseCrearContrato["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseCrearContrato['message']);
		}
		else {
			this._sweetAlertService.alertGeneral('¡Error!', responseCrearContrato["message"], 'OK', 'error');
			this.loading = false;
		}

	}
	/* Fin Seccion para registrar contrato a empleado */

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}