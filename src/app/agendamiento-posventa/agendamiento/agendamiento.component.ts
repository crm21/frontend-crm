import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { AgendaService } from '../../services/agenda/agenda.service';
import { DatePipe } from '@angular/common';

declare var $: any;
declare interface DataTable {
    headerRow: string[];
    dataRows: string[][];
}

@Component({
    selector: 'app-agendamiento',
    templateUrl: './agendamiento.component.html',
    styleUrls: ['./agendamiento.component.css']
})
export class AgendamientoComponent implements OnInit {
    loading: boolean = false;
    validarToken: boolean = true;
    consultaVaciaCargarSpinner: boolean = false;
    dataTable: DataTable;
    registrosArray: any[] = [];
    total_item_count = 0;
    page_prev: any;
    page_next: any;
    page_actual: any;
    total_pages: any;
    idCliente: string = '';
    agendamientoDetallado = {
        id: null,
        clienteId: null,
        clienteDocumento: null,
        clienteNombres: null,
        fechaRegistro: null,
        fechaAgendamiento: null,
        fechaConfirmacion: null,
        hora: null,
        tipoAgendamiento: null,
        placa: null,
        modelo: null,
        agendadaPor: null,
        agendadaA: null,
        estado: null,
        colorEstado: null,
        observacion: null
    };

    constructor(
        private _router: Router,
        private _sweetAlertService: SweetAlertService,
        private _agendaService: AgendaService
    ) {
        if (localStorage.getItem('identity_crm')) {
            let base_64_data = atob(localStorage.getItem('identity_crm'));
            let identityData = JSON.parse(base_64_data);
            let permisoVerFicha = identityData['data'];
            this.idCliente = permisoVerFicha.sub;
            this.listarAgendamientos('1');
        }
    }

    ngOnInit() {
        if (localStorage.getItem('identity_crm')) {
            this.dataTable = {
                headerRow: [
                    '#',
                    'Cliente',
                    'Fecha de Agendamiento',
                    'Hora',
                    'Vehículo',
                    'Estado',
                    'Acciones'
                ],
                dataRows: []
            };
        }
    }

    async listarAgendamientos(page: any) {
        this.loading = true;
        const responseListaAgendamiento = await this._agendaService.listarAgendaUsuario(page);
        //console.log(responseListaAgendamiento);
        if (responseListaAgendamiento["status"] === "success" && responseListaAgendamiento["code"] === "200") {
            this.llenarTabla(responseListaAgendamiento["data"], page);
            this.loading = false;
        }
        else if (responseListaAgendamiento["status"] === "success" && responseListaAgendamiento["code"] === "300") {
            this.loading = false;
            this.dataTable.dataRows = [];
            this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
            this._sweetAlertService.showNotification('top', 'right', 'success', 'warning', '<b>' + responseListaAgendamiento['message'] + '<b>', 2000);
        }
        else if (responseListaAgendamiento["status"] === "error" && responseListaAgendamiento["code"] === "100") {
            this.loading = false;
            if (this.validarToken) this.expiracionToken(responseListaAgendamiento['message']);
        }
        else {
            this.loading = false;
            this.dataTable.dataRows = [];
            this._sweetAlertService.showNotification('top', 'right', 'error', 'warning', '<b>' + responseListaAgendamiento['message'] + '<b>', 2000);
        }
    }

    llenarTabla(agendamientos: any, page: any) {
        //console.log(agendamientos);
        this.registrosArray = agendamientos.agendamientos;
        this.dataTable.dataRows = [];
        this.total_item_count = agendamientos['total_item_count'];
        this.page_actual = page;
        this.total_pages = agendamientos['total_pages'];

        if (page >= 2) this.page_prev = page - 1;
        else this.page_prev = 1;

        if (page < agendamientos['total_pages']) this.page_next = page + 1;
        else {
            if (agendamientos['total_pages'] == 0) this.page_next = 1;
            else this.page_next = agendamientos['total_pages'];
        }

        let jsonRegistro: any;

        this.registrosArray.forEach(element => {
            jsonRegistro = {
                id: element.id,
                clienteId: element.cliente.id,
                clienteDocumento: element.cliente.tipoDocumento.valor + ': ' + element.cliente.documento,
                clienteNombres: element.cliente.nombres + ' ' + element.cliente.apellidos,
                fechaRegistro: element.fechaRegistro,
                fechaAgendamiento: element.fechaAgendamiento,
                fechaConfirmacion: element.fechaConfirmacion,
                hora: element.hora.hora,
                tipoAgendamiento: element.tipoAgendamiento.nombre,
                placa: element.vehiculo.placa,
                modelo: element.vehiculo.modelo,
                agendadaPor: element.agendadaPor.nombres + ' ' + element.agendadaPor.apellidos,
                agendadaA: element.agendadaA.nombres + ' ' + element.agendadaA.apellidos,
                estado: element.estado.nombre,
                colorEstado: this.darColorEstadoEvento(element.estado.valor),
                observacion: element.observacion
            };
            this.dataTable.dataRows.push(jsonRegistro);
        });

        if (this.total_item_count || this.total_item_count == 0) {
            this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
        }
    }

    visualizarRowAgenda(rowAgenda: any) {
        this.agendamientoDetallado.id = rowAgenda.id;
        this.agendamientoDetallado.clienteId = rowAgenda.clienteId;
        this.agendamientoDetallado.clienteDocumento = rowAgenda.clienteDocumento;
        this.agendamientoDetallado.clienteNombres = rowAgenda.clienteNombres;
        this.agendamientoDetallado.fechaRegistro = rowAgenda.fechaRegistro;
        this.agendamientoDetallado.fechaAgendamiento = rowAgenda.fechaAgendamiento;
        this.agendamientoDetallado.fechaConfirmacion = rowAgenda.fechaConfirmacion;
        this.agendamientoDetallado.hora = rowAgenda.hora;
        this.agendamientoDetallado.tipoAgendamiento = rowAgenda.tipoAgendamiento;
        this.agendamientoDetallado.placa = rowAgenda.placa;
        this.agendamientoDetallado.modelo = rowAgenda.modelo;
        this.agendamientoDetallado.agendadaPor = rowAgenda.agendadaPor;
        this.agendamientoDetallado.agendadaA = rowAgenda.agendadaA;
        this.agendamientoDetallado.estado = rowAgenda.estado;
        this.agendamientoDetallado.colorEstado = rowAgenda.colorEstado;
        this.agendamientoDetallado.observacion = rowAgenda.observacion;
        //console.log(this.agendamientoDetallado);
        $("#modalInformacionDetallada").modal("show");
    }

    darColorEstadoEvento(estado: string): string {
        switch (estado) {
            case 'cancelada':
                return 'danger';
                break;
            case 'confirmada':
                return 'success';
                break;
            case 'reprogramada':
                return 'info';
                break;
            case 'no confirmada':
                return 'default';
                break;
            default:
                return 'default';
                break;
        }
    }

    expiracionToken(mensaje: any) {
        this.validarToken = false;
        this._sweetAlertService.cerrarSesionVenceToken(mensaje);
    }
}
