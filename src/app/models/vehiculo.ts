export class Vehiculo {
    constructor(
        public placa: string,
        public modelo: string,
        public version: string,
        public cliente: string,
        public vin: string,
        public tipoAceite: string
    ) { }
}