import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearEstudioCreditoComponent } from './crear-estudio-credito.component';

describe('CrearEstudioCreditoComponent', () => {
  let component: CrearEstudioCreditoComponent;
  let fixture: ComponentFixture<CrearEstudioCreditoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearEstudioCreditoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearEstudioCreditoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
