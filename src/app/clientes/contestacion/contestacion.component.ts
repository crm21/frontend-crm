import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from "@angular/router";
import swal from "sweetalert2";
import { modificarEstudioCredito, ActualizarProcesoVentas, ActualizarProcesoVentasContestacion, ActualizarReconsideradoContestacion } from '../../interfaces/interfaces';
import { ReconsideradoService } from '../../services/reconsiderado/reconsiderado.service';
import { ActualizarEstudioCredito } from '../../models/estudioCredito';
import { DatePipe } from "@angular/common";
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { EstudioCreditoService } from '../../services/estudio-credito/estudio-credito.service';
import { ProcesoVentasService } from '../../services/proceso-ventas/proceso-ventas.service';
import { ProcesoVentas } from '../../models/procesoVentas';
import { ActualizarReconsiderado } from '../../models/actualizarReconsiderado';

declare var $: any;

@Component({
	selector: 'app-contestacion',
	templateUrl: './contestacion.component.html',
	styleUrls: ['./contestacion.component.css']
})
export class ContestacionComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	idCotizacion: string = '';
	idContestacion: string = '';
	idEstudioCreditos: string = '';
	idReconsiderado: string = '';
	tipoSeguimiento: string = '';
	idProcesoVentas: string = '';
	campoDesembolso: boolean = false;
	campoContestacion: boolean = false;

	actualizarEstudioCreditosForm: FormGroup;
	procesoVentasForm: FormGroup;
	datosReconsideradoForm: FormGroup;

	bancosArray: any[] = [];
	contestacionArray: any[] = [];
	planArray: any[] = [];
	tipoReconsideradoArray: any[] = [];

	permisosRolValor: string = '';
	permisosRolNombre: string = '';
	radicadoFecha = {
		temp: null,
		guardar: null
	};
	contestacionFecha = {
		temp: null,
		guardar: null
	};
	desembolsoFecha = {
		temp: null,
		guardar: null
	};
	datosCliente = {
		nombres: null,
		documento: null
	};
	observacionEstudioCredito: string = '';
	observacionProcesoVentas: string = '';
	observacionReconsiderado: string = '';

	camposProcesoVentasChevy: boolean = true;
	iconoDataPicker: boolean = false;
	disabledIconoEC: boolean[] = [false, false, false];
	disabledIconoPV: boolean[] = [false, false, false, false, false, false, false, false];

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _formBuilder: FormBuilder,
		private _estudioCreditoService: EstudioCreditoService,
		private _procesoVentaService: ProcesoVentasService,
		private _reconsideradoService: ReconsideradoService,
		private _parametrizacionService: ParametrizacionService,
		private _sweetAlertService: SweetAlertService,
		private datepipe: DatePipe
	) {
		$("#confirmarDesembolso").attr('checked', false);
		this.idContestacion = this._route.snapshot.paramMap.get('idContestacion');
		this.idCotizacion = this._route.snapshot.paramMap.get('idCotizacion');
		this.tipoSeguimiento = this._route.snapshot.paramMap.get('tipoSeguimiento');

		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			let permisoVerFicha = identityData['data'];;
			this.permisosRolValor = permisoVerFicha.rol.valor;
			this.permisosRolNombre = permisoVerFicha.rol.nombre;
		}
	}

	ngOnInit() {
		this.actualizarEstudioCreditosForm = this._formBuilder.group({
			fechaRadicado: [''],
			fechaContestacion: [''],
			contestacion: [''],
			banco: [''],
			plan: [''],
			observacion: [''],
			fechaDesembolso: [''],
			checkDesembolso: ['']
		});
		this.procesoVentasForm = this._formBuilder.group({
			fechaFacturacion: [''],
			fechaEnvioMatricula: [''],
			fechaRecepcionMatricula: [''],
			fechaSoat: [''],
			fechaActivacionChevy: [''],
			fechaEntrega: [''],
			precioFinalVenta: ['', [Validators.pattern("[0-9]{4,25}$")]],
			observacion: [''],
			fechaFirmaContrato: [''],
			fechaConsignacion: [''],
			placa: ['', [Validators.pattern("^[a-zA-Z]{3}[a-zA-Z0-9]{3}$")]],
			vin: [''],
			kilometraje: ['', [Validators.pattern("[0-9]{1,15}$")]]
		});
		this.datosReconsideradoForm = this._formBuilder.group({
			reconsiderado: ['', Validators.required],
			observacion: ['']
		});
		if (localStorage.getItem('identity_crm')) {
			switch (this.tipoSeguimiento) {
				case 'EC':
					this.listarEstudioCreditos();
					this.cargarInformacionSelectEstudiosCredito();
					break;
				case 'PV':
					this.listarProcesoVentasDatos();
					break;
				case 'R':
					this.listarReconsideradosDatos();
					this.cargarInformacionSelectReconsiderado();
					break;
				default:
					this._router.navigate(["dashboard"]);
					break;
			}
		}
	}

	/**
	 * 
	 * Inicia Seccion de Estudios de Credito
	 */
	async cargarInformacionSelectEstudiosCredito() {
		this.loading = true;
		this.bancosArray = [];
		this.contestacionArray = [];
		this.planArray = [];
		let bancosParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Bancos');
		let contestacionParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Contestación');
		let planParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Plan de crédito');

		bancosParametros['data'].forEach((element) => {
			this.bancosArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		contestacionParametros['data'].forEach((element) => {
			this.contestacionArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		planParametros['data'].forEach((element) => {
			this.planArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
		this.loading = false;
	}

	async listarEstudioCreditos() {
		this.loading = true;
		let responseEstudioCredito = await this._estudioCreditoService.listarEstudioCreditoId(this.idContestacion);
		//console.log(responseEstudioCredito);
		if (responseEstudioCredito["status"] === "success" && responseEstudioCredito["code"] === "200") {
			this.llenarDatosCliente(responseEstudioCredito['data'].cotizacion);
			this.modificarEstudioCredito(responseEstudioCredito['data']);
			this.loading = false;
		}
		else if (responseEstudioCredito["status"] === "success" && responseEstudioCredito["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseEstudioCredito["message"], 'warning')
				.then(result => {
					this.regresar();
				}).catch(swal.noop);
		}
		else if (responseEstudioCredito["status"] === "error" && responseEstudioCredito["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseEstudioCredito['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseEstudioCredito["message"], 'error')
				.then(result => {
					this.regresar();
				}).catch(swal.noop);
		}
	}

	llenarDatosCliente(data: any) {
		if (data != 0) {
			let nombre = data.ingresoConcesionario.persona.nombres;
			let apellido = data.ingresoConcesionario.persona.apellidos;
			this.datosCliente.nombres = nombre + ' ' + apellido;
			this.datosCliente.documento = data.ingresoConcesionario.persona.documento;
		} else {
			this.datosCliente.nombres = 'Datos no registrados';
			this.datosCliente.documento = 'Datos no registrados';
		}
	}

	modificarEstudioCredito(estudioCredito: any) {
		//console.log(estudioCredito);
		this.loading = true;
		let rowEstudioCredito: modificarEstudioCredito = estudioCredito;
		this.idEstudioCreditos = estudioCredito.id;

		if (rowEstudioCredito.contestacion.valor == 'aprobado' || rowEstudioCredito.contestacion.valor == 'desembolsado') {
			this.campoContestacion = true;
		}

		this.radicadoFecha.temp = rowEstudioCredito.fechaRadicado;
		this.radicadoFecha.guardar = this.fechasValidacionEstudioCreditos(1, rowEstudioCredito.fechaRadicado);

		this.contestacionFecha.temp = rowEstudioCredito.fechaContestacion;
		this.contestacionFecha.guardar = this.fechasValidacionEstudioCreditos(1, rowEstudioCredito.fechaContestacion);

		this.desembolsoFecha.temp = rowEstudioCredito.fechaDesembolso;
		this.desembolsoFecha.guardar = this.fechasValidacionEstudioCreditos(1, rowEstudioCredito.fechaDesembolso);

		this.observacionEstudioCredito = rowEstudioCredito.observacion;

		this.actualizarEstudioCreditosForm.setValue({
			fechaRadicado: this.radicadoFecha.guardar,
			fechaContestacion: this.contestacionFecha.guardar,
			contestacion: rowEstudioCredito.contestacion.valor,
			banco: rowEstudioCredito.banco.valor,
			plan: rowEstudioCredito.plan.valor,
			observacion: '',
			fechaDesembolso: this.desembolsoFecha.guardar,
			checkDesembolso: '',
		});

		//let permisosEditarFechas: string[] = ['A'];
		let permisosEditarFechas: string[] = ['A', 'DN'];
		if (!permisosEditarFechas.includes(this.permisosRolValor)) {
			this.validarCamposEstudiosCredito();
		}
	}

	async onSubmitActualizarEstudioCreditos() {
		this.loading = true;
		let datosEstudioCredito: string[] = this.validarCamposGuardarEstudioCredito();
		
		let estudioCreditos = new ActualizarEstudioCredito(
			datosEstudioCredito[0],
			datosEstudioCredito[1],
			this.idCotizacion,
			datosEstudioCredito[2],
			this.actualizarEstudioCreditosForm.value.banco,
			this.actualizarEstudioCreditosForm.value.plan,
			datosEstudioCredito[3],
			datosEstudioCredito[4]
		); //console.log(estudioCreditos);

		const responseModificarEstudioCreditos = await this._estudioCreditoService.modificarEstudioCredito(this.idEstudioCreditos, estudioCreditos);
		//console.log(responseModificarEstudioCreditos);
		if (responseModificarEstudioCreditos["status"] === "success" && responseModificarEstudioCreditos["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseModificarEstudioCreditos["message"], 'success')
				.then(result => {
					this.actualizarEstudioCreditosForm.reset();
					this.regresar();
				}).catch(swal.noop);
		}
		else if (responseModificarEstudioCreditos["status"] === "success" && responseModificarEstudioCreditos["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseModificarEstudioCreditos["message"], 'OK', 'warning');
		}
		else if (responseModificarEstudioCreditos["status"] === "error" && responseModificarEstudioCreditos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseModificarEstudioCreditos['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseModificarEstudioCreditos["message"], 'OK', 'error');
		}
	}

	validarCamposGuardarEstudioCredito(): string[] {
		let datosArray: string[] =[];
		let fechaRadicadoGuardar = this.fechasValidacionEstudioCreditos(2, this.actualizarEstudioCreditosForm.value.fechaRadicado, this.radicadoFecha);
		let fechaContestacionGuardar = this.fechasValidacionEstudioCreditos(2, this.actualizarEstudioCreditosForm.value.fechaContestacion, this.contestacionFecha);
		let fechaDesembolsoGuardarTemp = this.fechasValidacionEstudioCreditos(2, this.actualizarEstudioCreditosForm.value.fechaDesembolso, this.desembolsoFecha);
		let fechaDesembolsoGuardar = '';
		let contestacionEstudioCredito: string = this.actualizarEstudioCreditosForm.value.contestacion;
		let observacionConcatenar: string = '';

		if (this.actualizarEstudioCreditosForm.value.observacion == '') {
			observacionConcatenar = this.observacionEstudioCredito;
		} else {
			observacionConcatenar = `${this.observacionEstudioCredito} <br><strong>Observación ${this.permisosRolNombre}: </strong> ${this.actualizarEstudioCreditosForm.value.observacion}`;
		}

		if (this.campoDesembolso) {
			contestacionEstudioCredito = 'desembolsado';

			if (fechaDesembolsoGuardarTemp == '0000-00-00') {
				fechaDesembolsoGuardar = this.fechasValidacionEstudioCreditos(3, fechaDesembolsoGuardarTemp);
				fechaDesembolsoGuardarTemp = fechaDesembolsoGuardar;
			}
		}
		datosArray.push(fechaRadicadoGuardar);
		datosArray.push(fechaContestacionGuardar);
		datosArray.push(contestacionEstudioCredito);
		datosArray.push(observacionConcatenar);
		datosArray.push(fechaDesembolsoGuardarTemp);

		return datosArray;
	}

	cambiarADesembolso(event: any) {
		let checkCambiarEstado = event.currentTarget.checked;
		this.campoDesembolso = checkCambiarEstado;
	}
	/**
	 * 
	 * Finaliza Seccion de Estudios de Credito
	 */

	/**
	 * 
	 * Inicia Seccion de Procesos de Venta
	 */
	async listarProcesoVentasDatos() {
		this.loading = true;
		let responseProcesoVentas = await this._procesoVentaService.listarProcesoVentasPorId(this.idContestacion);
		//console.log(responseProcesoVentas);
		if (responseProcesoVentas["status"] === "success" && responseProcesoVentas["code"] === "200") {
			this.llenarDatosCliente(responseProcesoVentas["data"].cotizacionId);
			this.modificarProcesoVenta(responseProcesoVentas["data"]);
			this.loading = false;
			$(".main-panel").scrollTop(0);
		}
		else if (responseProcesoVentas["status"] === "success" && responseProcesoVentas["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseProcesoVentas['message'] + '<b>', 2000);
			this.datosCliente.nombres = 'Datos no registrados';
			this.datosCliente.documento = 'Datos no registrados';
		}
		else if (responseProcesoVentas["status"] === "error" && responseProcesoVentas["code"] === "100") {
			this.expiracionToken(responseProcesoVentas['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseProcesoVentas["message"], 'error')
				.then(result => {
					this._router.navigate(["clientes/buscar"]);
				}).catch(swal.noop);
		}
	}

	modificarProcesoVenta(procesoVentas: any) {
		let rowProcesoVentas: ActualizarProcesoVentasContestacion = procesoVentas;
		this.idProcesoVentas = procesoVentas.id;
		this.observacionProcesoVentas = rowProcesoVentas.observacion == null ? '' : rowProcesoVentas.observacion;

		this.comprobarPermisosCampoEmpresa();

		this.procesoVentasForm.setValue({
			fechaFacturacion: rowProcesoVentas.fechaFacturacion == null ? '' : rowProcesoVentas.fechaFacturacion,
			fechaEnvioMatricula: rowProcesoVentas.fechaEnvioMatricula == null ? '' : rowProcesoVentas.fechaEnvioMatricula,
			fechaRecepcionMatricula: rowProcesoVentas.fechaRecepcionMatricula == null ? '' : rowProcesoVentas.fechaRecepcionMatricula,
			fechaSoat: rowProcesoVentas.fechaSoat == null ? '' : rowProcesoVentas.fechaSoat,
			fechaActivacionChevy: rowProcesoVentas.fechaActivacionChevyOnStar == null ? '' : rowProcesoVentas.fechaActivacionChevyOnStar,
			fechaEntrega: rowProcesoVentas.fechaEntrega == null ? '' : rowProcesoVentas.fechaEntrega,
			fechaFirmaContrato: rowProcesoVentas.fechaFirmaContrato == null ? '' : rowProcesoVentas.fechaFirmaContrato,
			fechaConsignacion: rowProcesoVentas.fechaConsignacion == null ? '' : rowProcesoVentas.fechaConsignacion,
			precioFinalVenta: rowProcesoVentas.precioFinalVenta == null ? '' : rowProcesoVentas.precioFinalVenta,
			placa: rowProcesoVentas.placa == null ? '' : rowProcesoVentas.placa,
			vin: rowProcesoVentas.vin == null ? '' : rowProcesoVentas.vin,
			kilometraje: rowProcesoVentas.kilometrajeInicial,
			observacion: '',
		});

		let permisosEditarFechas: string[] = ['A'];
		if (!permisosEditarFechas.includes(this.permisosRolValor)) {
			this.validarCamposProcesosVenta();
		}
	}

	comprobarPermisosCampoEmpresa() {
		let letraEmpresa = localStorage.getItem('business');
		switch (letraEmpresa) {
			case 'A':
				this.camposProcesoVentasChevy = true;
				break;
			case 'M':
				this.camposProcesoVentasChevy = false;
				break;
			case 'T':
				this.camposProcesoVentasChevy = false;
				break;
			default:
				this.camposProcesoVentasChevy = false;
				break;
		}
	}

	async guardarDatosProcesoVentas() {
		this.loading = true;
		let observacion: string = '';

		if (this.procesoVentasForm.value.observacion == '') {
			observacion = this.observacionProcesoVentas;
		} else {
			observacion = `${this.observacionProcesoVentas} <br><strong>Observación ${this.permisosRolNombre}: </strong> ${this.procesoVentasForm.value.observacion}`;
		}

		let procesoVenta = new ProcesoVentas(
			this.validacionFechasProcesoVentas(this.procesoVentasForm.value.fechaFacturacion),
			this.validacionFechasProcesoVentas(this.procesoVentasForm.value.fechaEnvioMatricula),
			this.validacionFechasProcesoVentas(this.procesoVentasForm.value.fechaRecepcionMatricula),
			this.validacionFechasProcesoVentas(this.procesoVentasForm.value.fechaSoat),
			this.validacionFechasProcesoVentas(this.procesoVentasForm.value.fechaActivacionChevy),
			this.validacionFechasProcesoVentas(this.procesoVentasForm.value.fechaEntrega),
			this.procesoVentasForm.value.precioFinalVenta,
			observacion,
			this.validacionFechasProcesoVentas(this.procesoVentasForm.value.fechaFirmaContrato),
			this.validacionFechasProcesoVentas(this.procesoVentasForm.value.fechaConsignacion),
			this.procesoVentasForm.value.placa == '' ? '' : this.procesoVentasForm.value.placa,
			this.procesoVentasForm.value.vin == '' ? '' : this.procesoVentasForm.value.vin,
			this.procesoVentasForm.value.kilometraje
		); //console.log(procesoVenta);

		let responseActualizacionProcesoVentas = await this._procesoVentaService.actualizarProcesoVentas(this.idProcesoVentas, procesoVenta);
		//console.log(responseActualizacionProcesoVentas);
		if (responseActualizacionProcesoVentas["status"] === "success" && responseActualizacionProcesoVentas["code"] === "200") {
			this.procesoVentasForm.reset();
			this.loading = false;
			swal('Bien!', responseActualizacionProcesoVentas["message"], 'success')
				.then(result => {
					this.regresar();
				}).catch(swal.noop);
		}
		else if (responseActualizacionProcesoVentas["status"] === "success" && responseActualizacionProcesoVentas["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseActualizacionProcesoVentas["message"], 'warning');
		}
		else if (responseActualizacionProcesoVentas["status"] === "error" && responseActualizacionProcesoVentas["code"] === "100") {
			this.expiracionToken(responseActualizacionProcesoVentas['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseActualizacionProcesoVentas["message"], 'error');
		}
	}

	mayusculas(letra: any) {
		(document.getElementById('placa') as HTMLInputElement).value = (document.getElementById('placa') as HTMLInputElement).value.toUpperCase();
	}
	/**
	* 
	* Finaliza Seccion de Procesos de Venta
	*/

	/**
	 * Inicia Reconsiderados
	 */
	async cargarInformacionSelectReconsiderado() {
		this.loading = true;
		this.tipoReconsideradoArray = [];
		let tipoReconsideradoParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Reconsiderado');;

		tipoReconsideradoParametros['data'].forEach((element) => {
			this.tipoReconsideradoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
		this.loading = false;
	}

	async listarReconsideradosDatos() {
		this.loading = true;
		let responseReconsiderado = await this._reconsideradoService.listarReconsideradosPorId(this.idContestacion);
		//console.log(responseReconsiderado);
		if (responseReconsiderado["status"] === "success" && responseReconsiderado["code"] === "200") {
			this.llenarDatosCliente(responseReconsiderado["data"].estudioCredito);
			this.modificarReconsiderado(responseReconsiderado["data"]);
			this.loading = false;
			$(".main-panel").scrollTop(0);
		}
		else if (responseReconsiderado["status"] === "success" && responseReconsiderado["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseReconsiderado['message'] + '<b>', 2000);
			this.datosCliente.nombres = 'Datos no registrados';
			this.datosCliente.documento = 'Datos no registrados';
		}
		else if (responseReconsiderado["status"] === "error" && responseReconsiderado["code"] === "100") {
			this.expiracionToken(responseReconsiderado['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseReconsiderado["message"], 'error')
				.then(result => {
					this._router.navigate(["clientes/buscar"]);
				}).catch(swal.noop);
		}
	}

	modificarReconsiderado(reconsideradoRow: any) {
		let rowReconsiderado: ActualizarReconsideradoContestacion = reconsideradoRow;
		this.idReconsiderado = reconsideradoRow.id;
		this.observacionReconsiderado = rowReconsiderado.observacion == null ? '' : rowReconsiderado.observacion;

		this.datosReconsideradoForm.setValue({
			reconsiderado: rowReconsiderado.reconsiderado,
			observacion: '',
		});
	}

	async guardarDatosReconsiderado() {
		console.log(this.datosReconsideradoForm.value);

		/* this.loading = true;
		let observacion: string = '';

		if (this.datosReconsideradoForm.value.observacion == '') {
			observacion = this.observacionReconsiderado;
		} else {
			observacion = `${this.observacionReconsiderado} <br><strong>Observación ${this.permisosRolNombre}: </strong> ${this.datosReconsideradoForm.value.observacion}`;
		}

		let reconsideradoData = new ActualizarReconsiderado(
			this.datosReconsideradoForm.value.reconsiderado,
			observacion,
		); //console.log(reconsideradoData);

		let responseActualizacionReconsiderado = await this._reconsideradoService.actualizarReconsiderado(this.idReconsiderado, reconsideradoData);
		//console.log(responseActualizacionReconsiderado);
		if (responseActualizacionReconsiderado["status"] === "success" && responseActualizacionReconsiderado["code"] === "200") {
			this.datosReconsideradoForm.reset();
			this.loading = false;
			swal('Bien!', responseActualizacionReconsiderado["message"], 'success')
				.then(result => {
					this.regresar();
				}).catch(swal.noop);
		}
		else if (responseActualizacionReconsiderado["status"] === "success" && responseActualizacionReconsiderado["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseActualizacionReconsiderado["message"], 'warning');
		}
		else if (responseActualizacionReconsiderado["status"] === "error" && responseActualizacionReconsiderado["code"] === "100") {
			this.expiracionToken(responseActualizacionReconsiderado['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseActualizacionReconsiderado["message"], 'error');
		} */
	}
	/**
	* Finaliza Reconsiderados
	*/

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	fechasValidacionEstudioCreditos(opc: any, formatoFecha: any, fechaTemp: any = null) {
		if (opc == 1) {
			if (formatoFecha != '0000-00-00') {
				//Formatear fecha
				const fechaActualTem = this.datepipe
					.transform(formatoFecha, "yyyy/MM/dd")
					.replace(/\//g, "-");
				return fechaActualTem;
			}
			return formatoFecha;
		}
		else if (opc == 2) {
			if (formatoFecha != '0000-00-00') {
				let fechaLlega = this.fechasValidacionEstudioCreditos(1, formatoFecha);
				if (fechaTemp.guardar === fechaLlega) {
					return fechaTemp.temp;
				} else {
					const fechaActual = new Date();
					const horasMinSeg = this.datepipe.transform(fechaActual, "H:mm:ss");
					let fechaFormateadaTemp = this.fechasValidacionEstudioCreditos(1, formatoFecha);
					return fechaFormateadaTemp + ' ' + horasMinSeg;
				}
			}
			return formatoFecha;
		}
		else if (opc == 3) {
			if (formatoFecha == '0000-00-00') {
				const fechaActualT = new Date();
				const fechaActualTiempo = this.datepipe.transform(fechaActualT, "yyyy-MM-dd H:mm:ss");
				return fechaActualTiempo;
			}
			return formatoFecha;
		}
		else {
			return '';
		}
	}

	validacionFechasProcesoVentas(formatoFecha: string) {
		if (formatoFecha != '') {
			//Formatear fecha
			const fechaActualTem = this.datepipe
				.transform(formatoFecha, "yyyy/MM/dd")
				.replace(/\//g, "-");
			//console.log("fecha formato: "+fechaActualTem);
			return fechaActualTem;
		}
		return '';
	}

	regresar() {
		if (localStorage.getItem('volverRuta')) {
			let rutaVolver: string = localStorage.getItem('volverRuta');
			localStorage.removeItem('volverRuta');
			let idIngreso: string = localStorage.getItem('idIngreso');
			this._router.navigate([rutaVolver, idIngreso]);
		}
		else if (localStorage.getItem('rutaSeguimiento')) {
			let rutaVolver: string = localStorage.getItem('rutaSeguimiento');
			localStorage.removeItem('rutaSeguimiento');
			this._router.navigate([rutaVolver]);
		}
		else this._router.navigate(["dashboard"]);
	}

	validarCamposEstudiosCredito() {
		let arrayFechasBloqueadas = ['fechaRadicado', 'fechaContestacion', 'fechaDesembolso'];
		let posicion: number = 0;

		arrayFechasBloqueadas.forEach((element) => {
			if ((document.getElementById(element) as HTMLInputElement)) {
				if ((document.getElementById(element) as HTMLInputElement).value) {
					(document.getElementById(element) as HTMLInputElement).disabled = true;
					this.disabledIconoEC[posicion] = true;
					posicion = posicion + 1;
				}
				else {
					(document.getElementById(element) as HTMLInputElement).disabled = false;
					this.disabledIconoEC[posicion] = false;
					posicion = posicion + 1;
				}
			}
		});
	}

	validarCamposProcesosVenta() {
		let arrayFechasBloqueadas = ['fechaFacturacion', 'fechaEnvioMatricula', 'fechaRecepcionMatricula',
			'fechaSoat', 'fechaActivacionChevy', 'fechaEntrega',
			'fechaFirmaContrato', 'fechaConsignacion', 'precioFinalVenta'];
		let posicion: number = 0;

		arrayFechasBloqueadas.forEach((element) => {
			if ((document.getElementById(element) as HTMLInputElement)) {
				if ((document.getElementById(element) as HTMLInputElement).value) {
					(document.getElementById(element) as HTMLInputElement).disabled = true;
					this.disabledIconoPV[posicion] = true;
					posicion = posicion + 1;
				}
				else {
					(document.getElementById(element) as HTMLInputElement).disabled = false;
					this.disabledIconoPV[posicion] = false;
					posicion = posicion + 1;
				}
			}
		});
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}