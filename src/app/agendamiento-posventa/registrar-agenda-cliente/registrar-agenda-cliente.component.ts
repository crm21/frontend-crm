import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import { HorasDisponibles, AgendarCita } from '../../models/agenda';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { ClienteService } from '../../services/cliente/cliente.service';
import { VehiculosService } from '../../services/vehiculos/vehiculos.service';
import { AgendaService } from '../../services/agenda/agenda.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { DatePipe } from '@angular/common';
import swal from 'sweetalert2';

declare var $: any;

@Component({
	selector: 'app-registrar-agenda-cliente',
	templateUrl: './registrar-agenda-cliente.component.html',
	styleUrls: ['./registrar-agenda-cliente.component.css']
})
export class RegistrarAgendaClienteComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	agendarCitaForm: FormGroup;
	buscarDocumentoForm: FormGroup;
	disponibilidadHoras: HorasDisponibles;
	idCliente: string;
	tecnicosArray: any[] = [];
	horasArray: any[] = [];
	vehiculosArray: any[] = [];
	mostrarHoras: boolean = false;
	mostrarFormVehiculos: boolean = false;
	mostrarMensajeVehiculos: boolean = false;
	mensajeNoDisponibilidad: boolean = false;
	borrarHoraArray: boolean = false;
	iconoDataPicker: boolean = false;
	mostrarFecha: string = '';
	mensajeCliente: string = '';
	horasDisponibles = {
		horas: null
	};
	rolUsuario: string = '';
	cliente = {
		nombres: null,
		apellidos: null
	};

	constructor(
		private _router: Router,
		private _formBuilder: FormBuilder,
		private _sweetAlertService: SweetAlertService,
		private _clienteService: ClienteService,
		private _vehiculosService: VehiculosService,
		private _agendaService: AgendaService,
		private _parametrizacionService: ParametrizacionService,
		private _datepipe: DatePipe
	) {
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);
			let permisoVerFicha = identityData['data'];
			this.rolUsuario = identityData['data'].rol.valor;
			this.cliente.nombres = permisoVerFicha.nombres;
			this.cliente.apellidos = permisoVerFicha.apellidos;
			this.idCliente = permisoVerFicha.sub;

		}
		this.disponibilidadHoras = new HorasDisponibles('', '');
	}

	ngOnInit() {
		this.agendarCitaForm = this._formBuilder.group({
			vehiculo: ['', Validators.required],
			observacion: ['']
		});
	}

	activarSelectTecnivo(event: any) {
		if (event.value) {
			this.listarTecnicosTotal();
		}
	}

	async listarTecnicosTotal() {
		this.loading = true;
		const responseListaTecnicos = await this._agendaService.listarTecnicos();
		//console.log(responseListaTecnicos);
		if (responseListaTecnicos["status"] === "success" && responseListaTecnicos["code"] === "200") {
			let tecnicosArray = responseListaTecnicos["data"];
			tecnicosArray.forEach((element) => {
				this.tecnicosArray.push({
					id: element.id,
					nombre: element.nombres + ' ' + element.apellidos
				});
			});
			this.loading = false;
		}
		else if (responseListaTecnicos["status"] === "success" && responseListaTecnicos["code"] === "300") {
			this.tecnicosArray = [];
			this.loading = false;
		}
		else if (responseListaTecnicos["status"] === "error" && responseListaTecnicos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseListaTecnicos['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseListaTecnicos["message"], 'error');
		}
	}

	onSubmitConsultarDisponibilidadTecnico() {
		let fechaConsulta: string, tecnicoConsulta: string;
		/* Se verifica que el usuario seleccione la fecha antes que el técnico */
		if (this.disponibilidadHoras.fecha) {
			fechaConsulta = this.fechasValidacion(this.disponibilidadHoras.fecha);
			this.mostrarFecha = this.fechasValidacion(this.disponibilidadHoras.fecha);

			/* Se valida que la fecha sea superior a la fecha actual */
			if (this.validarFechasSuperior(fechaConsulta)) {
				tecnicoConsulta = this.disponibilidadHoras.tecnico;
				//console.log(`${fechaConsulta}, ${tecnicoConsulta}`);
				this.consultarDisponibilidadTecnico(fechaConsulta, tecnicoConsulta);
			} else {
				swal('Advertencia!', 'La fecha ingresada debe superior a la fecha actual', 'warning');
			}
		} else {
			swal('Advertencia!', 'Debe seleccionar una fecha', 'warning');
		}
	}

	async consultarDisponibilidadTecnico(fechaConsulta: string, tecnicoConsulta: string) {
		this.loading = true;
		const responseHorasDisponibles = await this._agendaService.listarHorasDisponibles(fechaConsulta, tecnicoConsulta);
		//console.log(responseHorasDisponibles);
		if (responseHorasDisponibles["status"] === "success" && responseHorasDisponibles["code"] === "200") {
			if (responseHorasDisponibles['data'].disponibilidad) {
				/* Se verifica si el técnico tiene tiene horas disponibles */
				this.mensajeNoDisponibilidad = false;
				this.borrarHoraArray = true;
				this.horasArray = responseHorasDisponibles['data'].horas;
				this.iconoDataPicker = true;
				this._sweetAlertService.showNotification('top', 'right', 'success', 'success', '<b>' + responseHorasDisponibles['message'] + '<b>', 2000);
			} else {
				this.horasArray = [];
				this.borrarHoraArray = false;
				this.mensajeNoDisponibilidad = true;
				this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseHorasDisponibles['message'] + '<b>', 2000);
			}
			this.mostrarFormVehiculos = false;
			this.mostrarHoras = responseHorasDisponibles['data'].disponibilidad;
			this.loading = false;
		}
		else if (responseHorasDisponibles["status"] === "success" && responseHorasDisponibles["code"] === "300") {
			this.mostrarHoras = false;
			this.borrarHoraArray = false;
			this.horasArray = [];
			this.mostrarFormVehiculos = false;
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseHorasDisponibles['message'] + '<b>', 2000);
		}
		else if (responseHorasDisponibles["status"] === "error" && responseHorasDisponibles["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseHorasDisponibles['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseHorasDisponibles["message"], 'OK', 'error');
		}
	}

	/* Listar vehiculos asociados al cliente */
	async buscarVehiculosCliente() {
		this.loading = true;
		const responseVehiculosCliente = await this._vehiculosService.listarVehiculosCliente(2, this.idCliente, '1');
		//console.log(responseVehiculosCliente);
		if (responseVehiculosCliente["status"] === "success" && responseVehiculosCliente["code"] === "200") {
			this.llenarArrayVehiculos(responseVehiculosCliente["data"]);
			this.loading = false;
		}
		else if (responseVehiculosCliente["status"] === "success" && responseVehiculosCliente["code"] === "300") {
			this.vehiculosArray = [];
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseVehiculosCliente['message'] + '<b>', 2000);
			this.loading = false;
		}
		else if (responseVehiculosCliente["status"] === "error" && responseVehiculosCliente["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseVehiculosCliente['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'error', 'warning', '<b>' + responseVehiculosCliente['message'] + '<b>', 2000);
		}
	}

	llenarArrayVehiculos(dataVehiculos: any) {
		this.vehiculosArray = [];
		this.cliente.nombres = dataVehiculos.cliente.nombres;
		this.cliente.apellidos = dataVehiculos.cliente.apellidos;

		this.mensajeCliente = `Señor ${this.cliente.nombres} ${this.cliente.apellidos} seleccione el vehículo y escriba una observación para confirmar el agendamiento.`;

		if (dataVehiculos.vehiculos != 0) {
			let vehiculosArrayTemp: any[] = dataVehiculos.vehiculos;
			vehiculosArrayTemp.forEach((element) => {
				this.vehiculosArray.push({
					id: element.id,
					modelo: element.modelo,
					placa: element.placa
				});
			});
			this.mostrarFormVehiculos = true;
			this.mostrarMensajeVehiculos = false;
		} else {
			this.mostrarFormVehiculos = false;
			this.mostrarMensajeVehiculos = true;
		}
		//console.log(this.vehiculosArray);
	}

	async onSubmitAgendarCita() {
		this.loading = true;

		let agendarCitaVar = new AgendarCita(
			this.mostrarFecha,
			this.disponibilidadHoras.tecnico,
			this.horasDisponibles.horas,
			this.idCliente,
			this.agendarCitaForm.value.vehiculo,
			this.agendarCitaForm.value.observacion,
			'taller'
		); //console.log(agendarCitaVar);

		const responseAgendarCita = await this._agendaService.registrarAgendaCita(agendarCitaVar);
		//console.log(responseAgendarCita);
		if (responseAgendarCita["status"] === "success" && responseAgendarCita["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseAgendarCita["message"], 'success')
				.then(result => {
					this.limpiarCamposAgenda();
				}).catch(swal.noop);
		}
		else if (responseAgendarCita["status"] === "success" && responseAgendarCita["code"] === "300") {
			this.loading = false;
			//this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseAgendarCita['message'] + '<b>', 2000);
			swal('Advertencia!', responseAgendarCita["message"] + '. Seleccione otra fecha.', 'warning');
		}
		else if (responseAgendarCita["status"] === "error" && responseAgendarCita["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseAgendarCita['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'error', 'warning', '<b>' + responseAgendarCita['message'] + '<b>', 2000);
		}
	}

	seleccionarHora() {
		this.buscarVehiculosCliente();
	}

	limpiarCamposAgenda() {
		this.disponibilidadHoras.fecha = '';
		this.disponibilidadHoras.tecnico = '';
		this.horasDisponibles.horas = '';
		this.mostrarHoras = false;
		this.mostrarFormVehiculos = false;
		this.mostrarMensajeVehiculos = false;
		this.mensajeNoDisponibilidad = false;
		this.agendarCitaForm.reset();
		(document.getElementById('fecha') as HTMLInputElement).value = '';
		(document.getElementById('tecnico') as HTMLInputElement).value = '';
		this.iconoDataPicker = false;
		if (this.borrarHoraArray) (document.getElementById('horas') as HTMLInputElement).value = '';
		this.borrarHoraArray = false;
		this.tecnicosArray = [];
		$(".main-panel").scrollTop(0);
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	filtroDiasBloqueados = (d: Date | null): boolean => {
		const day = (d || new Date()).getDay();
		// Prevent Saturday and Sunday from being selected.
		//return day !== 0 && day !== 6;
		return day !== 0;
	}

	validarFechasSuperior(fechaCita: string) {
		const fechaActual = new Date();
		const fechaIngresada = new Date(fechaCita);

		const diaInicio = fechaActual.getDate();
		const mesInicio = fechaActual.getMonth() + 1;
		const anioInicio = fechaActual.getFullYear();
		const fechaActualTem = diaInicio + "-" + mesInicio + "-" + anioInicio;

		const diaFin = fechaIngresada.getUTCDate();
		const mesFin = fechaIngresada.getUTCMonth() + 1;
		const anioFin = fechaIngresada.getUTCFullYear();
		const fechaIngresadaTem = diaFin + "-" + mesFin + "-" + anioFin;

		/* console.log("fechaActual: "+fechaActualTem);
		console.log("fechaIngresada: "+fechaIngresadaTem); */

		if (!(anioFin > anioInicio)) {
			if (anioFin < anioInicio) return false;

			if (!(mesFin > mesInicio)) {
				if (mesFin < mesInicio) return false;

				if (!(diaFin > diaInicio)) return false;
				return true;
			}
			return true;
		}
		return true;
	}

	fechasValidacion(formatoFecha) {
		if (formatoFecha) {
			//Formatear fecha
			const fechaActualTem = this._datepipe
				.transform(formatoFecha, "yyyy/MM/dd")
				.replace(/\//g, "-");
			//console.log("fecha formato: "+fechaActualTem);
			return fechaActualTem;
		}
		return '';
	}

	regresar() {
		this._router.navigate(["agendamiento-posventa/listar"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}