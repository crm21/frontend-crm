import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import swal from "sweetalert2";
import { TableroAsignacion, TableroAsignacionEsperar } from "../../models/tableroAsignacion";
import { TableroAsignacionService } from '../../services/tablero-asignacion/tablero-asignacion.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare const $: any;

@Component({
	selector: 'app-reasignar-asesor',
	templateUrl: './reasignar-asesor.component.html',
	styleUrls: ['./reasignar-asesor.component.css']
})
export class ReasignarAsesorComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	disponibilidadAsesorArray: any[] = [];
	todosAsesoresArray: any[] = [];
	idIngreso: any;
	idCliente: any;
	opcionVentana: any;
	asesorId: any;
	asesorIdIngreso: any;
	ventanaAsesoresDisponibles: boolean = false;
	ventanaTodosAsesores: boolean = false;
	estoyDisponible: boolean = true;
	asesorIdEsperar: TableroAsignacionEsperar;
	observacionAsignacion: any;

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _tableroAsignacionService: TableroAsignacionService,
		private _sweetAlertService: SweetAlertService
	) {
		this.idIngreso = this._route.snapshot.paramMap.get('idIngreso');
		localStorage.setItem('idIngreso', this.idIngreso);
		this.asesorIdEsperar = new TableroAsignacionEsperar('');
		if (localStorage.getItem('identity_crm')) {
			//this.cargarDisponibilidadAsesores();
			this.cargarRolesAsignacion();
		}
	}

	ngOnInit() { }

	async onSubmitReasignarAsesor(asignoAsesorIngresoForm) {
		this.loading = true;
		let idAsesor: string = '' + this.asesorIdIngreso;
		//console.log(idAsesor);

		const responseReasignacionAsesor = await this._tableroAsignacionService.reasignarAsesor(1, idAsesor, '' + this.idIngreso);
		//console.log(responseReasignacionAsesor);
		if (responseReasignacionAsesor["status"] === "success" && responseReasignacionAsesor["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseReasignacionAsesor["message"], 'success')
				.then(result => {
					this.regresar();
				}).catch(swal.noop);
		}
		else if (responseReasignacionAsesor["status"] === "success" && responseReasignacionAsesor["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseReasignacionAsesor["message"], 'warning')
				.then(result => {
					this.regresar();
				}).catch(swal.noop);
		}
		else if (responseReasignacionAsesor["status"] === "error" && responseReasignacionAsesor["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseReasignacionAsesor['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseReasignacionAsesor["message"], 'error')
				.then(result => {
					this.regresar();
				}).catch(swal.noop);
		}
	}

	async cargarDisponibilidadAsesores() {
		this.loading = true;
		const responseDisponibilidadAsesores = await this._tableroAsignacionService.disponibilidadAsesores();
		console.log(responseDisponibilidadAsesores);
		if (responseDisponibilidadAsesores["status"] === "success" && responseDisponibilidadAsesores["code"] === "200") {
			this.loading = false;
			//this.cargarInformacionSelect(responseDisponibilidadAsesores['data']);
		}
		else if (responseDisponibilidadAsesores["status"] === "success" && responseDisponibilidadAsesores["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseDisponibilidadAsesores["message"], 'OK', 'warning');
		}
		else if (responseDisponibilidadAsesores["status"] === "error" && responseDisponibilidadAsesores["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseDisponibilidadAsesores['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseDisponibilidadAsesores["message"], 'OK', 'error');
		}
	}

	async cargarRolesAsignacion() {
		this.loading = true;
		const responseTotalRolesAsignar = await this._tableroAsignacionService.cargarRolesActivoInactivos();
		//console.log(responseTotalRolesAsignar);
		if (responseTotalRolesAsignar["status"] === "success" && responseTotalRolesAsignar["code"] === "200") {
			this.loading = false;
			this.cargarInformacionSelectRoles(responseTotalRolesAsignar['data']);
		}
		else if (responseTotalRolesAsignar["status"] === "success" && responseTotalRolesAsignar["code"] === "300") {
			this.loading = false;
			this.ventanaTodosAsesores = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseTotalRolesAsignar["message"], 'OK', 'warning');
		}
		else if (responseTotalRolesAsignar["status"] === "error" && responseTotalRolesAsignar["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseTotalRolesAsignar['message']);
		}
		else {
			this.loading = false;
			this.ventanaTodosAsesores = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseTotalRolesAsignar["message"], 'OK', 'error');
		}
	}

	async cargarInformacionSelect(asesores) {
		this.disponibilidadAsesorArray = [];
		this.todosAsesoresArray = [];

		if (asesores.asesoresDisponibles.length > 0) {
			this.disponibilidadAsesorArray = asesores.asesoresDisponibles;
			this.ventanaAsesoresDisponibles = true;
		}
		if (asesores.asesoresOcupados.length >= 0) {
			this.todosAsesoresArray = this.disponibilidadAsesorArray.concat(asesores.asesoresOcupados);
			this.ventanaTodosAsesores = true;
		}
	}

	async cargarInformacionSelectRoles(asesores: any) {
		this.ventanaTodosAsesores = true;
		this.todosAsesoresArray = [];
		this.todosAsesoresArray = asesores;
	}

	/* Metodos para la caja de busqueda de todos los asesores */

	selectListTodosAsesores() {
		document.getElementById("myDropdownTodos").classList.toggle("show");
	}

	filtroTodosAsesores(x: any) {
		let input, filter, ul, li, a, i;
		input = document.getElementById("asesorIdIngreso");
		filter = input.value.toUpperCase();
		let div = document.getElementById("myDropdownTodos");
		a = div.getElementsByTagName("a");

		for (i = 0; i < a.length; i++) {
			let txtValue = a[i].textContent || a[i].innerText;
			if (txtValue.toUpperCase().indexOf(filter) > -1) {
				a[i].style.display = "";
			} else {
				a[i].style.display = "none";
			}
		}
	}

	seleccionarTodosAsesor(asesor: any) {
		this.asesorIdEsperar.asesor = asesor.nombre + ' - ' + asesor.rol + ' - ' + asesor.estado;
		this.asesorIdIngreso = asesor.id;
		document.getElementById("myDropdownTodos").classList.toggle("show");
		(document.getElementById('asesorIdIngreso') as HTMLInputElement).value = asesor.nombre + ' - ' + asesor.rol + ' - ' + asesor.estado;
	}

	regresar() {
		this._router.navigate(["clientes/ficha-clientes/", this.idIngreso]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}