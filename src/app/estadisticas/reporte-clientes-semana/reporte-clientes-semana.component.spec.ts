import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteClientesSemanaComponent } from './reporte-clientes-semana.component';

describe('ReporteClientesSemanaComponent', () => {
  let component: ReporteClientesSemanaComponent;
  let fixture: ComponentFixture<ReporteClientesSemanaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteClientesSemanaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteClientesSemanaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
