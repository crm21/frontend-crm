import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UsuarioService } from '../usuario/usuario.service';
import swal from "sweetalert2";
import { SweetAlertService } from '../sweet-alert/sweet-alert.service';

@Injectable({
	providedIn: 'root'
})
export class LoginGuard implements CanActivate {

	constructor(
		private _usuarioService: UsuarioService,
		private _sweetAlertService: SweetAlertService,
		private _router: Router
	) {
		console.log("[Guard is ready]...");
	}

	async canActivate() {

		if (this._usuarioService.isToken()) {

			const tokenVExpiro: any = await this._usuarioService.expirarToken();

			if (!tokenVExpiro.token && tokenVExpiro.code != 200) {
				this._sweetAlertService.fondoNegro(1);
				console.log('Su sesión expiró');
				let opcionEmpresa = localStorage.getItem('business');
				localStorage.clear();
				localStorage.setItem('business', opcionEmpresa);
				swal(
					"Su sesión expiró",
					tokenVExpiro.message,
					"error"
				).then((result: any) => {
					this._sweetAlertService.fondoNegro(2);
					localStorage.setItem('identity_crm', null);
					if (opcionEmpresa == 'A') {
						this._router.navigate(['/autodenar']);
					}
					else if (opcionEmpresa == 'M') {
						this._router.navigate(['/motodenar']);
					}
					else if (opcionEmpresa == 'T') {
						this._router.navigate(['/automotora']);
					}
					else {
						this._router.navigate(['/empresas']);
					}
					return false;
				});
			}

			return true;
		} else {
			console.log('No estas logueado');
			this._router.navigate(['/empresas']);
			return false;
		}
	}
}