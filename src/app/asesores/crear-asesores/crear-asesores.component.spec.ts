import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearAsesoresComponent } from './crear-asesores.component';

describe('CrearAsesoresComponent', () => {
  let component: CrearAsesoresComponent;
  let fixture: ComponentFixture<CrearAsesoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearAsesoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearAsesoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
