import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstudiosCreditoComponent } from './estudios-credito.component';

describe('EstudiosCreditoComponent', () => {
  let component: EstudiosCreditoComponent;
  let fixture: ComponentFixture<EstudiosCreditoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstudiosCreditoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstudiosCreditoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
