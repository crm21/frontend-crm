import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { SiembraCosechaService } from '../../services/siembra-cosecha/siembra-cosecha.service';
import swal from 'sweetalert2';

declare var $: any;
declare interface DataTable {
	headerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-clientes-proyectados',
	templateUrl: './clientes-proyectados.component.html',
	styleUrls: ['./clientes-proyectados.component.css']
})
export class ClientesProyectadosComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	consultaVaciaCargarSpinner: boolean = false;
	dataTable: DataTable;
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	clientesArray: any[] = [];

	constructor(
		private _router: Router,
		private _sweetAlertService: SweetAlertService,
		private _siembraCosechaService: SiembraCosechaService
	) {
		/* if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);
		} */
	}

	ngOnInit(): void {
		this.dataTable = {
			headerRow: [
				"#",
				"Documento",
				"Nombres Completos",
				"Vehículo",
				"Placa",
				"Fecha Registro Proyección",
				"Kilometraje Actual",
				"Fecha Proyección",
				"Kilometraje Proyectado",
				"Fecha Llamada",
				"Fecha de Cosecha",
				"Acciones"
			],
			dataRows: []
		};

		if (localStorage.getItem('identity_crm')) {
			this.listarClientesSC('1');
		}
	}

	async listarClientesSC(page: string) {
		this.loading = true;
		const responseClientesSC = await this._siembraCosechaService.listarClientesProyectadosSC('1', page);
		//console.log(responseClientesSC);
		if (responseClientesSC["status"] === "success" && responseClientesSC["code"] === "200") {
			this.loading = false;
			this.llenarDatosTabla(+page, responseClientesSC['data']);
		}
		else if (responseClientesSC["status"] === "success" && responseClientesSC["code"] === "300") {
			this.loading = false;
			this.clientesArray = [];
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseClientesSC['message'] + '<b>', 2000);
		}
		else if (responseClientesSC["status"] === "error" && responseClientesSC["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseClientesSC['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			this._sweetAlertService.showNotification('top', 'right', 'error', 'warning', '<b>' + responseClientesSC['message'] + '<b>', 2000);
		}
	}

	llenarDatosTabla(page: number, responseClientes: any) {
		this.clientesArray = responseClientes['clientes'];
		//console.log(responseClientes['clientesIngresos']);
		this.dataTable.dataRows = [];
		this.total_item_count = responseClientes['total_item_count'];
		this.page_actual = page;
		this.total_pages = responseClientes['total_pages'];

		if (page >= 2) this.page_prev = page - 1;
		else this.page_prev = 1;

		if (page < responseClientes['total_pages']) this.page_next = page + 1;
		else {
			if (responseClientes['total_pages'] == 0) this.page_next = 1;
			else this.page_next = responseClientes['total_pages'];
		}

		let JSONParam: any;

		this.clientesArray.forEach(element => {
			JSONParam = {
				id: element.vehiculo.cliente.id,
				tipoDoc: element.vehiculo.cliente.tipoDocumento.valor,
				documento: element.vehiculo.cliente.documento,
				nombres: element.vehiculo.cliente.nombres + ' ' + element.vehiculo.cliente.apellidos,
				vehiculo: element.vehiculo.version.nombre,
				modelo: element.vehiculo.modelo,
				placa: element.vehiculo.placa,
				fechaRegistroProyeccion: element.fechaRegistroProyeccion,
				kmActual: element.kmActual,
				fechaProyeccion: element.fechaProyeccion,
				kmProyectado: element.kmProyectado,
				fechaLlamada: element.fechaLlamada,
				fechaCosecha: element.fechaCosecha
			};
			this.dataTable.dataRows.push(JSONParam);
		});

		if (this.total_item_count || this.total_item_count == 0) {
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
		}
	}

	verClienteFicha(idCliente: string) {
		localStorage.setItem('rutaSC', 'siembra-y-cosecha/clientes-proyectados');
		this._router.navigate(["siembra-y-cosecha/ficha-cliente/", idCliente]);
	}

	regresar() {
		this._router.navigate(["siembra-y-cosecha/clientes"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}