import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SesionEmpresasComponent } from './sesion-empresas.component';

describe('SesionEmpresasComponent', () => {
  let component: SesionEmpresasComponent;
  let fixture: ComponentFixture<SesionEmpresasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SesionEmpresasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SesionEmpresasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
