import { Routes } from '@angular/router';

import { CrearClientesComponent } from './crear-clientes/crear-clientes.component';
import { BuscarClientesComponent } from './buscar-clientes/buscar-clientes.component';
import { FichaClientesComponent } from './ficha-clientes/ficha-clientes.component';
import { SolicitarCotizacionComponent } from './solicitar-cotizacion/solicitar-cotizacion.component';
import { ListarCotizacionComponent } from './listar-cotizacion/listar-cotizacion.component';
import { CrearEstudioCreditoComponent } from './crear-estudio-credito/crear-estudio-credito.component';
import { ListarEstudioCreditoComponent } from './listar-estudio-credito/listar-estudio-credito.component';
import { CrearReconsideradoComponent } from './crear-reconsiderado/crear-reconsiderado.component';
import { ListarReconsideradoComponent } from './listar-reconsiderado/listar-reconsiderado.component';
import { ListarProcesoVentasComponent } from './listar-proceso-ventas/listar-proceso-ventas.component';
import { MisClientesComponent } from './mis-clientes/mis-clientes.component';
import { MiPerfilComponent } from './mi-perfil/mi-perfil.component';
import { ClientesContactCenterComponent } from './clientes-contact-center/clientes-contact-center.component';
import { IngresosClientesComponent } from './ingresos-clientes/ingresos-clientes.component';
import { ContestacionComponent } from './contestacion/contestacion.component';

export const ClientesRoutes: Routes = [
    {
        path: '',
        redirectTo: 'listar-clientes',
        pathMatch: 'full',
    },
    {
        path: '',
        children: [
            {
                path: 'registrar',
                component: CrearClientesComponent
            }, {
                path: 'buscar',
                component: BuscarClientesComponent
            }, {
                path: 'ficha-clientes/:idIngreso',
                component: FichaClientesComponent
            }, {
                path: 'solicitar-cotizacion/:idIngresoConcesionario/:ingresoFinalizado',
                component: SolicitarCotizacionComponent
            }, {
                path: 'listar-cotizaciones/:idIngreso/:ingresoFinalizado',
                component: ListarCotizacionComponent
            }, {
                path: 'registrar-estudio-credito/:idCotizacion/:ingresoFinalizado',
                component: CrearEstudioCreditoComponent
            }, {
                path: 'listar-estudio-credito/:idCotizacion/:ingresoFinalizado',
                component: ListarEstudioCreditoComponent
            }, {
                path: 'registrar-reconsiderado/:idCotizacion/:ingresoFinalizado',
                component: CrearReconsideradoComponent
            }, {
                path: 'listar-reconsiderado/:idCotizacion/:ingresoFinalizado',
                component: ListarReconsideradoComponent
            }, {
                path: 'listar-proceso-ventas/:idCotizacion/:idIngreso/:ingresoFinalizado',
                component: ListarProcesoVentasComponent
            }, {
                path: 'listar-clientes',
                component: MisClientesComponent
            }, {
                path: '',
                component: MisClientesComponent
            }, {
                path: '*',
                component: MisClientesComponent
            },{
                path: 'mi-perfil',
                component: MiPerfilComponent
            }, {
                path: 'contact-center',
                component: ClientesContactCenterComponent
            }, {
                path: 'ingresos/:idCliente',
                component: IngresosClientesComponent
            }, {
                path: 'contestacion/:idContestacion/:idCotizacion/:tipoSeguimiento',
                component: ContestacionComponent
            }, {
                path: 'contestaciones/:idContestacion/:idCotizacion/:tipoSeguimiento',
                component: ContestacionComponent
            }
        ]
    }
];
