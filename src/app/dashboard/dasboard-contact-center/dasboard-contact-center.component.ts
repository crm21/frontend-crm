import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from "@angular/router";
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { TableroAsignacionService } from '../../services/tablero-asignacion/tablero-asignacion.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { EstadisticasService } from '../../services/estadisticas/estadisticas.service';
import { UsuarioService } from '../../services/usuario/usuario.service';
import swal from 'sweetalert2';
import * as Chartist from 'chartist';
import { SiembraCosechaService } from '../../services/siembra-cosecha/siembra-cosecha.service';
import { FinalizarAtencionesAgente } from '../../models/registrarClienteSiembraCosecha';

declare var $: any;

@Component({
	selector: 'app-dasboard-contact-center',
	templateUrl: './dasboard-contact-center.component.html',
	styleUrls: ['./dasboard-contact-center.component.css']
})
export class DasboardContactCenterComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	nombreRol: string = 'Usuario';
	nombresEmpresas: string = '';
	imgNombreEmpresa: string = '';

	/* Variables Cliente Siembra y Cosecha */
	finalizarAtencionAgenteForm: FormGroup;
	ventanaAgenteDisponible: boolean = false;
	estadisticasLeads: boolean = false;
	nombreClienteSC: string = '';
	documentoSC: string = '';
	fechaInicialSC: any;
	idAtencionSC: string = '';
	idClienteSC: string = '';
	idAgente: string = '';
	tipificacionArray: any[] = [];
	
	/* Variables Cliente Siembra y Cosecha */
	finalizarAtencionForm: FormGroup;
	ventanaAsesorDisponible: boolean = false;
	mostraAtencionAsesor: boolean = false;
	nombreCliente: any;
	documento: any;
	fechaInicial: any;
	idAtencion: any;
	idIngreso: string = '';
	idUsuario: number = 1;
	objecionesArray: any[] = [];
	nombreRolValor: string = '';
	
	constructor(
		private _parametrizacionService: ParametrizacionService,
		private _siembraCosechaService: SiembraCosechaService,
		private _tableroAsignacionService: TableroAsignacionService,
		private _sweetAlertService: SweetAlertService,
		private _usuarioService: UsuarioService,
		private _formBuilder: FormBuilder,
		private _router: Router
	) {
		if (localStorage.getItem('identity_crm')) {
			this.consultarNombreEmpresa(localStorage.getItem('business'));

			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			let datosUsuario = identityData['data'];
			this.nombreRolValor = datosUsuario.rol.valor;
			this.nombreRol = datosUsuario.rol.nombre;
			this.idAgente = '' + datosUsuario.sub;

			this.mostrarEstadisticasLeadsRoles();
			this.cargarInformacionSelect();
			this.consultarDisponibilidadAgente(this.idAgente);
			
			if (this.nombreRolValor == 'DT') {
				this.consultarDisponibilidadAsesor(this.idAgente);
				this.mostraAtencionAsesor = true;
			}
		}
	}

	ngOnInit() {
		this.finalizarAtencionAgenteForm = this._formBuilder.group({
			tipificacion: ['Ninguno'],
			observacion: [''],
		});
		this.finalizarAtencionForm = this._formBuilder.group({
			objecion: ['NN', Validators.required],
			tipificacion: ['Ninguno'],
			observacion: ['']
		});
	}

	ngOnDestroy() {
		localStorage.removeItem('ruta');
	}

	mostrarEstadisticasLeadsRoles() {
		switch (this.nombreRolValor) {
			case 'A':
				this.estadisticasLeads = true;
				break;
			case 'DT':
				this.estadisticasLeads = true;
				break;
			case 'ACC':
				this.estadisticasLeads = true;
				break;
			default:
				this.estadisticasLeads = false;
				break;
		}
	}

	consultarNombreEmpresa(nombreEmpresa: any) {
		this.nombresEmpresas = this._usuarioService.nombresEmpresa(nombreEmpresa);
		switch (nombreEmpresa) {
			case 'A':
				this.imgNombreEmpresa = './assets/img/empresa-autodenar.png';
				break;
			case 'M':
				this.imgNombreEmpresa = './assets/img/empresa-motodenar.png';
				break;
			case 'T':
				this.imgNombreEmpresa = './assets/img/empresa-automotora.png';
				break;
			case 'D':
				this.imgNombreEmpresa = './assets/img/empresa-autodenar.png';
				break;
			default:
				this.imgNombreEmpresa = './assets/img/empresa-autodenar.png';
				break;
		}
	}

	/**
	 * 
	 * Inicia Atenciones y Disponibilidad de Clientes
	 * Siembra y Cosecha
	 */
	async consultarDisponibilidadAgente(idAgente: string) {
		this.loading = true;
		const responseDisponibilidaAgente = await this._siembraCosechaService.consultarDisponibilidadAgente(idAgente);
		//console.log(responseDisponibilidaAgente);
		if (responseDisponibilidaAgente["status"] === "success" && responseDisponibilidaAgente["code"] === "200") {
			if (responseDisponibilidaAgente['data'].disponibilidad.estado == true) {
				const responseAtenciones = this.atencionesAsesorArraySC(responseDisponibilidaAgente['data'].disponibilidad.atenciones);
				//console.log(responseAtenciones);
				this.idAtencionSC = '' + responseAtenciones.id;
				this.idClienteSC = '' + responseAtenciones.idCliente;
				this.nombreClienteSC = responseAtenciones.cliente;
				this.documentoSC = responseAtenciones.documento;
				this.fechaInicialSC = responseAtenciones.fechaInicial;
				this.ventanaAgenteDisponible = true;
			}
			else {
				this.ventanaAgenteDisponible = false;
			}
			this.loading = false;
		}
		else if (responseDisponibilidaAgente["status"] === "error" && responseDisponibilidaAgente["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseDisponibilidaAgente['message']);
		}
		else {
			this.ventanaAgenteDisponible = false;
			this.loading = false;
		}
	}

	atencionesAsesorArraySC(atenciones): any {
		let atencionesAgente = {
			id: null,
			idCliente: null,
			cliente: null,
			documento: null,
			fechaInicial: null
		};
		atenciones.forEach((element) => {
			if (element.fechaFinal == '0000-00-00') {
				atencionesAgente.id = element.id;
				atencionesAgente.idCliente = element.cliente.id;
				atencionesAgente.cliente = element.cliente.nombres + ' ' + element.cliente.apellidos;
				atencionesAgente.documento = element.cliente.documento;
				atencionesAgente.fechaInicial = element.fechaInicial;
			}
		});

		return atencionesAgente;
	}

	async cargarInformacionSelect() {
		this.tipificacionArray = [];
		this.objecionesArray = [];
		const tipificacionParametros: Object = await this._parametrizacionService.listarCategoriaParametrizacion('Tipificación Clientes');
		const objecionesParametros: Object = await this._parametrizacionService.listarCategoriaParametrizacion('Objeciones');

		tipificacionParametros['data'].forEach((element) => {
			this.tipificacionArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
		objecionesParametros['data'].forEach((element) => {
			this.objecionesArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
	}

	finalizarAtencionSC(idAtencion) {
		$("#modalFinalizarAtencionAgente").modal("show");
	}

	async onSubmitFinalizarAtencionSC() {
		this.loading = true;

		let atencionAgente = new FinalizarAtencionesAgente(
			this.idAtencionSC,
			this.finalizarAtencionAgenteForm.value.tipificacion,
			this.finalizarAtencionAgenteForm.value.observacion
		); //console.log(atencionAgente);

		const responseFinalizarAtencion = await this._siembraCosechaService.finalizarAtencionesAgente(atencionAgente);
		//console.log(responseFinalizarAtencion);
		if (responseFinalizarAtencion["status"] === "success" && responseFinalizarAtencion["code"] === "200") {
			this.consultarDisponibilidadAgente(this.idAgente);
			$("#modalFinalizarAtencionAgente").modal("hide");
			this.finalizarAtencionAgenteForm.reset();
			this._sweetAlertService.showNotification('top', 'right', 'success', 'success', '<b>' + responseFinalizarAtencion['message'] + '<b>', 1000);
			this.loading = false;
		}
		else if (responseFinalizarAtencion["status"] === "error" && responseFinalizarAtencion["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseFinalizarAtencion['message']);
		}
		else {
			$("#modalFinalizarAtencionAgente").modal("hide");
			this.finalizarAtencionAgenteForm.reset();
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseFinalizarAtencion['message'] + '<b>', 1000);
			this.loading = false;
		}
	}

	verFichaClienteSC(idCliente: string) {
		this._router.navigate(["siembra-y-cosecha/ficha-cliente/", idCliente]);
	}
	/**
	 * 
	 * Finaliza Atenciones y Disponibilidad de Clintes
	 * Siembra y Cosecha
	 */

	/**
	 * 
	 * Inicia Atencion y disponibilidad de Asesores
	 * del area comercial
	 */

	async consultarDisponibilidadAsesor(idAsesor: any) {
		this.loading = true;
		const responseDisponibilidaAsesor = await this._tableroAsignacionService.consultarDisponibilidadAsesor(idAsesor);
		//console.log(responseDisponibilidaAsesor);
		if (responseDisponibilidaAsesor["status"] === "success" && responseDisponibilidaAsesor["code"] === "200") {
			if (responseDisponibilidaAsesor['data'].disponibilidad.estado == true) {
				const responseAtenciones = this.atencionesAsesorArray(responseDisponibilidaAsesor['data'].disponibilidad.atenciones);
				//console.log(responseAtenciones);
				this.idAtencion = responseAtenciones.id;
				this.idIngreso = responseAtenciones.idIngreso;
				this.nombreCliente = responseAtenciones.cliente;
				this.documento = responseAtenciones.documento;
				this.fechaInicial = responseAtenciones.fechaInicial;
				this.ventanaAsesorDisponible = true;
			}
			else {
				this.ventanaAsesorDisponible = false;
			}
			this.loading = false;
		}
		else if (responseDisponibilidaAsesor["status"] === "error" && responseDisponibilidaAsesor["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseDisponibilidaAsesor['message']);
		}
		else {
			this.ventanaAsesorDisponible = false;
			this.loading = false;
		}
	}

	atencionesAsesorArray(atenciones): any {
		let atencionesAsesor = {
			id: null,
			idIngreso: null,
			cliente: null,
			documento: null,
			fechaInicial: null
		};
		atenciones.forEach((element) => {
			if (element.fechaFinal == '0000-00-00') {
				atencionesAsesor.id = element.id;
				atencionesAsesor.idIngreso = '' + element.ingreso.id;
				atencionesAsesor.cliente = element.ingreso.persona.nombres + ' ' + element.ingreso.persona.apellidos;
				atencionesAsesor.documento = element.ingreso.persona.documento;
				atencionesAsesor.fechaInicial = element.fechaInicial;
			}
		});

		return atencionesAsesor;
	}

	finalizarAtencion(idAtencion) {
		$("#modalFinalizarAtencion").modal("show");
	}

	async onSubmitFinalizarAtencion() {
		this.loading = true;
		let objecion_ = this.finalizarAtencionForm.value.objecion;
		//let tipificacion_ = this.finalizarAtencionForm.value.tipificacion;
		let tipificacion_ = 'Ninguno';
		let observacion_ = this.finalizarAtencionForm.value.observacion;
		observacion_ = observacion_ == null ? '' : observacion_;
		//console.log(`${this.idAtencion}, ${objecion_}, ${observacion_}`);
		const responseFinalizarAtencion = await this._tableroAsignacionService.finalizarAtencionAsesor(this.idAtencion, objecion_, tipificacion_, observacion_);
		//console.log(responseFinalizarAtencion);
		if (responseFinalizarAtencion["status"] === "success" && responseFinalizarAtencion["code"] === "200") {
			this.consultarDisponibilidadAsesor(this.idAgente);
			$("#modalFinalizarAtencion").modal("hide");
			this.finalizarAtencionForm.reset();
			this._sweetAlertService.showNotification('top', 'right', 'success', 'success', '<b>' + responseFinalizarAtencion['message'] + '<b>', 1000);
			this.loading = false;
		}
		else if (responseFinalizarAtencion["status"] === "error" && responseFinalizarAtencion["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseFinalizarAtencion['message']);
		}
		else {
			this.ventanaAsesorDisponible = false;
			$("#modalFinalizarAtencion").modal("hide");
			this.finalizarAtencionForm.reset();
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseFinalizarAtencion['message'] + '<b>', 1000);
			this.loading = false;
		}
	}

	verFichaCliente(idIngreso: string) {
		this._router.navigate(["clientes/ficha-clientes/", idIngreso]);
	}

	/**
	* 
	* Finaliza Atencion y disponibilidad de Asesores
	* del area comercial
	*/

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}