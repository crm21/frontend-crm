import { Routes } from '@angular/router';

import { IngresoClientesComponent } from './ingreso-clientes/ingreso-clientes.component';
import { EstudiosCreditoComponent } from './estudios-credito/estudios-credito.component';

export const ReportesClientesRoutes: Routes = [

	{
		path: '',
		children: [ {
			path: 'listar-ingreso-clientes',
			component: IngresoClientesComponent
		},{
			path: 'listar-estudios-credito',
			component: EstudiosCreditoComponent
		}]
	}
];