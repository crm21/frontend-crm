import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { EstadisticasService } from '../../services/estadisticas/estadisticas.service';
import Chart from 'chart.js';

declare var $: any;

@Component({
	selector: 'app-estadisticas-leads',
	templateUrl: './estadisticas-leads.component.html',
	styleUrls: ['./estadisticas-leads.component.css']
})
export class EstadisticasLeadsComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	nombres: string;
	nombre: string = 'Usuario';
	nombresEmpresas: string = '';
	ocultarVentanaAsesor: any;
	permisosAtenciones: any[] = ['N', 'CL'];
	idUsuario: number = 0;

	/* Variables estadistica */
	clientesLeadsDiaNum: any = '----';
	clientesLeadsSemanaNum: any = '----';
	clientesLeadsMesNum: any = '----';
	clientesLeadsAnioNum: any = '----';
	clientesLeadsConCotizacionNum: any = '----';
	clientesLeadsCompraSiNum: any = '----';
	clientesLeadsProcesoVentaActivoNum: any = '----';

	constructor(
		private _estadisticastService: EstadisticasService,
		private _sweetAlertService: SweetAlertService,
		private _router: Router
	) {
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			/* this.ocultarVentanaAsesor = identityData['data'];
			this.nombres = this.ocultarVentanaAsesor.nombres + ' ' + this.ocultarVentanaAsesor.apellidos;
			this.nombre = this.ocultarVentanaAsesor.rol.nombre;
			this.idUsuario = this.ocultarVentanaAsesor.sub; */
		}
	}

	ngOnInit() {
		if (localStorage.getItem('identity_crm')) {
			this.clientesLeadsDiaReporte('1');
			this.clientesLeadsSemanaReporte('2');
			this.clientesLeadsMesReporte('3');
			this.clientesLeadsAnioReporte('4');
			this.clientesLeadsCanalIngreso('5');
			this.clientesLeadsConCotizacion('6');
			this.clientesLeadsEstadoCompraSi('7');
			this.clientesLeadsProcesoVentaAbierto('8');
		}
	}

	/* Estadisticas */
	async clientesLeadsDiaReporte(opcion: any) {
		this.loading = true;
		const clientesLeadsDiaDatos = await this._estadisticastService.reporteClientesLeads(opcion);
		//console.log(clientesLeadsDiaDatos);
		if (clientesLeadsDiaDatos["status"] === "success" && clientesLeadsDiaDatos["code"] === "200") {
			this.clientesLeadsDiaNum = clientesLeadsDiaDatos['data'];
			this.loading = false;
		}
		else if (clientesLeadsDiaDatos["status"] === "success" && clientesLeadsDiaDatos["code"] === "300") {
			this.clientesLeadsDiaNum = clientesLeadsDiaDatos['data'];
			this.loading = false;
		} else if (clientesLeadsDiaDatos["status"] === "error" && clientesLeadsDiaDatos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(clientesLeadsDiaDatos['message']);
		}
		else {
			this.loading = false;
		}
	}

	async clientesLeadsSemanaReporte(opcion: any) {
		this.loading = true;
		const clientesLeadsSemanaDatos = await this._estadisticastService.reporteClientesLeads(opcion);
		//console.log(clientesLeadsSemanaDatos);
		if (clientesLeadsSemanaDatos["status"] === "success" && clientesLeadsSemanaDatos["code"] === "200") {
			this.clientesLeadsSemanaNum = clientesLeadsSemanaDatos['data'];
			this.loading = false;
		}
		else if (clientesLeadsSemanaDatos["status"] === "success" && clientesLeadsSemanaDatos["code"] === "300") {
			this.clientesLeadsSemanaNum = clientesLeadsSemanaDatos['data'];
			this.loading = false;
		} else if (clientesLeadsSemanaDatos["status"] === "error" && clientesLeadsSemanaDatos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(clientesLeadsSemanaDatos['message']);
		}
		else {
			this.loading = false;
		}
	}

	async clientesLeadsMesReporte(opcion: any) {
		this.loading = true;
		const clientesLeadsMesDatos = await this._estadisticastService.reporteClientesLeads(opcion);
		//console.log(clientesLeadsMesDatos);
		if (clientesLeadsMesDatos["status"] === "success" && clientesLeadsMesDatos["code"] === "200") {
			this.clientesLeadsMesNum = clientesLeadsMesDatos['data'];
			this.loading = false;
		}
		else if (clientesLeadsMesDatos["status"] === "success" && clientesLeadsMesDatos["code"] === "300") {
			this.clientesLeadsMesNum = clientesLeadsMesDatos['data'];
			this.loading = false;
		} else if (clientesLeadsMesDatos["status"] === "error" && clientesLeadsMesDatos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(clientesLeadsMesDatos['message']);
		}
		else {
			this.loading = false;
		}
	}

	async clientesLeadsAnioReporte(opcion: any) {
		this.loading = true;
		const clientesLeadsAnioDatos = await this._estadisticastService.reporteClientesLeads(opcion);
		//console.log(clientesLeadsAnioDatos);
		if (clientesLeadsAnioDatos["status"] === "success" && clientesLeadsAnioDatos["code"] === "200") {
			this.clientesLeadsAnioNum = clientesLeadsAnioDatos['data'];
			this.loading = false;
		}
		else if (clientesLeadsAnioDatos["status"] === "success" && clientesLeadsAnioDatos["code"] === "300") {
			this.clientesLeadsAnioNum = clientesLeadsAnioDatos['data'];
			this.loading = false;
		} else if (clientesLeadsAnioDatos["status"] === "error" && clientesLeadsAnioDatos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(clientesLeadsAnioDatos['message']);
		}
		else {
			this.loading = false;
		}
	}

	async clientesLeadsCanalIngreso(opcion: any) {
		this.loading = true;
		const clientesLeadsClientesInActivosTotal = await this._estadisticastService.reporteClientesLeads(opcion);
		//console.log(clientesLeadsClientesInActivosTotal);
		if (clientesLeadsClientesInActivosTotal["status"] === "success" && clientesLeadsClientesInActivosTotal["code"] === "200") {
			this.leerDataCanalIngreso(clientesLeadsClientesInActivosTotal['data']);
			this.loading = false;
		}
		else if (clientesLeadsClientesInActivosTotal["status"] === "success" && clientesLeadsClientesInActivosTotal["code"] === "300") {
			this.loading = false;
		}
		else if (clientesLeadsClientesInActivosTotal["status"] === "error" && clientesLeadsClientesInActivosTotal["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(clientesLeadsClientesInActivosTotal['message']);
		}
		else {
			this.loading = false;
		}
	}
	
	async clientesLeadsConCotizacion(opcion: any) {
		this.loading = true;
		const responseClientesLeadsConCotizaciones = await this._estadisticastService.reporteClientesLeads(opcion);
		//console.log(responseClientesLeadsConCotizaciones);
		if (responseClientesLeadsConCotizaciones["status"] === "success" && responseClientesLeadsConCotizaciones["code"] === "200") {
			this.clientesLeadsConCotizacionNum = responseClientesLeadsConCotizaciones['data'];
			this.loading = false;
		}
		else if (responseClientesLeadsConCotizaciones["status"] === "success" && responseClientesLeadsConCotizaciones["code"] === "300") {
			this.clientesLeadsConCotizacionNum = responseClientesLeadsConCotizaciones['data'];
			this.loading = false;
		}
		else if (responseClientesLeadsConCotizaciones["status"] === "error" && responseClientesLeadsConCotizaciones["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseClientesLeadsConCotizaciones['message']);
		}
		else {
			this.loading = false;
		}
	}
	
	async clientesLeadsEstadoCompraSi(opcion: any) {
		this.loading = true;
		const responseClientesLeadsEstadoCompraSi = await this._estadisticastService.reporteClientesLeads(opcion);
		//console.log(responseClientesLeadsEstadoCompraSi);
		if (responseClientesLeadsEstadoCompraSi["status"] === "success" && responseClientesLeadsEstadoCompraSi["code"] === "200") {
			this.clientesLeadsCompraSiNum = responseClientesLeadsEstadoCompraSi['data'];
			this.loading = false;
		}
		else if (responseClientesLeadsEstadoCompraSi["status"] === "success" && responseClientesLeadsEstadoCompraSi["code"] === "300") {
			this.clientesLeadsCompraSiNum = responseClientesLeadsEstadoCompraSi['data'];
			this.loading = false;
		}
		else if (responseClientesLeadsEstadoCompraSi["status"] === "error" && responseClientesLeadsEstadoCompraSi["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseClientesLeadsEstadoCompraSi['message']);
		}
		else {
			this.loading = false;
		}
	}
	
	async clientesLeadsProcesoVentaAbierto(opcion: any) {
		this.loading = true;
		const responseClientesLeadsProcesoVentaAbierto = await this._estadisticastService.reporteClientesLeads(opcion);
		//console.log(responseClientesLeadsProcesoVentaAbierto);
		if (responseClientesLeadsProcesoVentaAbierto["status"] === "success" && responseClientesLeadsProcesoVentaAbierto["code"] === "200") {
			this.clientesLeadsProcesoVentaActivoNum = responseClientesLeadsProcesoVentaAbierto['data'];
			this.loading = false;
		}
		else if (responseClientesLeadsProcesoVentaAbierto["status"] === "success" && responseClientesLeadsProcesoVentaAbierto["code"] === "300") {
			this.clientesLeadsProcesoVentaActivoNum = responseClientesLeadsProcesoVentaAbierto['data'];
			this.loading = false;
		}
		else if (responseClientesLeadsProcesoVentaAbierto["status"] === "error" && responseClientesLeadsProcesoVentaAbierto["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseClientesLeadsProcesoVentaAbierto['message']);
		}
		else {
			this.loading = false;
		}
	}

	leerDataCanalIngreso(dataCanalIngreso: any) {
		let dataArrayCanalNumeros: number[] = [];
		let dataArrayCanalNombres: string[] = [
			'Agendamiento', 'Lead', 'Lead Facebook',
			'Lead Instagram', 'Lead Web', 'Lead Whatsapp',
			'Llamadas Entrantes', 'Orgánico',
			'Radio', 'Taller', 'Televisión',
			'Usados', 'Visita a barrio'
		];

		dataArrayCanalNumeros.push(dataCanalIngreso['Agendamiento']);
		dataArrayCanalNumeros.push(dataCanalIngreso['Lead']);
		dataArrayCanalNumeros.push(dataCanalIngreso['Lead Facebook']);
		dataArrayCanalNumeros.push(dataCanalIngreso['Lead Instagram']);
		dataArrayCanalNumeros.push(dataCanalIngreso['Lead Web']);
		dataArrayCanalNumeros.push(dataCanalIngreso['Lead Whatsapp']);
		dataArrayCanalNumeros.push(dataCanalIngreso['Llamadas Entrantes']);
		dataArrayCanalNumeros.push(dataCanalIngreso['Orgánico']);
		dataArrayCanalNumeros.push(dataCanalIngreso['Radio']);
		dataArrayCanalNumeros.push(dataCanalIngreso['Taller']);
		dataArrayCanalNumeros.push(dataCanalIngreso['Televisión']);
		dataArrayCanalNumeros.push(dataCanalIngreso['Usados']);
		dataArrayCanalNumeros.push(dataCanalIngreso['Visita a barrio']);

		//console.log(dataArrayCanalNumeros);
		//console.log(dataArrayCanalNombres);
		
		this.llenarDatosDiagramaBarrasCanalIngreso(dataArrayCanalNumeros, dataArrayCanalNombres);
	}

	llenarDatosDiagramaBarrasCanalIngreso(dataDiagramaNumero: number[], dataDiagramaNombres: string[]) {
		let datosMesSerieNumero: number[] = dataDiagramaNumero;
		let datosMesSerieNombres: string[] = dataDiagramaNombres;

		var ctx = document.getElementById('canalIngresoGrafica');
		var myChart = new Chart(ctx, {
			type: 'bar',
			data: {
				labels: datosMesSerieNombres,
				datasets: [{
					label: '# Canal Ingreso',
					data: datosMesSerieNumero,
					backgroundColor: [
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)'
					],
					borderColor: [
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)'
					],
					borderWidth: 1
				}]
			},
			options: {
				responsive: true,
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});
	}

	/* Listado de Estadisticas por cada uno de los card */
	onSubmitClientesLeadsDiaReporte() {
		this._router.navigate(["estadisticas/leads-clientes-dia"]);
	}

	onSubmitClientesLeadsSemanaReporte() {
		this._router.navigate(["estadisticas/leads-clientes-semana"]);
	}

	onSubmitClientesLeadsMesReporte() {
		this._router.navigate(["estadisticas/leads-clientes-mes"]);
	}

	onSubmitClientesLeadsAnioReporte() {
		this._router.navigate(["estadisticas/leads-clientes-anio"]);
	}

	onSubmitCanalIngresoReporte() {
		this._router.navigate(["estadisticas/leads-clientes-canal-de-ingreso"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}