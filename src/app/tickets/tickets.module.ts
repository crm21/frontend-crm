import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFileUploaderModule } from "angular-file-uploader";

import { TicketsRoutes } from './tickets.routing';
import { LoadingModule } from '../shared/loading/loading.module';
import { LoadingSpinnerModule } from '../shared/loading-spinner/loading-spinner.module';
import { TicketsListarComponent } from './tickets-listar/tickets-listar.component';
import { TicketsRevisionComponent } from './tickets-revision/tickets-revision.component';

@NgModule({
	declarations: [TicketsListarComponent, TicketsRevisionComponent],
	imports: [
		CommonModule,
		RouterModule.forChild(TicketsRoutes),
		FormsModule,
		MaterialModule,
		ReactiveFormsModule,
		LoadingModule,
		LoadingSpinnerModule,
		AngularFileUploaderModule
	],
	providers: [DatePipe]
})
export class TicketsModule { }
