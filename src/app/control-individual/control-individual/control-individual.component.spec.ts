import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlIndividualComponent } from './control-individual.component';

describe('ControlIndividualComponent', () => {
  let component: ControlIndividualComponent;
  let fixture: ComponentFixture<ControlIndividualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlIndividualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlIndividualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
