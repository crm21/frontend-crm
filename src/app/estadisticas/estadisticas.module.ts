import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { EstadisticasRoutes } from './estadisticas.routing';
import { LoadingModule } from '../shared/loading/loading.module';
import { LoadingSpinnerModule } from '../shared/loading-spinner/loading-spinner.module';
import { ReporteClientesDiaComponent } from './reporte-clientes-dia/reporte-clientes-dia.component';
import { ReporteClientesSemanaComponent } from './reporte-clientes-semana/reporte-clientes-semana.component';
import { ReporteClientesMesComponent } from './reporte-clientes-mes/reporte-clientes-mes.component';
import { ReporteClientesAnioComponent } from './reporte-clientes-anio/reporte-clientes-anio.component';
import { ReporteVehiculosVendidosComponent } from './reporte-vehiculos-vendidos/reporte-vehiculos-vendidos.component';
import { ReporteClientesNoVentasComponent } from './reporte-clientes-no-ventas/reporte-clientes-no-ventas.component';
import { ReporteClientesConCotizacionesComponent } from './reporte-clientes-con-cotizaciones/reporte-clientes-con-cotizaciones.component';
import { ReporteClientesSinCotizacionesComponent } from './reporte-clientes-sin-cotizaciones/reporte-clientes-sin-cotizaciones.component';
import { ReporteClientesVehiculosUsadosComponent } from './reporte-clientes-vehiculos-usados/reporte-clientes-vehiculos-usados.component';
import { ReporteClientesEsperaComponent } from './reporte-clientes-espera/reporte-clientes-espera.component';
import { ReporteEstudiosCreditoComponent } from './reporte-estudios-credito/reporte-estudios-credito.component';
import { ReporteClientesIngresosActivosComponent } from './reporte-clientes-ingresos-activos/reporte-clientes-ingresos-activos.component';
import { ReporteProcesosVentaComponent } from './reporte-procesos-venta/reporte-procesos-venta.component';
import { ReporteClientesLeadsDiaComponent } from './reporte-clientes-leads-dia/reporte-clientes-leads-dia.component';
import { ReporteClientesLeadsSemanaComponent } from './reporte-clientes-leads-semana/reporte-clientes-leads-semana.component';
import { ReporteClientesLeadsMesComponent } from './reporte-clientes-leads-mes/reporte-clientes-leads-mes.component';
import { ReporteClientesLeadsAnioComponent } from './reporte-clientes-leads-anio/reporte-clientes-leads-anio.component';
import { ReporteClientesCanalIngresoComponent } from './reporte-clientes-canal-ingreso/reporte-clientes-canal-ingreso.component';

@NgModule({
	declarations: [
		ReporteClientesDiaComponent,
		ReporteClientesSemanaComponent,
		ReporteClientesMesComponent,
		ReporteClientesAnioComponent,
		ReporteVehiculosVendidosComponent,
		ReporteClientesNoVentasComponent,
		ReporteClientesConCotizacionesComponent,
		ReporteClientesSinCotizacionesComponent,
		ReporteClientesVehiculosUsadosComponent,
		ReporteClientesEsperaComponent,
		ReporteEstudiosCreditoComponent,
		ReporteClientesIngresosActivosComponent,
		ReporteProcesosVentaComponent,
		ReporteClientesLeadsDiaComponent,
		ReporteClientesLeadsSemanaComponent,
		ReporteClientesLeadsMesComponent,
		ReporteClientesLeadsAnioComponent,
		ReporteClientesCanalIngresoComponent
	],
	imports: [
		CommonModule,
		RouterModule.forChild(EstadisticasRoutes),
		FormsModule,
		MaterialModule,
		ReactiveFormsModule,
		LoadingModule,
		LoadingSpinnerModule
	],
	providers: [DatePipe]
})
export class EstadisticasModule { }
