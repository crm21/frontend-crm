import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { DatePipe } from '@angular/common';
import { RegistroClienteSiembraCosecha, RegistroAtencionesAgente, FinalizarAtencionesAgente } from '../../models/registrarClienteSiembraCosecha';
import { SiembraCosechaService } from '../../services/siembra-cosecha/siembra-cosecha.service';
import { ClienteSiembraCosecha, interfaceVehiculo, ProyectarKm } from '../../interfaces/interfaces';
import swal from 'sweetalert2';
import { UsuarioService } from '../../services/usuario/usuario.service';
import { VehiculosService } from '../../services/vehiculos/vehiculos.service';
import { Vehiculo } from '../../models/vehiculo';
import { ProyeccionService } from '../../services/proyeccion/proyeccion.service';

declare var $: any;

@Component({
	selector: 'app-ficha-clientes-sc',
	templateUrl: './ficha-clientes-sc.component.html',
	styleUrls: ['./ficha-clientes-sc.component.css']
})
export class FichaClientesScComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	clienteSiembraCosechaForm: FormGroup;
	crearAtencionAgenteForm: FormGroup;
	finalizarAtencionAgenteForm: FormGroup;
	vehiculoClienteRegActFormGroup: FormGroup;
	calcularProyeccionVehiculoFormGroup: FormGroup;

	/*Selects*/
	tipoDocumentosArray: any[] = [];
	tipificacionArray: any[] = [];
	totalAtencionesArray: any[] = [];
	tipoEntradaArray: any[] = [];

	atencionesAgente: boolean = false;
	cambiarEstadoForm: number = 1;
	opcionVentana: any = '';
	idCliente: string = '';
	correoEmpresa: string = '';
	telefonoEmpresa: string = '';

	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;

	/* Atenciones Agente */
	rolUsuario: string = '';
	idAgente: string = '';
	nombreUsuarioAtencion: string = '';
	idAtencion: string = '';
	nombreCliente: string = '';
	documento: string = '';
	fechaInicial: string = '';
	botonIniciarAtencion: boolean = true;
	finalizarAtencionVentana: boolean = false;
	permisosAtenciones: any[] = ['ACC', 'DT'];

	/* Vehiculos */
	idVehiculoCliente: string = '';
	vehiculosArray: any[] = [];
	versionVehiculoArray: any[] = [];
	modeloArray: any[] = [];
	tipoAceiteArray: any[] = [];
	tituloModal: string = '';
	opcionForm: number = 1;
	existenVehiculos: boolean = false;
	documentoCliente: string = '';

	/* Proyeccion */
	ocultarCampoKM: boolean = true;
	vehiculoUsadoNuevo: number = 0;
	idVehiculo: string = '';
	placaVehiculo: string = '';
	idProyeccion: string = '';
	nombreVehiculo: string = '';

	constructor(
		private _router: Router,
		private _datepipe: DatePipe,
		private _route: ActivatedRoute,
		private _formBuilder: FormBuilder,
		private _usuariosService: UsuarioService,
		private _vehiculosService: VehiculosService,
		private _proyeccionService: ProyeccionService,
		private _sweetAlertService: SweetAlertService,
		private _siembraCosechaService: SiembraCosechaService,
		private _parametrizacionService: ParametrizacionService
	) {
		this.opcionVentana = this._route.snapshot.data.opc;
		this.idCliente = this._route.snapshot.paramMap.get('idCliente');
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);
			this.rolUsuario = identityData['data'].rol.valor;
			this.idAgente = identityData['data'].sub;
			if (this.opcionVentana == '2') {
				this.cambiarEstadoForm = 2;
				this.buscarIdCliente('' + this.idCliente);
				this.historialLlamadasCliente(this.idCliente, '1');
				this.buscarVehiculosCliente();
			}
		}
	}

	ngOnInit(): void {
		this.clienteSiembraCosechaForm = this._formBuilder.group({
			tipoDoc: ['', Validators.required],
			documento: ['', [Validators.required, Validators.pattern("[0-9]{5,13}$")]],
			nombres: ['', [Validators.required, Validators.pattern("^[a-zA-ZñÑ ]{1,80}$")]],
			apellidos: ['', [Validators.required, Validators.pattern("^[a-zA-ZñÑ ]{1,80}$")]],
			correo: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
			tipoEntrada: ['']
		});
		this.finalizarAtencionAgenteForm = this._formBuilder.group({
			tipificacion: ['Ninguno'],
			observacion: [''],
		});
		this.vehiculoClienteRegActFormGroup = this._formBuilder.group({
			placa: ['', [Validators.required, Validators.pattern("^[a-zA-Z]{3}[0-9a-zA-Z]{3}$")]],
			modelo: ['0000'],
			version: ['', Validators.required],
			vin: [''],
			tipoAceite: ['', Validators.required]
		});
		this.calcularProyeccionVehiculoFormGroup = this._formBuilder.group({
			kmActual: ['', [Validators.required, Validators.pattern("[0-9]{1,13}$")]],
			kmProyectar: ['', [Validators.pattern("[0-9]{3,13}$")]]
		});

		if (localStorage.getItem('identity_crm')) {
			this.cargarInformacionSelect();
			let informacionEmpresa: string[] = this._sweetAlertService.datosEmpresas(localStorage.getItem('business'));
			//console.log(informacionEmpresa);
			this.correoEmpresa = informacionEmpresa[0];
			this.telefonoEmpresa = informacionEmpresa[1];
			if (this.permisosAtenciones.includes(this.rolUsuario)) {
				this.consultarDisponibilidadAgente(this.idAgente);
			}
		}
	}

	async cargarInformacionSelect() {
		this.loading = true;

		this.tipoDocumentosArray = [];
		this.tipificacionArray = [];
		this.versionVehiculoArray = [];
		this.tipoEntradaArray = [];
		this.tipoAceiteArray = [];
		this.modeloArray = [];

		let tipoDocumentosParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo Documento');
		let tipificacionParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Tipificación Clientes');
		let versionVehiculoParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo Vehículo');
		let tipoEntradaParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo Entrada');
		let tipoAceiteParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo Aceite');

		tipoDocumentosParametros['data'].forEach((element) => {
			this.tipoDocumentosArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		tipificacionParametros['data'].forEach((element) => {
			this.tipificacionArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		versionVehiculoParametros['data'].forEach((element) => {
			this.versionVehiculoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		tipoEntradaParametros['data'].forEach((element) => {
			this.tipoEntradaArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		tipoAceiteParametros['data'].forEach((element) => {
			this.tipoAceiteArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		const anioActual = new Date();
		const rangoAnioActual: number = + anioActual.getFullYear();

		for (var i = rangoAnioActual + 2; i >= 1990; --i) {
			this.modeloArray.push({
				valor: '' + i,
				nombre: '' + i
			});
		}

		this.loading = false;
	}

	/**
	 * Inicia Crear Cliente
	 */
	onSubmitCrearCliente(opcion: number) {
		//console.log(opcion);
		if (opcion === 1) {
			this.crearClienteSC();
		} else {
			this.actualizarClienteSC();
		}
	}

	async crearClienteSC() {
		this.loading = true;
		let crearClienteSC = new RegistroClienteSiembraCosecha(
			this.clienteSiembraCosechaForm.value.nombres,
			this.clienteSiembraCosechaForm.value.apellidos,
			this.clienteSiembraCosechaForm.value.correo,
			this.clienteSiembraCosechaForm.value.documento,
			this.clienteSiembraCosechaForm.value.tipoDoc,
			'Contact Center',
			this.correoEmpresa,
			this.telefonoEmpresa
		); //console.log(crearClienteSC);

		let responseCrearClienteSC = await this._siembraCosechaService.registrarClientesSiembraC(crearClienteSC);
		//console.log(responseCrearClienteSC);
		if (responseCrearClienteSC["status"] === "success" && responseCrearClienteSC["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseCrearClienteSC["message"], 'success')
				.then(result => {
					this.clienteSiembraCosechaForm.reset();
					this.cambiarEstadoForm = 2;
					this.idCliente = responseCrearClienteSC["data"].id;
					this._router.navigate(["siembra-y-cosecha/ficha-cliente/", this.idCliente]);
				}).catch(swal.noop);
		}
		else if (responseCrearClienteSC["status"] === "success" && responseCrearClienteSC["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseCrearClienteSC["message"], 'warning');
		}
		else if (responseCrearClienteSC["status"] === "error" && responseCrearClienteSC["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseCrearClienteSC['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseCrearClienteSC["message"], 'error');
		}
	}
	/**
	 * Fin Crear Cliente
	 */

	/**
	 * Inicia Buscar Cliente para actualizar
	 */
	async buscarIdCliente(idCliente: string) {
		this.loading = true;
		const responseIdCliente = await this._siembraCosechaService.listarClienteSCPorId(idCliente);
		//console.log(responseIdCliente);
		if (responseIdCliente["status"] === "success" && responseIdCliente["code"] === "200") {
			let datosCliente: ClienteSiembraCosecha = responseIdCliente['data'].cliente;
			datosCliente.tipoEntrada = responseIdCliente['data'].registro.tipoEntrada;
			datosCliente.correo = responseIdCliente['data'].infoPersonal.correo;
			this.documentoCliente = datosCliente.documento;
			//console.log(datosCliente);
			this.clienteSiembraCosechaForm.setValue({
				tipoDoc: datosCliente.tipoDocumento.valor,
				documento: datosCliente.documento,
				nombres: datosCliente.nombres,
				apellidos: datosCliente.apellidos,
				correo: datosCliente.correo,
				tipoEntrada: datosCliente.tipoEntrada.valor
			});
			this.loading = false;
		}
		else if (responseIdCliente["status"] === "success" && responseIdCliente["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseIdCliente["message"], 'warning')
				.then(result => {
					this.regresar();
				}).catch(swal.noop);
		}
		else if (responseIdCliente["status"] === "error" && responseIdCliente["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseIdCliente['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseIdCliente["message"], 'OK', 'error');
		}
	}

	async actualizarClienteSC() {
		this.loading = true;
		let actualizarClienteSC = new RegistroClienteSiembraCosecha(
			this.clienteSiembraCosechaForm.value.nombres,
			this.clienteSiembraCosechaForm.value.apellidos,
			this.clienteSiembraCosechaForm.value.correo,
			this.clienteSiembraCosechaForm.value.documento,
			this.clienteSiembraCosechaForm.value.tipoDoc,
			this.clienteSiembraCosechaForm.value.tipoEntrada,
			'',
			''
		); //console.log(actualizarClienteSC);
		//this.documentoCliente = this.clienteSiembraCosechaForm.value.documento;

		let responseActualizarClienteSC = await this._siembraCosechaService.actualizarClientesSiembraC('' + this.idCliente, actualizarClienteSC);
		//console.log(responseActualizarClienteSC);
		if (responseActualizarClienteSC["status"] === "success" && responseActualizarClienteSC["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseActualizarClienteSC["message"], 'success')
				.then(result => {
					//this.clienteSiembraCosechaForm.reset();
					this.cambiarEstadoForm = 2;
					this.idCliente = responseActualizarClienteSC["data"].id;
					this._router.navigate(["siembra-y-cosecha/ficha-cliente/", this.idCliente]);
				}).catch(swal.noop);
		}
		else if (responseActualizarClienteSC["status"] === "success" && responseActualizarClienteSC["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseActualizarClienteSC["message"], 'warning');
		}
		else if (responseActualizarClienteSC["status"] === "error" && responseActualizarClienteSC["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseActualizarClienteSC['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseActualizarClienteSC["message"], 'error');
		}
	}
	/**
	 * Fin Buscar Cliente para actualizar
	 */

	/**
	 * Inicia Historial de Llamadas
	 */
	crearAtencion() {
		swal({
			title: 'Atención a Clientes',
			text: '¿Desea atender al cliente ahora?',
			type: 'question',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-warning',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: 'Sí atender',
			cancelButtonText: 'No atender',
			buttonsStyling: true,
			reverseButtons: true
		}).then((result) => {
			if (result.value) {
				swal({
					title: 'Escribir una observación para la atención',
					html: '<div class="form-group">' +
						'<input id="input-field" type="text" class="form-control" />' +
						'</div>',
					showCancelButton: true,
					confirmButtonClass: 'btn btn-success',
					cancelButtonClass: 'btn btn-danger',
					buttonsStyling: false
				})
					.then((result) => {
						let observacionAtencion = $('#input-field').val();
						this.onSubmitCrearAtencionAgente(observacionAtencion);
					}).catch(swal.noop);
			}
		}).catch(swal.noop);

	}

	async onSubmitCrearAtencionAgente(observacion: string) {
		this.loading = true;

		let atencionAgente = new RegistroAtencionesAgente(
			this.idCliente,
			observacion
		); //console.log(atencionAgente);

		const responseCrearAtencionAgente = await this._siembraCosechaService.registrarAtencionesAgente(atencionAgente);
		//console.log(responseCrearAtencionAgente);
		if (responseCrearAtencionAgente["status"] === "success" && responseCrearAtencionAgente["code"] === "200") {
			this.consultarDisponibilidadAgente(this.idAgente);
			this.historialLlamadasCliente(this.idCliente, '1');
			this._sweetAlertService.showNotification('top', 'right', 'success', 'success', '<b>' + responseCrearAtencionAgente['message'] + '<b>', 1000);
			this.loading = false;
		}
		else if (responseCrearAtencionAgente["status"] === "success" && responseCrearAtencionAgente["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseCrearAtencionAgente['message'] + '<b>', 1000);
		}
		else if (responseCrearAtencionAgente["status"] === "error" && responseCrearAtencionAgente["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseCrearAtencionAgente['message']);
		}
		else {
			this._sweetAlertService.showNotification('top', 'right', 'danger', 'danger', '<b>' + responseCrearAtencionAgente['message'] + '<b>', 1000);
			this.loading = false;
		}
	}

	async historialLlamadasCliente(idCliente: string, page: string) {
		this.loading = true;
		let responseLlamadasCliente = await this._siembraCosechaService.llamadasPorCliente(idCliente, '' + page);
		//console.log(responseLlamadasCliente);
		if (responseLlamadasCliente["status"] === "success" && responseLlamadasCliente["code"] === "200") {
			this.llenarAtenciones(responseLlamadasCliente['data'].llamadas, +page)
			this.atencionesAgente = true;
			this.loading = false;
		}
		else if (responseLlamadasCliente["status"] === "success" && responseLlamadasCliente["code"] === "300") {
			this.loading = false;
			this.atencionesAgente = false;
			//this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseLlamadasCliente['message'] + '<b>', 1000);
		}
		else if (responseLlamadasCliente["status"] === "error" && responseLlamadasCliente["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseLlamadasCliente['message']);
		}
		else {
			this.loading = false;
			this.atencionesAgente = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseLlamadasCliente["message"], 'OK', 'error');
		}
	}

	llenarAtenciones(atenciones: any, page: number) {
		//console.log(atenciones);
		this.totalAtencionesArray = [];

		if (atenciones.llamadas.length > 0) {
			this.atencionesAgente = true;
			let dataAtenciones = atenciones.llamadas;
			this.total_item_count = atenciones['total_item_count'];
			this.page_actual = page;

			this.total_pages = atenciones['total_pages'];

			if (page >= 2) this.page_prev = page - 1;
			else this.page_prev = 1;

			if (page < atenciones['total_pages']) this.page_next = page + 1;
			else {
				if (atenciones['total_pages'] == 0) this.page_next = 1;
				else this.page_next = atenciones['total_pages'];
			}

			dataAtenciones.forEach((element) => {
				this.totalAtencionesArray.push({
					atencionId: element.id,
					fechaInicial: element.fechaInicial,
					fechaFinal: element.fechaFinal == '0000-00-00' ? '- - -' : element.fechaFinal,
					tiempoAtencion: element.tiempoAtencion,
					tipificacion: element.tipificacion.nombre,
					agente: element.agente.nombres + ' ' + element.agente.apellidos,
					estado: element.estado.nombre,
					observacion: element.observacion
				});
			});
			//this.consultarDisponibilidadAgente(this.idAgente);
		} else {
			this.atencionesAgente = false;
		}
	}

	finalizarAtencion(idAtencion: string) {
		this.idAtencion = idAtencion;
		$("#modalFinalizarAtencionAgente").modal("show");
	}

	async onSubmitFinalizarAtencion() {
		this.loading = true;

		let atencionAgente = new FinalizarAtencionesAgente(
			this.idAtencion,
			this.finalizarAtencionAgenteForm.value.tipificacion,
			this.finalizarAtencionAgenteForm.value.observacion
		); //console.log(atencionAgente);

		const responseFinalizarAtencion = await this._siembraCosechaService.finalizarAtencionesAgente(atencionAgente);
		//console.log(responseFinalizarAtencion);
		if (responseFinalizarAtencion["status"] === "success" && responseFinalizarAtencion["code"] === "200") {
			this.consultarDisponibilidadAgente(this.idAgente);
			this.historialLlamadasCliente(this.idCliente, '1');
			$("#modalFinalizarAtencionAgente").modal("hide");
			this.finalizarAtencionAgenteForm.reset();
			this._sweetAlertService.showNotification('top', 'right', 'success', 'success', '<b>' + responseFinalizarAtencion['message'] + '<b>', 1000);
			this.loading = false;
		}
		else if (responseFinalizarAtencion["status"] === "error" && responseFinalizarAtencion["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseFinalizarAtencion['message']);
		}
		else {
			$("#modalFinalizarAtencionAgente").modal("hide");
			this.finalizarAtencionAgenteForm.reset();
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseFinalizarAtencion['message'] + '<b>', 1000);
			this.loading = false;
		}
	}
	/**
	 * Fin Historial de Llamadas
	 */

	/**
	 * Inicio Consultar disponibilidad del agente si esta en alguna atencion
	 */
	async consultarDisponibilidadAgente(idAgente: string) {
		this.loading = true;
		const disponibilidaAgente = await this._siembraCosechaService.consultarDisponibilidadAgente(idAgente);
		//console.log(disponibilidaAgente);
		if (disponibilidaAgente["status"] === "success" && disponibilidaAgente["code"] === "200") {
			this.loading = false;
			if (disponibilidaAgente['data'].disponibilidad.estado == true) {
				const responseAtenciones = this.atencionesAsesorArray(disponibilidaAgente['data'].disponibilidad.atenciones);
				//console.log(responseAtenciones);
				this.nombreUsuarioAtencion = disponibilidaAgente['data'].nombre;
				this.idAtencion = responseAtenciones.id;
				this.nombreCliente = responseAtenciones.cliente;
				this.documento = responseAtenciones.documento;
				this.fechaInicial = responseAtenciones.fechaInicial;
				this.botonIniciarAtencion = false;
				this.finalizarAtencionVentana = true;
			}
			else {
				this.botonIniciarAtencion = true;
				this.finalizarAtencionVentana = false;
			}
		}
		else if (disponibilidaAgente["status"] === "error" && disponibilidaAgente["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(disponibilidaAgente['message']);
		}
		else {
			this.loading = false;
			this.finalizarAtencionVentana = false;
		}
		/* Se debe cargar otra vez las atenciones a la ficha del cliente */

	}

	atencionesAsesorArray(atenciones): any {
		let atencionesAgente = {
			id: null,
			cliente: null,
			documento: null,
			fechaInicial: null
		};
		atenciones.forEach((element) => {
			if (element.fechaFinal == '0000-00-00') {
				atencionesAgente.id = element.id;
				atencionesAgente.cliente = element.cliente.nombres + ' ' + element.cliente.apellidos;
				atencionesAgente.documento = element.cliente.documento;
				atencionesAgente.fechaInicial = element.fechaInicial;
			}
		});

		return atencionesAgente;
	}
	/**
	 * Fin Consultar disponibilidad del agente si esta en alguna atencion
	 */

	/**
	 * Inicia Seccion Vehiculos
	 * Listar vehiculos asociados al cliente
	 */
	async buscarVehiculosCliente() {
		this.loading = true;
		const responseVehiculosCliente = await this._vehiculosService.listarVehiculosCliente(2, this.idCliente, '1');
		//console.log(responseVehiculosCliente);
		if (responseVehiculosCliente["status"] === "success" && responseVehiculosCliente["code"] === "200") {
			this.loading = false;
			if (responseVehiculosCliente["data"].estadoVehiculos) {
				this.existenVehiculos = responseVehiculosCliente["data"].estadoVehiculos;
				this.vehiculosArray = responseVehiculosCliente["data"].vehiculos;
				//this.llenarVehiculosArray(responseVehiculosCliente["data"]);
			} else {
				this.existenVehiculos = responseVehiculosCliente["data"].estadoVehiculos;
				this.vehiculosArray = [];
			}
		}
		else if (responseVehiculosCliente["status"] === "success" && responseVehiculosCliente["code"] === "300") {
			this.loading = false;
			this.existenVehiculos = false;
			this.vehiculosArray = [];
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseVehiculosCliente['message'] + '<b>', 1000);
		}
		else if (responseVehiculosCliente["status"] === "error" && responseVehiculosCliente["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseVehiculosCliente['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'error', 'warning', '<b>' + responseVehiculosCliente['message'] + '<b>', 1000);
		}
	}

	/* llenarVehiculosArray(dataVehiculo: any) {
		//console.log(dataVehiculo.vehiculos);
		this.vehiculosArray = [];
		let vehiculosDataArray = dataVehiculo.vehiculos;
		let JSONParam: any;

		vehiculosDataArray.forEach(element => {
			JSONParam = {
				id: element.id,
				placa: element.placa,
				modelo: element.modelo,
				version: element.version,
				tipoAceite: element.tipoAceite,
				vin: element.vin,
				estado: element.estado
			};
			this.vehiculosArray.push(JSONParam);
		});
		//console.log(this.vehiculosArray);
	} */

	/* Cargar datos para Actualizar vehículos */
	modalActualizarVehiculo(rowVehiculo: any) {
		//console.log(rowVehiculo);
		this.idVehiculoCliente = rowVehiculo.id;
		this.tituloModal = 'Actualizar';
		this.opcionForm = 1;
		let vehiculoRow: interfaceVehiculo = rowVehiculo;
		this.vehiculoClienteRegActFormGroup.setValue({
			placa: vehiculoRow.placa,
			modelo: vehiculoRow.modelo,
			version: vehiculoRow.version.valor,
			vin: vehiculoRow.vin,
			tipoAceite: vehiculoRow.tipoAceite.valor
		});
		$("#modalVehiculoCliente").modal("show");
	}

	/* Cargar formulario Registrar vehículos */
	modalRegistrarVehiculo() {
		this.tituloModal = 'Registrar';
		this.opcionForm = 2;
		$("#modalVehiculoCliente").modal("show");
	}

	/* Registrar/Actualizar vehículos */
	onSubmitVehiculoForm(opc: any) {
		if (opc == 1) {
			this.actualizarVehiculoCliente();
		} else {
			this.registrarVehiculoCliente();
		}
	}

	/* Actualizar vehículos del cliente */
	async actualizarVehiculoCliente() {
		this.loading = true;
		let actualizarVehiculo = new Vehiculo(
			this.vehiculoClienteRegActFormGroup.value.placa,
			'' + this.vehiculoClienteRegActFormGroup.value.modelo,
			this.vehiculoClienteRegActFormGroup.value.version,
			this.idCliente,
			this.vehiculoClienteRegActFormGroup.value.vin,
			this.vehiculoClienteRegActFormGroup.value.tipoAceite
		); //console.log(actualizarVehiculo);

		let responseActualizarVehiculo = await this._vehiculosService.actualizarVehiculoCliente(this.idVehiculoCliente, actualizarVehiculo);
		//console.log(responseActualizarVehiculo);
		if (responseActualizarVehiculo["status"] === "success" && responseActualizarVehiculo["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseActualizarVehiculo["message"], 'success')
				.then(result => {
					$("#modalVehiculoCliente").modal("hide");
					this.vehiculoClienteRegActFormGroup.reset();
					this.buscarVehiculosCliente();
				}).catch(swal.noop);
		}
		else if (responseActualizarVehiculo["status"] === "success" && responseActualizarVehiculo["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseActualizarVehiculo["message"], 'warning');
		}
		else if (responseActualizarVehiculo["status"] === "error" && responseActualizarVehiculo["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseActualizarVehiculo['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseActualizarVehiculo["message"], 'error');
		}
	}

	/* Registrar vehículos del cliente */
	async registrarVehiculoCliente() {
		this.loading = true;
		let registroVehiculo = new Vehiculo(
			this.vehiculoClienteRegActFormGroup.value.placa,
			'' + this.vehiculoClienteRegActFormGroup.value.modelo,
			this.vehiculoClienteRegActFormGroup.value.version,
			this.idCliente,
			this.vehiculoClienteRegActFormGroup.value.vin,
			this.vehiculoClienteRegActFormGroup.value.tipoAceite
		); //console.log(registroVehiculo);

		let responseRegistrarVehiculo = await this._vehiculosService.registrarVehiculoCliente(registroVehiculo);
		//console.log(responseRegistrarVehiculo);
		if (responseRegistrarVehiculo["status"] === "success" && responseRegistrarVehiculo["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseRegistrarVehiculo["message"], 'success')
				.then(result => {
					$("#modalVehiculoCliente").modal("hide");
					this.vehiculoClienteRegActFormGroup.reset();
					this.buscarVehiculosCliente();
				}).catch(swal.noop);
		}
		else if (responseRegistrarVehiculo["status"] === "success" && responseRegistrarVehiculo["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseRegistrarVehiculo["message"], 'warning');
		}
		else if (responseRegistrarVehiculo["status"] === "error" && responseRegistrarVehiculo["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseRegistrarVehiculo['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseRegistrarVehiculo["message"], 'error');
		}
	}

	mayusculas(letra: any) {
		(document.getElementById('placa') as HTMLInputElement).value = (document.getElementById('placa') as HTMLInputElement).value.toUpperCase();
	}
	/**
	* Finaliza Seccion Vehiculos
	*/

	/* Inicio Agendamiento de Citas */
	agendar() {
		if (this.existenVehiculos) {
			localStorage.setItem('rutaAgenda', 'siembra-y-cosecha/ficha-cliente/' + this.idCliente);
			localStorage.setItem('documentoC', this.documentoCliente);
			this._router.navigate(["agendamiento-posventa/agendar-taller"]);
		}
		else {
			swal('Advertencia!',
				'Para realizar un agendamiento al cliente, este debe tener al menos un vehículo registrado.' +
				'Por favor actualice la información del cliente e intente nuevamente',
				'warning')
				.then(result => {
					//this._router.navigate(["clientes/mi-perfil"]);
				}).catch(swal.noop);
		}
	}
	/* Fin Agendamiento de Citas */

	/* Inicia Proyeccion de kilometraje */
	async modalProyeccionVehiculo(rowVehiculo: any) {
		this.idVehiculo = '' + rowVehiculo['id'];
		this.placaVehiculo = rowVehiculo['placa'];
		await this.vehiculoComprobarUsuadoNuevoCliente(this.placaVehiculo);
		let valorProyeccionVehiculo = await this.proyeccionVehicularExistente(this.idVehiculo);

		if (valorProyeccionVehiculo) {
			this.camposEdicionRol();
			$("#modalCalcularProyeccionVehiculosCliente").modal("show");
		}
		else {
			swal({
				title: 'Vehículo a una Proyección',
				text: '¿Desea registrar el vehículo a una proyección ahora?',
				type: 'question',
				showCancelButton: true,
				confirmButtonClass: 'btn btn-warning',
				cancelButtonClass: 'btn btn-danger',
				confirmButtonText: 'Sí atender',
				cancelButtonText: 'No atender',
				buttonsStyling: true,
				reverseButtons: true
			}).then((result) => {
				if (result.value) this.registrarProyeccionVehiculoCliente(this.idVehiculo);
			}).catch(swal.noop);
		}
	}

	async proyeccionVehicularExistente(idVehiculo: string): Promise<boolean> {
		this.loading = true;
		let valorRetornado: boolean = false;
		const responseComprobarVehiculosClienteUN = await this._proyeccionService.proyeccionVehiculos(idVehiculo);
		//console.log(responseComprobarVehiculosClienteUN);
		if (responseComprobarVehiculosClienteUN["status"] === "success" && responseComprobarVehiculosClienteUN["code"] === "200") {
			this.loading = false;
			valorRetornado = responseComprobarVehiculosClienteUN["data"].proyeccionRegistrada;
			if (valorRetornado) {
				this.cargarProyeccionVehicular(responseComprobarVehiculosClienteUN["data"].infoProyeccion);
			}
		}
		else if (responseComprobarVehiculosClienteUN["status"] === "success" && responseComprobarVehiculosClienteUN["code"] === "300") {
			this.loading = false;
		}
		else if (responseComprobarVehiculosClienteUN["status"] === "error" && responseComprobarVehiculosClienteUN["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseComprobarVehiculosClienteUN['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'error', 'warning', '<b>' + responseComprobarVehiculosClienteUN['message'] + '<b>', 1000);
		}

		return valorRetornado;
	}

	cargarProyeccionVehicular(dataProyeccion: any) {
		//console.log(dataProyeccion);
		this.idProyeccion = dataProyeccion.id;
		this.nombreVehiculo = dataProyeccion.vehiculo.version.nombre;
		let datosProyeccion: ProyectarKm = dataProyeccion;

		this.calcularProyeccionVehiculoFormGroup.setValue({
			kmActual: datosProyeccion.kmActual,
			kmProyectar: datosProyeccion.kmProyectado
		});
	}

	async vehiculoComprobarUsuadoNuevoCliente(placaVehiculo: string) {
		this.loading = true;
		this.vehiculoUsadoNuevo = 0;
		const responseComprobarVehiculosClienteUN = await this._proyeccionService.comprobarVehiculoUsadoNuevo(placaVehiculo);
		//console.log(responseComprobarVehiculosClienteUN);
		if (responseComprobarVehiculosClienteUN["status"] === "success" && responseComprobarVehiculosClienteUN["code"] === "200") {
			this.loading = false;
			if (responseComprobarVehiculosClienteUN["data"].vehiculoNuevo) {
				this.vehiculoUsadoNuevo = 1;
				this.ocultarCampoKM = false;
			}
			else {
				this.vehiculoUsadoNuevo = 2;
				this.ocultarCampoKM = true;
			}
		}
		else if (responseComprobarVehiculosClienteUN["status"] === "success" && responseComprobarVehiculosClienteUN["code"] === "300") {
			this.loading = false;
		}
		else if (responseComprobarVehiculosClienteUN["status"] === "error" && responseComprobarVehiculosClienteUN["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseComprobarVehiculosClienteUN['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'error', 'warning', '<b>' + responseComprobarVehiculosClienteUN['message'] + '<b>', 1000);
		}
	}

	async registrarProyeccionVehiculoCliente(idVehiculo: string) {
		this.loading = true;
		const responseRegistrarProyeccionVehiculos = await this._proyeccionService.registrarProyeccionVehiculosUsados(idVehiculo);
		//console.log(responseRegistrarProyeccionVehiculos);
		if (responseRegistrarProyeccionVehiculos["status"] === "success" && responseRegistrarProyeccionVehiculos["code"] === "200") {
			this.loading = false;
			this.cargarProyeccionVehicular(responseRegistrarProyeccionVehiculos["data"]);
			await this.camposEdicionRol();
			$("#modalCalcularProyeccionVehiculosCliente").modal("show");
		}
		else if (responseRegistrarProyeccionVehiculos["status"] === "success" && responseRegistrarProyeccionVehiculos["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseRegistrarProyeccionVehiculos['message'] + '<b>', 1000);
		}
		else if (responseRegistrarProyeccionVehiculos["status"] === "error" && responseRegistrarProyeccionVehiculos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseRegistrarProyeccionVehiculos['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'error', 'warning', '<b>' + responseRegistrarProyeccionVehiculos['message'] + '<b>', 1000);
		}
	}

	onSubmitCalcularProyeccionVehiculoCliente() {
		//console.log(this.calcularProyeccionVehiculoFormGroup.value);
		let kmActual: number = + this.calcularProyeccionVehiculoFormGroup.value.kmActual;
		let kmProyectar: number = + this.calcularProyeccionVehiculoFormGroup.value.kmProyectar;

		if (this.ocultarCampoKM) {
			if (kmActual && kmProyectar) {
				this.calcularProyeccionVehiculoCliente(kmActual, kmProyectar);
			} else {
				swal('Advertencia!', 'Los campos debe contener un valor mayor a cero (0)', 'warning')
					.then(result => {
					}).catch(swal.noop);
			}
		} else {
			if (kmActual) {
				this.calcularProyeccionVehiculoCliente(kmActual, kmProyectar);
			} else {
				swal('Advertencia!', 'Los campos debe contener un valor mayor a cero (0)', 'warning')
					.then(result => {
					}).catch(swal.noop);
			}
		}
	}

	async calcularProyeccionVehiculoCliente(kmActual: number, kmProyectar: number) {
		this.loading = true;
		const responseCalcularProyecionVehiculo = await this._proyeccionService.calcularProyeccionVehiculos('' + this.vehiculoUsadoNuevo, this.idProyeccion, '' + kmActual, '' + kmProyectar);
		//console.log(responseCalcularProyecionVehiculo);
		if (responseCalcularProyecionVehiculo["status"] === "success" && responseCalcularProyecionVehiculo["code"] === "200") {
			this.loading = false;
			$("#modalCalcularProyeccionVehiculosCliente").modal("hide");
			this.calcularProyeccionVehiculoFormGroup.reset();
			this._sweetAlertService.showNotification('top', 'right', 'success', 'success', '<b>' + responseCalcularProyecionVehiculo['message'] + '<b>', 1000);
		}
		else if (responseCalcularProyecionVehiculo["status"] === "success" && responseCalcularProyecionVehiculo["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseCalcularProyecionVehiculo['message'] + '<b>', 1000);
		}
		else if (responseCalcularProyecionVehiculo["status"] === "error" && responseCalcularProyecionVehiculo["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseCalcularProyecionVehiculo['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'error', 'warning', '<b>' + responseCalcularProyecionVehiculo['message'] + '<b>', 1000);
		}
	}

	camposEdicionRol() {
		let permisosEditarCamposKm: string[] = ['A'];
		if (!permisosEditarCamposKm.includes(this.rolUsuario)) {
			this.validarCamposKilometrajeProyectar();
		}
	}

	validarCamposKilometrajeProyectar() {
		let arrayCamposBloqueadas: string[] = ['kmActual', 'kmProyectar'];

		arrayCamposBloqueadas.forEach((element) => {
			if ((document.getElementById(element) as HTMLInputElement)) {
				if ((document.getElementById(element) as HTMLInputElement).value) (document.getElementById(element) as HTMLInputElement).disabled = true;
				else (document.getElementById(element) as HTMLInputElement).disabled = false;
			}
		});
	}
	/* Finaliza Proyeccion de kilometraje */

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) {
			result = e1 === e2;
		}
		return result;
	}

	regresar() {
		if (localStorage.getItem('rutaSC')) {
			let rutaSC = localStorage.getItem('rutaSC');
			localStorage.removeItem('rutaSC');
			this._router.navigate([rutaSC]);
		}
		else this._router.navigate(["siembra-y-cosecha/clientes"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}

}