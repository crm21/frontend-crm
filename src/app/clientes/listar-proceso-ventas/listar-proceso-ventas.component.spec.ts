import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarProcesoVentasComponent } from './listar-proceso-ventas.component';

describe('ListarProcesoVentasComponent', () => {
  let component: ListarProcesoVentasComponent;
  let fixture: ComponentFixture<ListarProcesoVentasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarProcesoVentasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarProcesoVentasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
