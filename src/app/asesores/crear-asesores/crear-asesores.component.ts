import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import { Asesor } from "../../models/asesor";
import swal from 'sweetalert2';
import { AsesorService } from '../../services/asesor/asesor.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare const $: any;

@Component({
	selector: 'app-crear-asesores',
	templateUrl: './crear-asesores.component.html',
	styleUrls: ['./crear-asesores.component.css']
})
export class CrearAsesoresComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	datosAsesorForm: FormGroup;
	tipoDocumentos: any;
	tipoDocumentosArray: any[] = [];
	rolesExistentes: any;
	correoEmpresa: string = '';
	telefonoEmpresa: string = '';

	constructor(
		private _router: Router,
		private _asesorService: AsesorService,
		private _parametrizacionService: ParametrizacionService,
		private _sweetAlertService: SweetAlertService
	) { }

	ngOnInit() {
		this.datosAsesorForm = new FormGroup({
			tipoDoc: new FormControl(null, Validators.required),
			documento: new FormControl(null, [Validators.required, Validators.pattern("[0-9]{5,13}$")]),
			nombres: new FormControl(null, Validators.required),
			apellidos: new FormControl(null, Validators.required),
			correo: new FormControl(null, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")])
		});
		if (localStorage.getItem('identity_crm')) {
			let informacionEmpresa: string[] = this._sweetAlertService.datosEmpresas(localStorage.getItem('business'));
			//console.log(informacionEmpresa);
			this.correoEmpresa = informacionEmpresa[0];
			this.telefonoEmpresa = informacionEmpresa[1];
			this.cargarInformacionSelect();
		}
	}

	async onSubmitCrearAsesor() {
		this.loading = true;

		let usuario = new Asesor(
			this.datosAsesorForm.value.tipoDoc,
			this.datosAsesorForm.value.documento,
			this.datosAsesorForm.value.nombres,
			this.datosAsesorForm.value.apellidos,
			this.datosAsesorForm.value.correo,
			'S',
			this.correoEmpresa,
			this.telefonoEmpresa
		); //console.log(usuario);

		const responseAsesor = await this._asesorService.registrarAsesor(usuario);
		//console.log(responseAsesor);
		if (responseAsesor["status"] === "success" && responseAsesor["code"] === "200") {
			this.loading = false;
			swal('Bien!', responseAsesor["message"], 'success')
				.then(result => {
					this.regresar();
				}).catch(swal.noop);
		}
		else if (responseAsesor["status"] === "success" && responseAsesor["code"] === "300") {
			this.loading = false;
			(document.getElementById('documento') as HTMLInputElement).focus();
			this._sweetAlertService.alertGeneral('Advertencia!', responseAsesor["message"], 'OK', 'warning');
		}
		else if (responseAsesor["status"] === "error" && responseAsesor["code"] === "100") {
			this.expiracionToken(responseAsesor['message']);
		}
		else {
			this.loading = false;
			(document.getElementById('documento') as HTMLInputElement).focus();
			this._sweetAlertService.alertGeneral('¡Error!', responseAsesor["message"], 'OK', 'error');
		}
	}

	async cargarInformacionSelect() {
		this.loading = true;
		this.tipoDocumentos = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo Documento');

		this.tipoDocumentos.data.forEach((element) => {
			this.tipoDocumentosArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
		this.loading = false;
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	regresar() {
		this._router.navigate(["asesores/listar"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}
