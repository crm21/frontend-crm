import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardErpComponent } from './dashboard-erp.component';

describe('DashboardErpComponent', () => {
  let component: DashboardErpComponent;
  let fixture: ComponentFixture<DashboardErpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardErpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardErpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
