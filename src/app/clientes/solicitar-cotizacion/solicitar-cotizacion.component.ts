import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from "@angular/router";
import { Cotizacion } from "../../models/cotizacion";
import swal from "sweetalert2";
import { ClienteService } from '../../services/cliente/cliente.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare var $: any;

@Component({
	selector: 'app-solicitar-cotizacion',
	templateUrl: './solicitar-cotizacion.component.html',
	styleUrls: ['./solicitar-cotizacion.component.css']
})
export class SolicitarCotizacionComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	ingresoConcesionario: any;
	ingresoFinalizado: any = 0;
	datosCotizacionForm: FormGroup;
	tipoVehiculoArray: any[] = [];
	metodoPagoArray: any[] = [];
	versionVehiculoArray: any[] = [];
	colorVehiculoArray: any[] = [];
	modeloArray: any[] = [];
	cuotaInicialArray: any[] = [];
	estadosNegocioArray: any[] = [];

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _formBuilder: FormBuilder,
		private _clienteService: ClienteService,
		private _parametrizacionService: ParametrizacionService,
		private _sweetAlertService: SweetAlertService
	) {
		this.ingresoConcesionario = this._route.snapshot.paramMap.get('idIngresoConcesionario');
		this.ingresoFinalizado = this._route.snapshot.paramMap.get('ingresoFinalizado');
	}

	ngOnInit() {
		this.datosCotizacionForm = this._formBuilder.group({
			numCotizacion: [''],
			tipoVehiculo: ['', Validators.required],
			modelo: ['', Validators.required],
			cuotaInicial: ['', Validators.required],
			metodoPago: ['', Validators.required],
			versionId: ['', Validators.required],
			colorVehiculo: ['', Validators.required],
			valorVehiculo: ['', Validators.required],
			ultimoDigito: ['', [Validators.pattern("^[0-9-]{1,6}$")]],
			lugarMatricula: ['']
		});
		if (localStorage.getItem('identity_crm')) {
			this.cargarInformacionSelect();
		}
	}

	async onSubmitSolicitarCotizacion() {
		this.loading = true;

		let cotizacion = new Cotizacion(
			this.datosCotizacionForm.value.numCotizacion,
			this.datosCotizacionForm.value.tipoVehiculo,
			this.datosCotizacionForm.value.modelo,
			this.datosCotizacionForm.value.cuotaInicial,
			this.datosCotizacionForm.value.metodoPago,
			this.ingresoConcesionario,
			this.datosCotizacionForm.value.versionId,
			this.datosCotizacionForm.value.colorVehiculo,
			this.formatoValorEntero(this.datosCotizacionForm.value.valorVehiculo),
			'' + this.datosCotizacionForm.value.ultimoDigito,
			this.datosCotizacionForm.value.lugarMatricula,
			this.datosCotizacionForm.value.metodoPago == 'C' ? 'EP' : 'EE'
		); //console.log(cotizacion);

		const responseSolicitudCotizacion = await this._clienteService.registrarCotizacion(1, cotizacion);
		//console.log(responseSolicitudCotizacion);
		if (responseSolicitudCotizacion["status"] === "success" && responseSolicitudCotizacion["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseSolicitudCotizacion["message"], 'success')
				.then(result => {
					this.datosCotizacionForm.reset();
					this._router.navigate(["clientes/listar-cotizaciones/", this.ingresoConcesionario, this.ingresoFinalizado]);
				}).catch(swal.noop);
		}
		else if (responseSolicitudCotizacion["status"] === "success" && responseSolicitudCotizacion["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseSolicitudCotizacion["message"], 'OK', 'warning');
		}
		else if (responseSolicitudCotizacion["status"] === "error" && responseSolicitudCotizacion["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseSolicitudCotizacion['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseSolicitudCotizacion["message"], 'OK', 'error');
		}
	}

	async cargarInformacionSelect() {
		this.loading = true;
		
		let tipoVehiculoParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo Vehículo');
		let metodoPagoParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Método de Pago');
		let versionVehiculoParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Versión Vehículo');
		let colorVehiculoParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Color Vehículo');
		let estadoCotizacionParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Estados de Negocio');

		tipoVehiculoParametros['data'].forEach((element) => {
			this.tipoVehiculoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		metodoPagoParametros['data'].forEach((element) => {
			this.metodoPagoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		versionVehiculoParametros['data'].forEach((element) => {
			this.versionVehiculoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		colorVehiculoParametros['data'].forEach((element) => {
			this.colorVehiculoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		estadoCotizacionParametros['data'].forEach((element) => {
			this.estadosNegocioArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});


		const anioActual = new Date();
		const rangoAnioActual: number = + anioActual.getFullYear();

		for (var i = rangoAnioActual + 2; i >= 1990; --i) {
			this.modeloArray.push({
				valor: '' + i,
				nombre: '' + i
			});
		}

		for (var i = 1; i < 11; ++i) {

			let valor1;
			let valo2 = (0 + i) * 10;

			if (i == 1) valor1 = 0;
			else valor1 = ((10 * i) + 1) - 10;

			this.cuotaInicialArray.push({
				valor: valor1 + '% - ' + valo2 + '%',
				nombre: 'Entre ' + valor1 + '% - ' + valo2 + '%'
			});
		}
		this.loading = false;
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	formatoValorEntero(dato) {
		dato = dato.replace(/COP/gi, '');
		dato = dato.replace(/[-+()\s.]/g, '');
		dato = dato.replace(',00', '');
		return dato;
	}

	formatearMoneda(evento: any) {
		let numero: number = parseInt((document.getElementById('valorVehiculo') as HTMLInputElement).value);
		if (numero) {
			let formateado: any = new Intl.NumberFormat("COP", { style: "currency", currency: "COP" }).format(numero);
			(document.getElementById('valorVehiculo') as HTMLInputElement).value = formateado;
			//console.log(formateado);
		}
	}

	regresar() {
		this._router.navigate(["clientes/ficha-clientes/", this.ingresoConcesionario]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}