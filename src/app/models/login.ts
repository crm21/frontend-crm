export class Login {
    constructor(
        public usuario: string,
        public password: string,
        public empresas: string,
        public opc: string
    ) { }
};

export class LoginAnteriorNoUtilizar {
    constructor(
        public usuario: string,
        public password: string,
        public empresas: string,
        public sessionEmpresa: string,
        public opc: string
    ) { }
};