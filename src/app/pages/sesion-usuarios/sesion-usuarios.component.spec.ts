import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SesionUsuariosComponent } from './sesion-usuarios.component';

describe('SesionUsuariosComponent', () => {
  let component: SesionUsuariosComponent;
  let fixture: ComponentFixture<SesionUsuariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SesionUsuariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SesionUsuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
