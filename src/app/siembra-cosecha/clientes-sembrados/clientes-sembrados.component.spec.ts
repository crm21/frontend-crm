import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientesSembradosComponent } from './clientes-sembrados.component';

describe('ClientesSembradosComponent', () => {
  let component: ClientesSembradosComponent;
  let fixture: ComponentFixture<ClientesSembradosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientesSembradosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientesSembradosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
