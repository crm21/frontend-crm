import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { UsuarioService } from '../../services/usuario/usuario.service';
import { ClienteService } from '../../services/cliente/cliente.service';
import { TableroAsignacionService } from '../../services/tablero-asignacion/tablero-asignacion.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare var $: any;
declare interface DataTable {
	headerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-mis-clientes',
	templateUrl: './mis-clientes.component.html',
	styleUrls: ['./mis-clientes.component.css']
})
export class MisClientesComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	dataTable: DataTable;
	registros: any[] = [];
	todosAsesoresArray: any[] = [];
	canalIngresoArray: any[] = [];
	vehiculosArray: any[] = [];
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	eleccionFormulario: boolean = true;
	consultaVacia: boolean = false;

	usuario = {
		sub: null,
		rol: null
	};

	filtro = {
		asesor: '',
		canal: '',
		vehiculo: ''
	};
	textoExtendido: string = '';

	constructor(
		private _router: Router,
		private _usuariosService: UsuarioService,
		private _clienteService: ClienteService,
		private _tableroAsignacionService: TableroAsignacionService,
		private _parametrizacionService: ParametrizacionService,
		private _sweetAlertService: SweetAlertService
	) {
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			let identity = identityData['data'];
			this.usuario.sub = identity.sub;
			this.usuario.rol = identity.rol.valor;
			if (this.usuario.rol == 'S') {
				this.eleccionFormulario = false;
				this.filtro.asesor = this.usuario.sub;
				this.listarMisClientes(this.usuario.sub, '', '', 1);
				this.cargarDisponibilidadAsesores(1);
			}
			else {
				this.eleccionFormulario = true;
				this.listarMisClientes('', '', '', 1);
				this.cargarDisponibilidadAsesores(2);
			}
		}
	}

	ngOnInit() {
		this.dataTable = {
			headerRow: [
				'#',
				"Documento Cliente",
				"Nombres Completos",
				"Asesor",
				"Fecha de Ingreso",
				"Fecha de Cierre",
				"Canal de Ingreso",
				"Tarjeta de Opciones",
				"Observaciones",
				"Acciones"
			],
			dataRows: []
		};
	}

	onSubmitBuscarMisClientes() {
		let asesor, canal, vehiculo;

		if (this.usuario.rol == 'S') {
			asesor = this.usuario.sub;
			canal = this.filtro.canal;
			vehiculo = this.filtro.vehiculo;
			this.filtro.asesor = this.usuario.sub;
		}
		else {
			asesor = this.filtro.asesor;
			canal = this.filtro.canal;
			vehiculo = this.filtro.vehiculo;
		}

		/* console.log(`${asesor}, ${canal}, ${vehiculo}`); */
		this.listarMisClientes(asesor, canal, vehiculo, 1);
	}

	async listarMisClientes(idAsesor: any, canal: string, vehiculo: string, page: number) {
		this.loading = true;
		//console.log(`${idAsesor}, ${canal}, ${vehiculo}, ${page}`);
		const responseMisClientes = await this._clienteService.listarClientesPorFiltros(idAsesor, canal, vehiculo, page);
		//console.log(responseMisClientes);
		if (responseMisClientes['status'] == "success" && responseMisClientes['code'] == "200") {
			this.llenarDatosTabla(responseMisClientes['data'], page);
			this.loading = false;
		}
		else if (responseMisClientes['status'] == "success" && responseMisClientes['code'] == "300") {
			this.loading = false;
			this.dataTable.dataRows = [];
			this.total_item_count = 0;
			this.page_prev = 1;
			this.page_next = 1;
			this.page_actual = 1;
			this.total_pages = [];
			this.consultaVacia = true; // Ocultar spinner de carga de datos
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseMisClientes['message'] + '<b>', 2000);
		}
		else if (responseMisClientes["status"] === "error" && responseMisClientes["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseMisClientes['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseMisClientes['message'], 'error');
		}
	}

	async cargarDisponibilidadAsesores(opc: number) {
		if (opc == 1) {
			this.cargarInformacionSelect(opc, '');
		} else {
			this.loading = true;
			const responseDisponibilidadAsesores = await this._tableroAsignacionService.disponibilidadAsesores();
			//console.log(responseDisponibilidadAsesores);
			if (responseDisponibilidadAsesores["status"] === "success" && responseDisponibilidadAsesores["code"] === "200") {
				this.loading = false;
				this.cargarInformacionSelect(opc, responseDisponibilidadAsesores['data']);
			}
			else if (responseDisponibilidadAsesores["status"] === "success" && responseDisponibilidadAsesores["code"] === "300") {
				this.loading = false;
				this._sweetAlertService.alertGeneral('Advertencia!', responseDisponibilidadAsesores["message"], 'OK', 'warning');
			}
			else if (responseDisponibilidadAsesores["status"] === "error" && responseDisponibilidadAsesores["code"] === "100") {
				this.loading = false;
				if (this.validarToken) this.expiracionToken(responseDisponibilidadAsesores['message']);
			}
			else {
				this.loading = false;
				this._sweetAlertService.alertGeneral('¡Error!', responseDisponibilidadAsesores["message"], 'OK', 'error');
			}
		}
	}

	llenarDatosTabla(responseMisClientesDatos: any, page: number) {
		//console.log(responseMisClientesDatos);
		this.dataTable.dataRows = [];
		this.registros = responseMisClientesDatos.clientes;

		this.total_item_count = responseMisClientesDatos['total_item_count'];
		this.page_actual = page;

		this.total_pages = responseMisClientesDatos['total_pages'];

		if (page >= 2) this.page_prev = page - 1;
		else this.page_prev = 1;

		if (page < responseMisClientesDatos['total_pages']) this.page_next = page + 1;
		else {
			if (responseMisClientesDatos['total_pages'] == 0) this.page_next = 1;
			else this.page_next = responseMisClientesDatos['total_pages'];
		}

		let jsonRegistro;

		this.registros.forEach(element => {
			jsonRegistro = {
				id: element.informacionCliente.id,
				tipoDoc: element.informacionCliente.tipoDocumento.valor,
				documento: element.informacionCliente.documento,
				cliente: element.informacionCliente.nombres + ' ' + element.informacionCliente.apellidos,
				asesor: element.informacionIngreso.asesor.nombres + ' ' + element.informacionIngreso.asesor.apellidos,
				fechaIngreso: element.informacionIngreso.fechaIngreso,
				fechaSalida: element.informacionIngreso.fechaSalida,
				canalIngreso: element.informacionIngreso.canalIngreso.nombre,
				tarjetaOpciones: element.informacionIngreso.tarjetaOpciones.nombre,
				observacion: element.informacionIngreso.observacion
			};
			this.dataTable.dataRows.push(jsonRegistro);
		});
		if (this.total_item_count || this.total_item_count == 0) this.consultaVacia = true; // Ocultar spinner de carga de datos
	}

	async cargarInformacionSelect(opc: number, asesores: any) {
		this.loading = true;
		if (opc == 2) {
			let disponibilidadAsesorArray: any[] = [];
			this.todosAsesoresArray = [];

			if (asesores.asesoresDisponibles.length > 0) {
				disponibilidadAsesorArray = asesores.asesoresDisponibles;
			}
			if (asesores.asesoresOcupados.length >= 0) {
				this.todosAsesoresArray = disponibilidadAsesorArray.concat(asesores.asesoresOcupados);
			}
		}

		const canalIngresos: any = await this._parametrizacionService.listarCategoriaParametrizacion('Canal de Ingreso');
		const vehiculos: any = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo Vehículo');

		canalIngresos['data'].forEach((element) => {
			this.canalIngresoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		vehiculos['data'].forEach((element) => {
			this.vehiculosArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
		this.loading = false;
	}

	nuevaBusqueda() {
		this.filtro.canal = '';
		this.filtro.vehiculo = '';
		if (this.usuario.rol == 'S') {
			this.filtro.asesor = this.usuario.sub;
			this.listarMisClientes(this.usuario.sub, '', '', 1);
		}
		else {
			this.filtro.asesor = '';
			this.listarMisClientes('', '', '', 1);
		}
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	observarTexto(texto: any) {
		this.textoExtendido = texto;
		$("#modalObservacion").modal("show");
	}

	buscarIdCliente(rowId: any) {
		localStorage.setItem('ruta', 'clientes/listar-clientes');
		this._router.navigate(["clientes/ingresos/", rowId]);
	}

	buscarCliente() {
		this._router.navigate(["clientes/buscar"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}