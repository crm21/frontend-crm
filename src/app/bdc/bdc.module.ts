import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BdcRoutes } from './bdc.routing';
import { LoadingModule } from '../shared/loading/loading.module';
import { LoadingSpinnerModule } from '../shared/loading-spinner/loading-spinner.module';
import { ReporteSeguimientoComponent } from './reporte-seguimiento/reporte-seguimiento.component';

@NgModule({
	declarations: [ReporteSeguimientoComponent],
	imports: [
		CommonModule,
		RouterModule.forChild(BdcRoutes),
		FormsModule,
		MaterialModule,
		ReactiveFormsModule,
		LoadingModule,
		LoadingSpinnerModule
	],
	providers: [DatePipe]
})
export class BdcModule { }
