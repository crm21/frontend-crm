import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { EstadisticasService } from '../../services/estadisticas/estadisticas.service';

declare var $: any;
declare interface TableData {
	headerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-reporte-procesos-venta',
	templateUrl: './reporte-procesos-venta.component.html',
	styleUrls: ['./reporte-procesos-venta.component.css']
})
export class ReporteProcesosVentaComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	identityUsuario: any;
	idUsuario: any;
	dataTable: TableData;
	reporteEstadisticaArray: any[] = [];
	total_item_count: number = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	consultaVaciaCargarSpinner: boolean = false;
	camposProcesoVentasChevy: boolean = false;
	procesoVentasDetalles = {
		id: null,
		cotizacionNum: null,
		idPersona: null,
		tipoDocPersona: null,
		documentoPersona: null,
		nombresPersona: null,
		apellidosPersona: null,
		asesor: null,
		fechaInicioProceso: null,
		fechaFacturacion: null,
		fechaEnvioMatricula: null,
		fechaRecepcionMatricula: null,
		fechaSoat: null,
		fechaActivacionChevy: null,
		fechaEntrega: null,
		fechaFirmaContrato: null,
		fechaConsignacion: null,
		precioFinalVenta: null,
		placa: null,
		vin: null,
		kilometraje: null,
		observacion: null
	};

	constructor(
		private _router: Router,
		private _sweetAlertService: SweetAlertService,
		private _estadisticasService: EstadisticasService
	) {
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			this.identityUsuario = identityData['data'];
			this.idUsuario = this.identityUsuario.sub;
			this.listarEstadisticas(1);
		}
		this.datosTabla();
	}

	ngOnInit() { }

	datosTabla() {
		this.dataTable = {
			headerRow: [
				"#",
				"Documento Cliente",
				"Nombres Completos",
				"Fecha Inicio del Proceso",
				"Precio Final",
				"Asesor",
				"Mas Detalles",
				"Acciones"
			],
			dataRows: []
		};
	}

	async listarEstadisticas(page: any) {
		this.loading = true;
		const responseEstadisticas = await this._estadisticasService.estadisticasDashboardId(13, this.idUsuario, page, '', '');
		//console.log(responseEstadisticas);
		if (responseEstadisticas["status"] === "success" && responseEstadisticas["code"] === "200") {
			this.loading = false;
			this.llenarDatosTabla(page, responseEstadisticas['data']);
		}
		else if (responseEstadisticas["status"] === "success" && responseEstadisticas["code"] === "300") {
			this.loading = false;
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseEstadisticas['message'] + '<b>', 2000);
		}
		else if (responseEstadisticas["status"] === "error" && responseEstadisticas["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseEstadisticas['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			this._sweetAlertService.showNotification('top', 'right', 'error', 'danger', '<b>' + responseEstadisticas['message'] + '<b>', 2000);
		}
	}

	llenarDatosTabla(page: any, responseEstadisticas: any) {
		this.reporteEstadisticaArray = responseEstadisticas['procesos'];
		this.dataTable.dataRows = [];
		this.total_item_count = responseEstadisticas['total_item_count'];
		this.page_actual = page;

		this.total_pages = responseEstadisticas['total_pages'];

		if (page >= 2) this.page_prev = page - 1;
		else this.page_prev = 1;

		if (page < responseEstadisticas['total_pages']) this.page_next = page + 1;
		else {
			if (responseEstadisticas['total_pages'] == 0) this.page_next = 1;
			else this.page_next = responseEstadisticas['total_pages'];
		}

		let JSONParam;

		this.reporteEstadisticaArray.forEach(element => {
			JSONParam = {
				id: element.id,
				cotizacionId: element.proceso.cotizacionId.id,
				cotizacionNum: element.proceso.cotizacionId.numCotizacion,
				idIngreso: element.proceso.cotizacionId.ingresoConcesionario.id,
				idPersona: element.proceso.cotizacionId.ingresoConcesionario.persona.id,
				tipoDocPersona: element.proceso.cotizacionId.ingresoConcesionario.persona.tipoDocumento.valor,
				documentoPersona: element.proceso.cotizacionId.ingresoConcesionario.persona.documento,
				nombresPersona: element.proceso.cotizacionId.ingresoConcesionario.persona.nombres,
				apellidosPersona: element.proceso.cotizacionId.ingresoConcesionario.persona.apellidos,
				negocioAbierto: element.proceso.cotizacionId.ingresoConcesionario.fechaSalida == '0000-00-00' ? true : false,
				asesor: element.proceso.cotizacionId.ingresoConcesionario.asesor.nombres + ' ' + element.proceso.cotizacionId.ingresoConcesionario.asesor.apellidos,
				fechaInicioProceso: element.proceso.fechaInicioProceso,
				fechaFacturacion: element.proceso.fechaFacturacion == null ? '- - -' : element.proceso.fechaFacturacion,
				fechaEnvioMatricula: element.proceso.fechaEnvioMatricula == null ? '- - -' : element.proceso.fechaEnvioMatricula,
				fechaRecepcionMatricula: element.proceso.fechaRecepcionMatricula == null ? '- - -' : element.proceso.fechaRecepcionMatricula,
				fechaSoat: element.proceso.fechaSoat == null ? '- - -' : element.proceso.fechaSoat,
				fechaActivacionChevy: element.proceso.fechaActivacionChevyOnStar == null ? '- - -' : element.proceso.fechaActivacionChevyOnStar,
				fechaEntrega: element.proceso.fechaEntrega == null ? '- - -' : element.proceso.fechaEntrega,
				fechaFirmaContrato: element.proceso.fechaFirmaContrato == null ? '- - -' : element.proceso.fechaFirmaContrato,
				fechaConsignacion: element.proceso.fechaConsignacion == null ? '- - -' : element.proceso.fechaConsignacion,
				precioFinalVenta: element.proceso.precioFinalVenta == null ? '- - -' : element.proceso.precioFinalVenta,
				placa: element.proceso.placa == '' ? '- - -' : element.proceso.placa,
				vin: element.proceso.vin == '' ? '- - -' : element.proceso.vin,
				kilometraje: element.proceso.kilometrajeInicial,
				observacion: element.proceso.observacion == null ? '- - -' : element.proceso.observacion
			};
			this.dataTable.dataRows.push(JSONParam);
		});

		if (this.total_item_count || this.total_item_count == 0) {
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
		}
	}

	verProcesoVentas(rowProcesoVentas: any) {
		let letraEmpresa = localStorage.getItem('business');
		switch (letraEmpresa) {
			case 'A':
				this.camposProcesoVentasChevy = true;
				break;
			case 'M':
				this.camposProcesoVentasChevy = false;
				break;
			case 'T':
				this.camposProcesoVentasChevy = false;
				break;
			default:
				this.camposProcesoVentasChevy = false;
				break;
		}

		this.procesoVentasDetalles.id = rowProcesoVentas.id;
		this.procesoVentasDetalles.cotizacionNum = rowProcesoVentas.cotizacionNum;
		this.procesoVentasDetalles.idPersona = rowProcesoVentas.idPersona;
		this.procesoVentasDetalles.tipoDocPersona = rowProcesoVentas.tipoDocPersona;
		this.procesoVentasDetalles.documentoPersona = rowProcesoVentas.documentoPersona;
		this.procesoVentasDetalles.nombresPersona = rowProcesoVentas.nombresPersona;
		this.procesoVentasDetalles.apellidosPersona = rowProcesoVentas.apellidosPersona;
		this.procesoVentasDetalles.asesor = rowProcesoVentas.asesor;
		this.procesoVentasDetalles.fechaInicioProceso = rowProcesoVentas.fechaInicioProceso;
		this.procesoVentasDetalles.fechaFacturacion = rowProcesoVentas.fechaFacturacion;
		this.procesoVentasDetalles.fechaEnvioMatricula = rowProcesoVentas.fechaEnvioMatricula;
		this.procesoVentasDetalles.fechaRecepcionMatricula = rowProcesoVentas.fechaRecepcionMatricula;
		this.procesoVentasDetalles.fechaSoat = rowProcesoVentas.fechaSoat;
		this.procesoVentasDetalles.fechaActivacionChevy = rowProcesoVentas.fechaActivacionChevy;
		this.procesoVentasDetalles.fechaEntrega = rowProcesoVentas.fechaEntrega;
		this.procesoVentasDetalles.fechaFirmaContrato = rowProcesoVentas.fechaFirmaContrato;
		this.procesoVentasDetalles.fechaConsignacion = rowProcesoVentas.fechaConsignacion;
		this.procesoVentasDetalles.precioFinalVenta = rowProcesoVentas.precioFinalVenta;
		this.procesoVentasDetalles.placa = rowProcesoVentas.placa;
		this.procesoVentasDetalles.vin = rowProcesoVentas.vin;
		this.procesoVentasDetalles.kilometraje = rowProcesoVentas.kilometraje;
		this.procesoVentasDetalles.observacion = rowProcesoVentas.observacion;
		$("#modalInformacionDetalladaPV").modal("show");
	}

	buscarIdCliente(rowId: any) {
		localStorage.setItem('rutaSN', 'estadisticas/procesos-de-venta');
		this._router.navigate(["clientes/ficha-clientes/", rowId]);
	}

	regresar() {
		this._router.navigate(["dashboard"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}