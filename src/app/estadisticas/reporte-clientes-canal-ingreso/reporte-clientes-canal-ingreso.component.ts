import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { EstadisticasService } from '../../services/estadisticas/estadisticas.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { DatePipe } from "@angular/common";

declare var $: any;
declare interface TableData {
	headerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-reporte-clientes-canal-ingreso',
	templateUrl: './reporte-clientes-canal-ingreso.component.html',
	styleUrls: ['./reporte-clientes-canal-ingreso.component.css']
})
export class ReporteClientesCanalIngresoComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	identityUsuario: any;
	idUsuario: any;
	dataTable: TableData;
	buscarFiltrosForm: FormGroup;
	reporteEstadisticaArray: any[] = [];
	tipoCanalIngresoArray: any[] = [];
	total_item_count: number = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	consultaVaciaCargarSpinner: boolean = false;
	downloadMostrar: boolean = false;
	fechaIni: string = '';
	fechaFin: string = '';
	nombreArchivo: string = '';
	buscarCI = {
		canalIngreso: ''
	}

	constructor(
		private _router: Router,
		private _sweetAlertService: SweetAlertService,
		private _estadisticasService: EstadisticasService,
		private _parametrizacionService: ParametrizacionService,
		private _datepipe: DatePipe,
		private _formBuilder: FormBuilder,
	) {
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			this.identityUsuario = identityData['data'];
			this.idUsuario = this.identityUsuario.sub;
		}
		this.datosTabla();
	}

	ngOnInit() {
		if (localStorage.getItem('identity_crm')) {
			this.cargarInformacionSelect();
			this.listarEstadisticas(1);
		}
	 }

	datosTabla() {
		this.dataTable = {
			headerRow: [
				"#",
				"Documento Cliente",
				"Nombres Completos",
				"Fecha de Registro",
				"Canal de Ingreso",
				"Tipo de Entrada",
				"Registrado Por",
				"Acciones"
			],
			dataRows: []
		};
		this.buscarFiltrosForm = this._formBuilder.group({
			fechaInicial: ['', Validators.required],
			fechaFinal: ['', Validators.required]
		});
	}

	async cargarInformacionSelect() {
		this.loading = true;

		let tipoCanalIngresoParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Canal de Ingreso');
		
		tipoCanalIngresoParametros['data'].forEach((element) => {
			this.tipoCanalIngresoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		this.loading = false;
	}

	onSubmitBuscarReporte() {
		let _fechaIni: string, _fechaFin: string, asesor: string, financiera: string;
		let comprobarFecha: boolean = true;
		_fechaIni = this.buscarFiltrosForm.value.fechaInicial;
		_fechaFin = this.buscarFiltrosForm.value.fechaFinal;

		if (!_fechaIni || _fechaIni == '' || !_fechaFin || _fechaFin == '') {
			this.fechaIni = '';
			this.fechaFin = '';
		} else {
			this.fechaIni = this.fechasValidacion(this.buscarFiltrosForm.value.fechaInicial);
			this.fechaFin = this.fechasValidacion(this.buscarFiltrosForm.value.fechaFinal);
			if (this.validarFechasSuperior(this.fechaIni, this.fechaFin)) comprobarFecha = true;
			else comprobarFecha = false;
		}

		if (comprobarFecha) {
			//console.log(`${this.buscarCI.canalIngreso}, ${this.fechaIni}, ${this.fechaFin}`);
			this.listarEstadisticas(1);
		} else {
			//console.log('La fecha inicial debe ser inferior a la fecha final.');
			//console.log(`${this.buscarCI.canalIngreso}, ${this.fechaIni}, ${this.fechaFin}`);
			this._sweetAlertService.showNotification('top', 'right', 'danger', 'warning', '<b>' + 'La fecha inicial debe ser inferior a la fecha final.' + '<b>', 2000);
		}
	}

	async listarEstadisticas(page: any) {
		this.loading = true;
		const responseEstadisticas = await this._estadisticasService.reporteClientesLeadsEstadisticas(5, '' + page, this.fechaIni, this.fechaFin, this.buscarCI.canalIngreso);
		//console.log(responseEstadisticas);
		if (responseEstadisticas["status"] === "success" && responseEstadisticas["code"] === "200") {
			this.loading = false;
			this.downloadMostrar = false;
			//this.nombreArchivo = responseEstadisticas['data'].fechaEstadistica;
			this.llenarDatosTabla(page, responseEstadisticas['data']);
		}
		else if (responseEstadisticas["status"] === "success" && responseEstadisticas["code"] === "300") {
			this.loading = false;
			this.downloadMostrar = false;
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseEstadisticas['message'] + '<b>', 2000);
		}
		else if (responseEstadisticas["status"] === "error" && responseEstadisticas["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseEstadisticas['message']);
		}
		else {
			this.loading = false;
			this.downloadMostrar = false;
			this.dataTable.dataRows = [];
			this._sweetAlertService.showNotification('top', 'right', 'error', 'danger', '<b>' + responseEstadisticas['message'] + '<b>', 2000);
		}
	}

	llenarDatosTabla(page: any, responseEstadisticas: any) {
		this.reporteEstadisticaArray = responseEstadisticas['leads'];
		this.dataTable.dataRows = [];
		this.total_item_count = responseEstadisticas['total_item_count'];
		this.page_actual = page;

		this.total_pages = responseEstadisticas['total_pages'];

		if (page >= 2) this.page_prev = page - 1;
		else this.page_prev = 1;

		if (page < responseEstadisticas['total_pages']) this.page_next = page + 1;
		else {
			if (responseEstadisticas['total_pages'] == 0) this.page_next = 1;
			else this.page_next = responseEstadisticas['total_pages'];
		}

		let JSONParam;

		this.reporteEstadisticaArray.forEach(element => {
			JSONParam = {
				id: element.cliente.id,
				tipoDoc: element.cliente.tipoDocumento.valor,
				documento: element.cliente.documento,
				nombres: element.cliente.nombres,
				apellidos: element.cliente.apellidos,
				fechaRegistro: element.fechaRegistro,
				canalIngreso: element.canalIngreso.nombre,
				tipoEntrada: element.tipoEntrada.nombre,
				registradoPor: element.registradoPor.nombres + ' ' + element.registradoPor.apellidos,
			};
			this.dataTable.dataRows.push(JSONParam);
		});
		//console.log(this.usuarioArray);
		if (this.total_item_count || this.total_item_count == 0) {
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
		}
	}

	descargarArchivoEstadisticasAlert() {
		this._sweetAlertService.descargarEstadisticas('estadistica' + this.nombreArchivo, '_dia_', this.identityUsuario.documento);
	}

	async descargarArchivoEstadisticas() {
		this.loading = true;
		const responseFileDown = await this._estadisticasService.downloadFilesEstadisticas();
		//console.log(responseFileDown);
		if (responseFileDown["status"] === "success" && responseFileDown["code"] === "200") {
			this.loading = false;
		}
		else if (responseFileDown["status"] === "success" && responseFileDown["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseFileDown['message'] + '<b>', 2000);
		}
		else if (responseFileDown["status"] === "error" && responseFileDown["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseFileDown['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'error', 'danger', '<b>' + responseFileDown['message'] + '<b>', 2000);
		}
	}

	buscarIdCliente(rowId: any) {
		localStorage.setItem('ruta', 'estadisticas/leads-clientes-canal-de-ingreso');
		this._router.navigate(["clientes/ingresos/", rowId]);
	}

	fechasValidacion(formatoFecha: any) {
		if (formatoFecha) {
			//Formatear fecha
			const fechaActualTem = this._datepipe
				.transform(formatoFecha, "yyyy/MM/dd")
				.replace(/\//g, "-");
			//console.log("fecha formato: "+fechaActualTem);
			return fechaActualTem;
		}
		return '';
	}

	validarFechasSuperior(fechaStart: any, fechaEnd: any) {
		const fechaStart1 = new Date(fechaStart);
		const fechaEnd1 = new Date(fechaEnd);

		const diaInicio = fechaStart1.getDate();
		const mesInicio = fechaStart1.getMonth() + 1;
		const anioInicio = fechaStart1.getFullYear();
		const fechaStart1Tem = diaInicio + "-" + mesInicio + "-" + anioInicio;

		const diaFin = fechaEnd1.getUTCDate();
		const mesFin = fechaEnd1.getUTCMonth() + 1;
		const anioFin = fechaEnd1.getUTCFullYear();
		const fechaEnd1Tem = diaFin + "-" + mesFin + "-" + anioFin;

		//console.log("fechaInicio: "+fechaStart1Tem);
		//console.log("fechaFinal: "+fechaEnd1Tem);

		if (!(anioFin > anioInicio)) {
			if (anioFin < anioInicio) return false;

			if (!(mesFin > mesInicio)) {
				if (mesFin < mesInicio) return false;

				if (!(diaFin > diaInicio)) return false;
				return true;
			}
			return true;
		}
		return true;
	}

	limpiarFiltros() {
		this.buscarFiltrosForm.reset();
		this.buscarFiltrosForm.value.fechaInicial = null;
		this.buscarFiltrosForm.value.fechaFinal = null;
		this.fechaIni = '';
		this.fechaFin = '';
		this.listarEstadisticas(1);
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	regresar() {
		this._router.navigate(["dashboard"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}