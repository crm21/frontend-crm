import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DasboardContactCenterComponent } from './dasboard-contact-center.component';

describe('DasboardContactCenterComponent', () => {
  let component: DasboardContactCenterComponent;
  let fixture: ComponentFixture<DasboardContactCenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DasboardContactCenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DasboardContactCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
