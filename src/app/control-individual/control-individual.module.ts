import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFileUploaderModule } from "angular-file-uploader";

import { ControlIndividualRoutes } from './control-individual.routing';
import { LoadingModule } from '../shared/loading/loading.module';
import { LoadingSpinnerModule } from '../shared/loading-spinner/loading-spinner.module';
import { ListarCargosComponent } from './listar-cargos/listar-cargos.component';
import { RegistrarContratosComponent } from './registrar-contratos/registrar-contratos.component';
import { ListarAfiliacionesComponent } from './listar-afiliaciones/listar-afiliaciones.component';
import { ListarFuncionesComponent } from './listar-funciones/listar-funciones.component';
import { ControlIndividualComponent } from './control-individual/control-individual.component';
import { ContratosEmpleadoComponent } from './contratos-empleado/contratos-empleado.component';

@NgModule({
  declarations: [
    ListarCargosComponent,
    RegistrarContratosComponent,
    ListarAfiliacionesComponent,
    ListarFuncionesComponent,
    ControlIndividualComponent,
    ContratosEmpleadoComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(ControlIndividualRoutes),
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    LoadingModule,
    LoadingSpinnerModule,
    AngularFileUploaderModule
  ],
  providers: [DatePipe]
})
export class ControlIndividualModule { }
