export class Cotizacion {
    constructor(
        public numeroCotizacion: string,
        public vehiculo: string,
        public modelo: string,
        public cuotaInicial: string,
        public metodoPago: string,
        public ingresoConcesionario: number,
        public version: string,
        public color: string,
        public valorVehiculo: string,
        public ultimoDigito: string,
        public lugarMatricula: string,
        public estado: string
    ) { }
}

export class ModificarCotizacion {
    constructor(
        public numeroCotizacion: string,
        public vehiculo: string,
        public modelo: string,
        public cuotaInicial: string,
        public metodoPago: string,
        public version: string,
        public color: string,
        public valorVehiculo: string,
        public ultimoDigito: string,
        public lugarMatricula: string,
        public estado: string,
        public ingresoConcesionario: number
    ) { }
}