import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteClientesSinCotizacionesComponent } from './reporte-clientes-sin-cotizaciones.component';

describe('ReporteClientesSinCotizacionesComponent', () => {
  let component: ReporteClientesSinCotizacionesComponent;
  let fixture: ComponentFixture<ReporteClientesSinCotizacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteClientesSinCotizacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteClientesSinCotizacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
