import { Routes } from '@angular/router';

import { ReporteSeguimientoComponent } from './reporte-seguimiento/reporte-seguimiento.component';

export const BdcRoutes: Routes = [
	{
		path: '',
		redirectTo: 'reporte',
		pathMatch: 'full',
	},
	{
		path: '',
		children: [
			{
				path: 'reporte',
				component: ReporteSeguimientoComponent
			}, {
				path: 'seguimiento',
				component: ReporteSeguimientoComponent
			}
		]
	}
];