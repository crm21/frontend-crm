import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { Parametrizacion } from '../../models/parametrizacion';

declare var $: any;
declare interface DataTable {
	headerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-listar-parametros',
	templateUrl: './listar-parametros.component.html',
	styleUrls: ['./listar-parametros.component.css']
})
export class ListarParametrosComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	consultaVaciaCargarSpinner: boolean = false;
	dataTable: DataTable;
	parametrosArray: any[] = [];
	categoriasArray: string[] = [];
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	permisoRol: string = '';
	categoriasRol = ['Hobby', 'Profesión', 'Colores'];
	busquedaCategorias = {
		categoria: null,
		nombre: null
	};
	estadoPaginador: string = 'filtroTotal';
	parametros: Parametrizacion;
	idParametrizacion: string = '';

	constructor(
		private _router: Router,
		private _parametrizacionService: ParametrizacionService,
		private _sweetAlertService: SweetAlertService
	) {
		this.parametros = new Parametrizacion('', '', '', '');
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);
			let permiso = identityData['data'];

			this.permisoRol = permiso.rol.valor;
		}
	}

	ngOnInit() {
		this.dataTable = {
			headerRow: [
				'#',
				'Nombre Campo',
				'Valor',
				'Categoría',
				'Descripción',
				'Acciones'
			],
			dataRows: []
		};
		if (localStorage.getItem('identity_crm')) {
			this.listarDatosParametrizacion(1);
			this.cargarCategoriasNombresSelect();
		}
	}

	/* Inicia Listar parametrizacion */
	async listarDatosParametrizacion(page: any) {
		this.loading = true;

		const responseParametrizacion = await this._parametrizacionService.listarParametrizacion('' + page);
		//console.log(responseParametrizacion);
		if (responseParametrizacion['status'] == "success" && responseParametrizacion['code'] == "200") {
			this.loading = false;
			this.llenarTabla(responseParametrizacion['data'], page);
		}
		else if (responseParametrizacion['status'] == "success" && responseParametrizacion['code'] == "300") {
			this.loading = false;
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			swal('Advertencia!', responseParametrizacion['message'], 'warning');
		}
		else if (responseParametrizacion["status"] === "error" && responseParametrizacion["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseParametrizacion['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			swal('¡Error!', responseParametrizacion['message'], 'error');
		}
	}

	llenarTabla(responseParametrizacion: any, page: any) {
		this.parametrosArray = responseParametrizacion['parametrizaciones'];
		this.dataTable.dataRows = [];
		this.total_item_count = responseParametrizacion['total_item_count'];
		this.page_actual = page;

		this.total_pages = responseParametrizacion['total_pages'];

		if (page >= 2) { this.page_prev = page - 1; }
		else { this.page_prev = 1; }

		if (page < responseParametrizacion['total_pages']) { this.page_next = page + 1; }
		else {
			if (responseParametrizacion['total_pages'] == 0) { this.page_next = 1; }
			else { this.page_next = responseParametrizacion['total_pages']; }
		}

		let JSONParam;

		this.parametrosArray.forEach(element => {
			JSONParam = {
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion == '' || element.descripcion == null ? 'N/A' : element.descripcion,
				descripcionTemp: element.descripcion,
				editable: this.validacionEditableParametrizacion(element.categoria)
			};
			this.dataTable.dataRows.push(JSONParam);
		});
		if (this.total_item_count || this.total_item_count == 0) {
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
		}
		//console.log(this.dataTable.dataRows);
	}
	/* Finaliza Listar parametrizacion */

	/* Inicia Listar solo nombres de categorias de parametrizaciones */
	async cargarCategoriasNombresSelect() {
		this.loading = true;

		const responseCategoriasParametrizacion = await this._parametrizacionService.listarParametrizacionCategoriasNombres();
		//console.log(responseCategoriasParametrizacion);
		if (responseCategoriasParametrizacion['status'] == "success" && responseCategoriasParametrizacion['code'] == "200") {
			this.loading = false;
			this.categoriasArray = responseCategoriasParametrizacion['data'];
		}
		else if (responseCategoriasParametrizacion['status'] == "success" && responseCategoriasParametrizacion['code'] == "300") {
			this.loading = false;
			this.categoriasArray = [];
			swal('Advertencia!', responseCategoriasParametrizacion['message'], 'warning');
		}
		else if (responseCategoriasParametrizacion["status"] === "error" && responseCategoriasParametrizacion["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseCategoriasParametrizacion['message']);
		}
		else {
			this.loading = false;
			this.categoriasArray = [];
			swal('¡Error!', responseCategoriasParametrizacion['message'], 'error');
		}
	}
	/* Finaliza Listar solo nombres de categorias de parametrizaciones */

	/* Inicia Buscar parametrizaciones por categoria */
	async onSubmitBuscarCategoriaParametrizacion(page: number) {
		this.loading = true;
		this.estadoPaginador = 'filtroCategorias';

		const responseFiltroCategoria = await this._parametrizacionService.listarParametrizacionCategoriasPorCategoria(this.busquedaCategorias.categoria, page);
		//console.log(responseFiltroCategoria);
		if (responseFiltroCategoria['status'] == "success" && responseFiltroCategoria['code'] == "200") {
			this.loading = false;
			this.llenarTabla(responseFiltroCategoria['data'], page);
		}
		else if (responseFiltroCategoria['status'] == "success" && responseFiltroCategoria['code'] == "300") {
			this.loading = false;
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			swal('Advertencia!', responseFiltroCategoria['message'], 'warning');
		}
		else if (responseFiltroCategoria["status"] === "error" && responseFiltroCategoria["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseFiltroCategoria['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			swal('¡Error!', responseFiltroCategoria['message'], 'error');
		}
	}
	/* Finaliza Buscar parametrizaciones por categoria */

	/* Inicia buscar parametrizaciones por nombre de parametrizacion */
	async onSubmitBuscarParametrizacionPorNombre(page: number) {
		this.loading = true;
		this.estadoPaginador = 'filtroCategoriasNombre';

		const responseFiltroCategoria = await this._parametrizacionService.listarParametrizacionCategoriasPorNombres(this.busquedaCategorias.nombre, page);
		//console.log(responseFiltroCategoria);
		if (responseFiltroCategoria['status'] == "success" && responseFiltroCategoria['code'] == "200") {
			this.loading = false;
			this.llenarTabla(responseFiltroCategoria['data'], page);
		}
		else if (responseFiltroCategoria['status'] == "success" && responseFiltroCategoria['code'] == "300") {
			this.loading = false;
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			swal('Advertencia!', responseFiltroCategoria['message'], 'warning');
		}
		else if (responseFiltroCategoria["status"] === "error" && responseFiltroCategoria["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseFiltroCategoria['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			swal('¡Error!', responseFiltroCategoria['message'], 'error');
		}
	}
	/* Finaliza buscar parametrizaciones por nombre de parametrizacion */

	/* Inicia Metodos de filtros para buscar una categoria en especifico de las parametrizaciones */
	selectListCategorias() {
		document.getElementById("myDropdown").classList.toggle("show");
	}

	filtroCategorias(x: any) {
		let input, filter, ul, li, a, i;
		input = document.getElementById("categoria");
		filter = input.value.toUpperCase();
		let div = document.getElementById("myDropdown");
		a = div.getElementsByTagName("a");

		for (i = 0; i < a.length; i++) {
			let txtValue = a[i].textContent || a[i].innerText;
			if (txtValue.toUpperCase().indexOf(filter) > -1) {
				a[i].style.display = "";
			} else {
				a[i].style.display = "none";
			}
		}
	}

	seleccionarCategorias(categoria: any) {
		this.busquedaCategorias.categoria = categoria;
		document.getElementById("myDropdown").classList.toggle("show");
		(document.getElementById('categoria') as HTMLInputElement).value = categoria;
		this.onSubmitBuscarCategoriaParametrizacion(1);
	}
	/* Finaliza Metodos de filtros para buscar una categoria en especifico de las parametrizaciones */

	/* Inicia actualizar Parametrizacion */
	editarParametro(rowParametro: any) {
		//console.log(rowParametro);
		this.idParametrizacion = '' + rowParametro.id;
		this.parametros.nombre = rowParametro.nombre;
		this.parametros.valor = rowParametro.valor;
		this.parametros.categoria = rowParametro.categoria;
		this.parametros.descripcion = rowParametro.descripcionTemp;
		//console.log(this.parametros);

		$("#modalActualizarParametrizacion").modal("show");
	}

	async onSubmitActualizarParametrizacion(actualizarParametrizacionForm) {
		this.loading = true;
		//console.log(this.parametros);
		const responseParametros = await this._parametrizacionService.actualizarParametrizacionCategoria(this.idParametrizacion, this.parametros);
		//console.log(responseParametros);
		if (responseParametros["status"] === "success" && responseParametros["code"] === "200") {
			this.loading = false;
			swal('Bien!', responseParametros["message"], 'success')
			.then(result => {
				this.listarDatosParametrizacion(1);
				$("#modalActualizarParametrizacion").modal("hide");
				}).catch(swal.noop);
		}
		else if (responseParametros["status"] === "success" && responseParametros["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseParametros["message"], 'OK', 'warning');
		}
		else if (responseParametros["status"] === "error" && responseParametros["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseParametros['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseParametros["message"], 'OK', 'error');
		}
	}

	/* Inicia Carga de las parametrizaciones */
	selectListCategoriasParametro() {
		document.getElementById("myDropdownParametro").classList.toggle("show-parametro");
	}

	filtroCategoriasParametro(x: any) {
		let input, filter, ul, li, a, i;
		input = document.getElementById("categoriaParametro");
		filter = input.value.toUpperCase();
		let div = document.getElementById("myDropdownParametro");
		a = div.getElementsByTagName("a");

		for (i = 0; i < a.length; i++) {
			let txtValue = a[i].textContent || a[i].innerText;
			if (txtValue.toUpperCase().indexOf(filter) > -1) {
				a[i].style.display = "";
			} else {
				a[i].style.display = "none";
			}
		}
	}

	seleccionarCategoriasParametro(categoria: any) {
		this.busquedaCategorias.categoria = categoria;
		document.getElementById("myDropdownParametro").classList.toggle("show-parametro");
		(document.getElementById('categoriaParametro') as HTMLInputElement).value = categoria;
	}
	/* Finaliza Carga de las parametrizaciones */
	/* Finaliza actualizar Parametrizacion */

	crearParametro() {
		this._router.navigate(["parametrizacion/registrar"]);
	}

	limpiarFiltros() {
		//this.busquedaCategorias.categoria = '';
		this.busquedaCategorias.nombre = '';
		(document.getElementById('categoria') as HTMLInputElement).value = '';
		(document.getElementById('nombre') as HTMLInputElement).value = '';
		this.estadoPaginador = 'filtroTotal';
		this.listarDatosParametrizacion(1);
	}

	validacionEditableParametrizacion(parametro: any): boolean {
		//console.log(parametro);
		let editarParametro: boolean = false;

		if (this.categoriasRol.includes(parametro)) {
			editarParametro = true;
		} else {
			editarParametro = false;
		}

		return editarParametro;
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}
