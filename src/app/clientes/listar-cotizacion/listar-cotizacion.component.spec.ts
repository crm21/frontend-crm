import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarCotizacionComponent } from './listar-cotizacion.component';

describe('ListarCotizacionComponent', () => {
  let component: ListarCotizacionComponent;
  let fixture: ComponentFixture<ListarCotizacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarCotizacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarCotizacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
