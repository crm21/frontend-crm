import { Routes } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { PricingComponent } from './pricing/pricing.component';
import { LoginComponent } from './login/login.component';
import { RecuperarCuentaComponent } from './recuperar-cuenta/recuperar-cuenta.component';
import { ElegirEmpresasComponent } from './elegir-empresas/elegir-empresas.component';
import { SesionEmpresasComponent } from './sesion-empresas/sesion-empresas.component';
import { SesionUsuariosComponent } from './sesion-usuarios/sesion-usuarios.component';

export const PagesRoutes: Routes = [

    {
        path: '',
        children: [
            {
                path: 'iniciar-sesion',
                component: LoginComponent
            }, {
                path: 'recordar-cuenta',
                component: RecuperarCuentaComponent
            }, {
                path: 'autodenar/registrarse',
                component: RegisterComponent,
                data: {
                    opc: 'A'
                }
            }, {
                path: 'motodenar/registrarse',
                component: RegisterComponent,
                data: {
                    opc: 'M'
                }
            }, {
                path: 'automotora/registrarse',
                component: RegisterComponent,
                data: {
                    opc: 'T'
                }
            }, {
                path: 'pricing',
                component: PricingComponent
            }, {
                path: 'autodenar',
                component: SesionEmpresasComponent,
                data: {
                    opc: 'A'
                }
            }, {
                path: 'motodenar',
                component: SesionEmpresasComponent,
                data: {
                    opc: 'M'
                }
            }, {
                path: 'automotora',
                component: SesionEmpresasComponent,
                data: {
                    opc: 'T'
                }
            }, {
                path: 'autodenar/clientes',
                component: SesionUsuariosComponent,
                data: {
                    opc: 1,
                    business: 'A'
                }
            }, {
                path: 'motodenar/clientes',
                component: SesionUsuariosComponent,
                data: {
                    opc: 1,
                    business: 'M'
                }
            }, {
                path: 'automotora/clientes',
                component: SesionUsuariosComponent,
                data: {
                    opc: 1,
                    business: 'T'
                }
            }, {
                path: 'autodenar/colaboradores',
                component: SesionUsuariosComponent,
                data: {
                    opc: 2,
                    business: 'A'
                }
            }, {
                path: 'motodenar/colaboradores',
                component: SesionUsuariosComponent,
                data: {
                    opc: 2,
                    business: 'M'
                }
            }, {
                path: 'automotora/colaboradores',
                component: SesionUsuariosComponent,
                data: {
                    opc: 2,
                    business: 'T'
                }
            }, {
                path: 'autodenar/aseguradora',
                component: SesionUsuariosComponent,
                data: {
                    opc: 3,
                    business: 'A'
                }
            }, {
                path: 'motodenar/aseguradora',
                component: SesionUsuariosComponent,
                data: {
                    opc: 3,
                    business: 'M'
                }
            }, {
                path: 'automotora/aseguradora',
                component: SesionUsuariosComponent,
                data: {
                    opc: 3,
                    business: 'T'
                }
            }, {
                path: 'empresas',
                component: ElegirEmpresasComponent
            }
        ]
    }
];