import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { EstudioCredito } from "../../models/estudioCredito";
import { DatePipe } from "@angular/common";
import swal from "sweetalert2";
import { EstudioCreditoService } from '../../services/estudio-credito/estudio-credito.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare var $: any;

@Component({
	selector: 'app-crear-estudio-credito',
	templateUrl: './crear-estudio-credito.component.html',
	styleUrls: ['./crear-estudio-credito.component.css']
})
export class CrearEstudioCreditoComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	idCotizacion: any;
	ingresoFinalizado: any = 0;
	datosEstudioCreditoForm: FormGroup;
	bancosParametros: any;
	planParametros: any;
	bancosArray: any[] = [];
	planArray: any[] = [];

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _estudioCreditoService: EstudioCreditoService,
		private _parametrizacionService: ParametrizacionService,
		private _sweetAlertService: SweetAlertService
	) {
		this.idCotizacion = this._route.snapshot.paramMap.get('idCotizacion');
		this.ingresoFinalizado = this._route.snapshot.paramMap.get('ingresoFinalizado');
	}

	async ngOnInit() {
		if (localStorage.getItem('identity_crm')) {
			this.datosEstudioCreditoForm = new FormGroup({
				banco: new FormControl(null, Validators.required),
				plan: new FormControl(null, Validators.required),
				observacion: new FormControl(null),
			});
			this.loading = true;
			this.cargarInformacionSelect();
			this.loading = false;
		}
	}

	async onSubmitEstudioCredito() {
		this.loading = true;

		let estudioCredito = new EstudioCredito(
			this.idCotizacion,
			this.datosEstudioCreditoForm.value.banco,
			this.datosEstudioCreditoForm.value.plan,
			this.datosEstudioCreditoForm.value.observacion
		); //console.log(estudioCredito);

		const responseEstudioCredito = await this._estudioCreditoService.registrarEstudioCredito(1, estudioCredito);

		if (responseEstudioCredito["status"] === "success" && responseEstudioCredito["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseEstudioCredito["message"], 'success')
				.then(result => {
					this.datosEstudioCreditoForm.reset();
					this._router.navigate(["clientes/listar-estudio-credito/", this.idCotizacion, this.ingresoFinalizado]);
				}).catch(swal.noop);
		}
		else if (responseEstudioCredito["status"] === "success" && responseEstudioCredito["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseEstudioCredito["message"], 'OK', 'warning');
		}
		else if (responseEstudioCredito["status"] === "error" && responseEstudioCredito["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseEstudioCredito['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseEstudioCredito["message"], 'OK', 'error');
		}
	}

	async cargarInformacionSelect() {
		this.bancosParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Bancos');
		this.planParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Plan de crédito');

		this.bancosParametros.data.forEach((element) => {
			this.bancosArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		this.planParametros.data.forEach((element) => {
			this.planArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	regresar() {
		let idIngreso = localStorage.getItem('idIngreso');
		this._router.navigate(["clientes/ficha-clientes/", idIngreso]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}
