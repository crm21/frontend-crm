import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { EventosComercialesService } from '../../services/eventos-comerciales/eventos-comerciales.service';

declare var $: any;
declare interface DataTable {
	headerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-listar-agendamiento',
	templateUrl: './listar-agendamiento.component.html',
	styleUrls: ['./listar-agendamiento.component.css']
})
export class ListarAgendamientoComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	confirmarCitaForm: FormGroup;
	dataTable: DataTable;
	registrosArray: any[] = [];
	estadoAgendamientoArray: any[] = [];
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	confirmarBotonEventoComercial: boolean = true;
	consultaVaciaCargarSpinner: boolean = false;
	eventoDetallado = {
		id: null,
		titulo: null,
		clienteId: null,
		clienteDocumento: null,
		clienteNombres: null,
		fechaRegistro: null,
		fechaEvento: null,
		fechaConfirmacion: null,
		hora: null,
		tipoEvento: null,
		registradoPor: null,
		registradoA: null,
		estado: null,
		colorEstado: null,
		observacion: null
	};

	constructor(
		private _router: Router,
		private _formBuilder: FormBuilder,
		private _sweetAlertService: SweetAlertService,
		private _eventosComercialesService: EventosComercialesService,
		private _parametrizacionService: ParametrizacionService,
		private _datepipe: DatePipe
	) {
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);
			let permisoVerFicha = identityData['data'];
			this.listarEventosComerciales('1');
		}
	}

	ngOnInit() {
		if (localStorage.getItem('identity_crm')) {
			this.dataTable = {
				headerRow: [
					'#',
					'Cliente',
					'Título',
					'Fecha del Evento',
					'Hora',
					'Estado',
					'Acciones'
				],
				dataRows: []
			};
			this.confirmarCitaForm = this._formBuilder.group({
				estadoAgendamiento: ['', Validators.required],
				observacion: ['']
			});
		}
	}

	async listarEventosComerciales(page: string) {
		this.loading = true;
		const responseConfirmarEvento = await this._eventosComercialesService.listarEventosComerciales(page);
		//console.log(responseConfirmarEvento);
		if (responseConfirmarEvento["status"] === "success" && responseConfirmarEvento["code"] === "200") {
			this.llenarTabla(responseConfirmarEvento["data"], +page);
			this.loading = false;
		}
		else if (responseConfirmarEvento["status"] === "success" && responseConfirmarEvento["code"] === "300") {
			this.loading = false;
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			this._sweetAlertService.showNotification('top', 'right', 'success', 'warning', '<b>' + responseConfirmarEvento['message'] + '<b>', 2000);
		}
		else if (responseConfirmarEvento["status"] === "error" && responseConfirmarEvento["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseConfirmarEvento['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			this._sweetAlertService.showNotification('top', 'right', 'error', 'warning', '<b>' + responseConfirmarEvento['message'] + '<b>', 2000);
		}
	}

	llenarTabla(eventosComerciales: any, page: number) {
		//console.log(eventosComerciales);
		this.registrosArray = eventosComerciales.eventos;
		this.dataTable.dataRows = [];
		this.total_item_count = eventosComerciales['total_item_count'];
		this.page_actual = page;
		this.total_pages = eventosComerciales['total_pages'];

		if (page >= 2) { this.page_prev = page - 1; }
		else { this.page_prev = 1; }

		if (page < eventosComerciales['total_pages']) { this.page_next = page + 1; }
		else {
			if (eventosComerciales['total_pages'] == 0) { this.page_next = 1; }
			else { this.page_next = eventosComerciales['total_pages']; }
		}

		let jsonRegistro: any;

		this.registrosArray.forEach(element => {
			jsonRegistro = {
				id: element.id,
				clienteId: element.cliente.id,
				clienteDocumento: element.cliente.tipoDocumento.valor + ': ' + element.cliente.documento,
				clienteNombres: element.cliente.nombres + ' ' + element.cliente.apellidos,
				fechaRegistro: element.fechaRegistro,
				fechaEvento: element.fechaEvento,
				fechaConfirmacion: element.fechaConfirmacion,
				hora: element.hora.hora,
				tipoEvento: element.tipoEvento.nombre,
				registradoPor: element.registradoPor.nombres + ' ' + element.registradoPor.apellidos,
				registradoA: element.registradoA.nombres + ' ' + element.registradoA.apellidos,
				estado: element.estado.nombre,
				colorEstado: this.darColorEstadoEvento(element.estado.valor),
				titulo: element.titulo,
				observacion: element.observacion,
				confirmarEventoComercial: this.confirmarBotonEventoComercial
			};
			this.dataTable.dataRows.push(jsonRegistro);
		});
		//console.log(this.dataTable.dataRows);
		if (this.total_item_count || this.total_item_count == 0) {
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
		}
	}

	visualizarRowAgenda(rowAgenda: any) {
		this.eventoDetallado.id = rowAgenda.id;
		this.eventoDetallado.titulo = rowAgenda.titulo;
		this.eventoDetallado.clienteId = rowAgenda.clienteId;
		this.eventoDetallado.clienteDocumento = rowAgenda.clienteDocumento;
		this.eventoDetallado.clienteNombres = rowAgenda.clienteNombres;
		this.eventoDetallado.fechaRegistro = rowAgenda.fechaRegistro;
		this.eventoDetallado.fechaEvento = rowAgenda.fechaEvento;
		this.eventoDetallado.fechaConfirmacion = rowAgenda.fechaConfirmacion;
		this.eventoDetallado.hora = rowAgenda.hora;
		this.eventoDetallado.tipoEvento = rowAgenda.tipoEvento;
		this.eventoDetallado.registradoPor = rowAgenda.registradoPor;
		this.eventoDetallado.registradoA = rowAgenda.registradoA;
		this.eventoDetallado.estado = rowAgenda.estado;
		this.eventoDetallado.colorEstado = rowAgenda.colorEstado;
		this.eventoDetallado.observacion = rowAgenda.observacion;
		//console.log(this.eventoDetallado);
		$("#modalInformacionDetallada").modal("show");
	}

	confirmarRowAgenda(rowAgendaConfirmar: any) {
		this.cargarInformacionSelect();
		this.eventoDetallado.id = rowAgendaConfirmar.id;
		//console.log(rowAgendaConfirmar);
		$("#modalConfirmarEventoComercial").modal("show");
	}

	async onSubmitConfirmarCita() {
		this.loading = true;
		let estado: string = this.confirmarCitaForm.value.estadoAgendamiento;
		let observacion: string = this.confirmarCitaForm.value.observacion;
		const responseConfirmarAgenda = await this._eventosComercialesService.confirmarEventoComercial(this.eventoDetallado.id, estado, observacion);
		//console.log(responseConfirmarAgenda);
		if (responseConfirmarAgenda["status"] === "success" && responseConfirmarAgenda["code"] === "200") {
			this._sweetAlertService.showNotification('top', 'right', 'success', 'success', '<b>' + responseConfirmarAgenda['message'] + '<b>', 2000);
			this.listarEventosComerciales('1');
			this.confirmarCitaForm.reset();
			$("#modalConfirmarEventoComercial").modal("hide");
			this.loading = false;
		}
		else if (responseConfirmarAgenda["status"] === "success" && responseConfirmarAgenda["code"] === "300") {
			swal('Advertencia!', responseConfirmarAgenda["message"], 'warning')
				.then(result => {
					this.confirmarCitaForm.reset();
					this.listarEventosComerciales('1');
					$("#modalConfirmarEventoComercial").modal("hide");
				}).catch(swal.noop);
			this.loading = false;
		}
		else if (responseConfirmarAgenda["status"] === "error" && responseConfirmarAgenda["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseConfirmarAgenda['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'error', 'warning', '<b>' + responseConfirmarAgenda['message'] + '<b>', 2000);
		}
	}

	async cargarInformacionSelect() {
		const estadoAgendamiento: any = await this._parametrizacionService.listarCategoriaParametrizacion('Agendamiento');
		this.estadoAgendamientoArray = [];
		estadoAgendamiento['data'].forEach((element) => {
			this.estadoAgendamientoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
	}

	darColorEstadoEvento(estado: string): string {
		switch (estado) {
			case 'cancelada':
				this.confirmarBotonEventoComercial = false;
				return 'danger';
				break;
			case 'confirmada':
				this.confirmarBotonEventoComercial = false;
				return 'success';
				break;
			case 'reprogramada':
				this.confirmarBotonEventoComercial = false;
				return 'info';
				break;
			case 'no confirmada':
				this.confirmarBotonEventoComercial = true;
				return 'default';
				break;
			default:
				this.confirmarBotonEventoComercial = false;
				return 'default';
				break;
		}
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) {
			result = e1 === e2;
		}
		return result;
	}

	fechasValidacion(formatoFecha: any) {
		//console.log(formatoFecha);
		if (formatoFecha) {
			//Formatear fecha
			const fechaActualTem = this._datepipe
				.transform(formatoFecha, "yyyy/MM/dd")
				.replace(/\//g, "-");
			//console.log("fecha formato: "+fechaActualTem);
			return fechaActualTem;
		}
		return '';
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}