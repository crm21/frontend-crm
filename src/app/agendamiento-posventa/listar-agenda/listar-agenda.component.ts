import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { AgendaService } from '../../services/agenda/agenda.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { VehiculosService } from '../../services/vehiculos/vehiculos.service';

declare var $: any;
declare interface DataTable {
	headerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-listar-agenda',
	templateUrl: './listar-agenda.component.html',
	styleUrls: ['./listar-agenda.component.css']
})
export class ListarAgendaComponent implements OnInit {
	loading: boolean = false;
	validarToken: boolean = true;
	confirmarCitaForm: FormGroup;
	dataTable: DataTable;
	registrosArray: any[] = [];
	estadoAgendamientoArray: any[] = [];
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	idCliente: string = '';
	rolUsuario: string = '';
	existenVehiculos: boolean = false;
	confirmarAgendaBoton: boolean = true;
	consultaVaciaCargarSpinner: boolean = false;
	rolesPermisos: string[] = ['A', 'DT', 'ACC'];
	idUsuario: string = '';
	agendamientoDetallado = {
		id: null,
		clienteId: null,
		clienteDocumento: null,
		clienteNombres: null,
		fechaRegistro: null,
		fechaAgendamiento: null,
		fechaConfirmacion: null,
		hora: null,
		tipoAgendamiento: null,
		placa: null,
		modelo: null,
		agendadaPor: null,
		agendadaA: null,
		estado: null,
		colorEstado: null,
		observacion: null
	};

	constructor(
		private _router: Router,
		private _formBuilder: FormBuilder,
		private _sweetAlertService: SweetAlertService,
		private _vehiculosService: VehiculosService,
		private _agendaService: AgendaService,
		private _parametrizacionService: ParametrizacionService,
		private _datepipe: DatePipe
	) {
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);
			let permisoVerFicha = identityData['data'];
			this.rolUsuario = identityData['data'].rol.valor;
			this.idUsuario = permisoVerFicha.sub;
			this.idCliente = permisoVerFicha.sub;
			if (this.rolUsuario == 'CL') {
				this.confirmarAgendaBoton = false;
			}
			if (this.rolesPermisos.includes(this.rolUsuario)) {
				this.existenVehiculos = true;
			} else {
				this.buscarVehiculosCliente();
			}
			this.listarAgendamientos('1');
		}
	}

	ngOnInit() {
		if (localStorage.getItem('identity_crm')) {
			this.dataTable = {
				headerRow: [
					'#',
					'Cliente',
					'Fecha de Agendamiento',
					'Hora',
					'Vehículo',
					'Estado',
					'Acciones'
				],
				dataRows: []
			};
		}
		this.confirmarCitaForm = this._formBuilder.group({
			estadoAgendamiento: ['', Validators.required],
			observacion: ['']
		});
	}

	async listarAgendamientos(page: string) {
		this.loading = true;
		const responseListaAgendamiento = await this._agendaService.listarAgendaUsuario(page);
		//console.log(responseListaAgendamiento);
		if (responseListaAgendamiento["status"] === "success" && responseListaAgendamiento["code"] === "200") {
			this.llenarTabla(responseListaAgendamiento["data"], +page);
			this.loading = false;
		}
		else if (responseListaAgendamiento["status"] === "success" && responseListaAgendamiento["code"] === "300") {
			this.loading = false;
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			this._sweetAlertService.showNotification('top', 'right', 'success', 'warning', '<b>' + responseListaAgendamiento['message'] + '<b>', 2000);
		}
		else if (responseListaAgendamiento["status"] === "error" && responseListaAgendamiento["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseListaAgendamiento['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			this._sweetAlertService.showNotification('top', 'right', 'error', 'warning', '<b>' + responseListaAgendamiento['message'] + '<b>', 2000);
		}
	}

	llenarTabla(agendamientos: any, page: number) {
		//console.log(agendamientos);
		this.registrosArray = agendamientos.agendamientos;
		this.dataTable.dataRows = [];
		this.total_item_count = agendamientos['total_item_count'];
		this.page_actual = page;
		this.total_pages = agendamientos['total_pages'];

		if (page >= 2) this.page_prev = page - 1;
		else this.page_prev = 1;

		if (page < agendamientos['total_pages']) this.page_next = page + 1;
		else {
			if (agendamientos['total_pages'] == 0) this.page_next = 1;
			else this.page_next = agendamientos['total_pages'];
		}

		let jsonRegistro: any;

		this.registrosArray.forEach(element => {
			jsonRegistro = {
				id: element.id,
				clienteId: element.cliente.id,
				clienteDocumento: element.cliente.tipoDocumento.valor + ': ' + element.cliente.documento,
				clienteNombres: element.cliente.nombres + ' ' + element.cliente.apellidos,
				fechaRegistro: element.fechaRegistro,
				fechaAgendamiento: element.fechaAgendamiento,
				fechaConfirmacion: element.fechaConfirmacion,
				hora: element.hora.hora,
				tipoAgendamiento: element.tipoAgendamiento.nombre,
				placa: element.vehiculo.placa,
				modelo: element.vehiculo.modelo,
				agendadaPor: element.agendadaPor.nombres + ' ' + element.agendadaPor.apellidos,
				agendadaA: element.agendadaA.nombres + ' ' + element.agendadaA.apellidos,
				estado: element.estado.nombre,
				colorEstado: this.darColorEstadoEvento(element.estado.valor),
				observacion: element.observacion
			};
			this.dataTable.dataRows.push(jsonRegistro);
		});
		//console.log(this.dataTable.dataRows);
		if (this.total_item_count || this.total_item_count == 0) {
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
		}
	}

	/* Listar vehiculos asociados al cliente */
	async buscarVehiculosCliente() {
		this.loading = true;
		const responseVehiculosCliente = await this._vehiculosService.listarVehiculosCliente(2, this.idCliente, '1');
		//console.log(responseVehiculosCliente);
		if (responseVehiculosCliente["status"] === "success" && responseVehiculosCliente["code"] === "200") {
			this.existenVehiculos = responseVehiculosCliente["data"].estadoVehiculos;
			this.loading = false;
		}
		else if (responseVehiculosCliente["status"] === "success" && responseVehiculosCliente["code"] === "300") {
			this.existenVehiculos = responseVehiculosCliente["data"].estadoVehiculos;
			this.loading = false;
		}
		else if (responseVehiculosCliente["status"] === "error" && responseVehiculosCliente["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseVehiculosCliente['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'error', 'warning', '<b>' + responseVehiculosCliente['message'] + '<b>', 2000);
		}
	}

	visualizarRowAgenda(rowAgenda: any) {
		this.agendamientoDetallado.id = rowAgenda.id;
		this.agendamientoDetallado.clienteId = rowAgenda.clienteId;
		this.agendamientoDetallado.clienteDocumento = rowAgenda.clienteDocumento;
		this.agendamientoDetallado.clienteNombres = rowAgenda.clienteNombres;
		this.agendamientoDetallado.fechaRegistro = rowAgenda.fechaRegistro;
		this.agendamientoDetallado.fechaAgendamiento = rowAgenda.fechaAgendamiento;
		this.agendamientoDetallado.fechaConfirmacion = rowAgenda.fechaConfirmacion;
		this.agendamientoDetallado.hora = rowAgenda.hora;
		this.agendamientoDetallado.tipoAgendamiento = rowAgenda.tipoAgendamiento;
		this.agendamientoDetallado.placa = rowAgenda.placa;
		this.agendamientoDetallado.modelo = rowAgenda.modelo;
		this.agendamientoDetallado.agendadaPor = rowAgenda.agendadaPor;
		this.agendamientoDetallado.agendadaA = rowAgenda.agendadaA;
		this.agendamientoDetallado.estado = rowAgenda.estado;
		this.agendamientoDetallado.colorEstado = rowAgenda.colorEstado;
		this.agendamientoDetallado.observacion = rowAgenda.observacion;
		//console.log(this.agendamientoDetallado);
		$("#modalInformacionDetalladaAgenda").modal("show");
	}

	confirmarRowAgenda(rowAgendaConfirmar: any) {
		this.cargarInformacionSelect();
		this.agendamientoDetallado.id = rowAgendaConfirmar.id;
		//console.log(rowAgendaConfirmar);
		$("#modalConfirmarAgendamientoAgenda").modal("show");
	}

	agendar(opc: number) {
		if (opc === 1) {
			switch (true) {
				case this.rolUsuario == 'CL':
					this._router.navigate(["agendamiento-posventa/agendar-cliente"]);
					break;
				case this.rolUsuario == 'DT' || this.rolUsuario == 'ACC' || this.rolUsuario == 'A':
					localStorage.setItem('rutaAgenda', 'agendamiento-posventa/listar');
					this._router.navigate(["agendamiento-posventa/agendar-taller"]);
					break;

				default:
					return;
					break;
			}
		} else {
			swal('Advertencia!',
				'Para realizar un agendamiento, debe tener al menos un vehículo registrado. Por favor actualice su información e intente nuevamente',
				'warning')
				.then(result => {
					this._router.navigate(["clientes/mi-perfil"]);
				}).catch(swal.noop);
		}
	}

	async onSubmitConfirmarCita() {
		this.loading = true;
		let estado: string = this.confirmarCitaForm.value.estadoAgendamiento;
		let observacion: string = this.confirmarCitaForm.value.observacion;
		const responseConfirmarAgenda = await this._agendaService.confirmarAgendaCita(this.agendamientoDetallado.id, estado, observacion);
		//console.log(responseConfirmarAgenda);
		if (responseConfirmarAgenda["status"] === "success" && responseConfirmarAgenda["code"] === "200") {
			this._sweetAlertService.showNotification('top', 'right', 'success', 'success', '<b>' + responseConfirmarAgenda['message'] + '<b>', 2000);
			this.listarAgendamientos('1');
			$("#modalConfirmarAgendamientoAgenda").modal("hide");
			this.confirmarCitaForm.reset();
			this.loading = false;
		}
		else if (responseConfirmarAgenda["status"] === "success" && responseConfirmarAgenda["code"] === "300") {
			swal('Advertencia!', responseConfirmarAgenda["message"], 'warning')
				.then(result => {
					$("#modalConfirmarAgendamientoAgenda").modal("hide");
					this.listarAgendamientos('1');
				}).catch(swal.noop);
			this.loading = false;
		}
		else if (responseConfirmarAgenda["status"] === "error" && responseConfirmarAgenda["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseConfirmarAgenda['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'error', 'warning', '<b>' + responseConfirmarAgenda['message'] + '<b>', 2000);
		}
	}

	async cargarInformacionSelect() {
		const estadoAgendamiento: any = await this._parametrizacionService.listarCategoriaParametrizacion('Agendamiento');
		this.estadoAgendamientoArray = [];
		estadoAgendamiento['data'].forEach((element) => {
			this.estadoAgendamientoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
	}

	darColorEstadoEvento(estado: string): string {
		switch (estado) {
			case 'cancelada':
				return 'danger';
				break;
			case 'confirmada':
				return 'success';
				break;
			case 'reprogramada':
				return 'info';
				break;
			case 'no confirmada':
				return 'default';
				break;
			default:
				return 'default';
				break;
		}
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) {
			result = e1 === e2;
		}
		return result;
	}

	fechasValidacion(formatoFecha: any) {
		//console.log(formatoFecha);
		if (formatoFecha) {
			//Formatear fecha
			const fechaActualTem = this._datepipe
				.transform(formatoFecha, "yyyy/MM/dd")
				.replace(/\//g, "-");
			//console.log("fecha formato: "+fechaActualTem);
			return fechaActualTem;
		}
		return '';
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}
