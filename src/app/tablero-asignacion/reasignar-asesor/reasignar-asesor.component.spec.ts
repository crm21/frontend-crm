import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReasignarAsesorComponent } from './reasignar-asesor.component';

describe('ReasignarAsesorComponent', () => {
  let component: ReasignarAsesorComponent;
  let fixture: ComponentFixture<ReasignarAsesorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReasignarAsesorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReasignarAsesorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
