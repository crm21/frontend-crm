import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteClientesIngresosActivosComponent } from './reporte-clientes-ingresos-activos.component';

describe('ReporteClientesIngresosActivosComponent', () => {
  let component: ReporteClientesIngresosActivosComponent;
  let fixture: ComponentFixture<ReporteClientesIngresosActivosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteClientesIngresosActivosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteClientesIngresosActivosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
