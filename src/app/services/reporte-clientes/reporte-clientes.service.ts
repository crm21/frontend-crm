import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UsuarioService } from '../usuario/usuario.service';

@Injectable({
	providedIn: 'root'
})
export class ReporteClientesService {

	headers: any;
	url: string = '';
	token: string;

	constructor(
		private http: HttpClient,
		private _usuarioService: UsuarioService
	) {
		console.log('[Reporte Clientes services is ready]...');
	}

	async listarReporteIngresoClientes(fechaIni: string, fechaFin: string) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			let params = "?fechaIni=" + fechaIni + "&fechaFin=" + fechaFin;
			this.http.get(this.url + "/reporteActualizado" + params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}
}