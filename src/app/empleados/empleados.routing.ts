import { Routes } from '@angular/router';

import { ListarEmpleadosComponent } from './listar-empleados/listar-empleados.component';
import { CrearHojaVidaComponent } from './crear-hoja-vida/crear-hoja-vida.component';
import { FichaEmpleadosComponent } from './ficha-empleados/ficha-empleados.component';

export const EmpleadosRoutes: Routes = [
    {
        path: '',
        children: [{
            path: 'listar',
            component: ListarEmpleadosComponent
        }, {
            path: 'registrar',
            component: CrearHojaVidaComponent,
            data: {
                opc: 1
            }
        }, {
            path: 'actualizar/:idEmpleado',
            component: CrearHojaVidaComponent,
            data: {
                opc: 2
            }
        },
        {
            path: 'ficha-empleados/:idEmpleado',
            component: FichaEmpleadosComponent
        }]
    }
];