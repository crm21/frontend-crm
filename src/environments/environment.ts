// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  apiCrmAutodenar: 'http://backend-crm.local',
  apiCrmMotodenar: 'http://backend-crm-motodenar.local',
  apiCrmAutomotora: 'http://backend-crm-automotora.local',
  apiCrmTest: 'https://olgaviriascs.com/api-crm/public',
  correoEmpresaAutodenar: 'info@autodenar.com.co',
  correoEmpresaMotodenar: 'info@motodenar.com',
  correoEmpresaAutomotora: 'info@automotoradelsur.com',
  telefonoEmpresaAutodenar: '7365012',
  telefonoEmpresaMotodenar: '+57 3003831558',
  telefonoEmpresaAutomotora: '+57 3147712956',
  localImag: './assets/img/',
  localFiles: './assets/files/',
  serverImg: '/images/',
  project: true,
  version: 'v.2.3.5'
};