import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientesProyectadosComponent } from './clientes-proyectados.component';

describe('ClientesProyectadosComponent', () => {
  let component: ClientesProyectadosComponent;
  let fixture: ComponentFixture<ClientesProyectadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientesProyectadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientesProyectadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
