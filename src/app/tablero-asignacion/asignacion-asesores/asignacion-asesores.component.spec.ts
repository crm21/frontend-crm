import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignacionAsesoresComponent } from './asignacion-asesores.component';

describe('AsignacionAsesoresComponent', () => {
  let component: AsignacionAsesoresComponent;
  let fixture: ComponentFixture<AsignacionAsesoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignacionAsesoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignacionAsesoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
