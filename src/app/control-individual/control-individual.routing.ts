import { Routes } from '@angular/router';

import { ListarCargosComponent } from './listar-cargos/listar-cargos.component';
import { RegistrarContratosComponent } from './registrar-contratos/registrar-contratos.component';
import { ListarAfiliacionesComponent } from './listar-afiliaciones/listar-afiliaciones.component';
import { ListarFuncionesComponent } from './listar-funciones/listar-funciones.component';
import { ControlIndividualComponent } from './control-individual/control-individual.component';
import { ContratosEmpleadoComponent } from './contratos-empleado/contratos-empleado.component';

export const ControlIndividualRoutes: Routes = [
	{
		path: '',
		children: [
			{
				path: 'listar-cargos',
				component: ListarCargosComponent
			}, {
				path: 'listar-funcion/:idCargo',
				component: ListarFuncionesComponent,
				data: {
					opc: 1
				}
			}, {
				path: 'listar-funciones/:idCargo',
				component: ListarFuncionesComponent,
				data: {
					opc: 2
				}
			}, {
				path: 'registrar-contratos',
				component: RegistrarContratosComponent
			}, {
				path: 'listar-afiliacion/:idContrato',
				component: ListarAfiliacionesComponent,
				data: {
					opc: 1
				}
			}, {
				path: 'listar-afiliaciones/:idContrato',
				component: ListarAfiliacionesComponent,
				data: {
					opc: 2
				}
			}, {
				path: 'listar-afiliaciones-empleado/:idContrato',
				component: ListarAfiliacionesComponent,
				data: {
					opc: 3
				}
			}, {
				path: 'control-individual',
				component: ControlIndividualComponent,
				data: {
					opc: 1
				}
			}, {
				path: 'listar-control-individual',
				component: ControlIndividualComponent,
				data: {
					opc: 2
				}
			}, {
				path: 'ver-contrato-empleado/:idContrato',
				component: ContratosEmpleadoComponent,
				data: {
					opc: 1
				}
			}, {
				path: 'registrar-contrato-empleado/:idEmpleado',
				component: ContratosEmpleadoComponent,
				data: {
					opc: 2
				}
			}
		]
	}
];