import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { AuditoriaServices } from '../../services/auditoria/auditoria.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare var $: any;
declare interface DataTable {
	headerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-excepciones',
	templateUrl: './excepciones.component.html',
	styleUrls: ['./excepciones.component.css']
})
export class ExcepcionesComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	consultaVaciaCargarSpinner: boolean = false;
	dataTable: DataTable;
	excepciones: any[] = [];
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	textoExtendido: string = '';

	constructor(
		private _router: Router,
		private _auditoriaService: AuditoriaServices,
		private _sweetAlertService: SweetAlertService,
	) { }

	ngOnInit() {
		if (localStorage.getItem('identity_crm')) {
			this.dataTable = {
				headerRow: [
					'#',
					'ID Usuario',
					'Nombre Usuario',
					'Módulo',
					'Método',
					'Mensaje',
					'Tipo de excepción',
					'Pila',
					'Origen',
					'Fecha Creación'
				],
				dataRows: []
			};
			this.listarExcepcionesAuditoria(1);
		}
	}

	async listarExcepcionesAuditoria(page) {
		this.loading = true;

		const responseExcepcion = await this._auditoriaService.listarExcepciones(page);
		//console.log(responseExcepcion);
		if (responseExcepcion['status'] == "exito" && responseExcepcion['code'] == "200") {
			this.loading = false;
			this.excepciones = responseExcepcion['excepcion'];
			this.dataTable.dataRows = [];
			this.total_item_count = responseExcepcion['total_item_count'];
			this.page_actual = page;

			this.total_pages = responseExcepcion['total_pages'];

			if (page >= 2) this.page_prev = page - 1;
			else this.page_prev = 1;

			if (page < responseExcepcion['total_pages']) this.page_next = page + 1;
			else {
				if (responseExcepcion['total_pages'] == 0) this.page_next = 1;
				else this.page_next = responseExcepcion['total_pages'];
			};

			let jsonRegistro;

			this.excepciones.forEach(element => {
				jsonRegistro = {
					id: element.id,
					idUsuario: element.idUsuario,
					nombreUsuario: element.nombreUsuario,
					modulo: element.modulo,
					metodo: element.metodo,
					mensaje: element.mensaje,
					tipoExcepcion: element.tipoExcepcion,
					pila: element.pila,
					origenExcepcion: element.origenExcepcion,
					creacionExcepcion: element.creacionExcepcion
				};
				this.dataTable.dataRows.push(jsonRegistro);
			});

			if (this.total_item_count || this.total_item_count == 0) {
				this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			}
		}
		else if (responseExcepcion['status'] == "exito" && responseExcepcion['code'] == "300") {
			this.loading = false;
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			swal('Advertencia!', responseExcepcion['message'], 'warning');
		}
		else if (responseExcepcion["status"] === "error" && responseExcepcion["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseExcepcion['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			swal('¡Error!', responseExcepcion['message'], 'error');
		}
	}

	listarRegistros(opc: number) {
		if (opc == 1) {
			this._router.navigate(["registros-sistema/auditoria"]);
		} else {
			this._router.navigate(["registros-sistema/excepciones"]);
		}
	}

	observarTexto(texto: any) {
		this.textoExtendido = texto;
		$("#modalTextoDetallado").modal("show");
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}