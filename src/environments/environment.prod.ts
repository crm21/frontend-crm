export const environment = {
  production: false,
  apiCrmAutodenar: 'https://olgaviriascs.com/api-crm-autodenar/public',
  apiCrmMotodenar: 'https://olgaviriascs.com/api-crm-motodenar/public',
  apiCrmAutomotora: 'https://olgaviriascs.com/api-crm-automotora/public',
  apiCrmTest: 'https://olgaviriascs.com/api-crm/public',
  correoEmpresaAutodenar: 'info@autodenar.com.co',
  correoEmpresaMotodenar: 'info@motodenar.com',
  correoEmpresaAutomotora: 'info@automotoradelsur.com',
  telefonoEmpresaAutodenar: '7365012',
  telefonoEmpresaMotodenar: '+57 3003831558',
  telefonoEmpresaAutomotora: '+57 3147712956',
  localImag: './assets/img/',
  localFiles: './assets/files/',
  serverImg: '/images/',
  project: false,
  version: 'v.2.3.5'
};
