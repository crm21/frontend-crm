export class EstudioCredito {
    constructor(
        public cotizacion: string,
        public banco: string,
        public plan: string,
        public observacion?: string
    ) { }
}

export class ActualizarEstudioCredito {
    constructor(
		public fechaRadicado: string,
		public fechaContestacion: string,
		public cotizacion: string,
		public contestacion: string,
		public banco: string,
		public plan: string,
		public observacion: string,
		public fechaDesembolso: string
    ) { }
}