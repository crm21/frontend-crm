export class Asesor {
    constructor(
        public tipoDocumento: string,
        public documento: string,
        public nombres: string,
        public apellidos: string,
        public correo: string,
        public rol: string,
        public correoEmpresa: string,
        public telefonoEmpresa: string
    ) { }
}