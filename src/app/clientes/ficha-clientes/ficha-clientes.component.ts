import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from "@angular/router";
import { TableroAsignacion } from "../../models/tableroAsignacion";
import { ClienteService } from '../../services/cliente/cliente.service';
import { ProcesoVentasService } from '../../services/proceso-ventas/proceso-ventas.service';
import { TableroAsignacionService } from '../../services/tablero-asignacion/tablero-asignacion.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { EventosComercialesService } from '../../services/eventos-comerciales/eventos-comerciales.service';
import swal from "sweetalert2";

declare var $: any;

@Component({
	selector: 'app-ficha-clientes',
	templateUrl: './ficha-clientes.component.html',
	styleUrls: ['./ficha-clientes.component.css']
})
export class FichaClientesComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	permisoVerFicha: any;
	finalizarAtencionForm: FormGroup;
	cerrarNegocioForm: FormGroup;
	noDatosPersonales: boolean = false;
	noDatosCotizaciones: boolean = false;
	noDatosEstudioCreditos: boolean = false;
	noDatosProcesos: boolean = false;
	noDatosReconsiderados: boolean = false;
	noDatosAccesorios: boolean = false;
	noDatosAtenciones: boolean = false;
	clienteNoPertenece: boolean = false;
	idIngreso: any;
	infoCotizaciones: any;
	infoEstudioCreditos: any;
	infoProcesos: any;
	infoReconsiderados: any;
	infoAccesorios: any;
	infoCotizacionesArray: any[] = [];
	infoEstudioCreditosArray: any[] = [];
	infoProcesosArray: any[] = [];
	infoReconsideradosArray: any[] = [];
	infoAccesoriosArray: any[] = [];
	totalAtencionesArray: any[] = [];
	estadoCompraArray: any[] = [
		{ nombre: 'Sí', valor: 'SI' },
		{ nombre: 'No', valor: 'NO' }
	];
	generoArray: any[] = [
		{ nombre: 'Mujer', valor: 'M' },
		{ nombre: 'Hombre', valor: 'H' },
		{ nombre: 'Otro', valor: 'O' }
	];
	ocultarBotonesCliente = '';
	botonIniciarAsesoria: boolean = false;
	finalizarAtencionVentana: boolean = false;
	ingresoFinalizado: number;
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;

	idAtencion: any;
	idUsuario: number = 1;
	nombreCliente: any;
	nombreUsuarioAtencion: any;
	documento: any;
	fechaInicial: any;
	objecionesArray: any[] = [];
	tipificacionArray: any[] = [];
	infoIngreso = {
		id: null,
		nombres: null,
		tipoDoc: null,
		documento: null,
		fechaIngreso: null,
		fechaSalida: null,
		tarjetaOpciones: null,
		asesor: null,
		canalIngreso: null,
		observacion: null,
		estadoCompra: null,
		botonMostrar: false
	};
	infoPersonal = {
		id: null,
		nombres: null,
		tipoDoc: null,
		documento: null,
		usuario: null,
		fechaNacimiento: null,
		contacto1: null,
		contacto2: null,
		correo: null,
		residencia: null,
		direccion: null,
		testDrive: null,
		color: null,
		hobby1: null,
		hobby2: null,
		profesion: null,
		ultimaSesion: null,
		sesiones: null,
		rol: null,
		genero: null,
		estado: null
	};
	permisosFinalizarNegocio = ['N', 'CL'];
	permisosIniciarASesoria = ['N', 'CL'];

	botonFinalizarIngreso: boolean = true;
	permisosEdicion: boolean = false;
	botonEditarCreditos: boolean = false;
	botonEditarProcesosVenta: boolean = false;
	botonAnularProcesosVenta: boolean = false;
	botonNuevaCotizacion: boolean = false;
	edicionEstudioCredito: boolean = false;
	camposProcesoVentasChevy: boolean = false;
	nombreObjecion: string = '';

	/* Paginador Estudios de credito */
	numPaginaEstadosCredito: number = 0;
	numEstudiosCreditoTotal: number = 0;
	numEstudiosCreditoIni: number = 0;
	numEstudiosCreditoFin: number = 5;
	paginaActualEstudioCredito: number = 1;
	numTotalPagEstudioCredito: number = 0;

	/* Paginador Procesos de Ventas */
	numPaginaProcesosVenta: number = 0;
	numProcesosVentaTotal: number = 0;
	numProcesosVentaIni: number = 0;
	numProcesosVentaFin: number = 5;
	paginaActualProcesosVenta: number = 1;
	numTotalPagProcesosVenta: number = 0;

	/* Boton Cerrar Negocio */
	existeProcesoVentaBoton: boolean = true;

	/* Eventos Comerciales */
	eventosComercialesArray: any[] = [];
	consultaVaciaCargarSpinner: boolean = false;
	confirmarBotonEventoComercial: boolean = true;
	eventoDetallado = {
		id: null,
		titulo: null,
		clienteId: null,
		clienteDocumento: null,
		clienteNombres: null,
		fechaRegistro: null,
		fechaEvento: null,
		fechaConfirmacion: null,
		hora: null,
		tipoEvento: null,
		registradoPor: null,
		registradoA: null,
		estado: null,
		colorEstado: null,
		observacion: null
	};
	totalItemCountEventos = 0;
	pagePrevEventos: any;
	pageNextEventos: any;
	pageActualEventos: any;
	totalPagesEventos: any;
	existenEventosComerciales: boolean = false;

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _formBuilder: FormBuilder,
		private _clienteService: ClienteService,
		private _eventosComercialesService: EventosComercialesService,
		private _procesoVentasService: ProcesoVentasService,
		private _tableroAsignacionService: TableroAsignacionService,
		private _parametrizacionService: ParametrizacionService,
		private _sweetAlertService: SweetAlertService
	) {
		this.idIngreso = this._route.snapshot.paramMap.get('idIngreso');
		localStorage.setItem('idIngreso', this.idIngreso);

		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			this.permisoVerFicha = identityData['data'];
			this.ocultarBotonesCliente = this.permisoVerFicha.rol.valor;

			if (!this.permisosIniciarASesoria.includes(this.ocultarBotonesCliente)) {
				this.consultarDisponibilidadAsesor(this.permisoVerFicha.sub);
			}
			//if (!this.permisosIniciarASesoria.includes(this.ocultarBotonesCliente)) {
			if ((this.ocultarBotonesCliente == 'N') || (this.ocultarBotonesCliente == 'CL')) {
				this.botonIniciarAsesoria = false;
				this.botonFinalizarIngreso = false;
			}
			else {
				this.botonIniciarAsesoria = true;
				this.botonFinalizarIngreso = true;
			}
		}
	}

	ngOnInit() {
		if (localStorage.getItem('identity_crm')) {
			if (this.permisosFinalizarNegocio.includes(this.ocultarBotonesCliente)) {
				this.botonFinalizarIngreso = false;
			}
			this.comprobarEstadoBotonFinalizar();
			this.fichaClientes();
		}
		this.finalizarAtencionForm = this._formBuilder.group({
			objecion: ['NN', Validators.required],
			tipificacion: ['Ninguno'],
			observacion: [''],
		});
		this.cerrarNegocioForm = this._formBuilder.group({
			estadoCompra: ['', Validators.required],
			kilometraje: ['', [Validators.pattern("[0-9]{1,15}$")]],
			observacion: [''],
		});
	}

	/* Inicia Seguimiento del negocio */
	async fichaClientes() {
		this.loading = true;
		const responseFichaCliente = await this._clienteService.buscarIdIngresoCliente(this.idIngreso);
		//console.log(responseFichaCliente);
		if (responseFichaCliente["status"] === "success" && responseFichaCliente["code"] === "200") {
			this.llenarInfoIngreso(responseFichaCliente['data'].informacionIngreso);
			this.llenarInfoPersonal(responseFichaCliente['data'].informacionPersonal);
			this.llenarInfoCotizaciones(responseFichaCliente['data'].informacionCotizaciones);
			this.llenarInfoEstudioCreditos(responseFichaCliente['data'].informacionEstudioCreditos);
			this.llenarInfoProcesos(responseFichaCliente['data'].informacionProcesos);
			this.llenarInfoReconsiderados(responseFichaCliente['data'].informacionReconsiderados);
			this.llenarInfoAccesorios(responseFichaCliente['data'].informacionAccesorios);
			if (this.ocultarBotonesCliente != 'CL') {
				this.listarAtencionesIngreso(1);
			} else this.noDatosAtenciones = true;
			this.loading = false;
		}
		else if (responseFichaCliente["status"] === "success" && responseFichaCliente["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseFichaCliente["message"], 'warning')
				.then(result => {
					this._router.navigate(["clientes/buscar"]);
				}).catch(swal.noop);
		}
		else if (responseFichaCliente["status"] === "error" && responseFichaCliente["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseFichaCliente['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseFichaCliente["message"], 'error')
				.then(result => {
					this._router.navigate(["clientes/buscar"]);
				}).catch(swal.noop);
		}
	}

	llenarInfoIngreso(data: any) {
		let permisosBotonNewCotizacion: string[] = ['N', 'CL'];
		if (!permisosBotonNewCotizacion.includes(this.ocultarBotonesCliente)) {
			this.botonNuevaCotizacion = true;
		}
		if (this.ocultarBotonesCliente == 'S') {
			if (this.permisoVerFicha.sub != data.asesor.id) {
				this.clienteNoPertenece = true;
			}
		}

		this.infoIngreso.id = data.id;
		this.infoIngreso.nombres = data.persona.nombres + ' ' + data.persona.apellidos;
		this.infoIngreso.tipoDoc = data.persona.tipoDocumento.valor;
		this.infoIngreso.documento = data.persona.documento;
		this.infoIngreso.fechaIngreso = data.fechaIngreso;
		this.infoIngreso.fechaSalida = data.fechaSalida;
		this.infoIngreso.tarjetaOpciones = data.tarjetaOpciones.nombre;
		this.infoIngreso.asesor = data.asesor.nombres + ' ' + data.asesor.apellidos;
		this.infoIngreso.canalIngreso = data.canalIngreso.nombre;
		this.infoIngreso.observacion = data.observacion;
		this.infoIngreso.botonMostrar = data.fechaSalida == '0000-00-00' ? true : false;
		this.infoIngreso.estadoCompra = data.estadoCompra;
		// 1 fechaSalida de ingreso sin cerrar
		// 2 fechaSalida de ingreso cerrado
		this.ingresoFinalizado = data.fechaSalida == '0000-00-00' ? 1 : 2;
		this.finalizarAtencionVentana = data.fechaSalida == '0000-00-00' ? true : false;
	}

	llenarInfoPersonal(data: any) {
		if (data.length > 0) {
			this.noDatosPersonales = false;
			this.infoPersonal.id = data[0].id;
			this.infoPersonal.nombres = data[0].nombres + ' ' + data[0].apellidos;
			this.infoPersonal.tipoDoc = data[0].tipoDocumento.valor;
			this.infoPersonal.documento = data[0].documento;
			this.infoPersonal.usuario = data[0].usuario;
			this.infoPersonal.fechaNacimiento = data[1].fechaNacimiento == '' ? "- - -" : data[1].fechaNacimiento;
			this.infoPersonal.contacto1 = data[1].contactoPrincipal == '' ? "- - -" : data[1].contactoPrincipal;
			this.infoPersonal.contacto2 = data[1].contactoSecundario == '' ? "- - -" : data[1].contactoSecundario;
			this.infoPersonal.correo = data[1].correo == '' ? "- - -" : data[1].correo;
			this.infoPersonal.residencia = data[1].lugarResidencia == '' ? "- - -" : data[1].lugarResidencia;
			this.infoPersonal.direccion = data[1].direccion == '' ? "- - -" : data[1].direccion;
			this.infoPersonal.testDrive = data[1].testDrive == true ? 'Sí' : 'No';
			this.infoPersonal.color = data[1].color.nombre == 'Sin Asignar' ? '- - -' : data[1].color.nombre;
			this.infoPersonal.hobby1 = data[1].hobby1.nombre == 'Sin Asignar' ? '- - -' : data[1].hobby1.nombre;
			this.infoPersonal.hobby2 = data[1].hobby2.nombre == 'Sin Asignar' ? '- - -' : data[1].hobby2.nombre;
			this.infoPersonal.profesion = data[1].profesion.nombre == 'Sin Asignar' ? '- - -' : data[1].profesion.nombre;
			this.infoPersonal.ultimaSesion = data[0].ultimaSesion == '0000-00-00' ? '- - -' : data[0].ultimaSesion;
			this.infoPersonal.sesiones = data[0].sesiones == 0 ? 'Aún no ingresa' : data[0].sesiones;
			this.infoPersonal.rol = data[0].rol.nombre;
			this.infoPersonal.genero = this.devolverNombreGenero(data[1].genero);
			this.infoPersonal.estado = data[0].estado.nombre;

		} else {
			this.noDatosPersonales = true;
		}
	}

	llenarInfoCotizaciones(data: any) {
		//console.log(data);
		if (data.length > 0) {
			this.comprobarEmpresaEdicion();
			this.noDatosCotizaciones = false;
			this.infoCotizaciones = null;
			this.infoCotizacionesArray = [];
			this.infoCotizaciones = data;

			this.infoCotizaciones.forEach((element) => {
				
				this.infoCotizacionesArray.push({
					id: element.id,
					numCotizacion: element.numCotizacion,
					fechaCotizacion: element.fechaCotizacion,
					fechaIngreso: element.ingresoConcesionario.fechaIngreso,
					fechaSalida: element.ingresoConcesionario.fechaSalida,
					pago: element.metodoPago.nombre,
					cuotaInicial: element.cuotaInicial,
					vehiculo: element.vehiculo.nombre,
					modelo: element.modelo,
					versionId: element.versionId.nombre,
					colorVehiculo: element.colorVehiculo.nombre,
					valorVehiculo: element.valorVehiculo,
					ultimoDigito: element.ultimoDigito,
					lugarMatricula: element.lugarMatricula,
					estado: element.estado,
					colorEtiqueta: this.darColorEstadoCotizacion(element.estado.valor)
				});
			});
		} else {
			this.noDatosCotizaciones = true;
		}
	}

	comprobarEmpresaEdicion() {
		let permisosBotonesCotizacion: string[] = ['A', 'GC', 'DN', 'CB'];
		let permisosBotonesCotizacionEmpresa: string[] = ['A', 'GC', 'DN', 'S'];

		let letraEmpresa = localStorage.getItem('business');

		switch (letraEmpresa) {
			case 'A':
				if (permisosBotonesCotizacion.includes(this.ocultarBotonesCliente)) {
					this.permisosEdicion = true;
				}
				break;
			case 'M':
				if (permisosBotonesCotizacionEmpresa.includes(this.ocultarBotonesCliente)) {
					this.permisosEdicion = true;
				}
				break;
			case 'T':
				if (permisosBotonesCotizacionEmpresa.includes(this.ocultarBotonesCliente)) {
					this.permisosEdicion = true;
				}
				break;
			default:
				this.permisosEdicion = false;
				break;
		}
	}

	llenarInfoEstudioCreditos(data: any) {
		if (data.length > 0) {
			let permisosEditarEstudiosCredito: string[] = ['A', 'GC', 'DN'];
			if (permisosEditarEstudiosCredito.includes(this.ocultarBotonesCliente)) {
				this.botonEditarCreditos = true;
			}

			this.noDatosEstudioCreditos = false;
			this.infoEstudioCreditos = null;
			this.infoEstudioCreditosArray = [];
			this.infoEstudioCreditos = data;
			let totalEstudiosCredito: number = 0;

			this.infoEstudioCreditos.forEach((element) => {
				this.infoEstudioCreditosArray.push({
					cotizacionId: element.cotizacion.id,
					cotizacion: element.cotizacion.numCotizacion,
					pagoCotizacion: element.cotizacion.metodoPago.nombre,
					estadoCotizacion: element.cotizacion.estado.valor,
					id: element.id,
					fechaContestacion: element.fechaContestacion,
					fechaRadicado: element.fechaRadicado,
					fechaDesembolso: element.fechaDesembolso,
					banco: element.banco.nombre,
					contestacion: element.contestacion.nombre,
					colorContestacion: this.darColorEstadoEvento(element.contestacion.valor),
					plan: element.plan.nombre,
					observacion: element.observacion,
					editarEstudioCredito: this.edicionEstudioCredito
				});
				totalEstudiosCredito = totalEstudiosCredito + 1;
			});
			//console.log(this.infoEstudioCreditosArray);
			this.numEstudiosCreditoTotal = totalEstudiosCredito;
			this.numTotalPagEstudioCredito = Math.ceil(totalEstudiosCredito / 5);
		} else {
			this.noDatosEstudioCreditos = true;
		}
	}

	llenarInfoProcesos(data: any) {
		//console.log(data);
		if (data.length > 0) {
			let permisosBotonEditarProcesosVenta: string[] = ['A', 'GC', 'DN', 'S'];
			if (permisosBotonEditarProcesosVenta.includes(this.ocultarBotonesCliente)) {
				this.botonEditarProcesosVenta = true;
			}

			let permisosBotonAnularProcesosVenta: string[] = ['A', 'GC', 'DN'];
			if (permisosBotonAnularProcesosVenta.includes(this.ocultarBotonesCliente)) {
				this.botonAnularProcesosVenta = true;
			}

			let letraEmpresa = localStorage.getItem('business');
			switch (letraEmpresa) {
				case 'A':
					this.camposProcesoVentasChevy = true;
					break;
				case 'M':
					this.camposProcesoVentasChevy = false;
					break;
				case 'T':
					this.camposProcesoVentasChevy = false;
					break;
				default:
					this.camposProcesoVentasChevy = false;
					break;
			}

			this.noDatosProcesos = false;
			this.infoProcesos = null;
			this.infoProcesosArray = [];
			this.infoProcesos = data;
			let totalProcesosVenta: number = 0;

			this.infoProcesos.forEach((element) => {
				this.infoProcesosArray.push({
					procesoId: element.id,
					cotizacionId: element.cotizacionId.id,
					numCotizacion: element.cotizacionId.numCotizacion,
					fechaInicioProceso: element.fechaInicioProceso,
					fechaEntrega: element.fechaEntrega == null ? '- - -' : element.fechaEntrega,
					fechaFacturacion: element.fechaFacturacion == null ? '- - -' : element.fechaFacturacion,
					fechaConsignacion: element.fechaConsignacion == null ? '- - -' : element.fechaConsignacion,
					fechaEnvioMatricula: element.fechaEnvioMatricula == null ? '- - -' : element.fechaEnvioMatricula,
					fechaRecepcionMatricula: element.fechaRecepcionMatricula == null ? '- - -' : element.fechaRecepcionMatricula,
					fechaFirmaContrato: element.fechaFirmaContrato == null ? '- - -' : element.fechaFirmaContrato,
					fechaActivacionChevyOnStar: element.fechaActivacionChevyOnStar == null ? '- - -' : element.fechaActivacionChevyOnStar,
					fechaSoat: element.fechaSoat == null ? '- - -' : element.fechaSoat,
					precioFinalVenta: element.precioFinalVenta,
					placa: element.placa,
					vin: element.vin,
					kilometraje: element.kilometrajeInicial,
					estado: element.estado.valor,
					estadoTemp: element.estado.nombre,
					observacion: element.observacion == null ? '- - -' : element.observacion
				});
				totalProcesosVenta = totalProcesosVenta + 1;
			});
			this.numProcesosVentaTotal = totalProcesosVenta;
			this.numTotalPagProcesosVenta = Math.ceil(totalProcesosVenta / 5);
		} else {
			this.noDatosProcesos = true;
		}
	}

	llenarInfoReconsiderados(data: any) {
		if (data.length > 0) {
			this.noDatosReconsiderados = false;
			this.infoReconsiderados = null;
			this.infoReconsideradosArray = [];
			this.infoReconsiderados = data;

			this.infoReconsiderados.forEach((element) => {
				this.infoReconsideradosArray.push({
					id: element.id,
					cotizacion: element.estudioCredito.numCotizacion,
					fechaReconsiderado: element.fechaReconsiderado,
					reconsiderado: element.reconsiderado.nombre,
					observacion: element.observacion == null ? 'Sin registrar' : element.observacion
				});
			});
		} else {
			this.noDatosReconsiderados = true;
		}
	}

	llenarInfoAccesorios(data: any) {
		if (data.length > 0) {
			this.noDatosAccesorios = false;
			this.infoAccesorios = null;
			this.infoAccesoriosArray = [];
			this.infoAccesorios = data;

			this.infoAccesorios.forEach((element) => {
				this.infoAccesoriosArray.push({
					cotizacion: element.estudioCredito.cotizacion.numCotizacion,
					contestacion: element.procesoVenta.estudioCredito.contestacion.nombre,
					plan: element.procesoVenta.estudioCredito.plan.nombre,
					fechaVenta: element.fechaVenta,
					fechaEntrega: element.procesoVenta.fechaEntrega,
					accesorioNombre: element.accesorio.nombre,
					accesorioReferencia: element.accesorio.referenciaFabrica,
					accesorioCosto: element.accesorio.costoFabrica
				});
			});
		} else {
			this.noDatosAccesorios = true;
		}

		//Llamar a los eventos del cliente
		this.listarEventosClientePorId('1');
	}
	/* Finaliza Seguimiento del negocio */

	/* Paginacion estudios credito */
	paginationEstudiosCredito(numeroTotal: number, operacion: string) {
		$(".main-panel").scrollTop(0);
		let numTempMas: number = 0, numTempMenos: number = 0;
		if (operacion === 'mas') {
			numTempMas = this.numEstudiosCreditoIni + numeroTotal;

			if (numTempMas >= this.numEstudiosCreditoTotal) {
				this.numEstudiosCreditoIni = 0;
				this.numEstudiosCreditoFin = 5;
				this.paginaActualEstudioCredito = 1;
			} else {
				this.numEstudiosCreditoIni = this.numEstudiosCreditoIni + numeroTotal;
				this.numEstudiosCreditoFin = this.numEstudiosCreditoFin + numeroTotal;
				this.paginaActualEstudioCredito = this.paginaActualEstudioCredito + 1;
			}
		}
		else if (operacion === 'menos') {
			numTempMenos = this.numEstudiosCreditoIni - numeroTotal;
			if (numTempMenos <= 0) {
				this.numEstudiosCreditoIni = 0;
				this.numEstudiosCreditoFin = 5;
				this.paginaActualEstudioCredito = 1;
			} else {
				this.numEstudiosCreditoIni = this.numEstudiosCreditoIni - numeroTotal;
				this.numEstudiosCreditoFin = this.numEstudiosCreditoFin - numeroTotal;
				this.paginaActualEstudioCredito = this.paginaActualEstudioCredito - 1;
			}
		}
	}

	/* Paginador procesos de venta */
	paginationProcesosVenta(numeroTotal: number, operacion: string) {
		$(".main-panel").scrollTop(0);
		let numTempMas: number = 0, numTempMenos: number = 0;
		if (operacion === 'mas') {
			numTempMas = this.numProcesosVentaIni + numeroTotal;

			if (numTempMas >= this.numProcesosVentaTotal) {
				this.numProcesosVentaIni = 0;
				this.numProcesosVentaFin = 5;
				this.paginaActualProcesosVenta = 1;
			} else {
				this.numProcesosVentaIni = this.numProcesosVentaIni + numeroTotal;
				this.numProcesosVentaFin = this.numProcesosVentaFin + numeroTotal;
				this.paginaActualProcesosVenta = this.paginaActualProcesosVenta + 1;
			}
		}
		else if (operacion === 'menos') {
			numTempMenos = this.numProcesosVentaIni - numeroTotal;
			if (numTempMenos <= 0) {
				this.numProcesosVentaIni = 0;
				this.numProcesosVentaFin = 5;
				this.paginaActualEstudioCredito = 1;
			} else {
				this.numProcesosVentaIni = this.numProcesosVentaIni - numeroTotal;
				this.numProcesosVentaFin = this.numProcesosVentaFin - numeroTotal;
				this.paginaActualEstudioCredito = this.paginaActualEstudioCredito - 1;
			}
		}
	}

	/* Formulario de Cerrar el negocio */
	finalizarIngreso() {
		swal({
			title: '¿Esta seguro que desea finalizar el negocio?',
			text: 'Tenga en cuenta que esta acción no se podrá revertir',
			type: 'question',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: 'Sí',
			cancelButtonText: 'Cancelar',
			buttonsStyling: true,
			reverseButtons: true
		}).then((result) => {

			if (result.value) {
				$("#modalCerrarNegocio").modal("show");
			}

		}).catch(swal.noop);
	}

	async onSubmitCerrarNegocio() {
		this.loading = true;

		let ingresoId = '' + this.idIngreso;
		let estadoCompra = this.cerrarNegocioForm.value.estadoCompra;
		let kilometraje = this.cerrarNegocioForm.value.kilometraje;
		let observacion = this.cerrarNegocioForm.value.observacion;

		if (kilometraje === '' || estadoCompra === 'NO') {
			kilometraje = '';
		}
		observacion = observacion == null ? '' : observacion;
		//console.log(`idIngreso: ${ingresoId}, kilometraje: ${kilometraje}, observacion: ${observacion} y estadoCompra: ${estadoCompra}`);
		const responseFinalizarIngresoOut = await this._clienteService.finalizarIngresoSalida(ingresoId, kilometraje, observacion, estadoCompra);
		//console.log(responseFinalizarIngresoOut);
		if (responseFinalizarIngresoOut["status"] === "success" && responseFinalizarIngresoOut["code"] === "200") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Bien!', 'El negocio se ha finalizado', 'OK', 'success');
			$("#modalCerrarNegocio").modal("hide");
			this.fichaClientes();
		}
		else if (responseFinalizarIngresoOut["status"] === "success" && responseFinalizarIngresoOut["code"] === "300") {
			this.loading = false;
			//this._sweetAlertService.showNotification('top', 'right', 'danger', 'warning', '<b>' + responseFinalizarIngresoOut['message'] + '<b>', 2000);
			swal('¡Bien!', responseFinalizarIngresoOut["message"], 'warning')
				.then((result) => {
					$("#modalCerrarNegocio").modal("hide");
					this.cerrarNegocioForm.reset();
				});
		}
		else if (responseFinalizarIngresoOut["status"] === "error" && responseFinalizarIngresoOut["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseFinalizarIngresoOut['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseFinalizarIngresoOut['message'] + '<b>', 2000);
		}
	}

	async comprobarEstadoBotonFinalizar() {
		this.loading = true;
		const responseComprobarProcesoVenta = await this._clienteService.comprobarProcesoVentaActivo(this.idIngreso);
		//console.log(responseComprobarProcesoVenta);
		if (responseComprobarProcesoVenta["status"] === "success" && responseComprobarProcesoVenta["code"] === "200") {
			this.loading = false;
			this.existeProcesoVentaBoton = responseComprobarProcesoVenta["data"].cierreProceso;
		}
		else if (responseComprobarProcesoVenta["status"] === "success" && responseComprobarProcesoVenta["code"] === "300") {
			this.loading = false;
			this.existeProcesoVentaBoton = false;
			//this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseComprobarProcesoVenta['message'] + '<b>', 2000);
		}
		else if (responseComprobarProcesoVenta["status"] === "error" && responseComprobarProcesoVenta["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseComprobarProcesoVenta['message']);
		}
		else {
			this.loading = false;
			this.existeProcesoVentaBoton = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseComprobarProcesoVenta['message'] + '<b>', 2000);
		}
	}

	/* Inicio Consultar disponibilidad del usuario si esta en alguna atencion */
	async consultarDisponibilidadAsesor(idAsesor: any) {
		this.loading = true;
		const disponibilidaAsesor = await this._tableroAsignacionService.consultarDisponibilidadAsesor(idAsesor);
		//console.log(disponibilidaAsesor);
		if (disponibilidaAsesor["status"] === "success" && disponibilidaAsesor["code"] === "200") {
			this.loading = false;
			if (disponibilidaAsesor['data'].disponibilidad.estado == true) {
				const responseAtenciones = this.atencionesAsesorArray(disponibilidaAsesor['data'].disponibilidad.atenciones);
				//console.log(responseAtenciones);
				this.nombreUsuarioAtencion = disponibilidaAsesor['data'].nombre;
				this.idAtencion = responseAtenciones.id;
				this.nombreCliente = responseAtenciones.cliente;
				this.documento = responseAtenciones.documento;
				this.fechaInicial = responseAtenciones.fechaInicial;
				this.botonIniciarAsesoria = false;
				this.finalizarAtencionVentana = true;
				this.cargarInformacionSelect();
			}
			else {
				this.botonIniciarAsesoria = true;
			}
		}
		else if (disponibilidaAsesor["status"] === "error" && disponibilidaAsesor["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(disponibilidaAsesor['message']);
		}
		else {
			this.loading = false;
			this.botonIniciarAsesoria = false;
		}
		/* Se debe cargar otra vez las atenciones a la ficha del cliente */
		this.listarAtencionesIngreso(1);
	}

	atencionesAsesorArray(atenciones): any {
		let atencionesAsesor = {
			id: null,
			cliente: null,
			documento: null,
			fechaInicial: null
		};
		atenciones.forEach((element) => {
			if (element.fechaFinal == '0000-00-00') {
				atencionesAsesor.id = element.id;
				atencionesAsesor.cliente = element.ingreso.persona.nombres + ' ' + element.ingreso.persona.apellidos;
				atencionesAsesor.documento = element.ingreso.persona.documento;
				atencionesAsesor.fechaInicial = element.fechaInicial;
			}
		});

		return atencionesAsesor;
	}
	/* Fin Consultar disponibilidad del usuario si esta en alguna atencion */

	crearCotizacion() {
		this._router.navigate(["clientes/solicitar-cotizacion/", this.idIngreso, this.ingresoFinalizado]);
	}

	verCotizaciones() {
		this._router.navigate(["clientes/listar-cotizaciones/", this.idIngreso, this.ingresoFinalizado]);
	}

	crearEstudioCredito(rowId) {
		this._router.navigate(["clientes/registrar-estudio-credito/", rowId, this.ingresoFinalizado]);
	}

	listarEstudioCredito(rowId) {
		this._router.navigate(["clientes/listar-estudio-credito/", rowId, this.ingresoFinalizado]);
	}

	crearReconsiderado(rowId) {
		this._router.navigate(["clientes/registrar-reconsiderado/", rowId, this.ingresoFinalizado]);
	}

	listarReconsiderado(rowId) {
		this._router.navigate(["clientes/listar-reconsiderado/", rowId, this.ingresoFinalizado]);
	}

	listarProcesoVentas(rowIdCotizacion: any) {
		this._router.navigate(["clientes/listar-proceso-ventas/", rowIdCotizacion, this.idIngreso, this.ingresoFinalizado]);
	}

	buscarIdInformacionPersonal(rowId, opc) {
		localStorage.setItem('ficha', 'volver');
		this._router.navigate(["usuarios/actualizar-informacion/", rowId, opc]);
	}

	/* Inicia Procesos de Ventas */
	crearProcesoVentas(rowIdCotizacion: any) {
		swal({
			title: 'Crear Proceso de Ventas',
			text: "¿Esta seguro de registrar un proceso de ventas?",
			type: 'question',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: 'Si, registrar!',
			cancelButtonText: 'Cancelar',
			buttonsStyling: true,
			reverseButtons: true
		}).then((result) => {
			if (result.value) {
				this.crearProcesoVentasDatos(rowIdCotizacion);
			}
		});
	}

	async crearProcesoVentasDatos(idCotizacion: any) {
		this.loading = true;
		const responseProcesoVentas = await this._procesoVentasService.registrarProcesoVentas(idCotizacion);
		//console.log(responseProcesoVentas);
		if (responseProcesoVentas["status"] === "success" && responseProcesoVentas["code"] === "200") {
			this.loading = false;
			let idProcesoVenta = responseProcesoVentas["data"].id;
			//this._router.navigate(["clientes/listar-proceso-ventas/", idCotizacion, this.idIngreso, this.ingresoFinalizado]);
			localStorage.setItem('volverRuta', 'clientes/ficha-clientes/');
			this._router.navigate(["clientes/contestacion/", idProcesoVenta, idCotizacion, 'PV']);
		}
		else if (responseProcesoVentas["status"] === "success" && responseProcesoVentas["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseProcesoVentas["message"] + '. Si desea otro Proceso de Venta, anule el actual y genere otro.', 'warning');
		}
		else if (responseProcesoVentas["status"] === "error" && responseProcesoVentas["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseProcesoVentas['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseProcesoVentas["message"], 'error');
		}
	}
	/* Finaliza Procesos de Ventas */

	/* Editar Estudios de Credito */
	editarEstudioCredito(rowId: string, rowCotizacion: string) {
		localStorage.setItem('volverRuta', 'clientes/ficha-clientes/');
		this._router.navigate(["clientes/contestacion/", rowId, rowCotizacion, 'EC']);
	}

	/* Editar Procesos de Venta */
	editarProcesosVenta(rowId: string, rowCotizacion: string) {
		localStorage.setItem('volverRuta', 'clientes/ficha-clientes/');
		this._router.navigate(["clientes/contestacion/", rowId, rowCotizacion, 'PV']);
	}

	/* Inicia Finalizar atencion */
	finalizarAtencion(idAtencion) {
		$("#modalFinalizarAtencion").modal("show");
	}

	async cargarInformacionSelect() {
		this.loading = true;
		this.objecionesArray = [];
		this.tipificacionArray = [];
		const objecionesParametros: Object = await this._parametrizacionService.listarCategoriaParametrizacion('Objeciones');
		const tipificacionParametros: Object = await this._parametrizacionService.listarCategoriaParametrizacion('Tipificación Clientes');

		objecionesParametros['data'].forEach((element) => {
			this.objecionesArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
		tipificacionParametros['data'].forEach((element) => {
			this.tipificacionArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
		this.loading = false;
	}

	async onSubmitFinalizarAtencion() {
		this.loading = true;
		let objecion_ = this.finalizarAtencionForm.value.objecion;
		//let tipificacion_ = this.finalizarAtencionForm.value.tipificacion;
		let tipificacion_ = 'Ninguno';
		let observacion_ = this.finalizarAtencionForm.value.observacion;
		observacion_ = observacion_ == null ? '' : observacion_;
		//console.log(`${this.idAtencion}, ${objecion_}, ${observacion_}`);
		const responseFinalizarAtencion = await this._tableroAsignacionService.finalizarAtencionAsesor(this.idAtencion, objecion_, tipificacion_, observacion_);
		//console.log(responseFinalizarAtencion);
		if (responseFinalizarAtencion["status"] === "success" && responseFinalizarAtencion["code"] === "200") {
			this.consultarDisponibilidadAsesor(this.permisoVerFicha.sub);
			$("#modalFinalizarAtencion").modal("hide");
			this.finalizarAtencionForm.reset();
			this._sweetAlertService.showNotification('top', 'right', 'success', 'success', '<b>' + responseFinalizarAtencion['message'] + '<b>', 2000);
			this.loading = false;
		}
		else if (responseFinalizarAtencion["status"] === "error" && responseFinalizarAtencion["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseFinalizarAtencion['message']);
		}
		else {
			//this.ventanaAsesorDisponible = false;
			$("#modalFinalizarAtencion").modal("hide");
			this.finalizarAtencionForm.reset();
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseFinalizarAtencion['message'] + '<b>', 2000);
			this.loading = false;
		}
	}
	/* Finaliza Finalizar atencion */

	/* Inicia Listar todas las atenciones del cliente con paginacion */
	async listarAtencionesIngreso(pagination: any) {
		this.loading = true;
		const responseAtencionesIngreso = await this._tableroAsignacionService.listarAtencionesPorIngreso(this.idIngreso, pagination);
		//console.log(responseAtencionesIngreso);
		if (responseAtencionesIngreso["status"] === "success" && responseAtencionesIngreso["code"] === "200") {
			this.llenarAtencionesDatos(responseAtencionesIngreso['data'], pagination);
			this.loading = false;
			this.noDatosAtenciones = false;
		}
		else if (responseAtencionesIngreso["status"] === "success" && responseAtencionesIngreso["code"] === "300") {
			this.loading = false;
			this.noDatosAtenciones = true;
			//console.log('No existen atenciones');
		}
		else if (responseAtencionesIngreso["status"] === "error" && responseAtencionesIngreso["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseAtencionesIngreso['message']);
		}
		else {
			this.loading = false;
			this.noDatosAtenciones = true;
			//console.log('No existen atenciones');
		}
	}

	llenarAtencionesDatos(atenciones: any, page: any) {
		//console.log(atenciones);

		if (atenciones.atenciones.length > 0) {
			//console.log(atenciones);
			let ultimaAtencion = atenciones.atenciones[0];
			this.nombreObjecion = ultimaAtencion.objecion.nombre;
			this.totalAtencionesArray = atenciones.atenciones;
			this.total_item_count = atenciones['total_item_count'];
			this.page_actual = page;

			this.total_pages = atenciones['total_pages'];

			if (page >= 2) this.page_prev = page - 1;
			else this.page_prev = 1;

			if (page < atenciones['total_pages']) this.page_next = page + 1;
			else {
				if (atenciones['total_pages'] == 0) this.page_next = 1;
				else this.page_next = atenciones['total_pages'];
			}
		} else {
			this.noDatosAtenciones = true;
		}

	}
	/* Finaliza Listar todas las atenciones del cliente con paginacion */

	/* Inicio Crear atencion al cliente */
	agregarAtencionAlUsuario() {
		swal({
			title: 'Atención a Clientes',
			text: '¿Desea atender al cliente ahora?',
			type: 'question',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-warning',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: 'Sí atender',
			cancelButtonText: 'No atender',
			buttonsStyling: true,
			reverseButtons: true
		}).then((result) => {
			if (result.value) {
				swal({
					title: 'Escribir una observación para la asignación',
					html: '<div class="form-group">' +
						'<input id="input-field" type="text" class="form-control" />' +
						'</div>',
					showCancelButton: true,
					confirmButtonClass: 'btn btn-success',
					cancelButtonClass: 'btn btn-danger',
					buttonsStyling: false
				})
					.then((result) => {
						let observacionAtencion = $('#input-field').val();
						this.onSubmitAtenderAlCliente(observacionAtencion);
					}).catch(swal.noop);
			}
		}).catch(swal.noop);

	}

	async onSubmitAtenderAlCliente(AtencionObservacion) {
		this.loading = true;
		let tableroAsignacion = new TableroAsignacion(
			this.permisoVerFicha.sub,
			this.idIngreso,
			AtencionObservacion
		);

		const responseAsignacionAsesor = await this._tableroAsignacionService.registrarAsignacionOcupar(3, tableroAsignacion);
		//console.log(responseAsignacionAsesor);
		if (responseAsignacionAsesor["status"] === "success" && responseAsignacionAsesor["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseAsignacionAsesor["message"], 'success')
				.then((result) => {
					this.consultarDisponibilidadAsesor(this.permisoVerFicha.sub);
				});
		}
		else if (responseAsignacionAsesor["status"] === "success" && responseAsignacionAsesor["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseAsignacionAsesor['message'] + '<b>', 2000);
		}
		else if (responseAsignacionAsesor["status"] === "error" && responseAsignacionAsesor["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseAsignacionAsesor['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'error', 'danger', '<b>' + responseAsignacionAsesor['message'] + '<b>', 2000);
		}
	}
	/* Fin Crear atencion al cliente */

	/* Inicia Anular Proceso de Ventas */
	confirmarAnulacionProcesoVentas(idProcesoVentas: string) {
		swal({
			title: '¿Esta seguro que desea anular este Proceso de Venta?',
			text: 'Tenga en cuenta que esta acción no se podrá revertir',
			type: 'question',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: 'Sí',
			cancelButtonText: 'Cancelar',
			buttonsStyling: true,
			reverseButtons: true
		}).then((result) => {

			if (result.value) {
				this.anulacionProcesoVentas(idProcesoVentas);
			}

		}).catch(swal.noop);
	}

	async anulacionProcesoVentas(idProcesoVentas: string) {
		this.loading = true;

		const responseAnularProcesoVentas = await this._clienteService.anularProcesoVentas(idProcesoVentas);
		console.log(responseAnularProcesoVentas);
		if (responseAnularProcesoVentas["status"] === "success" && responseAnularProcesoVentas["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseAnularProcesoVentas["message"], 'success')
				.then((result) => {
					this.ngOnInit();
				});
		}
		else if (responseAnularProcesoVentas["status"] === "success" && responseAnularProcesoVentas["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseAnularProcesoVentas['message'] + '<b>', 2000);
		}
		else if (responseAnularProcesoVentas["status"] === "error" && responseAnularProcesoVentas["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseAnularProcesoVentas['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'error', 'danger', '<b>' + responseAnularProcesoVentas['message'] + '<b>', 2000);
		}
	}
	/* Finaliza Anular Proceso de Ventas */

	/* Inicia listado de Eventos del cliente */
	async listarEventosClientePorId(page: any) {
		this.loading = true;
		const responseListaEventosCliente = await this._eventosComercialesService.listarEventosComercialesCliente('' + this.infoPersonal.id, '' + page);
		//console.log(responseListaEventosCliente);
		if (responseListaEventosCliente["status"] === "success" && responseListaEventosCliente["code"] === "200") {
			this.loading = false;
			if (responseListaEventosCliente["data"] == 0) {
				this.existenEventosComerciales = false;
				this.eventosComercialesArray = [];
				this.pagePrevEventos = '1';
				this.pageNextEventos = '1';
				this.pageActualEventos = '1';
				this.totalPagesEventos = '1';
			}
			else {
				this.existenEventosComerciales = true;
				this.llenarTablaEventosComerciales(responseListaEventosCliente["data"], +page);
			}
		}
		else if (responseListaEventosCliente["status"] === "success" && responseListaEventosCliente["code"] === "300") {
			this.loading = false;
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			//this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseListaEventosCliente['message'] + '<b>', 2000);
		}
		else if (responseListaEventosCliente["status"] === "error" && responseListaEventosCliente["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseListaEventosCliente['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'error', 'danger', '<b>' + responseListaEventosCliente['message'] + '<b>', 2000);
		}
	}

	llenarTablaEventosComerciales(eventosComerciales: any, page: number) {
		//console.log(eventosComerciales);
		let eventosComercialesCliente: any[] = eventosComerciales['eventos'];
		this.eventosComercialesArray = [];
		this.totalItemCountEventos = eventosComerciales['total_item_count'];
		this.pageActualEventos = page;
		this.totalPagesEventos = eventosComerciales['total_pages'];

		if (page >= 2) { this.pagePrevEventos = page - 1; }
		else { this.pagePrevEventos = 1; }

		if (page < eventosComerciales['total_pages']) { this.pageNextEventos = page + 1; }
		else {
			if (eventosComerciales['total_pages'] == 0) { this.pageNextEventos = 1; }
			else { this.pageNextEventos = eventosComerciales['total_pages']; }
		}

		let jsonRegistro: any;

		eventosComercialesCliente.forEach(element => {
			jsonRegistro = {
				id: element.id,
				clienteId: element.cliente.id,
				clienteDocumento: element.cliente.tipoDocumento.valor + ': ' + element.cliente.documento,
				clienteNombres: element.cliente.nombres + ' ' + element.cliente.apellidos,
				fechaRegistro: element.fechaRegistro,
				fechaEvento: element.fechaEvento,
				fechaConfirmacion: element.fechaConfirmacion,
				hora: element.hora.hora,
				tipoEvento: element.tipoEvento.nombre,
				registradoPor: element.registradoPor.nombres + ' ' + element.registradoPor.apellidos,
				registradoA: element.registradoA.nombres + ' ' + element.registradoA.apellidos,
				estado: element.estado.nombre,
				colorEstado: this.darColorEstadoEventoComercial(element.estado.valor),
				titulo: element.titulo,
				observacion: element.observacion,
				confirmarEventoComercial: this.confirmarBotonEventoComercial
			};
			this.eventosComercialesArray.push(jsonRegistro);
		});

		if (this.total_item_count || this.total_item_count == 0) {
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
		}
	}

	darColorEstadoEventoComercial(estado: string): string {
		switch (estado) {
			case 'cancelada':
				this.confirmarBotonEventoComercial = false;
				return 'danger';
				break;
			case 'confirmada':
				this.confirmarBotonEventoComercial = false;
				return 'success';
				break;
			case 'reprogramada':
				this.confirmarBotonEventoComercial = false;
				return 'info';
				break;
			case 'no confirmada':
				this.confirmarBotonEventoComercial = true;
				return 'default';
				break;
			default:
				this.confirmarBotonEventoComercial = false;
				return 'default';
				break;
		}
	}

	visualizarRowAgenda(rowAgenda: any) {
		this.eventoDetallado.id = rowAgenda.id;
		this.eventoDetallado.titulo = rowAgenda.titulo;
		this.eventoDetallado.clienteId = rowAgenda.clienteId;
		this.eventoDetallado.clienteDocumento = rowAgenda.clienteDocumento;
		this.eventoDetallado.clienteNombres = rowAgenda.clienteNombres;
		this.eventoDetallado.fechaRegistro = rowAgenda.fechaRegistro;
		this.eventoDetallado.fechaEvento = rowAgenda.fechaEvento;
		this.eventoDetallado.fechaConfirmacion = rowAgenda.fechaConfirmacion;
		this.eventoDetallado.hora = rowAgenda.hora;
		this.eventoDetallado.tipoEvento = rowAgenda.tipoEvento;
		this.eventoDetallado.registradoPor = rowAgenda.registradoPor;
		this.eventoDetallado.registradoA = rowAgenda.registradoA;
		this.eventoDetallado.estado = rowAgenda.estado;
		this.eventoDetallado.colorEstado = rowAgenda.colorEstado;
		this.eventoDetallado.observacion = rowAgenda.observacion;
		//console.log(this.eventoDetallado);
		$("#modalInformacionDetallada").modal("show");
	}
	/* Finaliza listado de Eventos del cliente */

	darColorEstadoEvento(estado: string): string {
		switch (estado) {
			case 'rechazado':
				if (this.ocultarBotonesCliente == 'A') {
					this.edicionEstudioCredito = true;
				} else {
					this.edicionEstudioCredito = false;
				}
				return 'danger';
				break;
			case 'activo':
				this.edicionEstudioCredito = true;
				return 'warning';
				break;
			case 'aprobado':
				this.edicionEstudioCredito = true;
				return 'success';
				break;
			case 'desembolsado':
				if (this.ocultarBotonesCliente == 'A') {
					this.edicionEstudioCredito = true;
				} else {
					this.edicionEstudioCredito = false;
				}
				return 'info';
				break;
			case 'SAG':
				this.edicionEstudioCredito = true;
				return 'default';
				break;
			default:
				if (this.ocultarBotonesCliente == 'A') {
					this.edicionEstudioCredito = true;
				} else {
					this.edicionEstudioCredito = false;
				}
				return 'default';
				break;
		}
	}

	darColorEstadoCotizacion(estado: string): string {
		switch (estado) {
			case 'INC':
				return 'info';
				break;
			case 'EE':
				return 'warning';
				break;
			case 'EP':
				return 'success';
				break;
			case 'RC':
				return 'danger';
				break;
			case 'AB':
				return 'optimus-black';
				break;
			case 'CR':
				return 'default';
				break;
			default:
				return 'default';
				break;
		}
	}

	devolverNombreGenero(valorBuscar: string): string {
		let devolverCadena: string = '';
		const generoNombre = this.generoArray.find(element => element.valor == valorBuscar);

		if (generoNombre) devolverCadena = generoNombre.nombre;
		else devolverCadena = valorBuscar;
		return devolverCadena;
	}

	agregarEventoAlUsuario() {
		this._router.navigate(["eventos-comerciales/registrar/", this.infoPersonal.id]);
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	regresar() {
		if (localStorage.getItem('rutaSN')) {
			let volverRutaAtras = localStorage.getItem('rutaSN');
			localStorage.removeItem('rutaSN');
			this._router.navigate([volverRutaAtras]);
		}
		else this._router.navigate(["clientes/ingresos/", this.infoPersonal.id]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}