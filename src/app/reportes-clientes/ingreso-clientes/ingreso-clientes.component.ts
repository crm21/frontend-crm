import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { DatePipe } from "@angular/common";
import { Router } from "@angular/router";
import swal from "sweetalert2";
import { ReporteClientesService } from '../../services/reporte-clientes/reporte-clientes.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare var $: any;
declare interface TableData {
	headerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-ingreso-clientes',
	templateUrl: './ingreso-clientes.component.html',
	styleUrls: ['./ingreso-clientes.component.css']
})
export class IngresoClientesComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	dataTable: TableData;
	buscarFiltrosForm: FormGroup;
	reporteArray: any[] = [];
	todosAsesoresArray: any[] = [];
	fechaInicial: string = '';
	fechaFinal: string = '';

	constructor(
		private _reporteClientesService: ReporteClientesService,
		private _sweetAlertService: SweetAlertService,
		private _datepipe: DatePipe,
		private _router: Router
	) { this.datosTabla(); }

	ngOnInit() {
		if (localStorage.getItem('identity_crm')) {
			let fechaActual = new Date();
			this.buscarFiltrosForm = new FormGroup({
				fechaInicial: new FormControl('', Validators.required),
				fechaFinal: new FormControl('', Validators.required),
			});
			//this.listarReportesClientesIngresados('','');
		}
	}

	datosTabla() {
		this.dataTable = {
			headerRow: [
				"#",
				"Documento",
				"Cliente",
				"Fecha Ingreso",
				"Duración Última Atención",
				"Objeción",
				"Observaciones",
				"Asesor",
				"Total Atenciones",
				"Atenciones"
			],
			dataRows: []
		};
	}

	onSubmitBuscarReporte() {
		let fechaIni, fechaFin;
		fechaIni = this.fechasValidacion(this.buscarFiltrosForm.value.fechaInicial);
		fechaFin = this.fechasValidacion(this.buscarFiltrosForm.value.fechaFinal);
		if (this.validarFechasSuperior(fechaIni, fechaFin)) {
			//this.listarReportesClientesIngresados(fechaIni, fechaFin);
		} else {
			//swal('Advertencia!', 'La fecha inicial debe ser inferior a la fecha final', 'warning');
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + 'La fecha inicial debe ser inferior a la fecha final' + '<b>', 2000);
		}
		//console.log(`${fechaIni}, ${fechaFin}`);
	}

	async listarReportesClientesIngresados(fechaIni: string, fechaFin: string) {
		this.loading = true;
		const responseReporteClientesIngresados = await this._reporteClientesService.listarReporteIngresoClientes(fechaIni, fechaFin);
		//console.log(responseReporteClientesIngresados);
		if (responseReporteClientesIngresados['status'] == "success" && responseReporteClientesIngresados['code'] == "200") {
			this.llenarDatosTabla(responseReporteClientesIngresados);
			this.loading = false;
		}
		else if (responseReporteClientesIngresados['status'] == "success" && responseReporteClientesIngresados['code'] == "300") {
			this.loading = false;
			this.dataTable.dataRows = [];
			//swal('Advertencia!', responseReporteClientesIngresados['message'], 'warning');
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseReporteClientesIngresados['message'] + '<b>', 2000);
			//console.log(responseReporteClientesIngresados);
		}
		else if (responseReporteClientesIngresados["status"] === "error" && responseReporteClientesIngresados["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseReporteClientesIngresados['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			swal('¡Error!', responseReporteClientesIngresados['message'], 'error');
			//console.log(responseReporteClientesIngresados);
		}
	}

	llenarDatosTabla(reporteBdc: any) {
		this.dataTable.dataRows = [];
		this.reporteArray = reporteBdc['data'];
		let JSONParam, tiempoTem, objecionTem, asesorTem, observacionTem;

		this.reporteArray.forEach(element => {
			if ((element.ultimaAtencion == 0) || (element.ultimaAtencion == null)) {
				tiempoTem = '---';
				objecionTem = '---';
				asesorTem = '---';
				observacionTem = '---';
			} else {
				tiempoTem = element.ultimaAtencion.tiempoAtencion;
				objecionTem = element.ultimaAtencion.objecion.nombre;
				asesorTem = element.ultimaAtencion.asesor.nombres + ' ' + element.ultimaAtencion.asesor.apellidos;
				observacionTem = element.ultimaAtencion.observacion;
			}
			JSONParam = {
				id: element.idIngreso,
				tipoDoc: element.cliente.tipoDocumento.valor,
				documento: element.cliente.documento,
				cliente: element.cliente.nombres + ' ' + element.cliente.apellidos,
				fechaIngreso: element.fechaIngreso,
				tiempo: tiempoTem,
				objecion: objecionTem,
				asesor: asesorTem,
				observacion: observacionTem,
				atenciones: element.atenciones,
				total: element.total
			};
			this.dataTable.dataRows.push(JSONParam);
		});
		//console.log(this.dataTable.dataRows);
	}


	limpiarFiltros() {
		this.buscarFiltrosForm.reset();
		this.buscarFiltrosForm.value.fechaInicial = null;
		this.buscarFiltrosForm.value.fechaFinal = null;
		//this.listarReportesClientesIngresados('', '');
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	fechasValidacion(formatoFecha) {
		if (formatoFecha) {
			//Formatear fecha
			const fechaActualTem = this._datepipe
				.transform(formatoFecha, "yyyy/MM/dd")
				.replace(/\//g, "-");
			//console.log("fecha formato: "+fechaActualTem);
			return fechaActualTem;
		}
		return '';
	}

	validarFechasSuperior(fechaStart: any, fechaEnd: any) {
		const fechaStart1 = new Date(fechaStart);
		const fechaEnd1 = new Date(fechaEnd);

		const diaInicio = fechaStart1.getDate();
		const mesInicio = fechaStart1.getMonth() + 1;
		const anioInicio = fechaStart1.getFullYear();
		const fechaStart1Tem = diaInicio + "-" + mesInicio + "-" + anioInicio;

		const diaFin = fechaEnd1.getUTCDate();
		const mesFin = fechaEnd1.getUTCMonth() + 1;
		const anioFin = fechaEnd1.getUTCFullYear();
		const fechaEnd1Tem = diaFin + "-" + mesFin + "-" + anioFin;

		//console.log("fechaInicio: "+fechaStart1Tem);
		//console.log("fechaFinal: "+fechaEnd1Tem);

		if (!(anioFin > anioInicio)) {
			if (anioFin < anioInicio) return false;

			if (!(mesFin > mesInicio)) {
				if (mesFin < mesInicio) return false;

				if (!(diaFin > diaInicio)) return false;
				return true;
			}
			return true;
		}
		return true;
	}

	verFichaCliente(idIngreso: any) {
		this._router.navigate(["clientes/ficha-clientes/", idIngreso]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}
