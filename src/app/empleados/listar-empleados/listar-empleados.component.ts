import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EmpleadosService } from '../../services/empleados/empleados.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare var $: any;
declare interface DataTable {
	headerRow: string[];
	footerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-listar-empleados',
	templateUrl: './listar-empleados.component.html',
	styleUrls: ['./listar-empleados.component.css']
})
export class ListarEmpleadosComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	consultaVaciaCargarSpinner: boolean = false;
	buscarEmpleadoFormGroup: FormGroup;
	dataTable: any;
	registros: any[] = [];
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	paginacionUsuarios: boolean = true;
	filtroBusqueda: string = '';
	opcion = 1;

	usuario = {
		sub: null,
		rol: null
	};

	constructor(
		private _router: Router,
		private _formBuilder: FormBuilder,
		private _empleadosService: EmpleadosService,
		private _sweetAlertService: SweetAlertService
	) {
		if (localStorage.getItem('identity_crm')) {
			this.listarEmpleadosPorFiltro(this.opcion, '', 1);
		}
	}

	ngOnInit() {
		this.dataTable = {
			headerRow: [
				'#',
				"Documento Cliente",
				"Nombres Completos",
				"Cargo",
				"Estado",
				"Acciones"
			],
			dataRows: []
		};
		this.buscarEmpleadoFormGroup = this._formBuilder.group({
			filtroEmpleado: ['', Validators.required]
		});
	}

	onSubmitBuscarEmpleado() {
		this.filtroBusqueda = this.buscarEmpleadoFormGroup.value.filtroEmpleado;
		this.opcion = 3;
		this.listarEmpleadosPorFiltro(this.opcion, this.filtroBusqueda, 1);
	}

	async listarEmpleadosPorFiltro(opc: any, filtro: any, page: any) {
		this.loading = true;

		const responseEmpleados = await this._empleadosService.listarEmpleados(opc, '', filtro, page);
		//console.log(responseEmpleados);
		if (responseEmpleados["status"] === "success" && responseEmpleados["code"] === "200") {
			this.llenarDatosTabla(page, responseEmpleados['data'], responseEmpleados['data'].empleados);
			this.loading = false;
		}
		else if (responseEmpleados["status"] === "success" && responseEmpleados["code"] === "300") {
			this.loading = false;
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseEmpleados['message'] + '<b>', 2000);
		}
		else if (responseEmpleados["status"] === "error" && responseEmpleados["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseEmpleados['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			this._sweetAlertService.alertGeneral('¡Error!', responseEmpleados["message"], 'OK', 'error');
		}
	}

	llenarDatosTabla(page: any, responseEmpleados: any, datosEmpleados: any) {
		this.registros = datosEmpleados;
		this.dataTable.dataRows = [];
		this.total_item_count = responseEmpleados['total_item_count'];
		this.page_actual = page;

		this.total_pages = responseEmpleados['total_pages'];

		if (page >= 2) {this.page_prev = page - 1;}
		else {this.page_prev = 1;}

		if (page < responseEmpleados['total_pages']) {this.page_next = page + 1;}
		else {
			if (responseEmpleados['total_pages'] == 0) {this.page_next = 1;}
			else {this.page_next = responseEmpleados['total_pages'];}
		}

		let jsonRegistro;

		this.registros.forEach(element => {
			jsonRegistro = {
				id: element.empleado.id,
				tipoDoc: element.empleado.tipoDocumento.valor,
				documento: element.empleado.documento,
				nombres: element.empleado.nombres,
				apellidos: element.empleado.apellidos,
				estado: element.empleado.estado.nombre,
				cargo: element.cargo == 0 ? 'Sin cargo' : element.cargo

			};
			this.dataTable.dataRows.push(jsonRegistro);
		});
		
		if (this.total_item_count || this.total_item_count == 0) {
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
		}
	}

	limpiarFormulario() {
		this.buscarEmpleadoFormGroup.reset();
		this.opcion = 1;
		this.listarEmpleadosPorFiltro(this.opcion, '', 1);
	}

	buscarIdCliente(rowIdEmpleado) {
		// opc = 2 para modificar empleado
		this._router.navigate(["empleados/actualizar/", rowIdEmpleado]);
	}

	fichaEmpleado(rowIdEmpleado) {
		this._router.navigate(["empleados/ficha-empleados/", rowIdEmpleado]);
	}

	crearEmpleado() {
		// opc = 1 para crear empleado
		this._router.navigate(["empleados/registrar/"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}