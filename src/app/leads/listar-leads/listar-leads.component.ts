import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { LeadsService } from '../../services/leads/leads.service';
import { TableroAsignacionService } from '../../services/tablero-asignacion/tablero-asignacion.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { ActualizarLeads } from '../../interfaces/interfaces';
import { ClienteLeads } from "../../models/cliente";
import { DatePipe } from "@angular/common";
import swal from "sweetalert2";

declare var $: any;
declare interface TableData {
	headerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-listar-leads',
	templateUrl: './listar-leads.component.html',
	styleUrls: ['./listar-leads.component.css']
})
export class ListarLeadsComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	consultaVaciaCargarSpinner: boolean = false;
	botonCrearLeads: boolean = false;
	botonReasignarLeads: boolean = false;
	dataTable: TableData;
	reasignarClientesForm: FormGroup;
	actualizarLeadForm: FormGroup;
	listadoLeadsArray: any[] = [];
	todosAsesoresArray: any[] = [];
	tipoDocumentosArray: any[] = [];
	tipoCanalIngresoArray: any[] = [];
	permisosCrearLeads: string[] = ['A', 'DT', 'GC', 'ACC'];
	permisosReasignarA: string[] = ['A', 'GC'];
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	rolValor: string = '';
	idRegistro: string = '';
	personaId: string = '';
	textoExtendido: string = '';
	observacionLead: string = '';

	constructor(
		private _router: Router,
		private _datepipe: DatePipe,
		private _formBuilder: FormBuilder,
		private _leadsService: LeadsService,
		private _sweetAlertService: SweetAlertService,
		private _parametrizacionService: ParametrizacionService,
		private _tableroAsignacionService: TableroAsignacionService,
	) {
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			let permisoVerFicha = identityData['data'];
			this.rolValor = permisoVerFicha.rol.valor;

			if (this.permisosCrearLeads.includes(this.rolValor)) {
				this.botonCrearLeads = true;
			}

			this.listarLeadsClientes(1);
		}
	}

	ngOnInit() {
		this.reasignarClientesForm = this._formBuilder.group({
			asesorId: ['', Validators.required],
		});
		this.datosTabla();
	}

	datosTabla() {
		this.dataTable = {
			headerRow: [
				"#",
				"Documento Cliente",
				"Nombres Completos",
				"# Teléfono",
				"Correo",
				"Observaciones",
				"Asesor",
				"Acciones"
			],
			dataRows: []
		};
		this.actualizarLeadForm = this._formBuilder.group({
			tipoDoc: [''],
			documento: ['', [Validators.pattern("[0-9]{5,13}$")]],
			nombres: ['', [Validators.required, Validators.pattern("^[a-zA-ZñÑ ]{1,80}$")]],
			apellidos: ['', [Validators.required, Validators.pattern("^[a-zA-ZñÑ ]{1,80}$")]],
			canalIngreso: ['', Validators.required],
			correo: ['', [Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
			telefono: ['', [Validators.pattern("[0-9]{1,15}$")]],
			observacion: ['']
		});
	}

	/* Inicia Listar leads */
	async listarLeadsClientes(page: number) {
		this.loading = true;
		let responseLeadsClientes = await this._leadsService.listarLeadsDatos('' + page);
		//console.log(responseLeadsClientes);
		if (responseLeadsClientes["status"] === "success" && responseLeadsClientes["code"] === "200") {
			this.llenarDatosLista(responseLeadsClientes['data'], page);
			this.loading = false;
		}
		else if (responseLeadsClientes["status"] === "success" && responseLeadsClientes["code"] === "300") {
			this.loading = false;
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseLeadsClientes['message'] + '<b>', 2000);
		}
		else if (responseLeadsClientes["status"] === "error" && responseLeadsClientes["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseLeadsClientes['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			this._sweetAlertService.showNotification('top', 'right', 'error', 'danger', '<b>' + responseLeadsClientes['message'] + '<b>', 2000);
		}
	}

	llenarDatosLista(responseClienteLeads: any, page: number) {
		this.mostrarBotonReasignarA();
		this.cargarInformacionSelectActualizarLead();
		this.listadoLeadsArray = [];
		this.listadoLeadsArray = responseClienteLeads['leads'];
		this.dataTable.dataRows = [];
		this.total_item_count = responseClienteLeads['total_item_count'];
		this.page_actual = page;

		this.total_pages = responseClienteLeads['total_pages'];

		if (page >= 2) this.page_prev = page - 1;
		else this.page_prev = 1;

		if (page < responseClienteLeads['total_pages']) this.page_next = page + 1;
		else {
			if (responseClienteLeads['total_pages'] == 0) this.page_next = 1;
			else this.page_next = responseClienteLeads['total_pages'];
		}

		let JSONParam: any;

		this.listadoLeadsArray.forEach(element => {
			JSONParam = {
				idRegistro: element.registro.id,
				idPersona: element.registro.cliente.id,
				tipoDocPersona: element.registro.cliente.tipoDocumento.valor,
				tipoDocumento: element.registro.cliente.tipoDocumento,
				documentoPersona: element.registro.cliente.documento,
				nombresPersona: element.registro.cliente.nombres,
				apellidosPersona: element.registro.cliente.apellidos,
				contactoPrincipal: element.infoPersonal.contactoPrincipal,
				correo: element.infoPersonal.correo,
				canalIngreso: element.registro.canalIngreso,
				observaciones: element.registro.observacion,
				asesor: element.registro.asignadoA.nombres + ' ' + element.registro.asignadoA.apellidos
			};
			this.dataTable.dataRows.push(JSONParam);
		});

		if (this.total_item_count || this.total_item_count == 0) {
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
		}
	}
	/* Finaliza Listar leads */

	/* Inicio Reasignar Clientes a Asesores */
	reasignarClienteAAsesor(rowIdRegistro: any) {
		//console.log(rowIdRegistro);
		this.idRegistro = rowIdRegistro;
		$("#modalReasinarClientes").modal("show");
		this.cargarDisponibilidadAsesores();
	}

	async cargarDisponibilidadAsesores() {
		this.loading = true;
		const responseDisponibilidadAsesores = await this._tableroAsignacionService.disponibilidadAsesores();
		//console.log(responseDisponibilidadAsesores);
		if (responseDisponibilidadAsesores["status"] === "success" && responseDisponibilidadAsesores["code"] === "200") {
			this.loading = false;
			this.cargarInformacionSelect(responseDisponibilidadAsesores['data']);
		}
		else if (responseDisponibilidadAsesores["status"] === "success" && responseDisponibilidadAsesores["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseDisponibilidadAsesores["message"], 'OK', 'warning');
		}
		else if (responseDisponibilidadAsesores["status"] === "error" && responseDisponibilidadAsesores["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseDisponibilidadAsesores['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseDisponibilidadAsesores["message"], 'OK', 'error');
		}
	}

	async cargarInformacionSelect(asesores: any) {
		this.loading = true;

		let disponibilidadAsesorArray: any[] = [];
		this.todosAsesoresArray = [];

		if (asesores.asesoresDisponibles.length > 0) {
			disponibilidadAsesorArray = asesores.asesoresDisponibles;
		}
		if (asesores.asesoresOcupados.length >= 0) {
			this.todosAsesoresArray = disponibilidadAsesorArray.concat(asesores.asesoresOcupados);
		}

		this.loading = false;
	}

	async onSubmitReasignarClientesAsesores() {
		this.loading = true;
		let idAsesor: string = '' + this.reasignarClientesForm.value.asesorId;

		const responseAsignarClienteAsesores = await this._leadsService.asignarClienteAsesorLeads(idAsesor, '' + this.idRegistro);
		//console.log(responseAsignarClienteAsesores);
		if (responseAsignarClienteAsesores["status"] === "success" && responseAsignarClienteAsesores["code"] === "200") {
			this.loading = false;
			this.listarLeadsClientes(1);
			$("#modalReasinarClientes").modal("hide");
			this.reasignarClientesForm.reset();
		}
		else if (responseAsignarClienteAsesores["status"] === "success" && responseAsignarClienteAsesores["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseAsignarClienteAsesores["message"], 'OK', 'warning');
		}
		else if (responseAsignarClienteAsesores["status"] === "error" && responseAsignarClienteAsesores["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseAsignarClienteAsesores['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseAsignarClienteAsesores["message"], 'OK', 'error');
		}
	}
	/* Finaliza Reasignar Clientes a Asesores */

	buscarIdCliente(rowId: any) {
		localStorage.setItem('ruta', 'leads');
		this._router.navigate(["clientes/ingresos/", rowId]);
	}

	mostrarBotonReasignarA() {
		if (this.permisosReasignarA.includes(this.rolValor)) {
			this.botonReasignarLeads = true;
		}
	}

	crearLeads() {
		this._router.navigate(["leads/registrar"]);
	}

	/* Inicia Actualizar Lead */
	actualizarDatosLead(rowLead: any) {
		let clienteLead: ActualizarLeads = rowLead;
		this.personaId = '' + rowLead.idPersona;

		this.observacionLead = rowLead.observaciones;
		this.actualizarLeadForm.setValue({
			tipoDoc: clienteLead.tipoDocumento.valor,
			documento: clienteLead.documentoPersona,
			nombres: clienteLead.nombresPersona,
			apellidos: clienteLead.apellidosPersona,
			canalIngreso: clienteLead.canalIngreso.valor,
			correo: clienteLead.correo,
			telefono: clienteLead.contactoPrincipal,
			observacion: ''
		});

		$("#modalActualizarLead").modal("show");
	}

	async onSubmitActualizarLead() {
		this.loading = true;
		let observacionTemp = this.validarObservacion(this.actualizarLeadForm.value.observacion);

		let clienteLeadActualizar = new ClienteLeads(
			this.actualizarLeadForm.value.tipoDoc,
			this.actualizarLeadForm.value.documento,
			this.actualizarLeadForm.value.nombres,
			this.actualizarLeadForm.value.apellidos,
			this.actualizarLeadForm.value.correo,
			this.actualizarLeadForm.value.telefono,
			this.actualizarLeadForm.value.canalIngreso,
			observacionTemp
		); //console.log(clienteLeadActualizar);

		const responseActualizarLead = await this._leadsService.modificarClienteLeads(this.personaId, clienteLeadActualizar);
		//console.log(responseActualizarLead);
		if (responseActualizarLead["status"] === "success" && responseActualizarLead["code"] === "200") {
			this.loading = false;
			this.listarLeadsClientes(1);
			$("#modalActualizarLead").modal("hide");
			this.actualizarLeadForm.reset();
		}
		else if (responseActualizarLead["status"] === "success" && responseActualizarLead["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseActualizarLead["message"], 'OK', 'warning');
		}
		else if (responseActualizarLead["status"] === "error" && responseActualizarLead["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseActualizarLead['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseActualizarLead["message"], 'OK', 'error');
		}
	}

	validarObservacion(observaciones: any): string {
		let observacionLead: string = '';
		//console.log(observaciones);
		if (observaciones == '') {
			observacionLead = this.observacionLead
		} else {
			const fechaActual = new Date();
			let fecha = this.fechasValidacion(fechaActual);
			observacionLead = this.observacionLead + '<strong>- Observación Asesoria: </strong>' + observaciones + ' <i>(' + fecha + ')</i>' + '<br>'
		}

		return observacionLead;
	}
	/* Finaliza Actualizar Lead */

	/* Inicio Cargar informacion en leads */
	async cargarInformacionSelectActualizarLead() {
		this.loading = true;
		this.tipoDocumentosArray = [];
		this.tipoCanalIngresoArray = [];

		let tipoDocumentosParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo Documento');
		let tipoCanalIngresoParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Canal de Ingreso');

		tipoDocumentosParametros['data'].forEach((element) => {
			this.tipoDocumentosArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		tipoCanalIngresoParametros['data'].forEach((element) => {
			this.tipoCanalIngresoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
		this.loading = false;
	}
	/* Finaliza Cargar informacion en leads */

	fechasValidacion(formatoFecha: any) {
		if (formatoFecha) {
			//Formatear fecha
			const fechaActualTem = this._datepipe
				.transform(formatoFecha, "yyyy/MM/dd HH:mm:ss")
				.replace(/\//g, "-");
			//console.log("fecha formato: "+fechaActualTem);
			return fechaActualTem;
		}
		return '';
	}

	observarTexto(texto: any) {
		this.textoExtendido = texto === '' ? '- - -' : texto;
		$("#modalObservacion").modal("show");
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}