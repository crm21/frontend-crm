import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientesContactCenterComponent } from './clientes-contact-center.component';

describe('ClientesContactCenterComponent', () => {
  let component: ClientesContactCenterComponent;
  let fixture: ComponentFixture<ClientesContactCenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientesContactCenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientesContactCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
