import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import swal from "sweetalert2";
import { ReconsideradoService } from '../../services/reconsiderado/reconsiderado.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare var $: any;

@Component({
	selector: 'app-listar-reconsiderado',
	templateUrl: './listar-reconsiderado.component.html',
	styleUrls: ['./listar-reconsiderado.component.css']
})
export class ListarReconsideradoComponent implements OnInit {
	
	loading: boolean = false;
	validarToken: boolean = true;
	idCotizacion: any;
	ingresoFinalizado: any = 0;
	botonMostrar: any;
	listadoReconsideradosArray: any[] = [];
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	ocultarBotonesCliente = '';
	botonesEdicion: boolean = false;

	datosCliente = {
		nombres: null,
		documento: null
	}

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _reconsideradoService: ReconsideradoService,
		private _sweetAlertService: SweetAlertService
	) {
		this.idCotizacion = this._route.snapshot.paramMap.get('idCotizacion');
		this.ingresoFinalizado = this._route.snapshot.paramMap.get('ingresoFinalizado');

		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			let permisoVerFicha = identityData['data'];
			this.ocultarBotonesCliente = permisoVerFicha.rol.valor;
			this.botonMostrar = this.ingresoFinalizado == 1 ? true : false;
			this.listarReconsiderados(1);
			if ((this.ocultarBotonesCliente == 'DN') || (this.ocultarBotonesCliente == 'A')) {
				this.botonesEdicion = true;
			}
		}
	}

	ngOnInit() { }

	async listarReconsiderados(page: any) {
		this.loading = true;
		let responseListadoReconsiderados = await this._reconsideradoService.listarReconsideradosPorCotizacion(this.idCotizacion, page);
		//console.log(responseListadoReconsiderados);
		if (responseListadoReconsiderados["status"] === "success" && responseListadoReconsiderados["code"] === "200") {
			this.llenarDatosCliente(responseListadoReconsiderados['data']);
			this.llenarDatosReconsiderados(responseListadoReconsiderados['data'], page);
			this.loading = false;
			$(".main-panel").scrollTop(0);
		}
		else if (responseListadoReconsiderados["status"] === "success" && responseListadoReconsiderados["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseListadoReconsiderados["message"], 'warning')
				.then(result => {
					this._router.navigate(["clientes/buscar"]);
				}).catch(swal.noop);
		}
		else if (responseListadoReconsiderados["status"] === "error" && responseListadoReconsiderados["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseListadoReconsiderados['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseListadoReconsiderados["message"], 'error')
				.then(result => {
					this._router.navigate(["clientes/buscar"]);
				}).catch(swal.noop);
		}
	}

	llenarDatosCliente(data: any) {
		if (data.total_item_count > 0) {
			let nombre = data.reconsiderados[0].estudioCredito.ingresoConcesionario.persona.nombres;
			let apellido = data.reconsiderados[0].estudioCredito.ingresoConcesionario.persona.apellidos;
			this.datosCliente.nombres = nombre + ' ' + apellido;
			this.datosCliente.documento = data.reconsiderados[0].estudioCredito.ingresoConcesionario.persona.documento;
		} else {
			this.datosCliente.nombres = 'Datos no registrados';
			this.datosCliente.documento = 'Datos no registrados';
		}
	}

	llenarDatosReconsiderados(data: any, page: any) {
		if (data.total_item_count > 0) {
			this.listadoReconsideradosArray = [];
			let datosReconsiderado: any = data.reconsiderados;

			datosReconsiderado.forEach((element) => {
				this.listadoReconsideradosArray.push({
					id: element.id,
					cotizacion: element.estudioCredito.numCotizacion,
					fechaReconsiderado: element.fechaReconsiderado,
					reconsiderado: element.reconsiderado.nombre,
					observacion: element.observacion == null ? 'Sin registrar' : element.observacion
				});
			});


			this.total_item_count = data.total_item_count;
			this.page_actual = page;

			this.total_pages = data.total_pages;

			if (page >= 2) this.page_prev = page - 1;
			else this.page_prev = 1;

			if (page < data.total_pages) this.page_next = page + 1;
			else {
				if (data.total_pages == 0) this.page_next = 1;
				else this.page_next = data.total_pages;
			}
		} else {
			this.listadoReconsideradosArray = [];
		}

	}

	crearReconsiderado(rowId) {
		this._router.navigate(["clientes/registrar-reconsiderado/", this.idCotizacion, this.ingresoFinalizado]);
	}

	modificarReconsiderado() {
		//console.log("Modificar cotización");
	}

	eliminarReconsiderado() {
		//console.log("Eliminar cotización");
	}

	regresar() {
		let idIngreso = localStorage.getItem('idIngreso');
		this._router.navigate(["clientes/ficha-clientes/", idIngreso]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}