import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketsListarComponent } from './tickets-listar.component';

describe('TicketsListarComponent', () => {
  let component: TicketsListarComponent;
  let fixture: ComponentFixture<TicketsListarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketsListarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketsListarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
