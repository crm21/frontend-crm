import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import PerfectScrollbar from 'perfect-scrollbar';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { EventosComercialesService } from '../../services/eventos-comerciales/eventos-comerciales.service';

declare var $: any;

@Component({
	selector: 'app-calendario-eventos',
	templateUrl: './calendario-eventos.component.html',
	styleUrls: ['./calendario-eventos.component.css']
})
export class CalendarioEventosComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	eventos: any[] = [];

	constructor(
		private _router: Router,
		private _sweetAlertService: SweetAlertService,
		private _eventosComercialesService: EventosComercialesService,
		private _datepipe: DatePipe
	) {
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);
			let permisoVerFicha = identityData['data'];
			//this.listarEventosComerciales('1');
		}
	}

	ngOnInit() {
		this.listarCalendarioEventosComerciales();
	}

	async listarCalendarioEventosComerciales() {
		this.loading = true;
		const responseCalendarioEC = await this._eventosComercialesService.listarEventosComercialesCalendario();
		//console.log(responseCalendarioEC);
		if (responseCalendarioEC["status"] === "success" && responseCalendarioEC["code"] === "200") {
			this.eventos = responseCalendarioEC["data"];
			this.cargarCalendario();
			this.loading = false;
		}
		else if (responseCalendarioEC["status"] === "success" && responseCalendarioEC["code"] === "300") {
			this._sweetAlertService.showNotification('top', 'right', 'success', 'warning', '<b>' + responseCalendarioEC['message'] + '<b>', 2000);
			this.loading = false;
		}
		else if (responseCalendarioEC["status"] === "error" && responseCalendarioEC["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseCalendarioEC['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'error', 'warning', '<b>' + responseCalendarioEC['message'] + '<b>', 2000);
		}
	}

	cargarCalendario() {
		const $calendar = $('#fullCalendar');

		const today = new Date();

		$calendar.fullCalendar({
			locale: 'es',
			viewRender: function (view: any, element: any) {
				// We make sure that we activate the perfect scrollbar when the view isn't on Month
				if (view.name != 'month') {
					var elem = $(element).find('.fc-scroller')[0];
					let ps = new PerfectScrollbar(elem);
				}
			},
			header: {
				left: 'prev,next,today',
				center: 'title',
				right: 'month,agendaWeek,listMonth'
			},

			defaultDate: today,
			selectable: true,
			selectHelper: true,
			views: {
				month: { // name of view
					titleFormat: 'MMMM YYYY'
					// other view-specific options here
				},
				week: {
					titleFormat: ' MMMM D YYYY'
				},
				day: {
					titleFormat: 'D MMM, YYYY'
				}
			},

			editable: false,
			eventLimit: true, // allow "more" link when too many events

			// color classes: [ event-blue | event-azure | event-green | event-orange | event-red ]
			events: this.eventos,

			eventClick: function (info: any) {
				//console.log(info);
				let fechaInicio = info.start._d;
				let fechaFinal = info.start._d;

				let fechaIni = new Date(fechaInicio);
				let hora_1 = fechaIni.getHours() + 5;
				let minuto_1 = fechaIni.getMinutes();

				let fechaFin = new Date(fechaFinal);
				let hora_2 = fechaFin.getHours() + 5;
				let minuto_2 = fechaFin.getMinutes() + 15;
				if (minuto_2 == 60) {
					hora_2 = hora_2 + 1;
					minuto_2 = 0;
				}

				//const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
				const options = { year: 'numeric', month: 'long', day: 'numeric' };
				let fechaFormateada = fechaFin.toLocaleDateString("es-CO", options);
				//let fechaFormateada = `${anio}/${mes}/${dia}`;
				let horaAgenda = `${hora_1}:${minuto_1} a ${hora_2}:${minuto_2}`;

				swal({
					title: '<span class="text-uppercase">' + info.title + '</span>',
					html: '<div class="row justify-content-center text-left">' +
						'<div class="col-sm-4"><strong> Fecha: </strong></div>' +
						'<div class="col-sm-7">' + fechaFormateada + '</div>' +
						'<div class="col-sm-4"> <strong> Hora: </strong> </div>' +
						'<div class="col-sm-7">' + horaAgenda + '</div>' +
						'</div>' +
						'<div class="row justify-content-center text-left mt-3">' +
						'<div class="col-sm-12 text-center py-1"><strong> Observación </strong></div>' +
						'<div class="col-sm-12">' + info.description + '</div>' +
						'</div>',
					showCancelButton: false,
					confirmButtonClass: 'btn btn-success',
					cancelButtonClass: 'btn btn-danger',
					buttonsStyling: false
				})
					.then((result) => {
						/* console.log(result);
						if (result.value) {} */
					}).catch(swal.noop);
			}
		});
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) {
			result = e1 === e2;
		}
		return result;
	}

	fechasValidacion(opc: number, formatoFecha: any) {
		switch (opc) {
			case 1:
				if (formatoFecha) {
					//Formatear fecha
					const fechaActualTem = this._datepipe
						.transform(formatoFecha, "yyyy/MM/dd")
						.replace(/\//g, "-");
					//console.log("fecha formato: "+fechaActualTem);
					return fechaActualTem;
				}
				break;
			case 2:
				if (formatoFecha) {
					//Formatear fecha
					const fechaActualTem = this._datepipe
						.transform(formatoFecha, "MMMM d, y");
					//console.log("fecha formato: " + fechaActualTem);
					return fechaActualTem;
				}
				break;
			default:
				return '';
				break;
		}
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}