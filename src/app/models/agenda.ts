export class HorasDisponibles {
    constructor(
        public fecha: string,
        public tecnico: string
    ) { }
}

export class AgendarCita {
    constructor(
        public fechaAgendamiento: string,
        public tecnico: string,
        public hora: string,
        public cliente: string,
        public vehiculo: string,
        public observacion: string,
        public tipoAgendamiento: string
    ) { }
}
