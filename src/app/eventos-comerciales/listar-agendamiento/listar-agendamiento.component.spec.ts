import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarAgendamientoComponent } from './listar-agendamiento.component';

describe('ListarAgendamientoComponent', () => {
  let component: ListarAgendamientoComponent;
  let fixture: ComponentFixture<ListarAgendamientoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarAgendamientoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarAgendamientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
