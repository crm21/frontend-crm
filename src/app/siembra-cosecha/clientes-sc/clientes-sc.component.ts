import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario/usuario.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { Router } from '@angular/router';

declare var $: any;

@Component({
	selector: 'app-clientes-sc',
	templateUrl: './clientes-sc.component.html',
	styleUrls: ['./clientes-sc.component.css']
})
export class ClientesScComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	nombres: string;
	nombre: string = 'Usuario';
	nombresEmpresas: string = '';
	imgNombreEmpresa: string = '';

	constructor(
		private _router: Router,
		private _usuarioService: UsuarioService,
		private _sweetAlertService: SweetAlertService
	) {
		if (localStorage.getItem('identity_crm')) {
			this.consultarNombreEmpresa(localStorage.getItem('business'));

			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			let dataUsuario = identityData['data'];
			this.nombres = dataUsuario.nombres + ' ' + dataUsuario.apellidos;
			this.nombre = dataUsuario.rol.nombre;
		}
	}

	ngOnInit() { }

	consultarNombreEmpresa(nombreEmpresa: any) {
		this.nombresEmpresas = this._usuarioService.nombresEmpresa(nombreEmpresa);
		switch (nombreEmpresa) {
			case 'A':
				this.imgNombreEmpresa = './assets/img/empresa-autodenar.png';
				break;
			case 'M':
				this.imgNombreEmpresa = './assets/img/empresa-motodenar.png';
				break;
			case 'T':
				this.imgNombreEmpresa = './assets/img/empresa-automotora.png';
				break;
			case 'D':
				this.imgNombreEmpresa = './assets/img/empresa-autodenar.png';
				break;
			default:
				this.imgNombreEmpresa = './assets/img/empresa-autodenar.png';
				break;
		}
	}

	onSubmitClientesSemilla() {
		this._router.navigate(["siembra-y-cosecha/clientes-semilla"]);
	}

	onSubmitClientesProyectados() {
		this._router.navigate(["siembra-y-cosecha/clientes-proyectados"]);
	}
	
	onSubmitClientesSembrados() {
		this._router.navigate(["siembra-y-cosecha/clientes-sembrados"]);
	}

	onSubmitClientesCosechados() {
		this._router.navigate(["siembra-y-cosecha/clientes-cosechados"]);
	}

	regresar() {
		this._router.navigate(["siembra-y-cosecha"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}