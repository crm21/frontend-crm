import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { APP_BASE_HREF, registerLocaleData } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';

import { AppComponent } from './app.component';

import { SidebarModule } from './sidebar/sidebar.module';
import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule } from './shared/navbar/navbar.module';
import { FixedpluginModule } from './shared/fixedplugin/fixedplugin.module';
import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';

import { AppRoutes } from './app.routing';

//Componentes
import { PricingComponent } from './pages/pricing/pricing.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { ActualizarPassComponent } from './mi-cuenta/actualizar-pass/actualizar-pass.component';
import { RecuperarCuentaComponent } from './pages/recuperar-cuenta/recuperar-cuenta.component';
import { SesionEmpresasComponent } from './pages/sesion-empresas/sesion-empresas.component';
import { SesionUsuariosComponent } from './pages/sesion-usuarios/sesion-usuarios.component';
import { ElegirEmpresasComponent } from './pages/elegir-empresas/elegir-empresas.component';

//Servicios
import { ServicesModule } from './services/services.module';
import { LoadingModule } from './shared/loading/loading.module';
import { LoadingSistemaModule } from './shared/loading-sistema/loading-sistema.module';
import { LoadingSpinnerModule } from './shared/loading-spinner/loading-spinner.module';

//Librerias
import { AngularFileUploaderModule } from "angular-file-uploader";

//Fecha
import localeEsCo from '@angular/common/locales/es-CO';
registerLocaleData(localeEsCo, 'es-Co');

@NgModule({
	exports: [
		MatAutocompleteModule,
		MatButtonToggleModule,
		MatCardModule,
		MatChipsModule,
		MatStepperModule,
		MatDialogModule,
		MatExpansionModule,
		MatGridListModule,
		MatIconModule,
		MatInputModule,
		MatListModule,
		MatMenuModule,
		MatPaginatorModule,
		MatProgressBarModule,
		MatProgressSpinnerModule,
		MatRadioModule,
		MatSelectModule,
		MatDatepickerModule,
		MatButtonModule,
		MatSidenavModule,
		MatSliderModule,
		MatSlideToggleModule,
		MatSnackBarModule,
		MatSortModule,
		MatTableModule,
		MatTabsModule,
		MatToolbarModule,
		MatTooltipModule,
		MatNativeDateModule
	]
})
export class MaterialModule { }

@NgModule({
	imports: [
		CommonModule,
		BrowserAnimationsModule,
		FormsModule,
		RouterModule.forRoot(AppRoutes, {
			useHash: true
		}),
		HttpClientModule,
		MaterialModule,
		SidebarModule,
		NavbarModule,
		FooterModule,
		FixedpluginModule,
		ServicesModule,
		LoadingModule,
		LoadingSistemaModule,
		LoadingSpinnerModule,
		AngularFileUploaderModule
	],
	declarations: [
		AppComponent,
		AdminLayoutComponent,
		AuthLayoutComponent,
		LoginComponent,
		RegisterComponent,
		RecuperarCuentaComponent,
		PricingComponent,
		ActualizarPassComponent,
		SesionEmpresasComponent,
		SesionUsuariosComponent,
		ElegirEmpresasComponent
	],
	providers: [
		MatNativeDateModule,
		{ provide: LOCALE_ID, useValue: 'es-Co' }
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
