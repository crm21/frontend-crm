export class RegistroClienteSiembraCosecha {
    constructor(
        public nombres: string,
        public apellidos: string,
        public correo: string,
        public documento: string,
        public tipoDocumento: string,
        public tipoEntrada: string,
        public correoEmpresa: string,
        public telefonoEmpresa: string
    ) { }
};

export class RegistroAtencionesAgente {
    constructor(
        public cliente: string,
        public observacion: string
    ) { }
};

export class FinalizarAtencionesAgente {
    constructor(
        public atencion: string,
        public tipificacion: string,
        public observacion: string
    ) { }
};