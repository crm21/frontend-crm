import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {
	ActualizarCliente,
	InformacionPersonal,
	ActualizarClienteDatos,
	InformacionPersonalDatos
} from '../../models/cliente';
import { Router } from "@angular/router";
import swal from "sweetalert2";
import { CambiarContrasenia, ConfirmarContrasenia, UsuariosCambiarContrasenia } from '../../models/actualizarContrasena';

@Injectable({
	providedIn: 'root'
})
export class UsuarioService {

	identity: any;
	_headers: any;
	headers: any;
	url: string = '';
	token: string;

	constructor(
		private http: HttpClient,
		private router: Router
	) {
		console.log('[Usuario services is ready]...');
	}

	async iniciarSesion(usuario: any, gettoken = null) {
		this.url = this.comprobarEmpresaServicios(localStorage.getItem('business'));
		this._headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

		return await new Promise((resolve, reject) => {
			if (gettoken != null) {
				usuario.gettoken = "true";
			}
			this.url = this.comprobarEmpresaServicios(usuario.empresas);
			const json = JSON.stringify(usuario);
			const params = "json=" + json;
			this.http.post(this.url + "/login", params, { headers: this._headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async expirarToken() {
		this.url = this.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this.getToken();
		this.headers = new HttpHeaders()
			.set("Content-Type", "application/x-www-form-urlencoded")
			.set("Authorization", this.token);

		return await new Promise((resolve, reject) => {
			this.http.get(this.url + "/validarToken", { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	cerrarSesion(): void {
		let opcionEmpresa = localStorage.getItem('business');

		swal({
			title: "Cerrar Sesión",
			text: "¿Esta seguro que desea salir del sistema?",
			type: "info",
			showCancelButton: true,
			confirmButtonClass: "btn btn-success",
			cancelButtonClass: "btn btn-danger",
			confirmButtonText: "Si",
			cancelButtonText: "Cancelar",
			buttonsStyling: true,
			reverseButtons: true
		}).then(result => {
			if (result.value) {
				localStorage.clear();
				localStorage.setItem('business', opcionEmpresa);
				this.token = "";
				this.identity = null;

				if (opcionEmpresa == 'A') {
					this.router.navigate(['/autodenar']);
				}
				else if (opcionEmpresa == 'M') {
					this.router.navigate(['/motodenar']);
				}
				else if (opcionEmpresa == 'T') {
					this.router.navigate(['/automotora']);
				}
				else {
					this.router.navigate(['/empresas']);
				}
			}
		});
	}

	getIdentity() {
		const identity = JSON.parse(localStorage.getItem('identity_crm'));

		if (identity && identity !== "undefined") this.identity = identity;
		else this.identity = null;

		return this.identity;
	}

	async getToken(): Promise<string> {
		const token = localStorage.getItem("token_crm");

		if (token && token !== "undefined") this.token = token;
		else this.token = null;

		return await this.token;
	}

	isToken() {
		const isToken = localStorage.getItem("token_crm");
		return isToken ? true : false;
	}

	terminarSesion(): void {
		this.token = "";
		this.identity = null;

		let opcionEmpresa = localStorage.getItem('business');
		localStorage.clear();
		localStorage.setItem('business', opcionEmpresa);

		if (opcionEmpresa == 'A') {
			this.router.navigate(['/autodenar']);
		}
		else if (opcionEmpresa == 'M') {
			this.router.navigate(['/motodenar']);
		}
		else if (opcionEmpresa == 'T') {
			this.router.navigate(['/automotora']);
		}
		else {
			this.router.navigate(['/empresas']);
		}
	}

	refresh(): void {
		window.location.reload();
	}

	async listarUsuarios(opc: any, page: any, filtro: any = null) {
		this.url = this.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this.getToken();
		this.headers = new HttpHeaders()
			.set("Content-Type", "application/x-www-form-urlencoded")
			.set("Authorization", this.token);

		return await new Promise((resolve, reject) => {
			const params = "?pagination=" + page + "&filtro=" + filtro;
			this.http.get(this.url + "/usuarios/" + opc + params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async buscarIdCliente(idCliente: any) {
		this.url = this.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this.getToken();
		this.headers = new HttpHeaders()
			.set("Content-Type", "application/x-www-form-urlencoded")
			.set("Authorization", this.token);

		return await new Promise((resolve, reject) => {
			const params = "?id=" + idCliente;
			this.http.get(this.url + "/clientes/" + idCliente, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async buscarIdIformacionPersonal(idCliente: any) {
		this.url = this.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this.getToken();
		this.headers = new HttpHeaders()
			.set("Content-Type", "application/x-www-form-urlencoded")
			.set("Authorization", this.token);

		return await new Promise((resolve, reject) => {
			const params = "?id=" + idCliente;
			this.http.get(this.url + "/informacionPersonal/" + idCliente, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async actualizarCliente(idCliente: any, actualizarCliente: ActualizarCliente) {
		this.url = this.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this.getToken();
		this.headers = new HttpHeaders()
			.set("Content-Type", "application/x-www-form-urlencoded")
			.set("Authorization", this.token);

		return await new Promise((resolve, reject) => {
			const json = JSON.stringify(actualizarCliente);
			let params = "json=" + json;
			this.http.put(this.url + "/clientes/" + idCliente, params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async actualizarClienteDatos(idCliente: any, actualizarCliente: ActualizarClienteDatos) {
		this.url = this.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this.getToken();
		this.headers = new HttpHeaders()
			.set("Content-Type", "application/x-www-form-urlencoded")
			.set("Authorization", this.token);

		return await new Promise((resolve, reject) => {
			const json = JSON.stringify(actualizarCliente);
			let params = "json=" + json;
			this.http.put(this.url + "/clientes/" + idCliente, params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async actualizarInformacionPersonal(idCliente: any, informacionPersonal: InformacionPersonal) {
		this.url = this.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this.getToken();
		this.headers = new HttpHeaders()
			.set("Content-Type", "application/x-www-form-urlencoded")
			.set("Authorization", this.token);

		return await new Promise((resolve, reject) => {
			const json = JSON.stringify(informacionPersonal);
			let params = json;
			this.http.put(this.url + "/informacionPersonal/" + idCliente, params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async actualizarInformacionPersonalDatos(idCliente: any, informacionPersonal: InformacionPersonalDatos) {
		this.url = this.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this.getToken();
		this.headers = new HttpHeaders()
			.set("Content-Type", "application/x-www-form-urlencoded")
			.set("Authorization", this.token);

		return await new Promise((resolve, reject) => {
			const json = JSON.stringify(informacionPersonal);
			let params = json;
			this.http.put(this.url + "/informacionPersonal/" + idCliente, params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async cambiarContrasenia(id: string, cambiarContrasenia: CambiarContrasenia) {
		this.url = this.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this.getToken();
		this.headers = new HttpHeaders()
			.set("Content-Type", "application/x-www-form-urlencoded")
			.set("Authorization", this.token);

		return await new Promise((resolve, reject) => {
			const json = JSON.stringify(cambiarContrasenia);
			let params = "json=" + json;
			this.http.post(this.url + "/actualizarPassword/" + id, params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async confirmarContrasenia(confirmarContrasenia: ConfirmarContrasenia) {
		this.url = this.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this.getToken();
		this.headers = new HttpHeaders()
			.set("Content-Type", "application/x-www-form-urlencoded")
			.set("Authorization", this.token);

		return await new Promise((resolve, reject) => {
			const json = JSON.stringify(confirmarContrasenia);
			let params = "json=" + json;
			this.http.post(this.url + "/confirmarPassword", params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async cambiarContraseniaUsuarios(cambiarContraseniaUsuario: UsuariosCambiarContrasenia) {
		this.url = this.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this.getToken();
		this.headers = new HttpHeaders()
			.set("Content-Type", "application/x-www-form-urlencoded")
			.set("Authorization", this.token);

		return await new Promise((resolve, reject) => {
			const json = JSON.stringify(cambiarContraseniaUsuario);
			let params = "json=" + json;
			this.http.post(this.url + "/resetearPassword", params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	comprobarEmpresaServicios(empresas: string): string {
		if (empresas === 'A') return environment.apiCrmAutodenar;
		if (empresas === 'M') return environment.apiCrmMotodenar;
		if (empresas === 'T') return environment.apiCrmAutomotora;
	}

	nombresEmpresa(empresas: string): string {
		if (empresas === 'A') return 'Autodenar';
		if (empresas === 'M') return 'Motodenar';
		if (empresas === 'T') return 'Automotora';
	}

	rutaNombreEmpresa(nombreEmpresa: string) {
		switch (nombreEmpresa) {
			case 'A':
				return 'autodenar';
				break;
			case 'M':
				return 'motodenar';
				break;
			case 'T':
				return 'automotora';
				break;
			default:
				return 'autodenar';
				break;
		}
	}
}