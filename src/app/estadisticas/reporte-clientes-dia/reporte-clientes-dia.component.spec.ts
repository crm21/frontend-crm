import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteClientesDiaComponent } from './reporte-clientes-dia.component';

describe('ReporteClientesDiaComponent', () => {
  let component: ReporteClientesDiaComponent;
  let fixture: ComponentFixture<ReporteClientesDiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteClientesDiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteClientesDiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
