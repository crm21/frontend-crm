import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarProcesosVentaComponent } from './listar-procesos-venta.component';

describe('ListarProcesosVentaComponent', () => {
  let component: ListarProcesosVentaComponent;
  let fixture: ComponentFixture<ListarProcesosVentaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarProcesosVentaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarProcesosVentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
