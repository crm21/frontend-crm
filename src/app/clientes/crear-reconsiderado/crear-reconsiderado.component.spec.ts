import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearReconsideradoComponent } from './crear-reconsiderado.component';

describe('CrearReconsideradoComponent', () => {
  let component: CrearReconsideradoComponent;
  let fixture: ComponentFixture<CrearReconsideradoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearReconsideradoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearReconsideradoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
