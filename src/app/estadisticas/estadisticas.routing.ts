import { Routes } from '@angular/router';

import { ReporteClientesDiaComponent } from './reporte-clientes-dia/reporte-clientes-dia.component';
import { ReporteClientesSemanaComponent } from './reporte-clientes-semana/reporte-clientes-semana.component';
import { ReporteClientesMesComponent } from './reporte-clientes-mes/reporte-clientes-mes.component';
import { ReporteClientesAnioComponent } from './reporte-clientes-anio/reporte-clientes-anio.component';
import { ReporteVehiculosVendidosComponent } from './reporte-vehiculos-vendidos/reporte-vehiculos-vendidos.component';
import { ReporteClientesNoVentasComponent } from './reporte-clientes-no-ventas/reporte-clientes-no-ventas.component';
import { ReporteClientesConCotizacionesComponent } from './reporte-clientes-con-cotizaciones/reporte-clientes-con-cotizaciones.component';
import { ReporteClientesSinCotizacionesComponent } from './reporte-clientes-sin-cotizaciones/reporte-clientes-sin-cotizaciones.component';
import { ReporteClientesVehiculosUsadosComponent } from './reporte-clientes-vehiculos-usados/reporte-clientes-vehiculos-usados.component';
import { ReporteClientesEsperaComponent } from './reporte-clientes-espera/reporte-clientes-espera.component';
import { ReporteEstudiosCreditoComponent } from './reporte-estudios-credito/reporte-estudios-credito.component';
import { ReporteClientesIngresosActivosComponent } from './reporte-clientes-ingresos-activos/reporte-clientes-ingresos-activos.component';
import { ReporteProcesosVentaComponent } from './reporte-procesos-venta/reporte-procesos-venta.component';
import { ReporteClientesLeadsDiaComponent } from './reporte-clientes-leads-dia/reporte-clientes-leads-dia.component';
import { ReporteClientesLeadsSemanaComponent } from './reporte-clientes-leads-semana/reporte-clientes-leads-semana.component';
import { ReporteClientesLeadsMesComponent } from './reporte-clientes-leads-mes/reporte-clientes-leads-mes.component';
import { ReporteClientesLeadsAnioComponent } from './reporte-clientes-leads-anio/reporte-clientes-leads-anio.component';
import { ReporteClientesCanalIngresoComponent } from './reporte-clientes-canal-ingreso/reporte-clientes-canal-ingreso.component';

export const EstadisticasRoutes: Routes = [
	{
		path: '',
		children: [
			{
				path: 'clientes-dia',
				component: ReporteClientesDiaComponent
			}, {
				path: 'clientes-semana',
				component: ReporteClientesSemanaComponent
			}, {
				path: 'clientes-mes',
				component: ReporteClientesMesComponent
			}, {
				path: 'clientes-anio',
				component: ReporteClientesAnioComponent
			}, {
				path: 'vehiculos-vendidos',
				component: ReporteVehiculosVendidosComponent
			}, {
				path: 'clientes-no-ventas',
				component: ReporteClientesNoVentasComponent
			}, {
				path: 'clientes-con-cotizaciones',
				component: ReporteClientesConCotizacionesComponent
			}, {
				path: 'clientes-sin-cotizaciones',
				component: ReporteClientesSinCotizacionesComponent
			}, {
				path: 'clientes-vehiculos-usados',
				component: ReporteClientesVehiculosUsadosComponent
			}, {
				path: 'clientes-espera',
				component: ReporteClientesEsperaComponent
			}, {
				path: 'estudios-de-credito',
				component: ReporteEstudiosCreditoComponent
			}, {
				path: 'clientes-ingresos-activos',
				component: ReporteClientesIngresosActivosComponent
			}, {
				path: 'procesos-de-venta',
				component: ReporteProcesosVentaComponent
			}, {
				path: 'leads-clientes-dia',
				component: ReporteClientesLeadsDiaComponent
			}, {
				path: 'leads-clientes-semana',
				component: ReporteClientesLeadsSemanaComponent
			}, {
				path: 'leads-clientes-mes',
				component: ReporteClientesLeadsMesComponent
			}, {
				path: 'leads-clientes-anio',
				component: ReporteClientesLeadsAnioComponent
			}, {
				path: 'leads-clientes-canal-de-ingreso',
				component: ReporteClientesCanalIngresoComponent
			}
		]
	}
];