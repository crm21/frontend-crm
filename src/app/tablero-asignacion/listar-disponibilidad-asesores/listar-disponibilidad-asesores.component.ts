import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { TableroAsignacionService } from '../../services/tablero-asignacion/tablero-asignacion.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare const $: any;

@Component({
	selector: 'app-listar-disponibilidad-asesores',
	templateUrl: './listar-disponibilidad-asesores.component.html',
	styleUrls: ['./listar-disponibilidad-asesores.component.css']
})
export class ListarDisponibilidadAsesoresComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	ventanaAsesoresDisponibles: boolean = true;
	ventanaAsesoresOcupados: boolean = true;
	actualizarDisponibilidad: boolean = true;
	infoUsuario: any;
	asignacionAsesorForm: FormGroup;
	disponibilidadAsesor: any;
	asesorDisponibilidadArray: any[] = [];
	asesorOcupadosArray: any[] = [];
	idCliente: any;

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _tableroAsignacionService: TableroAsignacionService,
		private _sweetAlertService: SweetAlertService
	) {
		this.idCliente = this._route.snapshot.paramMap.get('idCliente');
	}

	ngOnInit() {
		if (localStorage.getItem('identity_crm')) {
			this.loading = true;
			this.cargarInformacionCard();
			this.loading = false;
		}
	}

	async cargarInformacionCard() {
		const disponibilidadAsesor = await this._tableroAsignacionService.disponibilidadAsesores();
		//console.log(disponibilidadAsesor);
		if (disponibilidadAsesor["status"] === "success" && disponibilidadAsesor["code"] === "200") {
			this.loading = false;
			this.cargarAsesorDisponibles(disponibilidadAsesor['data'].asesoresDisponibles);
			this.cargarAsesorOcupados(disponibilidadAsesor['data'].asesoresOcupados);
		}
		else if (disponibilidadAsesor["status"] === "success" && disponibilidadAsesor["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', disponibilidadAsesor["message"], 'OK', 'warning');
		}
		else if (disponibilidadAsesor["status"] === "error" && disponibilidadAsesor["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(disponibilidadAsesor['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', disponibilidadAsesor["message"], 'OK', 'error');
		}
	}

	cargarAsesorDisponibles(disponibles: any) {
		if (disponibles.length <= 0) {
			this.asesorDisponibilidadArray = [];
			this.ventanaAsesoresDisponibles = false;
		} else {
			//console.log(disponibles);
			this.ventanaAsesoresDisponibles = true;
			disponibles.forEach((element) => {
				this.asesorDisponibilidadArray.push({
					id: element.id,
					nombre: element.nombre
				});
			});
		}
	}

	cargarAsesorOcupados(ocupados: any) {
		if (ocupados.length <= 0) {
			this.asesorOcupadosArray = [];
			this.ventanaAsesoresOcupados = false;
		} else {
			//console.log(ocupados);
			this.ventanaAsesoresOcupados = true;
			ocupados.forEach((element) => {
				this.asesorOcupadosArray.push({
					id: element.id,
					nombre: element.nombre
				});
			});
		}
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}
