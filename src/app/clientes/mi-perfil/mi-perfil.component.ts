import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { ActualizarDatosCliente, ActualizarDatosInformacionPersonal, interfaceVehiculo } from '../../interfaces/interfaces';
import { ActualizarClienteDatos, InformacionPersonalDatos } from '../../models/cliente';
import { Vehiculo } from '../../models/vehiculo';
import { DatePipe } from "@angular/common";
import swal from "sweetalert2";
import { UsuarioService } from '../../services/usuario/usuario.service';
import { VehiculosService } from '../../services/vehiculos/vehiculos.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare var $: any;

@Component({
	selector: 'app-mi-perfil',
	templateUrl: './mi-perfil.component.html',
	styleUrls: ['./mi-perfil.component.css']
})
export class MiPerfilComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	isLinear: boolean = false;
	mensaje: string;
	idCliente: string;
	idClienteInfoPersonal: any;
	idVehiculoCliente: string;
	infoPersonalFormGroup: FormGroup;
	infoGeneralFormGroup: FormGroup;
	vehiculoClienteRegActFormGroup: FormGroup;
	tipoDocumentosArray: any[] = [];
	estadosArray: any[] = [];
	coloresArray: any[] = [];
	hobbyArray: any[] = [];
	profesionArray: any[] = [];
	vehiculosArray: any[] = [];
	versionVehiculoArray: any[] = [];
	modeloArray: any[] = [];
	tipoAceiteArray: any[] = [];
	generoArray = [
		{ nombre: 'Mujer', valor: 'M' },
		{ nombre: 'Hombre', valor: 'H' },
		{ nombre: 'Otro', valor: 'O' },
	];
	tituloModal: string = '';
	opcionForm: number = 1;
	existenVehiculos: boolean = false;

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _formBuilder: FormBuilder,
		private _usuariosService: UsuarioService,
		private _vehiculosService: VehiculosService,
		private _parametrizacionService: ParametrizacionService,
		private _sweetAlertService: SweetAlertService,
		private _datepipe: DatePipe
	) {
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);
			let permisoVerFicha = identityData['data'];
			this.idCliente = permisoVerFicha.sub;
		}
	}

	ngOnInit(): void {
		this.infoPersonalFormGroup = this._formBuilder.group({
			tipoDoc: ['', Validators.required],
			documento: ['', [Validators.required, Validators.pattern("[0-9]{5,13}$")]],
			nombres: ['', Validators.required],
			apellidos: ['', Validators.required]
		});

		this.infoGeneralFormGroup = this._formBuilder.group({
			correo: ['', [Validators.required, Validators.email]],
			fechaNacimiento: [''],
			direccion: [''],
			lugarResidencia: [''],
			contactoPrincipal: ['', Validators.required],
			contactoSecundario: [''],
			color: [''],
			hobby1: [''],
			hobby2: [''],
			profesion: [''],
			genero: ['']
		});

		this.vehiculoClienteRegActFormGroup = this._formBuilder.group({
			placa: ['', [Validators.required, Validators.pattern("^[a-zA-Z]{3}[0-9a-zA-Z]{3}$")]],
			modelo: ['0000'],
			version: ['', Validators.required],
			vin: [''],
			tipoAceite: ['', Validators.required]
		});

		if (localStorage.getItem('identity_crm')) {
			this.buscarCliente();
		}
	}

	async cargarInformacionSelect() {
		this.loading = true;
		let tipoDocumentosParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo Documento');
		let coloresParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Colores');
		let hobbyParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Hobby');
		let profesionParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Profesión');
		let versionVehiculoParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo Vehículo');
		let tipoAceiteParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo Aceite');

		tipoDocumentosParametros['data'].forEach((element) => {
			this.tipoDocumentosArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});


		coloresParametros['data'].forEach((element) => {
			this.coloresArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		hobbyParametros['data'].forEach((element) => {
			this.hobbyArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		profesionParametros['data'].forEach((element) => {
			this.profesionArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		versionVehiculoParametros['data'].forEach((element) => {
			this.versionVehiculoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		tipoAceiteParametros['data'].forEach((element) => {
			this.tipoAceiteArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		const anioActual = new Date();
		const rangoAnioActual: number = + anioActual.getFullYear();

		for (var i = rangoAnioActual + 2; i >= 1990; --i) {
			this.modeloArray.push({
				valor: '' + i,
				nombre: '' + i
			});
		}

		this.loading = false;
	}

	buscarCliente() {
		this.cargarInformacionSelect();
		this.buscarIdCliente();
		this.buscarIdInformacionPersonal();
		this.buscarVehiculosCliente();
	}

	/* Actualizacion de clientes e informacion personal */
	async buscarIdCliente() {
		this.loading = true;
		const responseIdCliente = await this._usuariosService.buscarIdCliente(this.idCliente);
		//console.log(responseIdCliente);
		if (responseIdCliente["status"] === "success" && responseIdCliente["code"] === "200") {

			let datosCliente: ActualizarDatosCliente = responseIdCliente['data'];
			//console.log(datosCliente);
			this.infoPersonalFormGroup.setValue({
				tipoDoc: datosCliente.tipoDocumento.valor,
				documento: datosCliente.documento,
				nombres: datosCliente.nombres,
				apellidos: datosCliente.apellidos
			});
			this.loading = false;
		}
		else if (responseIdCliente["status"] === "success" && responseIdCliente["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseIdCliente["message"], 'OK', 'warning');
		}
		else if (responseIdCliente["status"] === "error" && responseIdCliente["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseIdCliente['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseIdCliente["message"], 'OK', 'error');
		}
	}

	async onSubmitActualizarInformacionCliente() {
		this.loading = true;
		let actualizarCliente = new ActualizarClienteDatos(
			this.infoPersonalFormGroup.value.tipoDoc,
			this.infoPersonalFormGroup.value.documento,
			this.infoPersonalFormGroup.value.nombres,
			this.infoPersonalFormGroup.value.apellidos
		); //console.log(actualizarCliente);

		let responseActualizarCliente = await this._usuariosService.actualizarClienteDatos(this.idCliente, actualizarCliente);
		//console.log(responseActualizarCliente);
		if (responseActualizarCliente["status"] === "success" && responseActualizarCliente["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseActualizarCliente["message"], 'success')
				.then(result => {
					this.buscarIdCliente();
				}).catch(swal.noop);
		}
		else if (responseActualizarCliente["status"] === "success" && responseActualizarCliente["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseActualizarCliente["message"], 'warning');
		}
		else if (responseActualizarCliente["status"] === "error" && responseActualizarCliente["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseActualizarCliente['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseActualizarCliente["message"], 'error');
		}
	}

	/* Actualizar iformacion personal */
	async buscarIdInformacionPersonal() {
		this.loading = true;
		const responseIdInformacionPersonal = await this._usuariosService.buscarIdIformacionPersonal(this.idCliente);
		//console.log(responseIdInformacionPersonal);
		if (responseIdInformacionPersonal["status"] === "success" && responseIdInformacionPersonal["code"] === "200") {
			let datosInformacionPersonal: ActualizarDatosInformacionPersonal = responseIdInformacionPersonal['data'];
			this.idClienteInfoPersonal = datosInformacionPersonal.id;
			this.infoGeneralFormGroup.setValue({
				correo: datosInformacionPersonal.correo,
				fechaNacimiento: datosInformacionPersonal.fechaNacimiento,
				direccion: datosInformacionPersonal.direccion,
				lugarResidencia: datosInformacionPersonal.lugarResidencia,
				contactoPrincipal: datosInformacionPersonal.contactoPrincipal,
				contactoSecundario: datosInformacionPersonal.contactoSecundario,
				color: datosInformacionPersonal.color.valor,
				hobby1: datosInformacionPersonal.hobby1.valor,
				hobby2: datosInformacionPersonal.hobby2.valor,
				profesion: datosInformacionPersonal.profesion.valor,
				genero: datosInformacionPersonal.genero
			});
			this.loading = false;
		}
		else if (responseIdInformacionPersonal["status"] === "success" && responseIdInformacionPersonal["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseIdInformacionPersonal["message"], 'OK', 'warning');
		}
		else if (responseIdInformacionPersonal["status"] === "error" && responseIdInformacionPersonal["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseIdInformacionPersonal['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseIdInformacionPersonal["message"], 'OK', 'error');
		}
	}

	async onSubmitActualizarInformacionPersonal() {
		this.loading = true;
		let actualizarInfoPersonal = new InformacionPersonalDatos(
			this.infoGeneralFormGroup.value.correo,
			this.fechasValidacion(this.infoGeneralFormGroup.value.fechaNacimiento),
			this.infoGeneralFormGroup.value.direccion,
			this.infoGeneralFormGroup.value.lugarResidencia,
			this.infoGeneralFormGroup.value.contactoPrincipal,
			this.infoGeneralFormGroup.value.contactoSecundario,
			this.infoGeneralFormGroup.value.color,
			this.infoGeneralFormGroup.value.hobby1,
			this.infoGeneralFormGroup.value.hobby2,
			this.infoGeneralFormGroup.value.profesion,
			this.infoGeneralFormGroup.value.genero
		); //console.log(actualizarInfoPersonal);

		let responseActualizarInfoPersonal = await this._usuariosService.actualizarInformacionPersonalDatos(this.idClienteInfoPersonal, actualizarInfoPersonal);
		//console.log(responseActualizarInfoPersonal);
		if (responseActualizarInfoPersonal["status"] === "success" && responseActualizarInfoPersonal["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseActualizarInfoPersonal["message"], 'success')
				.then(result => {
					this.buscarIdInformacionPersonal();
				}).catch(swal.noop);
		}
		else if (responseActualizarInfoPersonal["status"] === "success" && responseActualizarInfoPersonal["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseActualizarInfoPersonal["message"], 'warning');
		}
		else if (responseActualizarInfoPersonal["status"] === "error" && responseActualizarInfoPersonal["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseActualizarInfoPersonal['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseActualizarInfoPersonal["message"], 'error');
		}
	}

	/* Listar vehiculos asociados al cliente */
	async buscarVehiculosCliente() {
		this.loading = true;
		const responseVehiculosCliente = await this._vehiculosService.listarVehiculosCliente(2, this.idCliente, '1');
		//console.log(responseVehiculosCliente);
		if (responseVehiculosCliente["status"] === "success" && responseVehiculosCliente["code"] === "200") {
			this.existenVehiculos = responseVehiculosCliente["data"].estadoVehiculos;
			this.vehiculosArray = responseVehiculosCliente["data"].vehiculos;
			this.loading = false;
		}
		else if (responseVehiculosCliente["status"] === "success" && responseVehiculosCliente["code"] === "300") {
			this.loading = false;
			this.existenVehiculos = false;
			this.vehiculosArray = [];
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseVehiculosCliente['message'] + '<b>', 2000);
		}
		else if (responseVehiculosCliente["status"] === "error" && responseVehiculosCliente["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseVehiculosCliente['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'error', 'warning', '<b>' + responseVehiculosCliente['message'] + '<b>', 2000);
		}
	}

	/* Seccion de vehículos */
	modalActualizarVehiculo(rowVehiculo: any) {
		//console.log(rowVehiculo);
		this.idVehiculoCliente = rowVehiculo.id;
		this.tituloModal = 'Actualizar';
		this.opcionForm = 1;
		let vehiculoRow: interfaceVehiculo = rowVehiculo;
		this.vehiculoClienteRegActFormGroup.setValue({
			placa: vehiculoRow.placa,
			modelo: vehiculoRow.modelo,
			version: vehiculoRow.version.valor,
			vin: vehiculoRow.vin,
			tipoAceite: vehiculoRow.tipoAceite.valor
		});
		$("#modalVehiculoCliente").modal("show");
	}

	modalRegistrarVehiculo() {
		this.tituloModal = 'Registrar';
		this.opcionForm = 2;
		$("#modalVehiculoCliente").modal("show");
	}

	onSubmitVehiculoForm(opc: any) {
		if (opc == 1) {
			this.actualizarVehiculoCliente();
		} else {
			this.registrarVehiculoCliente();
		}
	}

	async actualizarVehiculoCliente() {
		this.loading = true;
		let actualizarVehiculo = new Vehiculo(
			this.vehiculoClienteRegActFormGroup.value.placa,
			'' + this.vehiculoClienteRegActFormGroup.value.modelo,
			this.vehiculoClienteRegActFormGroup.value.version,
			this.idCliente,
			this.vehiculoClienteRegActFormGroup.value.vin,
			this.vehiculoClienteRegActFormGroup.value.tipoAceite
		); //console.log(actualizarVehiculo);

		let responseActualizarVehiculo = await this._vehiculosService.actualizarVehiculoCliente(this.idVehiculoCliente, actualizarVehiculo);
		//console.log(responseActualizarVehiculo);
		if (responseActualizarVehiculo["status"] === "success" && responseActualizarVehiculo["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseActualizarVehiculo["message"], 'success')
				.then(result => {
					$("#modalVehiculoCliente").modal("hide");
					this.vehiculoClienteRegActFormGroup.reset();
					this.buscarVehiculosCliente();
				}).catch(swal.noop);
		}
		else if (responseActualizarVehiculo["status"] === "success" && responseActualizarVehiculo["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseActualizarVehiculo["message"], 'warning');
		}
		else if (responseActualizarVehiculo["status"] === "error" && responseActualizarVehiculo["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseActualizarVehiculo['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseActualizarVehiculo["message"], 'error');
		}
	}

	async registrarVehiculoCliente() {
		this.loading = true;
		let registroVehiculo = new Vehiculo(
			this.vehiculoClienteRegActFormGroup.value.placa,
			'' + this.vehiculoClienteRegActFormGroup.value.modelo,
			this.vehiculoClienteRegActFormGroup.value.version,
			this.idCliente,
			this.vehiculoClienteRegActFormGroup.value.vin,
			this.vehiculoClienteRegActFormGroup.value.tipoAceite
		); //console.log(registroVehiculo);

		let responseRegistrarVehiculo = await this._vehiculosService.registrarVehiculoCliente(registroVehiculo);
		//console.log(responseRegistrarVehiculo);
		if (responseRegistrarVehiculo["status"] === "success" && responseRegistrarVehiculo["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseRegistrarVehiculo["message"], 'success')
				.then(result => {
					$("#modalVehiculoCliente").modal("hide");
					this.vehiculoClienteRegActFormGroup.reset();
					this.buscarVehiculosCliente();
				}).catch(swal.noop);
		}
		else if (responseRegistrarVehiculo["status"] === "success" && responseRegistrarVehiculo["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseRegistrarVehiculo["message"], 'warning');
		}
		else if (responseRegistrarVehiculo["status"] === "error" && responseRegistrarVehiculo["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseRegistrarVehiculo['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseRegistrarVehiculo["message"], 'error');
		}
	}

	mayusculas(letra: any) {
		(document.getElementById('placa') as HTMLInputElement).value = (document.getElementById('placa') as HTMLInputElement).value.toUpperCase();
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) {
			result = e1 === e2;
		}
		return result;
	}

	fechasValidacion(formatoFecha) {
		//console.log(formatoFecha);
		if (formatoFecha) {
			//Formatear fecha
			const fechaActualTem = this._datepipe
				.transform(formatoFecha, "yyyy/MM/dd")
				.replace(/\//g, "-");
			//console.log("fecha formato: "+fechaActualTem);
			return fechaActualTem;
		}
		return '';
	}

	regresar() {
		if (localStorage.getItem('ficha')) {
			let idIngreso = localStorage.getItem('idIngreso');
			localStorage.removeItem('ficha');
			this._router.navigate(["clientes/ficha-clientes/", idIngreso]);
		}
		else { this._router.navigate(["usuarios/listar"]); }
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}
