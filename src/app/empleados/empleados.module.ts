import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { EmpleadosRoutes } from './empleados.routing';
import { LoadingModule } from '../shared/loading/loading.module';
import { LoadingSpinnerModule } from '../shared/loading-spinner/loading-spinner.module';
import { ListarEmpleadosComponent } from './listar-empleados/listar-empleados.component';
import { CrearHojaVidaComponent } from './crear-hoja-vida/crear-hoja-vida.component';
import { FichaEmpleadosComponent } from './ficha-empleados/ficha-empleados.component';

@NgModule({
	declarations: [
		ListarEmpleadosComponent,
		CrearHojaVidaComponent,
		FichaEmpleadosComponent
	],
	imports: [
		CommonModule,
		RouterModule.forChild(EmpleadosRoutes),
		FormsModule,
		MaterialModule,
		ReactiveFormsModule,
		LoadingModule,
		LoadingSpinnerModule
	],
	providers: [DatePipe]
})
export class EmpleadosModule { }
