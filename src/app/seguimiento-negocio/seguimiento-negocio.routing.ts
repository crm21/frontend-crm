import { Routes } from '@angular/router';

import { ListarEstudiosCreditoComponent } from './listar-estudios-credito/listar-estudios-credito.component';
import { ListarProcesosVentaComponent } from './listar-procesos-venta/listar-procesos-venta.component';
import { ListarReconsideradosComponent } from './listar-reconsiderados/listar-reconsiderados.component';

export const SeguimientoNegocioRoutes: Routes = [
	{
		path: '',
		children: [
			{
				path: 'estudios-de-credito',
				component: ListarEstudiosCreditoComponent
			},
			{
				path: 'procesos-de-venta',
				component: ListarProcesosVentaComponent
			},
			{
				path: 'reconsiderados',
				component: ListarReconsideradosComponent
			}
		]
	}
];