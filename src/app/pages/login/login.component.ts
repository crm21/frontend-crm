import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { LoginAnteriorNoUtilizar } from '../../models/login';
import { Router, ActivatedRoute } from '@angular/router';
import { UsuarioService } from '../../services/usuario/usuario.service';
import swal from 'sweetalert2';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { environment } from '../../../environments/environment';
import { MatSnackBar } from '@angular/material/snack-bar';

declare var $: any;
export interface EmpresaArray {
	nombre: string;
	valor: string;
}

@Component({
	selector: 'app-login-cmp',
	templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit, OnDestroy {
	test: Date = new Date();
	private toggleButton: any;
	private sidebarVisible: boolean;
	private nativeElement: Node;
	public loading = false;

	public login: LoginAnteriorNoUtilizar;
	public identity: any;
	public idSearch: any;
	public token: any;
	public hide = true;
	public empresasArray: EmpresaArray[] = [
		{ nombre: 'Autodenar', valor: 'A' },
		{ nombre: 'Motodenar', valor: 'M' },
		{ nombre: 'Automotora', valor: 'T' }
	];
	public recordarSessionEmpresa = null;

	public identity_box: any;
	public version = environment.version;

	constructor(
		private element: ElementRef,
		private _router: Router,
		private _usuarioService: UsuarioService,
		private _sweetAlertService: SweetAlertService,
		private _route: ActivatedRoute,
		private _snackBar: MatSnackBar
	) {

		this.nativeElement = element.nativeElement;
		this.sidebarVisible = false;
		//this.recordarSessionEmpresa = localStorage.getItem('SesionEmpresa');
		this.login = new LoginAnteriorNoUtilizar('', '', '', '','1');
	}

	ngOnInit() {
		var navbar: HTMLElement = this.element.nativeElement;
		this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
		const body = document.getElementsByTagName('body')[0];
		body.classList.add('login-page');
		body.classList.add('off-canvas-sidebar');
		this.checkSesion();
	}

	sidebarToggle() {
		var toggleButton = this.toggleButton;
		var body = document.getElementsByTagName('body')[0];
		var sidebar = document.getElementsByClassName('navbar-collapse')[0];
		if (this.sidebarVisible == false) {
			setTimeout(function () {
				toggleButton.classList.add('toggled');
			}, 500);
			body.classList.add('nav-open');
			this.sidebarVisible = true;
		} else {
			this.toggleButton.classList.remove('toggled');
			this.sidebarVisible = false;
			body.classList.remove('nav-open');
		}
	}

	ngOnDestroy() {
		const body = document.getElementsByTagName('body')[0];
		body.classList.remove('login-page');
		body.classList.remove('off-canvas-sidebar');
	}

	checkSesion() {
		let checkToken = this._usuarioService.isToken();

		if (checkToken) this._router.navigate(['/dashboard']);
		else this._router.navigate(['/iniciar-sesion']);
	}

	async iniciarSesion(loginForm) {
		this.loading = true;
		/*
		let recordarSession = this.login.sessionEmpresa;
		console.log(this.login);
		console.log(this.recordarSessionEmpresa);*/
		const responseLogin: any = await this._usuarioService.iniciarSesion(this.login);
		let base_64_data = atob(responseLogin);
		let identityData = JSON.parse(base_64_data);

		if (identityData['status'] == "success") {
			this.identity = identityData['data'];
			const responseDatosLoginToken = await this._usuarioService.iniciarSesion(this.login, true);

			this.token = responseDatosLoginToken;
			this.loading = false;

			// Guardar token en el localstorage
			localStorage.setItem("system", btoa("crm"));
			localStorage.setItem("token_crm", this.token);
			localStorage.setItem("business", this.login.empresas);
			//localStorage.setItem("SesionEmpresa", this.login.sessionEmpresa);
			localStorage.setItem('identity_crm', responseLogin);
			this._snackBar.open('¡BIENVENIDO! ' + this.identity.nombres + ' ' + this.identity.apellidos, 'X', {
				duration: 4000,
			});
			this._router.navigate(['/dashboard']);
		} else {
			this.loading = false;
			swal('Advertencia!', identityData['message'], 'warning');
		}
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}
}