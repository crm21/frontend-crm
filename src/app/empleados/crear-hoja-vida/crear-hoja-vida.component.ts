import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
	EmpleadoInfoPersonal,
	EmpleadoInfoGeneral,
	EmpleadoInfoEmpleado,
	EmpleadoInfoFamiliar,
	EmpleadoInfoEducacion,
	ActualizarEmpleadoInfoPersonal,
	ActualizarEmpleadoInfoEducacion
} from "../../models/empleado";
import { DatePipe } from "@angular/common";
import swal from 'sweetalert2';
import { EmpleadosService } from '../../services/empleados/empleados.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import {
	InfoPersonalEmpleado,
	InfoGeneralEmpleado,
	InfoEmpleadoEmpleado,
	InfoFamiliarEmpleado
} from '../../interfaces/interfaces';

declare const $: any;
interface FileReaderEventTarget extends EventTarget {
	result: string;
}
interface FileReaderEvent extends Event {
	target: EventTarget;
	getMessage(): string;
}
export interface OpcionesArray {
	nombre: string;
	valor: string;
}

@Component({
	selector: 'app-crear-hoja-vida',
	templateUrl: './crear-hoja-vida.component.html',
	styleUrls: ['./crear-hoja-vida.component.css']
})
export class CrearHojaVidaComponent implements OnInit {
	
	loading: boolean = false;
	validarToken: boolean = true;
	isLinear: boolean = true;
	mensajeRespuesta = '';

	/*Formularios*/
	infoPersonalFormGroup: FormGroup;
	infoGeneralFormGroup: FormGroup;
	infoEmpleadoFormGroup: FormGroup;
	infoFamiliarFormGroup: FormGroup;
	infoEducacionFormGroup: FormGroup;

	/* Variables Globales Secciones */
	idGeneralData: any;
	idEmpleadoData: any;
	idFamiliarData: any;

	/*Selects*/
	tipoDocumentosArray: any[] = [];
	categoriaLicenciaArray: any[] = [];
	claseLibretaMilitarArray: any[] = [];
	colorPreferidoArray: any[] = [];
	estadoCivilArray: any[] = [];
	hobbyArray: any[] = [];
	profesionArray: any[] = [];
	tipoContratoArray: any[] = [];
	tipoEmpleadoArray: any[] = [];
	tipoViviendaArray: any[] = [];
	tipoDeportesArray: any[] = [];
	tipoEstudiosArray: any[] = [];
	tipoCursosArray: any[] = [];

	generoArray: OpcionesArray[] = [
		{ nombre: 'Mujer', valor: 'M' },
		{ nombre: 'Hombre', valor: 'H' },
		{ nombre: 'Otro', valor: 'O' }
	];
	grupoSanguineoArray: OpcionesArray[] = [
		{ nombre: 'A+', valor: 'Ap' },
		{ nombre: 'A-', valor: 'A-' },
		{ nombre: 'B+', valor: 'Bp' },
		{ nombre: 'B-', valor: 'B-' },
		{ nombre: 'O+', valor: 'Op' },
		{ nombre: 'O-', valor: 'O-' },
		{ nombre: 'AB+', valor: 'ABp' },
		{ nombre: 'AB-', valor: 'AB-' }
	];
	opcionesVacanteArray: OpcionesArray[] = [
		{ nombre: 'Anuncio', valor: 'Anuncio' },
		{ nombre: 'Amigo', valor: 'Amigo' },
		{ nombre: 'Por medio de agencia', valor: 'Por medio de agencia' }
	];
	opcionesRespuestaArray: OpcionesArray[] = [
		{ nombre: 'Si', valor: 'Si' },
		{ nombre: 'No', valor: 'No' }
	];

	/*Inputs validacion*/
	botonFinalizar: boolean = true;
	tenerLibretaMilitar: boolean = false;
	tenerLicencia: boolean = false;
	tenerTrabajoActual: boolean = false;
	tenerIngresoAdicional: boolean = false;
	esRecomendado: boolean = false;
	tieneParientes: boolean = false;
	viviendaAlquilada: boolean = false;
	pertenecerGrupo: boolean = false;
	estadoCursoOpc: boolean = false;
	estudiosBasicos: boolean = true;
	duracionMesesVisible: boolean = false;
	mostrarTiposEducacion: boolean = false;
	botonNuevoTituloAcademico: boolean = true;
	campoConocimientoVacante: boolean = true;
	opcionVentana: any = '';
	ideEmpleado: string = '';
	tituloPagina: string = '';
	mensajeNuevotituloAcademico: string = '';

	/* Educacion */
	tituloColumnas: string[] = [
		'tipoEstudio',
		'anioFinalizacion',
		'aniosCursados',
		'duracionMeses',
		'tituloObtenido',
		'institucion',
		'ciudad',
		'intensidadHoraria',
		'estadoCurso',
		'anioSemestreCursa',
		'observacion',
		'acciones'
	];
	/* Educacion */
	infoEducacionArray: EmpleadoInfoEducacion[] = [];
	infoEducacionTempArray: EmpleadoInfoEducacion[] = [];
	tituloEducacion: EmpleadoInfoEducacion = new EmpleadoInfoEducacion('', 'SAG', '', '', '', '', '', '', '', 'SAG', '', '', '');
	tituloEducacionModificar: EmpleadoInfoEducacion = new EmpleadoInfoEducacion('', 'SAG', '', '', '', '', '', '', '', 'SAG', '', '', '');

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _datepipe: DatePipe,
		private _formBuilder: FormBuilder,
		private _empleadosService: EmpleadosService,
		private _sweetAlertService: SweetAlertService,
		private _parametrizacionService: ParametrizacionService
	) {
		this.opcionVentana = this._route.snapshot.data.opc;
		this.ideEmpleado = this._route.snapshot.paramMap.get('idEmpleado');

		if (localStorage.getItem('identity_crm')) {
			this.cargarInformacionSelect();
			if (this.opcionVentana == '1') {
				this.tituloPagina = 'REGISTRO';
				this.botonNuevoTituloAcademico = true;
				this.campoConocimientoVacante = true;
				this.mensajeNuevotituloAcademico = 'Antes de guardar la información verifique que los datos ingresados esten diligenciados correctamente.';
			}
			else {
				this.tituloPagina = 'ACTUALIZACIÓN';
				this.isLinear = false;
				this.botonFinalizar = false;
				this.botonNuevoTituloAcademico = false;
				this.campoConocimientoVacante = false;
				this.mensajeRespuesta = 'Antes dar clic en el botón Finalizar verifique que actualizó la información necesaria para el empleado';
				this.mensajeNuevotituloAcademico = 'Para registrar los nuevos títulos académicos diligenciados arriba dar clic en el boton Guardar Título Académico.';
				this.listarDatosPersonalesId(this.ideEmpleado);
			}
		}
	}

	ngOnInit() {
		this.infoPersonalFormGroup = this._formBuilder.group({
			tipoDoc: ['', Validators.required],
			documento: ['', [Validators.required, Validators.pattern("[0-9]{5,13}$")]],
			nombres: ['', Validators.required],
			apellidos: ['', Validators.required]
		});
		this.infoGeneralFormGroup = this._formBuilder.group({
			correo: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
			fechaExpedicion: ['', Validators.required],
			fechaNacimiento: [''],
			direccion: [''],
			lugarResidencia: [''],
			telefonoFijo: [''],
			celular: ['', [Validators.required, Validators.pattern("[0-9]{5,13}$")]],
			estadoCivil: ['SAG'],
			aniosExperienciaLaboral: [''],
			tieneLibretaMilitar: ['', [Validators.required]],
			libretaMilitar: [''],
			claseLibretaMilitar: ['SAG'],
			distritoLibretaMilitar: [''],
			tarjetaProfesional: [''],
			grupoSanguineo: ['SAG'],
			tieneLicenciaConduccion: ['', [Validators.required]],
			licenciaConduccion: [''],
			categoriaLicencia: ['SAG'],
			color: ['SAG'],
			hobby1: ['SAG'],
			hobby2: ['SAG'],
			profesion: ['SAG'],
			genero: ['Op'],
		});
		this.infoEmpleadoFormGroup = this._formBuilder.group({
			trabajaActualmente: ['', [Validators.required]],
			empresaActual: [''],
			tipoEmpleado: ['SAG'],
			tipoContrato: ['SAG'],
			trabajoEmpresa: [''],
			empleoAntes: [''],
			fechaEmpleoAntes: [''],
			recomendado: [''],
			nombreRecomendado: [''],
			dependenciaRecomendado: [''],
			parientesEmpresa: ['', [Validators.required]],
			nombrePariente: [''],
			dependenciaPariente: [''],
			conocimientoVacante: ['SAG'],
			ciudadVivido: [''],
			ciudadesTrabajado: [''],
			trabajoOtraCiudad: [''],
			tipoVivienda: ['SAG'],
			nombreArrendador: [''],
			telefonoArrendador: [''],
			tiempoVivienda: [''],
			tieneIngresosAdicionales: ['', [Validators.required]],
			ingresosAdicionales: [''],
			valorIngresosAdicionales: ['', [Validators.pattern("[0-9]{1,20}$")]],
			valorObligaciones: ['', [Validators.pattern("[0-9]{1,20}$")]],
			conceptoObligaciones: [''],
			aspiracionSalarial: ['', [Validators.pattern("[0-9]{1,20}$")]],
			reconocimientos: [''],
			tenerGrupoPertenece: ['', [Validators.required]],
			perteneceGrupo: [''],
			hobbies: [''],
			deportes: ['SAG'],
			expectativas: [''],
		});
		this.infoFamiliarFormGroup = this._formBuilder.group({
			nombrePareja: [''],
			profesionPareja: ['SAG'],
			empresaPareja: [''],
			cargoActualPareja: [''],
			direccionF: [''],
			contacto: [''],
			ciudad: [''],
			numHermanos: ['', [Validators.required, Validators.pattern("[0-9]{1,3}$")]],
		});
		this.infoEducacionFormGroup = this._formBuilder.group({});
	}
	/* Inicio Seccion para crear empleado */
	onSubmitCrearEmpleados(formOpc: number, formularioNombre: string) {
		/* Validacion para crear el empleado o guardar el empleado
		opc = 1 crear el empleado
		opc = 2 guardar por secciones del cliente
		 */
		if (formOpc == 1 && formularioNombre == 'infoEducacion') {
			swal({
				title: 'Guardar empleado',
				text: '¿Desea guardar la información del empleado?',
				type: 'question',
				showCancelButton: true,
				confirmButtonClass: 'btn btn-warning',
				cancelButtonClass: 'btn btn-danger',
				confirmButtonText: 'Guardar',
				cancelButtonText: 'Continuar editando',
				buttonsStyling: true,
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					this.guardarEmpleado();
				}
			}).catch(swal.noop);

		}
		if (formOpc == 2) {
			this.modificarEmpleado(formularioNombre);
		}
	}

	async guardarEmpleado() {
		this.loading = true;
		let empleadoInfPersonal = new EmpleadoInfoPersonal(
			this.infoPersonalFormGroup.value.tipoDoc,
			this.infoPersonalFormGroup.value.documento,
			this.infoPersonalFormGroup.value.nombres,
			this.infoPersonalFormGroup.value.apellidos,
			'E'
		); //console.log(empleadoInfPersonal);

		let empleadoInfoGeneral = new EmpleadoInfoGeneral(
			this.infoGeneralFormGroup.value.correo,
			this.infoGeneralFormGroup.value.direccion,
			this.infoGeneralFormGroup.value.lugarResidencia,
			this.infoGeneralFormGroup.value.telefonoFijo,
			this.infoGeneralFormGroup.value.celular,
			this.infoGeneralFormGroup.value.estadoCivil,
			this.infoGeneralFormGroup.value.aniosExperienciaLaboral,
			this.fechasValidacion(this.infoGeneralFormGroup.value.fechaExpedicion),
			this.fechasValidacion(this.infoGeneralFormGroup.value.fechaNacimiento),
			this.tenerLibretaMilitar ? this.infoGeneralFormGroup.value.libretaMilitar : 'No',
			this.tenerLibretaMilitar ? this.infoGeneralFormGroup.value.claseLibretaMilitar : 'SAG',
			this.tenerLibretaMilitar ? this.infoGeneralFormGroup.value.distritoLibretaMilitar : '',
			this.infoGeneralFormGroup.value.tarjetaProfesional,
			this.infoGeneralFormGroup.value.grupoSanguineo,
			this.tenerLicencia ? this.infoGeneralFormGroup.value.licenciaConduccion : 'No',
			this.tenerLicencia ? this.infoGeneralFormGroup.value.categoriaLicencia : 'SAG',
			this.infoGeneralFormGroup.value.color,
			this.infoGeneralFormGroup.value.hobby1,
			this.infoGeneralFormGroup.value.hobby2,
			this.infoGeneralFormGroup.value.profesion,
			this.infoGeneralFormGroup.value.genero
		); //console.log(empleadoInfoGeneral);

		let empleadoInfoEmpleado = new EmpleadoInfoEmpleado(
			this.tenerTrabajoActual ? this.infoEmpleadoFormGroup.value.trabajaActualmente : 'No',
			this.tenerTrabajoActual ? this.infoEmpleadoFormGroup.value.empresaActual : '',
			this.tenerTrabajoActual ? this.infoEmpleadoFormGroup.value.tipoEmpleado : 'SAG',
			this.tenerTrabajoActual ? this.infoEmpleadoFormGroup.value.tipoContrato : 'SAG',
			this.infoEmpleadoFormGroup.value.trabajoEmpresa,
			this.infoEmpleadoFormGroup.value.empleoAntes,
			this.fechasValidacion(this.infoEmpleadoFormGroup.value.fechaEmpleoAntes),
			this.esRecomendado ? this.infoEmpleadoFormGroup.value.recomendado : 'No',
			this.esRecomendado ? this.infoEmpleadoFormGroup.value.nombreRecomendado : '',
			this.esRecomendado ? this.infoEmpleadoFormGroup.value.dependenciaRecomendado : '',
			this.infoEmpleadoFormGroup.value.parientesEmpresa,
			this.infoEmpleadoFormGroup.value.parientesEmpresa == 'Si' ? this.infoEmpleadoFormGroup.value.nombrePariente : '',
			this.infoEmpleadoFormGroup.value.parientesEmpresa == 'Si' ? this.infoEmpleadoFormGroup.value.dependenciaPariente : '',
			this.infoEmpleadoFormGroup.value.conocimientoVacante,
			this.infoEmpleadoFormGroup.value.ciudadVivido,
			this.infoEmpleadoFormGroup.value.ciudadesTrabajado,
			this.infoEmpleadoFormGroup.value.trabajoOtraCiudad,
			this.infoEmpleadoFormGroup.value.tipoVivienda,
			this.viviendaAlquilada ? this.infoEmpleadoFormGroup.value.nombreArrendador : '',
			this.viviendaAlquilada ? this.infoEmpleadoFormGroup.value.telefonoArrendador : '',
			this.infoEmpleadoFormGroup.value.tiempoVivienda,
			this.tenerIngresoAdicional ? this.infoEmpleadoFormGroup.value.ingresosAdicionales : 'No',
			this.tenerIngresoAdicional ? this.infoEmpleadoFormGroup.value.valorIngresosAdicionales + '.0' : '.0',
			this.infoEmpleadoFormGroup.value.valorObligaciones + '.0',
			this.infoEmpleadoFormGroup.value.conceptoObligaciones,
			this.infoEmpleadoFormGroup.value.aspiracionSalarial + '.0',
			this.infoEmpleadoFormGroup.value.reconocimientos,
			this.pertenecerGrupo ? this.infoEmpleadoFormGroup.value.perteneceGrupo : 'No',
			this.infoEmpleadoFormGroup.value.hobbies,
			this.infoEmpleadoFormGroup.value.deportes,
			this.infoEmpleadoFormGroup.value.expectativas
		); //console.log(empleadoInfoEmpleado);

		let empleadoInfFamiliar = new EmpleadoInfoFamiliar(
			this.infoFamiliarFormGroup.value.nombrePareja,
			this.infoFamiliarFormGroup.value.profesionPareja,
			this.infoFamiliarFormGroup.value.empresaPareja,
			this.infoFamiliarFormGroup.value.cargoActualPareja,
			this.infoFamiliarFormGroup.value.direccionF,
			this.infoFamiliarFormGroup.value.contacto,
			this.infoFamiliarFormGroup.value.ciudad,
			this.infoFamiliarFormGroup.value.numHermanos
		); //console.log(empleadoInfFamiliar);
		//console.log(this.infoEducacionArray);
		this.botonFinalizar = false;

		const responseEmpleado = await this._empleadosService.registrarEmpleado(empleadoInfPersonal, empleadoInfoGeneral, empleadoInfoEmpleado, empleadoInfFamiliar, this.infoEducacionArray);
		//console.log(responseEmpleado);
		if (responseEmpleado["status"] === "success" && responseEmpleado["code"] === "200") {
			this.loading = false;
			this.mensajeRespuesta = '¡Perfecto! Empleado guardado con exito.';
			swal('¡Bien!', responseEmpleado["message"], 'success')
				.then(result => {
					this.regresar();
				}).catch(swal.noop);
		}
		else if (responseEmpleado["status"] === "success" && responseEmpleado["code"] === "300") {
			this.loading = false;
			this.mensajeRespuesta = responseEmpleado['message'];
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseEmpleado['message'] + '<b>', 2000);
		}
		else if (responseEmpleado["status"] === "error" && responseEmpleado["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseEmpleado['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'danger', 'warning', '<b>' + responseEmpleado['message'] + '<b>', 2000);
			console.log(responseEmpleado);
		}
	}

	async cargarInformacionSelect() {
		this.loading = true;
		let tipoDocumentos = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo Documento');
		let categoriaLicencia = await this._parametrizacionService.listarCategoriaParametrizacion('Licencia de Conducción');
		let claseLibretaMilitar = await this._parametrizacionService.listarCategoriaParametrizacion('Libreta militar');
		let colorPreferido = await this._parametrizacionService.listarCategoriaParametrizacion('Colores');
		let estadoCivil = await this._parametrizacionService.listarCategoriaParametrizacion('Estado Civil');
		let hobby = await this._parametrizacionService.listarCategoriaParametrizacion('Hobby');
		let profesion = await this._parametrizacionService.listarCategoriaParametrizacion('Profesión');
		let tipoContrato = await this._parametrizacionService.listarCategoriaParametrizacion('Tipos de Contrato');

		let tipoEmpleado = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo de Empleado');
		let tipoVivienda = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo de Vivienda');
		let tipoDeportes = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo de Deporte');

		let tipoEstudios = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo de Educación');
		let tipoCursos = await this._parametrizacionService.listarCategoriaParametrizacion('Estado Curso');

		tipoDocumentos['data'].forEach((element) => {
			this.tipoDocumentosArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		categoriaLicencia['data'].forEach((element) => {
			this.categoriaLicenciaArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		claseLibretaMilitar['data'].forEach((element) => {
			this.claseLibretaMilitarArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		colorPreferido['data'].forEach((element) => {
			this.colorPreferidoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		estadoCivil['data'].forEach((element) => {
			this.estadoCivilArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		hobby['data'].forEach((element) => {
			this.hobbyArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		profesion['data'].forEach((element) => {
			this.profesionArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		tipoContrato['data'].forEach((element) => {
			this.tipoContratoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		tipoEmpleado['data'].forEach((element) => {
			this.tipoEmpleadoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		tipoVivienda['data'].forEach((element) => {
			this.tipoViviendaArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		tipoDeportes['data'].forEach((element) => {
			this.tipoDeportesArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		tipoEstudios['data'].forEach((element) => {
			this.tipoEstudiosArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		tipoCursos['data'].forEach((element) => {
			this.tipoCursosArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
		this.loading = false;
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	fechasValidacion(formatoFecha) {
		if (formatoFecha) {
			//Formatear fecha
			const fechaActualTem = this._datepipe
				.transform(formatoFecha, "yyyy/MM/dd")
				.replace(/\//g, "-");
			//console.log("fecha formato: "+fechaActualTem);
			return fechaActualTem;
		}
		return '';
	}

	ocultarInput(opc: any, valor: any) {
		//console.log(opc);
		switch (opc) {
			case 'libreta':
				if (valor == 1) {
					this.tenerLibretaMilitar = true;
				} else {
					this.tenerLibretaMilitar = false;
				}
				break;
			case 'licencia':
				if (valor == 1) {
					this.tenerLicencia = true;
				} else {
					this.tenerLicencia = false;
				}
				break;
			case 'trabaja':
				if (valor == 1) {
					this.tenerTrabajoActual = true;
				} else {
					this.tenerTrabajoActual = false;
				}
				break;
			case 'ingresoAdicional':
				if (valor == 1) {
					this.tenerIngresoAdicional = true;
				} else {
					this.tenerIngresoAdicional = false;
				}
				break;
			case 'recomendado':
				if (valor == 1) {
					this.esRecomendado = true;
				} else {
					this.esRecomendado = false;
				}
				break;
			case 'pariente':
				if (valor == 1) {
					this.tieneParientes = true;
				} else {
					this.tieneParientes = false;
				}
				break;
			case 'vivienda':
				if (valor == 'Alquilada') {
					this.viviendaAlquilada = true;
				} else {
					this.viviendaAlquilada = false;
				}
				break;
			case 'grupoPertenecer':
				if (valor == 1) {
					this.pertenecerGrupo = true;
				} else {
					this.pertenecerGrupo = false;
				}
				break;
		}
	}

	onChangeVacantes(nombre: string, isChecked: boolean) {
		const opcionVacante = (this.infoEmpleadoFormGroup.controls.conocimientoVacante as FormArray);

		if (isChecked) {
			opcionVacante.push(new FormControl(nombre));
		} else {
			const index = opcionVacante.controls.findIndex(x => x.value === nombre);
			opcionVacante.removeAt(index);
		}
	}

	concatenarArray(datosArray: any) {
		//console.log(datosArray);
		let cadenaArray = datosArray.join(',');
		//console.log(cadenaArray);
		return cadenaArray;
	}

	regresar() {
		if (this.opcionVentana == '2') {
			if (!this.infoPersonalFormGroup.pristine || !this.infoGeneralFormGroup.pristine || !this.infoEmpleadoFormGroup || !this.infoFamiliarFormGroup.pristine) {
				swal({
					title: 'Salir',
					text: '¿Seguro que desea salir de esta pagina sin guardar cambios recientes?',
					type: 'question',
					showCancelButton: true,
					confirmButtonClass: 'btn btn-warning',
					cancelButtonClass: 'btn btn-danger',
					confirmButtonText: 'Sí',
					cancelButtonText: 'No',
					buttonsStyling: true,
					reverseButtons: true
				}).then((result) => {
					if (result.value) {
						this._router.navigate(["empleados/ficha-empleados/", this.ideEmpleado]);
					}
				}).catch(swal.noop);
			} else {
				this._router.navigate(["empleados/ficha-empleados/", this.ideEmpleado]);
			}

		} else {
			this._router.navigate(["empleados/listar"]);
		}
	}

	/* Inicio Educacion */
	borrarFilaalArrayEducacion(cod: number) {
		swal({
			title: 'Borrar título',
			text: '¿Desea borrar el título ingresado recientemente?',
			type: 'question',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-warning',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: 'Sí',
			cancelButtonText: 'No',
			buttonsStyling: true,
			reverseButtons: true
		}).then((result) => {
			if (result.value) {
				this.infoEducacionArray.splice(cod, 1);
				if (this.opcionVentana == '1') this.comprobarTamanioArrayTipoEducacion(this.infoEducacionArray);
				else {
					let array1: boolean = this.comprobarTamanioArrayEducacion(this.infoEducacionTempArray);
					let array2: boolean = this.comprobarTamanioArrayEducacion(this.infoEducacionArray);
					if (!array1 && !array2) this.mostrarTiposEducacion = false;
					else this.mostrarTiposEducacion = true;
				}
			}
		}).catch(swal.noop);
	}

	agregarTituloAlArrayEducacion() {
		if ((this.tituloEducacion.tipoEstudio != 'SAG') && this.tituloEducacion.tituloObtenido && this.tituloEducacion.institucion) {
			/* Arreglo para guardar los datos en la base de datos solo cuando este modificando al empleado */
			this.infoEducacionArray.push(new EmpleadoInfoEducacion(
				'',
				this.tituloEducacion.tipoEstudio,
				this.estudiosBasicos ? this.tituloEducacion.anioFinalizacion : '',
				this.estudiosBasicos ? this.tituloEducacion.aniosCursados : '',
				this.duracionMesesVisible ? this.tituloEducacion.duracionMeses : '',
				this.tituloEducacion.tituloObtenido,
				this.tituloEducacion.institucion,
				this.tituloEducacion.ciudad,
				this.estadoCursoOpc ? this.tituloEducacion.intensidadHoraria : '',
				this.estadoCursoOpc ? this.tituloEducacion.estadoCurso : 'SAG',
				this.duracionMesesVisible ? this.tituloEducacion.anioSemestreCursa : '',
				this.tituloEducacion.observacion,
				'No'
			));

			this.tituloEducacion = new EmpleadoInfoEducacion('', 'SAG', '', '', '', '', '', '', '', 'SAG', '', '', '');
			this.comprobarTamanioArrayTipoEducacion(this.infoEducacionArray);
		} else {
			swal('Advertencia!', 'Debe ingresar todos los campos del formulario para poder agregar un título, diplomado y/o curso.', 'warning');
		}
	}

	tipoEstudioCursoValidar(opc: any, tipo: any) {
		let estudioTipo: any[] = ['Curso', 'Diplomado', 'Seminario'];
		switch (opc) {
			case 'tipoEstudioValor':
				if (estudioTipo.includes(tipo)) {
					this.estadoCursoOpc = true;
					this.estudiosBasicos = false;
				} else {
					this.estadoCursoOpc = false;
					this.estudiosBasicos = true;
				}
				break;
			case 'tipoCursosValor':
				if (tipo == 'En curso') {
					this.duracionMesesVisible = true;
				} else {
					this.duracionMesesVisible = false;
				}
				break;
		}
	}

	/* Para el formulario de creacion de empleado */
	comprobarTamanioArrayTipoEducacion(arrayEducacion: any) {
		if (arrayEducacion.length > 0) this.mostrarTiposEducacion = true;
		else this.mostrarTiposEducacion = false;
	}

	/* Para el formulario de actualizar el empleado */
	comprobarTamanioArrayEducacion(arrayEducacion: any) {
		if (arrayEducacion.length > 0) return true;
		else return false;
	}
	/* Fin Educacion */
	/* Fin Seccion para crear empleado */

	/* Inicia seccion actualizar empleado */
	modificarEmpleado(formNombre: string) {
		switch (formNombre) {
			case 'infoPersonal':
				this.actualizarEmpleadoInfoPersonal();
				break;
			case 'infoGeneral':
				this.actualizarEmpleadoInfoGeneral();
				break;
			case 'infoEmpleado':
				this.actualizarEmpleadoInfoEmpleado();
				break;
			case 'infoFamiliar':
				this.actualizarEmpleadoInfoFamiliar();
				break;
			case 'infoEducacion':

				break;

			default:
				break;
		}
	}

	async listarDatosPersonalesId(idEmpleado: any) {
		this.loading = true;

		const responseDatosPersonalesId = await this._empleadosService.listarEmpleados(2, idEmpleado, '', '');
		//console.log(responseDatosPersonalesId);
		if (responseDatosPersonalesId["status"] === "success" && responseDatosPersonalesId["code"] === "200") {
			this.cargarInformacionPersonalId(responseDatosPersonalesId['data']);
			this.loading = false;
		}
		else if (responseDatosPersonalesId["status"] === "success" && responseDatosPersonalesId["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseDatosPersonalesId['message'] + '<b>', 2000);
		}
		else if (responseDatosPersonalesId["status"] === "error" && responseDatosPersonalesId["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseDatosPersonalesId['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseDatosPersonalesId["message"], 'OK', 'error');
		}
	}

	async listarInformacionGeneralId(idEmpleado: any) {
		this.loading = true;

		const responseInforGeneral = await this._empleadosService.listarInfGeneralEmpleadoPorId(2, idEmpleado);
		//console.log(responseInforGeneral);
		if (responseInforGeneral["status"] === "success" && responseInforGeneral["code"] === "200") {
			this.cargarInformacionGeneralId(responseInforGeneral['data']);
			this.loading = false;
		}
		else if (responseInforGeneral["status"] === "success" && responseInforGeneral["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseInforGeneral['message'] + '<b>', 2000);
		}
		else if (responseInforGeneral["status"] === "error" && responseInforGeneral["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseInforGeneral['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseInforGeneral["message"], 'OK', 'error');
		}
	}

	async listarInformacionEmpleadoId(idInfoGeneral: any) {
		this.loading = true;

		const responseInforEmpleado = await this._empleadosService.listarInfEmpleadoEmpleadoPorId(2, idInfoGeneral);
		//console.log(responseInforEmpleado);
		if (responseInforEmpleado["status"] === "success" && responseInforEmpleado["code"] === "200") {
			this.cargarInformacionEmpleadoId(responseInforEmpleado['data']);
			this.loading = false;
		}
		else if (responseInforEmpleado["status"] === "success" && responseInforEmpleado["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseInforEmpleado['message'] + '<b>', 2000);
		}
		else if (responseInforEmpleado["status"] === "error" && responseInforEmpleado["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseInforEmpleado['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseInforEmpleado["message"], 'OK', 'error');
		}
	}

	async listarInformacionFamiliarId(idEmpleado: any) {
		this.loading = true;

		const responseInforFamiliar = await this._empleadosService.listarInfFamiliarEmpleadoPorId(2, idEmpleado);
		//console.log(responseInforFamiliar);
		if (responseInforFamiliar["status"] === "success" && responseInforFamiliar["code"] === "200") {
			this.cargarInformacionFamiliarId(responseInforFamiliar['data']);
			this.loading = false;
		}
		else if (responseInforFamiliar["status"] === "success" && responseInforFamiliar["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseInforFamiliar['message'] + '<b>', 2000);
		}
		else if (responseInforFamiliar["status"] === "error" && responseInforFamiliar["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseInforFamiliar['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseInforFamiliar["message"], 'OK', 'error');
		}
	}

	async listarEducacionEmpleadoId(idEmpleado: any) {
		this.loading = true;

		const responseEducacion = await this._empleadosService.listarEducacionEmpleadoPorId(2, idEmpleado);
		//console.log(responseEducacion);
		if (responseEducacion["status"] === "success" && responseEducacion["code"] === "200") {
			this.cargarEducacionId(responseEducacion['data']);
			this.loading = false;
		}
		else if (responseEducacion["status"] === "success" && responseEducacion["code"] === "300") {
			this.infoEducacionArray = [];
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseEducacion['message'] + '<b>', 2000);
			this.mostrarTiposEducacion = false;
			this.loading = false;
		}
		else if (responseEducacion["status"] === "error" && responseEducacion["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseEducacion['message']);
		}
		else {
			this.infoEducacionArray = [];
			this._sweetAlertService.alertGeneral('¡Error!', responseEducacion["message"], 'OK', 'error');
			this.mostrarTiposEducacion = false;
			this.loading = false;
		}
	}

	cargarInformacionPersonalId(datosPersonales: any) {
		//console.log(datosPersonales);
		let datosPersonalesEmpleado: InfoPersonalEmpleado = datosPersonales.empleado;
		this.ideEmpleado = datosPersonales.empleado.id;

		this.infoPersonalFormGroup.setValue({
			tipoDoc: datosPersonalesEmpleado.tipoDocumento.valor,
			documento: datosPersonalesEmpleado.documento,
			nombres: datosPersonalesEmpleado.nombres,
			apellidos: datosPersonalesEmpleado.apellidos
		});

		this.listarInformacionGeneralId(datosPersonales.empleado.id);
	}

	cargarInformacionGeneralId(datosGenerales: any) {
		//console.log(datosGenerales);
		let datosGeneralesEmpleado: InfoGeneralEmpleado = datosGenerales;
		this.idGeneralData = datosGenerales.id;
		this.tenerLibretaMilitar = datosGeneralesEmpleado.libretaMilitar == 'No' ? false : true;
		this.tenerLicencia = datosGeneralesEmpleado.licenciaConduccion == 'No' ? false : true;

		this.infoGeneralFormGroup.setValue({
			correo: datosGeneralesEmpleado.correo,
			fechaExpedicion: datosGeneralesEmpleado.fechaExpedicion,
			fechaNacimiento: datosGeneralesEmpleado.fechaNacimiento,
			direccion: datosGeneralesEmpleado.direccion,
			lugarResidencia: datosGeneralesEmpleado.lugarResidencia,
			telefonoFijo: datosGeneralesEmpleado.telefonoFijo,
			celular: datosGeneralesEmpleado.celular,
			estadoCivil: datosGeneralesEmpleado.estadoCivil.valor,
			aniosExperienciaLaboral: datosGeneralesEmpleado.aniosExperienciaLaboral,
			tieneLibretaMilitar: datosGeneralesEmpleado.libretaMilitar == 'No' ? datosGeneralesEmpleado.libretaMilitar : 'Si',
			libretaMilitar: datosGeneralesEmpleado.libretaMilitar,
			claseLibretaMilitar: datosGeneralesEmpleado.claseLibretaMilitar.valor,
			distritoLibretaMilitar: datosGeneralesEmpleado.distritoLibretaMilitar,
			tarjetaProfesional: datosGeneralesEmpleado.tarjetaProfesional,
			grupoSanguineo: datosGeneralesEmpleado.grupoSanguineo,
			tieneLicenciaConduccion: datosGeneralesEmpleado.licenciaConduccion == 'No' ? datosGeneralesEmpleado.licenciaConduccion : 'Si',
			licenciaConduccion: datosGeneralesEmpleado.licenciaConduccion,
			categoriaLicencia: datosGeneralesEmpleado.categoriaLicencia.valor,
			color: datosGeneralesEmpleado.colorPreferido.valor,
			hobby1: datosGeneralesEmpleado.hobby1.valor,
			hobby2: datosGeneralesEmpleado.hobby2.valor,
			profesion: datosGeneralesEmpleado.profesion.valor,
			genero: datosGeneralesEmpleado.genero
		});

		this.listarInformacionEmpleadoId(datosGenerales.id);
	}

	cargarInformacionEmpleadoId(datosEmpleado: any) {
		//console.log(datosEmpleado);
		let datosEmpleadoEmpleado: InfoEmpleadoEmpleado = datosEmpleado;
		this.idEmpleadoData = datosEmpleado.id;
		this.tenerTrabajoActual = datosEmpleadoEmpleado.trabajaActualmente == 'No' ? false : true;
		this.esRecomendado = datosEmpleadoEmpleado.recomendado == 'No' ? false : true;
		this.tieneParientes = datosEmpleadoEmpleado.parientesEmpresa == 'No' ? false : true;
		this.tenerIngresoAdicional = datosEmpleadoEmpleado.ingresosAdicionales == 'No' ? false : true;
		this.viviendaAlquilada = datosEmpleadoEmpleado.tipoVivienda.valor == 'Alquilada' ? true : false;
		this.pertenecerGrupo = datosEmpleadoEmpleado.perteneceGrupo == 'No' ? false : true;

		this.infoEmpleadoFormGroup.setValue({
			trabajaActualmente: datosEmpleadoEmpleado.trabajaActualmente,
			empresaActual: datosEmpleadoEmpleado.empresaActual,
			tipoEmpleado: datosEmpleadoEmpleado.tipoEmpleado.valor,
			tipoContrato: datosEmpleadoEmpleado.tipoContrato.valor,
			trabajoEmpresa: datosEmpleadoEmpleado.trabajoEmpresa,
			empleoAntes: datosEmpleadoEmpleado.empleoAntes,
			fechaEmpleoAntes: datosEmpleadoEmpleado.fechaEmpleoAntes,
			recomendado: datosEmpleadoEmpleado.recomendado,
			nombreRecomendado: datosEmpleadoEmpleado.nombreRecomendado,
			dependenciaRecomendado: datosEmpleadoEmpleado.dependenciaRecomendado,
			parientesEmpresa: datosEmpleadoEmpleado.parientesEmpresa,
			nombrePariente: datosEmpleadoEmpleado.nombrePariente,
			dependenciaPariente: datosEmpleadoEmpleado.dependenciaPariente,
			conocimientoVacante: datosEmpleadoEmpleado.conocimientoVacante,
			ciudadVivido: datosEmpleadoEmpleado.ciudadVivido,
			ciudadesTrabajado: datosEmpleadoEmpleado.ciudadesTrabajado,
			trabajoOtraCiudad: datosEmpleadoEmpleado.trabajoOtraCiudad,
			tipoVivienda: datosEmpleadoEmpleado.tipoVivienda.valor,
			nombreArrendador: datosEmpleadoEmpleado.nombreArrendador,
			telefonoArrendador: datosEmpleadoEmpleado.telefonoArrendador,
			tiempoVivienda: datosEmpleadoEmpleado.tiempoVivienda,
			tieneIngresosAdicionales: datosEmpleadoEmpleado.ingresosAdicionales == 'No' ? datosEmpleadoEmpleado.ingresosAdicionales : 'Si',
			ingresosAdicionales: datosEmpleadoEmpleado.ingresosAdicionales,
			valorIngresosAdicionales: datosEmpleadoEmpleado.valorIngresosAdicionales,
			valorObligaciones: datosEmpleadoEmpleado.valorObligaciones,
			conceptoObligaciones: datosEmpleadoEmpleado.conceptoObligaciones,
			aspiracionSalarial: datosEmpleadoEmpleado.aspiracionSalarial,
			reconocimientos: datosEmpleadoEmpleado.reconocimientos,
			tenerGrupoPertenece: datosEmpleadoEmpleado.perteneceGrupo == 'No' ? datosEmpleadoEmpleado.perteneceGrupo : 'Si',
			perteneceGrupo: datosEmpleadoEmpleado.perteneceGrupo,
			hobbies: datosEmpleadoEmpleado.hobbies,
			deportes: datosEmpleadoEmpleado.deportes.valor,
			expectativas: datosEmpleadoEmpleado.expectativas
		});

		this.listarInformacionFamiliarId(datosEmpleado.id);
	}

	cargarInformacionFamiliarId(datosFamiliares: any) {
		//console.log(datosFamiliares);
		let datosFamiliaresEmpleado: InfoFamiliarEmpleado = datosFamiliares;
		this.idFamiliarData = datosFamiliares.id;

		this.infoFamiliarFormGroup.setValue({
			nombrePareja: datosFamiliaresEmpleado.nombrePareja,
			profesionPareja: datosFamiliaresEmpleado.profesionPareja.valor,
			empresaPareja: datosFamiliaresEmpleado.empresaPareja,
			cargoActualPareja: datosFamiliaresEmpleado.cargoActualPareja,
			direccionF: datosFamiliaresEmpleado.direccion,
			contacto: datosFamiliaresEmpleado.contacto,
			ciudad: datosFamiliaresEmpleado.ciudad,
			numHermanos: datosFamiliaresEmpleado.numHermanos
		});

		this.listarEducacionEmpleadoId(this.idEmpleadoData);
	}

	cargarEducacionId(datosEducacion: any) {
		//console.log(datosEducacion);
		this.mostrarTiposEducacion = true;
		this.infoEducacionTempArray = [];

		let jsonArray: any;
		datosEducacion.forEach((element) => {
			jsonArray = {
				id: element.id,
				tipoEstudio: element.tipoEstudio['valor'],
				anioFinalizacion: element.anioFinalizacion,
				aniosCursados: element.aniosCursados,
				duracionMeses: element.duracionMeses,
				tituloObtenido: element.tituloObtenido,
				institucion: element.institucion,
				ciudad: element.ciudad,
				intensidadHoraria: element.intensidadHoraria,
				estadoCurso: element.estadoCurso['valor'],
				anioSemestreCursa: element.anioSemestreCursa,
				observacion: element.observacion,
				eliminar: 'Si'
			};
			this.infoEducacionTempArray.push(jsonArray);
		});
	}

	modalActualizarInformacion(opc: any) {
		//$("#modalCerrarNegocio").modal("hide");
		//$("#modalCerrarNegocio").modal("show");
		switch (opc) {
			case 'datosPersonales':
				$("#modalInformacionPersonal").modal("show");
				break;
			case 'informacionGeneral':
				$("#modalInformacionGeneral").modal("show");
				break;
			case 'informacionEmpleado':
				$("#modalInformacionEmpleado").modal("show");
				break;
			case 'informacionFamiliar':
				$("#modalInformacionFamiliar").modal("show");
				break;
			case 'educacion':
				$("#modalEducacion").modal("show");
				break;
		}
	}

	validacionCamposVisibles(opc: any, valorBuscar: any) {
		let devolverCadena: string = '';
		switch (opc) {
			case 'genero':
				const generoNombre = this.generoArray.find(element => element.valor == valorBuscar);

				if (generoNombre) devolverCadena = generoNombre.nombre;
				else devolverCadena = valorBuscar;
				break;
			case 'grupoSanguineo':
				let gruposSanguineo = [
					{ nombre: 'A+', valor: 'Ap' },
					{ nombre: 'B+', valor: 'Bp' },
					{ nombre: 'O+', valor: 'Op' },
					{ nombre: 'AB+', valor: 'ABp' }
				];
				const grupoSan = gruposSanguineo.find(element => element.valor == valorBuscar);

				if (grupoSan) devolverCadena = grupoSan.nombre;
				else devolverCadena = valorBuscar;
				break;
		}


		return devolverCadena;
	}
	/* Fin seccion actualizar empleado */

	/* Inicio Modificar datos de cada una de las secciones por separado */
	async actualizarEmpleadoInfoPersonal() {
		this.loading = true;
		let actualizarEmpleadoInfPersonal = new ActualizarEmpleadoInfoPersonal(
			this.infoPersonalFormGroup.value.tipoDoc,
			this.infoPersonalFormGroup.value.documento,
			this.infoPersonalFormGroup.value.nombres,
			this.infoPersonalFormGroup.value.apellidos
		); //console.log(actualizarEmpleadoInfPersonal);

		let responseActualizarInfPersEmpleado = await this._empleadosService.actualizarInfoPersonalEmpleado(this.ideEmpleado, actualizarEmpleadoInfPersonal);
		//console.log(responseActualizarInfPersEmpleado);
		if (responseActualizarInfPersEmpleado["status"] === "success" && responseActualizarInfPersEmpleado["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseActualizarInfPersEmpleado["message"], 'success')
				.then(result => {
					//this.listarDatosPersonalesId(this.ideEmpleado);
				}).catch(swal.noop);
		}
		else if (responseActualizarInfPersEmpleado["status"] === "success" && responseActualizarInfPersEmpleado["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseActualizarInfPersEmpleado["message"], 'warning');
		}
		else if (responseActualizarInfPersEmpleado["status"] === "error" && responseActualizarInfPersEmpleado["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseActualizarInfPersEmpleado['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseActualizarInfPersEmpleado["message"], 'error');
		}
	}

	async actualizarEmpleadoInfoGeneral() {
		this.loading = true;
		let actualizarEmpleadoInfGeneral = new EmpleadoInfoGeneral(
			this.infoGeneralFormGroup.value.correo,
			this.infoGeneralFormGroup.value.direccion,
			this.infoGeneralFormGroup.value.lugarResidencia,
			this.infoGeneralFormGroup.value.telefonoFijo,
			this.infoGeneralFormGroup.value.celular,
			this.infoGeneralFormGroup.value.estadoCivil,
			this.infoGeneralFormGroup.value.aniosExperienciaLaboral,
			this.fechasValidacion(this.infoGeneralFormGroup.value.fechaExpedicion),
			this.fechasValidacion(this.infoGeneralFormGroup.value.fechaNacimiento),
			this.tenerLibretaMilitar ? this.infoGeneralFormGroup.value.libretaMilitar : 'No',
			this.tenerLibretaMilitar ? this.infoGeneralFormGroup.value.claseLibretaMilitar : 'SAG',
			this.tenerLibretaMilitar ? this.infoGeneralFormGroup.value.distritoLibretaMilitar : '.',
			this.infoGeneralFormGroup.value.tarjetaProfesional,
			this.infoGeneralFormGroup.value.grupoSanguineo,
			this.tenerLicencia ? this.infoGeneralFormGroup.value.licenciaConduccion : 'No',
			this.tenerLicencia ? this.infoGeneralFormGroup.value.categoriaLicencia : 'SAG',
			this.infoGeneralFormGroup.value.color,
			this.infoGeneralFormGroup.value.hobby1,
			this.infoGeneralFormGroup.value.hobby2,
			this.infoGeneralFormGroup.value.profesion,
			this.infoGeneralFormGroup.value.genero
		);

		let responseActualizarInfGenEmpleado = await this._empleadosService.actualizarInfoGeneralEmpleado(this.idGeneralData, actualizarEmpleadoInfGeneral);
		//console.log(responseActualizarInfGenEmpleado);
		if (responseActualizarInfGenEmpleado["status"] === "success" && responseActualizarInfGenEmpleado["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseActualizarInfGenEmpleado["message"], 'success')
				.then(result => {
					//this.regresar();
				}).catch(swal.noop);
		}
		else if (responseActualizarInfGenEmpleado["status"] === "success" && responseActualizarInfGenEmpleado["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseActualizarInfGenEmpleado["message"], 'warning');
		}
		else if (responseActualizarInfGenEmpleado["status"] === "error" && responseActualizarInfGenEmpleado["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseActualizarInfGenEmpleado['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseActualizarInfGenEmpleado["message"], 'error');
		}
	}

	async actualizarEmpleadoInfoEmpleado() {
		this.loading = true;
		let actualizarEmpleadoInfEmpleado = new EmpleadoInfoEmpleado(
			this.tenerTrabajoActual ? this.infoEmpleadoFormGroup.value.trabajaActualmente : 'No',
			this.tenerTrabajoActual ? this.infoEmpleadoFormGroup.value.empresaActual : '.',
			this.tenerTrabajoActual ? this.infoEmpleadoFormGroup.value.tipoEmpleado : 'SAG',
			this.tenerTrabajoActual ? this.infoEmpleadoFormGroup.value.tipoContrato : 'SAG',
			this.infoEmpleadoFormGroup.value.trabajoEmpresa,
			this.infoEmpleadoFormGroup.value.empleoAntes,
			this.fechasValidacion(this.infoEmpleadoFormGroup.value.fechaEmpleoAntes),
			this.esRecomendado ? this.infoEmpleadoFormGroup.value.recomendado : 'No',
			this.esRecomendado ? this.infoEmpleadoFormGroup.value.nombreRecomendado : '.',
			this.esRecomendado ? this.infoEmpleadoFormGroup.value.dependenciaRecomendado : '.',
			this.infoEmpleadoFormGroup.value.parientesEmpresa,
			this.infoEmpleadoFormGroup.value.parientesEmpresa == 'Si' ? this.infoEmpleadoFormGroup.value.nombrePariente : '.',
			this.infoEmpleadoFormGroup.value.parientesEmpresa == 'Si' ? this.infoEmpleadoFormGroup.value.dependenciaPariente : '.',
			this.infoEmpleadoFormGroup.value.conocimientoVacante,
			this.infoEmpleadoFormGroup.value.ciudadVivido,
			this.infoEmpleadoFormGroup.value.ciudadesTrabajado,
			this.infoEmpleadoFormGroup.value.trabajoOtraCiudad,
			this.infoEmpleadoFormGroup.value.tipoVivienda,
			this.viviendaAlquilada ? this.infoEmpleadoFormGroup.value.nombreArrendador : '.',
			this.viviendaAlquilada ? this.infoEmpleadoFormGroup.value.telefonoArrendador : '.',
			this.infoEmpleadoFormGroup.value.tiempoVivienda,
			this.tenerIngresoAdicional ? this.infoEmpleadoFormGroup.value.ingresosAdicionales : 'No',
			this.tenerIngresoAdicional ? this.infoEmpleadoFormGroup.value.valorIngresosAdicionales + '.0' : '.0',
			this.infoEmpleadoFormGroup.value.valorObligaciones + '.0',
			this.infoEmpleadoFormGroup.value.conceptoObligaciones,
			this.infoEmpleadoFormGroup.value.aspiracionSalarial + '.0',
			this.infoEmpleadoFormGroup.value.reconocimientos,
			this.pertenecerGrupo ? this.infoEmpleadoFormGroup.value.perteneceGrupo : 'No',
			this.infoEmpleadoFormGroup.value.hobbies,
			this.infoEmpleadoFormGroup.value.deportes,
			this.infoEmpleadoFormGroup.value.expectativas
		);

		let responseActualizarInfEmplEmpleado = await this._empleadosService.actualizarInfoEmpleadoEmpleado(this.idEmpleadoData, actualizarEmpleadoInfEmpleado);
		//console.log(responseActualizarInfEmplEmpleado);
		if (responseActualizarInfEmplEmpleado["status"] === "success" && responseActualizarInfEmplEmpleado["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseActualizarInfEmplEmpleado["message"], 'success')
				.then(result => {
					//this.regresar();
				}).catch(swal.noop);
		}
		else if (responseActualizarInfEmplEmpleado["status"] === "success" && responseActualizarInfEmplEmpleado["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseActualizarInfEmplEmpleado["message"], 'warning');
		}
		else if (responseActualizarInfEmplEmpleado["status"] === "error" && responseActualizarInfEmplEmpleado["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseActualizarInfEmplEmpleado['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseActualizarInfEmplEmpleado["message"], 'error');
		}
	}

	async actualizarEmpleadoInfoFamiliar() {
		this.loading = true;
		let actualizarEmpleadoInfFamiliar = new EmpleadoInfoFamiliar(
			this.infoFamiliarFormGroup.value.nombrePareja,
			this.infoFamiliarFormGroup.value.profesionPareja,
			this.infoFamiliarFormGroup.value.empresaPareja,
			this.infoFamiliarFormGroup.value.cargoActualPareja,
			this.infoFamiliarFormGroup.value.direccionF,
			this.infoFamiliarFormGroup.value.contacto,
			this.infoFamiliarFormGroup.value.ciudad,
			this.infoFamiliarFormGroup.value.numHermanos
		);

		let responseActualizarInfFamiEmpleado = await this._empleadosService.actualizarInfoFamiliarEmpleado(this.idEmpleadoData, actualizarEmpleadoInfFamiliar);
		//console.log(responseActualizarInfFamiEmpleado);
		if (responseActualizarInfFamiEmpleado["status"] === "success" && responseActualizarInfFamiEmpleado["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseActualizarInfFamiEmpleado["message"], 'success')
				.then(result => {
					//this.regresar();
				}).catch(swal.noop);
		}
		else if (responseActualizarInfFamiEmpleado["status"] === "success" && responseActualizarInfFamiEmpleado["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseActualizarInfFamiEmpleado["message"], 'warning');
		}
		else if (responseActualizarInfFamiEmpleado["status"] === "error" && responseActualizarInfFamiEmpleado["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseActualizarInfFamiEmpleado['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseActualizarInfFamiEmpleado["message"], 'error');
		}
	}

	/* Inicio modificar cada una de los titulos educativos de la seccion de educacion */
	modificarTituloEducacionColocar(tituloEducacion: any) {
		//console.log(tituloEducacion);
		this.tituloEducacionModificar.id = tituloEducacion.id;
		this.tituloEducacionModificar.tipoEstudio = tituloEducacion.tipoEstudio;
		this.tituloEducacionModificar.anioFinalizacion = tituloEducacion.anioFinalizacion;
		this.tituloEducacionModificar.aniosCursados = tituloEducacion.aniosCursados;
		this.tituloEducacionModificar.duracionMeses = tituloEducacion.duracionMeses;
		this.tituloEducacionModificar.tituloObtenido = tituloEducacion.tituloObtenido;
		this.tituloEducacionModificar.institucion = tituloEducacion.institucion;
		this.tituloEducacionModificar.ciudad = tituloEducacion.ciudad;
		this.tituloEducacionModificar.intensidadHoraria = tituloEducacion.intensidadHoraria;
		this.tituloEducacionModificar.estadoCurso = tituloEducacion.estadoCurso;
		this.tituloEducacionModificar.anioSemestreCursa = tituloEducacion.anioSemestreCursa;
		this.tituloEducacionModificar.observacion = tituloEducacion.observacion;
		this.tituloEducacionModificar.eliminar = tituloEducacion.eliminar;
		this.tipoEstudioCursoValidar('tipoEstudioValor', tituloEducacion.tipoEstudio);
		this.tipoEstudioCursoValidar('tipoCursosValor', tituloEducacion.estadoCurso);
		$("#modalEducacion").modal("show");
	}

	onSubmitModificarTituloEducacionEmpleado() {
		swal({
			title: 'Modificar título educativo',
			text: '¿Esta seguro de que desea actualizar este título acdémico?',
			type: 'question',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-warning',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: 'Sí',
			cancelButtonText: 'No',
			buttonsStyling: true,
			reverseButtons: true
		}).then((result) => {
			if (result.value) {
				this.modificarTituloEducacionEmpleado();
			}
		}).catch(swal.noop);
	}

	async modificarTituloEducacionEmpleado() {
		this.loading = true;
		let actualizarTituloEducacion = new ActualizarEmpleadoInfoEducacion(
			'',
			this.tituloEducacionModificar.tipoEstudio,
			this.estudiosBasicos ? this.tituloEducacionModificar.anioFinalizacion : '.',
			this.estudiosBasicos ? this.tituloEducacionModificar.aniosCursados : '.',
			this.duracionMesesVisible ? this.tituloEducacionModificar.duracionMeses : '.',
			this.tituloEducacionModificar.tituloObtenido,
			this.tituloEducacionModificar.institucion,
			this.tituloEducacionModificar.ciudad,
			this.estadoCursoOpc ? this.tituloEducacionModificar.intensidadHoraria : '.',
			this.estadoCursoOpc ? this.tituloEducacionModificar.estadoCurso : 'SAG',
			this.duracionMesesVisible ? this.tituloEducacionModificar.anioSemestreCursa : '.',
			this.tituloEducacionModificar.observacion,
			this.idEmpleadoData
		);

		let responseModificarTitEducacionEmpleado = await this._empleadosService.modificarTituloEducacionEmpleado(this.tituloEducacionModificar.id, actualizarTituloEducacion);
		//console.log(responseModificarTitEducacionEmpleado);
		if (responseModificarTitEducacionEmpleado["status"] === "success" && responseModificarTitEducacionEmpleado["code"] === "200") {
			this._sweetAlertService.showNotification('top', 'right', 'success', 'success', '<b>' + responseModificarTitEducacionEmpleado['message'] + '<b>', 2000);
			this.tituloEducacionModificar = new EmpleadoInfoEducacion('', 'SAG', '', '', '', '', '', '', '', 'SAG', '', '', '');
			this.listarEducacionEmpleadoId(this.idEmpleadoData);
			$("#modalEducacion").modal("hide");
			this.loading = false;
		}
		else if (responseModificarTitEducacionEmpleado["status"] === "success" && responseModificarTitEducacionEmpleado["code"] === "300") {
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseModificarTitEducacionEmpleado['message'] + '<b>', 2000);
			this.loading = false;
		}
		else if (responseModificarTitEducacionEmpleado["status"] === "error" && responseModificarTitEducacionEmpleado["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseModificarTitEducacionEmpleado['message']);
		}
		else {
			swal('¡Error!', responseModificarTitEducacionEmpleado["message"], 'error');
			this.loading = false;
		}
	}

	/* Inicio Eliminar titulos de la seccion de educacion */
	onSubmitEliminarTituloEducacion(idTituloEduc: any, posicionTitulo: any) {
		swal({
			title: 'Borrar título',
			text: '¿Seguro que desea borrar el título acdémico ingresado con aterioridad cuando fue creado el empleado?',
			type: 'question',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-warning',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: 'Sí',
			cancelButtonText: 'No',
			buttonsStyling: true,
			reverseButtons: true
		}).then((result) => {
			if (result.value) {
				this.eliminarTituloEducacion(idTituloEduc, posicionTitulo);
			}
		}).catch(swal.noop);
	}

	async eliminarTituloEducacion(idTituloEduc: any, posicionTitulo: any) {
		this.loading = true;

		let responseEliminarTitEducacionEmpleado = await this._empleadosService.eliminarTituloEducacionEmpleado(3, idTituloEduc);
		//console.log(responseEliminarTitEducacionEmpleado);
		if (responseEliminarTitEducacionEmpleado["status"] === "success" && responseEliminarTitEducacionEmpleado["code"] === "200") {
			this.loading = false;
			this.infoEducacionTempArray.splice(posicionTitulo, 1);
			let array1: boolean = this.comprobarTamanioArrayEducacion(this.infoEducacionTempArray);
			let array2: boolean = this.comprobarTamanioArrayEducacion(this.infoEducacionArray);
			if (!array1 && !array2) this.mostrarTiposEducacion = false;
			else this.mostrarTiposEducacion = true;
			this._sweetAlertService.showNotification('top', 'right', 'success', 'success', '<b>' + responseEliminarTitEducacionEmpleado['message'] + '<b>', 2000);
		}
		else if (responseEliminarTitEducacionEmpleado["status"] === "success" && responseEliminarTitEducacionEmpleado["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseEliminarTitEducacionEmpleado['message'] + '<b>', 2000);
		}
		else if (responseEliminarTitEducacionEmpleado["status"] === "error" && responseEliminarTitEducacionEmpleado["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseEliminarTitEducacionEmpleado['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseEliminarTitEducacionEmpleado["message"], 'error');
		}
	}
	/* Fin Eliminar titulos de la seccion de educacion */

	/* Inicio Agregar nuevos titulos educativos que se digitaron en la seccion de educacion */
	onSubmitCreartitulosAcademicos() {
		swal({
			title: 'Agregar títulos Académicos',
			text: '¿Esta seguro de que desea agregar nuevos títulos académicos?',
			type: 'question',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-warning',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: 'Sí',
			cancelButtonText: 'No',
			buttonsStyling: true,
			reverseButtons: true
		}).then((result) => {
			if (result.value) {
				this.registrarNuevosTitulosEducativos();
			}
		}).catch(swal.noop);
	}

	async registrarNuevosTitulosEducativos() {
		const responseNuevosTitulosEducativos = await this._empleadosService.registrarTitulosEducacionEmpleado(this.idEmpleadoData, this.infoEducacionArray);
		//console.log(responseNuevosTitulosEducativos);
		if (responseNuevosTitulosEducativos["status"] === "success" && responseNuevosTitulosEducativos["code"] === "200") {
			this._sweetAlertService.showNotification('top', 'right', 'success', 'success', '<b>' + responseNuevosTitulosEducativos['message'] + '<b>', 2000);
			this.infoEducacionArray = [];
			this.tituloEducacion = new EmpleadoInfoEducacion('', 'SAG', '', '', '', '', '', '', '', 'SAG', '', '', '');
			this.listarEducacionEmpleadoId(this.idEmpleadoData);
			this.loading = false;
		}
		else if (responseNuevosTitulosEducativos["status"] === "success" && responseNuevosTitulosEducativos["code"] === "300") {
			this.loading = false;
			this.mensajeRespuesta = responseNuevosTitulosEducativos['message'];
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseNuevosTitulosEducativos['message'] + '<b>', 2000);
		}
		else if (responseNuevosTitulosEducativos["status"] === "error" && responseNuevosTitulosEducativos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseNuevosTitulosEducativos['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'danger', 'warning', '<b>' + responseNuevosTitulosEducativos['message'] + '<b>', 2000);
			console.log(responseNuevosTitulosEducativos);
		}
	}
	/* Fin Agregar nuevos titulos educativos que se digitaron en la seccion de educacion */
	/* Fin Modificar datos de cada una de las secciones por separado */

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}