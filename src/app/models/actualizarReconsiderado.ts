export class ActualizarReconsiderado {
    constructor(
        public reconsiderado: string,
        public observacion: string
    ) { }
}