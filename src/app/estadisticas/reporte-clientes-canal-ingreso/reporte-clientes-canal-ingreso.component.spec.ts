import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteClientesCanalIngresoComponent } from './reporte-clientes-canal-ingreso.component';

describe('ReporteClientesCanalIngresoComponent', () => {
  let component: ReporteClientesCanalIngresoComponent;
  let fixture: ComponentFixture<ReporteClientesCanalIngresoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteClientesCanalIngresoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteClientesCanalIngresoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
