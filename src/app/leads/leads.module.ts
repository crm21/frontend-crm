import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LeadsRoutes } from './leads.routing';
import { LoadingModule } from '../shared/loading/loading.module';
import { LoadingSpinnerModule } from '../shared/loading-spinner/loading-spinner.module';
import { ListarLeadsComponent } from './listar-leads/listar-leads.component';
import { RegistrarLeadsComponent } from './registrar-leads/registrar-leads.component';

@NgModule({
	declarations: [
		ListarLeadsComponent,
		RegistrarLeadsComponent
	],
	imports: [
		CommonModule,
		RouterModule.forChild(LeadsRoutes),
		FormsModule,
		MaterialModule,
		ReactiveFormsModule,
		LoadingModule,
		LoadingSpinnerModule
	],
	providers: [DatePipe]
})
export class LeadsModule { }