export class Reconsiderado {
    constructor(
        public cotizacion: string,
        public reconsiderado: string,
        public observacion?: string
    ) { }
}