import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Cliente } from "../../models/cliente";
import { DatePipe } from "@angular/common";
import swal from "sweetalert2";
import { environment } from "../../../environments/environment";
import { ClienteService } from '../../services/cliente/cliente.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { UsuarioService } from '../../services/usuario/usuario.service';

declare var $: any;

@Component({
	selector: 'app-crear-clientes',
	templateUrl: './crear-clientes.component.html',
	styleUrls: ['./crear-clientes.component.css']
})
export class CrearClientesComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	infoUsuario: any;
	crearClienteForm: FormGroup;
	tipoDocumentos: any;
	tipoCanalIngreso: any;
	tarjetaOpciones: any;
	tipoDocumentosArray: any[] = [];
	tipoCanalIngresoArray: any[] = [];
	tarjetaOpcionesArray: any[] = [];

	idCliente: any;
	idIngreso: any;
	documentoBusqueda: any;

	localImg = environment.serverImg;
	rutaImg: string = '';
	correoEmpresa: string = '';
	telefonoEmpresa: string = '';

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _clienteService: ClienteService,
		private _parametrizacionService: ParametrizacionService,
		private _sweetAlertService: SweetAlertService,
		private _usuarioService: UsuarioService,
		private datepipe: DatePipe
	) {
		if (localStorage.getItem('identity_crm')) {
			this.rutaImg = _usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			this.infoUsuario = identityData['data'];
			if (localStorage.getItem('documentoCliente')) {
				this.documentoBusqueda = localStorage.getItem('documentoCliente');
				localStorage.removeItem('documentoCliente');
			} else this.documentoBusqueda = '';
		}
	}

	ngOnInit() {
		this.crearClienteForm = new FormGroup({
			tipoDoc: new FormControl(null, Validators.required),
			documento: new FormControl(null, [Validators.required, Validators.pattern("[0-9]{5,13}$")]),
			nombres: new FormControl(null, [Validators.required, Validators.pattern("^[a-zA-ZñÑ ]{1,80}$")]),
			apellidos: new FormControl(null, [Validators.required, Validators.pattern("^[a-zA-ZñÑ ]{1,80}$")]),
			canalIngreso: new FormControl(null, Validators.required),
			fechaNacimiento: new FormControl(null),
			tarjetaOpciones: new FormControl(null, Validators.required),
			contactoPrincipal: new FormControl(null),
			contactoSecundario: new FormControl(null),
			correo: new FormControl(null, [Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
			observacion: new FormControl(null),
		});
		if (localStorage.getItem('identity_crm')) {
			this.loading = true;
			let informacionEmpresa: string[] = this._sweetAlertService.datosEmpresas(localStorage.getItem('business'));
			//console.log(informacionEmpresa);
			this.correoEmpresa = informacionEmpresa[0];
			this.telefonoEmpresa = informacionEmpresa[1];
			this.cargarInformacionSelect();
			this.cargarDocumento();
			this.loading = false;
		}
	}

	async onSubmitCrearCliente() {
		this.loading = true;

		let cliente = new Cliente(
			this.crearClienteForm.value.tipoDoc,
			this.crearClienteForm.value.documento,
			this.crearClienteForm.value.nombres,
			this.crearClienteForm.value.apellidos,
			this.crearClienteForm.value.canalIngreso,
			this.crearClienteForm.value.tarjetaOpciones,
			this.infoUsuario.sub,
			this.crearClienteForm.value.contactoPrincipal,
			this.crearClienteForm.value.contactoSecundario,
			this.crearClienteForm.value.correo,
			this.fechasValidacion(this.crearClienteForm.value.fechaNacimiento),
			this.crearClienteForm.value.observacion,
			this.correoEmpresa,
			this.telefonoEmpresa
		); //console.log(cliente);

		const responseCliente = await this._clienteService.registrarCliente(cliente);
		//console.log(responseCliente);
		if (responseCliente["status"] === "success" && responseCliente["code"] === "200") {
			this.loading = false;
			this.idCliente = responseCliente["data"].persona.id;
			this.idIngreso = responseCliente["data"].ingresosConcesionario.id
			swal('¡Bien!', responseCliente["message"], 'success')
				.then(result => {
					this.crearClienteForm.reset();
					localStorage.setItem('idIngreso', this.idIngreso);
					this.asignarAsesor(this.idCliente);
				}).catch(swal.noop);
		}
		else if (responseCliente["status"] === "success" && responseCliente["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseCliente["message"], 'OK', 'warning');
		}
		else if (responseCliente["status"] === "error" && responseCliente["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseCliente['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseCliente["message"], 'OK', 'error');
		}
	}

	cargarDocumento() {
		this.crearClienteForm.setValue({
			tipoDoc: 0,
			documento: this.documentoBusqueda,
			nombres: '',
			apellidos: '',
			canalIngreso: 0,
			fechaNacimiento: '',
			tarjetaOpciones: 0,
			contactoPrincipal: '',
			contactoSecundario: '',
			correo: '',
			observacion: ''
		});
	}

	async cargarInformacionSelect() {
		this.tipoDocumentos = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo Documento');
		this.tipoCanalIngreso = await this._parametrizacionService.listarCategoriaParametrizacion('Canal de Ingreso');
		this.tarjetaOpciones = await this._parametrizacionService.listarCategoriaParametrizacion('Tarjeta de Opciones');

		this.tipoDocumentos.data.forEach((element) => {
			this.tipoDocumentosArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		this.tipoCanalIngreso.data.forEach((element) => {
			this.tipoCanalIngresoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		this.tarjetaOpciones.data.forEach((element) => {
			this.tarjetaOpcionesArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	fechasValidacion(formatoFecha) {
		if (formatoFecha) {
			//Formatear fecha
			const fechaActualTem = this.datepipe
				.transform(formatoFecha, "yyyy/MM/dd")
				.replace(/\//g, "-");
			//console.log("fecha formato: "+fechaActualTem);
			return fechaActualTem;
		}
		return '';
	}

	asignarAsesor(idCliente) {
		// opc = 1 => vista crear cliente
		let opc = 1;
		this._router.navigate(["atenciones/asignar-asesor/", this.idIngreso, idCliente, opc]);
	}

	regresar() {
		this._router.navigate(["clientes/buscar"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}
