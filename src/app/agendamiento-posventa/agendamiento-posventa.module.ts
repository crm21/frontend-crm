import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AgendamientoPosventaRoutes } from './agendamiento-posventa.routing';
import { LoadingModule } from '../shared/loading/loading.module';
import { LoadingSpinnerModule } from '../shared/loading-spinner/loading-spinner.module';
import { ListarAgendaComponent } from './listar-agenda/listar-agenda.component';
import { RegistrarAgendaClienteComponent } from './registrar-agenda-cliente/registrar-agenda-cliente.component';
import { AgendamientoComponent } from './agendamiento/agendamiento.component';
import { RegistrarAgendaTallerComponent } from './registrar-agenda-taller/registrar-agenda-taller.component';

@NgModule({
	declarations: [
		ListarAgendaComponent,
		RegistrarAgendaClienteComponent,
		AgendamientoComponent,
		RegistrarAgendaTallerComponent
	],
	imports: [
		CommonModule,
		RouterModule.forChild(AgendamientoPosventaRoutes),
		FormsModule,
		MaterialModule,
		ReactiveFormsModule,
		LoadingModule,
		LoadingSpinnerModule
	],
	providers: [DatePipe]
})
export class AgendamientoPosventaModule { }