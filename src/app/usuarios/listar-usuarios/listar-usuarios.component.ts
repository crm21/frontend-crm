import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import swal from "sweetalert2";
import { UsuarioService } from '../../services/usuario/usuario.service';
import { PermisosService } from '../../services/permisos/permisos.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { UsuariosCambiarContrasenia } from "../../models/actualizarContrasena";

declare var $: any;
declare interface TableData {
	headerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-listar-usuarios',
	templateUrl: './listar-usuarios.component.html',
	styleUrls: ['./listar-usuarios.component.css']
})
export class ListarUsuariosComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	consultaVaciaCargarSpinner: boolean = false;
	buscarUsuarioFormGroup: FormGroup;
	cambiarContraseniaForm: FormGroup;
	dataTable: TableData;
	usuarioArray: any[] = [];
	rolesExistentes: any[] = [];
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	opcion: string = '';
	filtroBusqueda: string = '';
	buscarRol: any = {
		rolUsuario: null
	};
	check: boolean = false;
	checkPass: boolean = false;

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _formBuilder: FormBuilder,
		private _usuariosService: UsuarioService,
		private _permisosService: PermisosService,
		private _sweetAlertService: SweetAlertService
	) {
		this.datosTabla();
	}

	ngOnInit() {
		this.buscarUsuarioFormGroup = this._formBuilder.group({
			filtroUsuario: ['', Validators.required]
		});
		this.cambiarContraseniaForm = this._formBuilder.group({
			documento: ['', Validators.required],
			password: ['', Validators.required],
			checkPassword: ['', Validators.required]
		});
		if (localStorage.getItem('identity_crm')) {
			this.opcion = '1';
			this.listarUsuarios(this.opcion, 1);
			this.listarRolesExistentes();
		}
	}

	datosTabla() {
		this.dataTable = {
			headerRow: [
				"#",
				"Documento",
				"Nombres Completos",
				"Usuario",
				"Rol",
				"Última Sesión",
				"Sesiones",
				"Estado",
				"Acciones"
			],
			dataRows: []
		};
	}

	async listarRolesExistentes() {
		this.loading = true;
		const responseRoles = await this._permisosService.listarRoles();
		//console.log(responseRoles);
		this.loading = false;
		if (responseRoles['status'] == "success" && responseRoles['code'] == "200") {
			this.rolesExistentes = responseRoles['data'];
		}
		else if (responseRoles['status'] == "success" && responseRoles['code'] == "300") {
			swal('Advertencia!', responseRoles['message'], 'warning');
		}
		else if (responseRoles["status"] === "error" && responseRoles["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseRoles['message']);
		}
		else {
			swal('¡Error!', responseRoles['message'], 'error');
		}
	}

	onSubmitBuscarUsuariosFiltros(opc: string) {
		if (opc == '1') {
			this.filtroBusqueda = this.buscarUsuarioFormGroup.value.filtroUsuario;
			this.opcion = '3';
		} else {
			this.filtroBusqueda = this.buscarRol.rolUsuario;
			this.opcion = '2';
		}
		//console.log(`Buscar: ${this.opcion},  ${this.filtroBusqueda}`);
		this.listarUsuarios(this.opcion, 1, this.filtroBusqueda);
	}

	async listarUsuarios(opcionBusqueda: any, page: any, filtroBusqueda: any = null) {
		this.loading = true;
		const responseUsuarios = await this._usuariosService.listarUsuarios(opcionBusqueda, page, filtroBusqueda);
		//console.log(responseUsuarios);
		if (responseUsuarios["status"] === "success" && responseUsuarios["code"] === "200") {
			this.loading = false;
			this.llenarDatosTabla(page, responseUsuarios['data']);
		}
		else if (responseUsuarios["status"] === "success" && responseUsuarios["code"] === "300") {
			this.loading = false;
			this.usuarioArray = [];
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseUsuarios['message'] + '<b>', 2000);
		}
		else if (responseUsuarios["status"] === "error" && responseUsuarios["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseUsuarios['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			this._sweetAlertService.showNotification('top', 'right', 'error', 'warning', '<b>' + responseUsuarios['message'] + '<b>', 2000);
		}
	}

	llenarDatosTabla(page: any, responseUsuarios: any) {
		this.usuarioArray = responseUsuarios['usuarios'];
		this.dataTable.dataRows = [];
		this.total_item_count = responseUsuarios['total_item_count'];
		this.page_actual = page;

		this.total_pages = responseUsuarios['total_pages'];

		if (page >= 2) this.page_prev = page - 1;
		else this.page_prev = 1;

		if (page < responseUsuarios['total_pages']) this.page_next = page + 1;
		else {
			if (responseUsuarios['total_pages'] == 0) this.page_next = 1;
			else this.page_next = responseUsuarios['total_pages'];
		}

		let JSONParam;

		this.usuarioArray.forEach(element => {
			JSONParam = {
				id: element.id,
				tipoDoc: element.tipoDocumento.valor,
				documento: element.documento,
				nombres: element.nombres + ' ' + element.apellidos,
				usuario: element.usuario,
				rol_valor: element.rol.valor,
				rol: element.rol.nombre,
				ultimaSesion: element.ultimaSesion,
				sesiones: element.sesiones == 0 ? 'Aún no ingresa' : element.sesiones,
				estado: element.estado.nombre
			};
			this.dataTable.dataRows.push(JSONParam);
		});
		//console.log(this.usuarioArray);
		if (this.total_item_count || this.total_item_count == 0) {
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
		}
	}

	actualizarContrasenia(rowUsuario: any) {
		let documentoUsuario = rowUsuario.documento;

		this.cambiarContraseniaForm.setValue({
			documento: documentoUsuario,
			password: '',
			checkPassword: true
		});

		$("#modalCambiarContrasenia").modal("show");
	}

	checkConfirmarPassword(event: any) {
		this.check = event.currentTarget.checked;

		if (this.check) {
			(document.getElementById('documento') as HTMLInputElement).disabled = false;
			this.checkPass = false;
		} else {
			(document.getElementById('documento') as HTMLInputElement).disabled = true;
			this.checkPass = true;
		}
	}

	async onSubmitCambiarContrasenia() {
		//console.log(this.cambiarContraseniaForm.value);
		let usuarioCambiarContrasenia = new UsuariosCambiarContrasenia(
			this.cambiarContraseniaForm.value.documento,
			this.cambiarContraseniaForm.value.password,
			this.cambiarContraseniaForm.value.checkPassword
		); //console.log(usuarioCambiarContrasenia);

		this.loading = true;
		const responseCambiarContrasenia = await this._usuariosService.cambiarContraseniaUsuarios(usuarioCambiarContrasenia);
		//console.log(responseCambiarContrasenia);
		this.loading = false;
		if (responseCambiarContrasenia['status'] == "success" && responseCambiarContrasenia['code'] == "200") {
			swal('¡Bien!', responseCambiarContrasenia["message"], 'success')
				.then(result => {
					$("#modalCambiarContrasenia").modal("hide");
					this.cambiarContraseniaForm.reset();
				}).catch(swal.noop);
		}
		else if (responseCambiarContrasenia['status'] == "success" && responseCambiarContrasenia['code'] == "300") {
			swal('Advertencia!', responseCambiarContrasenia['message'], 'warning');
		}
		else if (responseCambiarContrasenia["status"] === "error" && responseCambiarContrasenia["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseCambiarContrasenia['message']);
		}
		else {
			swal('¡Error!', responseCambiarContrasenia['message'], 'error');
		}
	}

	limpiarFormulario(opc: string) {
		this.buscarUsuarioFormGroup.reset();
		this.buscarRol.rolUsuario = null;
		(document.getElementById('rolUsuario') as HTMLInputElement).value = '';
		this.listarUsuarios(1, 1);
	}

	buscarIdCliente(rowId: any) {
		this._router.navigate(["usuarios/actualizar-informacion/", rowId, 1]);
	}

	buscarIdInformacionPersonal(rowId: any) {
		this._router.navigate(["usuarios/actualizar-informacion/", rowId, 2]);
	}

	buscarIdInformacionGeneral(rowId: any) {
		this._router.navigate(["usuarios/actualizar-informacion/", rowId, 3]);
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) {
			result = e1 === e2;
		}
		return result;
	}

	crearUsuario() {
		this._router.navigate(["usuarios/registrar"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}