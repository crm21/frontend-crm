import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import swal from "sweetalert2";
import { modificarEstudioCredito } from '../../interfaces/interfaces';
import { ActualizarEstudioCredito } from '../../models/estudioCredito';
import { DatePipe } from "@angular/common";
import { EstudioCreditoService } from '../../services/estudio-credito/estudio-credito.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare var $: any;

@Component({
	selector: 'app-listar-estudio-credito',
	templateUrl: './listar-estudio-credito.component.html',
	styleUrls: ['./listar-estudio-credito.component.css']
})
export class ListarEstudioCreditoComponent implements OnInit {
	loading: boolean = false;
	validarToken: boolean = true;
	idCotizacion: any;
	idEstudioCreditos: any;
	ingresoFinalizado: any = 0;
	botonMostrar: any;
	actualizarEstudioCreditosForm: FormGroup;
	listadoEstudioCreditoArray: any[] = [];

	bancosParametros: any;
	contestacionParametros: any;
	planParametros: any;
	bancosArray: any[] = [];
	contestacionArray: any[] = [];
	planArray: any[] = [];

	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	permisosRolValor: string = '';
	permisosRolNombre: string = '';
	radicadoFecha = {
		temp: null,
		guardar: null
	};
	contestacionFecha = {
		temp: null,
		guardar: null
	};
	fechaRadicadoContestacion = '';
	datosCliente = {
		nombres: null,
		documento: null
	};
	observacionEstudioCredito: string = '';
	permisosBotonEdicion = ['A', 'GC', 'DN'];
	permisosEdicion: boolean = false;

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _estudioCreditoService: EstudioCreditoService,
		private _parametrizacionService: ParametrizacionService,
		private _sweetAlertService: SweetAlertService,
		private datepipe: DatePipe
	) {
		this.idCotizacion = this._route.snapshot.paramMap.get('idCotizacion');
		this.ingresoFinalizado = this._route.snapshot.paramMap.get('ingresoFinalizado');

		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			let permisoVerFicha = identityData['data'];;
			this.permisosRolValor = permisoVerFicha.rol.valor;
			this.permisosRolNombre = permisoVerFicha.rol.nombre;
			this.botonMostrar = this.ingresoFinalizado == 1 ? true : false;
			if (this.permisosBotonEdicion.includes(this.permisosRolValor)) {
				this.permisosEdicion = true;
			}
		}
	}

	ngOnInit() {
		if (localStorage.getItem('identity_crm')) {
			this.actualizarEstudioCreditosForm = new FormGroup({
				fechaRadicado: new FormControl(null),
				fechaContestacion: new FormControl(null),
				contestacion: new FormControl(null),
				banco: new FormControl(null),
				plan: new FormControl(null),
				observacion: new FormControl(null),
			});
			this.listarEstudioCreditos(1);
		}
	}

	async listarEstudioCreditos(page: any) {
		this.loading = true;
		let responseListadoEstudioCreditos = await this._estudioCreditoService.listarEstudioCreditoPorCotizacion(this.idCotizacion, page);
		//console.log(responseListadoEstudioCreditos);
		if (responseListadoEstudioCreditos["status"] === "success" && responseListadoEstudioCreditos["code"] === "200") {
			this.llenarDatosCliente(responseListadoEstudioCreditos['data']);
			this.llenarDatosEstudioCreditos(responseListadoEstudioCreditos['data'], page);
			this.loading = false;
			$(".main-panel").scrollTop(0);
		}
		else if (responseListadoEstudioCreditos["status"] === "success" && responseListadoEstudioCreditos["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseListadoEstudioCreditos["message"], 'warning')
				.then(result => {
					this._router.navigate(["clientes/buscar"]);
				}).catch(swal.noop);
		}
		else if (responseListadoEstudioCreditos["status"] === "error" && responseListadoEstudioCreditos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseListadoEstudioCreditos['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseListadoEstudioCreditos["message"], 'error')
				.then(result => {
					this._router.navigate(["clientes/buscar"]);
				}).catch(swal.noop);
		}
	}

	llenarDatosCliente(data: any) {
		if (data.total_item_count) {
			let nombre = data.estudioCreditos[0].cotizacion.ingresoConcesionario.persona.nombres;
			let apellido = data.estudioCreditos[0].cotizacion.ingresoConcesionario.persona.apellidos;
			this.datosCliente.nombres = nombre + ' ' + apellido;
			this.datosCliente.documento = data.estudioCreditos[0].cotizacion.ingresoConcesionario.persona.documento;
		} else {
			this.datosCliente.nombres = 'Datos no registrados';
			this.datosCliente.documento = 'Datos no registrados';
		}
	}

	llenarDatosEstudioCreditos(data: any, page: any) {
		if (data.total_item_count) {
			//console.log(data.estudioCreditos[0]);
			this.listadoEstudioCreditoArray = [];
			//this.listadoEstudioCreditoArray = data.estudioCreditos;
			let datosEstudioCredito: any = data.estudioCreditos;

			datosEstudioCredito.forEach((element) => {
				this.listadoEstudioCreditoArray.push({
					id: element.id,
					fechaRadicado: element.fechaRadicado,
					fechaContestacion: element.fechaContestacion,
					fechaDesembolso: element.fechaDesembolso,
					cotizacion: element.cotizacion.numCotizacion,
					contestacion: element.contestacion,
					contestacionTem: element.contestacion.nombre,
					colorContestacion: this.darColorEstadoEvento(element.contestacion.valor),
					banco: element.banco,
					bancoTem: element.banco.nombre,
					plan: element.plan,
					planTem: element.plan.nombre,
					observacion: element.observacion
				});
			});

			this.total_item_count = data.total_item_count;
			this.page_actual = page;

			this.total_pages = data.total_pages;

			if (page >= 2) this.page_prev = page - 1;
			else this.page_prev = 1;

			if (page < data.total_pages) this.page_next = page + 1;
			else {
				if (data.total_pages == 0) this.page_next = 1;
				else this.page_next = data.total_pages;
			}
		} else {
			this.listadoEstudioCreditoArray = [];
		}

	}

	async cargarInformacionSelect() {
		this.bancosArray = [];
		this.contestacionArray = [];
		this.planArray = [];
		this.bancosParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Bancos');
		this.contestacionParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Contestación');
		this.planParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Plan de crédito');

		this.bancosParametros.data.forEach((element) => {
			this.bancosArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		this.contestacionParametros.data.forEach((element) => {
			this.contestacionArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		this.planParametros.data.forEach((element) => {
			this.planArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
	}

	crearEstudioCredito() {
		this._router.navigate(["clientes/registrar-estudio-credito/", this.idCotizacion, this.ingresoFinalizado]);
	}

	modificarEstudioCredito(estudioCredito) {
		this.loading = true;
		this.cargarInformacionSelect();
		this.loading = false;
		let rowEstudioCredito: modificarEstudioCredito = estudioCredito;
		this.idEstudioCreditos = estudioCredito.id;

		this.radicadoFecha.temp = rowEstudioCredito.fechaRadicado;
		this.radicadoFecha.guardar = this.fechasValidacion(1, rowEstudioCredito.fechaRadicado);

		this.contestacionFecha.temp = rowEstudioCredito.fechaContestacion;
		this.contestacionFecha.guardar = this.fechasValidacion(1, rowEstudioCredito.fechaContestacion);
		this.observacionEstudioCredito = rowEstudioCredito.observacion;

		this.actualizarEstudioCreditosForm.setValue({
			fechaRadicado: this.radicadoFecha.guardar,
			fechaContestacion: this.contestacionFecha.guardar,
			contestacion: rowEstudioCredito.contestacion.valor,
			banco: rowEstudioCredito.banco.valor,
			plan: rowEstudioCredito.plan.valor,
			observacion: ''
		});
		if (this.permisosRolValor == 'S') {
			this.validarCampos();
		}
		$("#modalFiltro").modal("show");
	}

	async onSubmitActualizarEstudioCreditos() {
		this.loading = true;
		let fechaRadicadoGuardar = this.fechasValidacion(2, this.actualizarEstudioCreditosForm.value.fechaRadicado, this.radicadoFecha);
		let fechaContestacionGuardar = this.fechasValidacion(2, this.actualizarEstudioCreditosForm.value.fechaContestacion, this.contestacionFecha);
		let observacionConcatenar: string = '';
		//console.log(this.actualizarEstudioCreditosForm.value.observacion);

		if (this.actualizarEstudioCreditosForm.value.observacion == '') {
			observacionConcatenar = this.observacionEstudioCredito;
		} else {
			//observacionConcatenar = this.observacionEstudioCredito + '<br/><strong>Observación ' + this.permisosRolNombre + ': &nbsp;</strong>' + this.actualizarEstudioCreditosForm.value.observacion;
			observacionConcatenar = `${this.observacionEstudioCredito} <br><strong>Observación ${this.permisosRolNombre}: </strong> ${this.actualizarEstudioCreditosForm.value.observacion}`;
			//observacionConcatenar = this.actualizarEstudioCreditosForm.value.observacion;
		}
		let estudioCreditos = new ActualizarEstudioCredito(
			fechaRadicadoGuardar,
			fechaContestacionGuardar,
			this.idCotizacion,
			this.actualizarEstudioCreditosForm.value.contestacion,
			this.actualizarEstudioCreditosForm.value.banco,
			this.actualizarEstudioCreditosForm.value.plan,
			observacionConcatenar,
			''
		); //console.log(estudioCreditos);

		const responseModificarEstudioCreditos = await this._estudioCreditoService.modificarEstudioCredito(this.idEstudioCreditos, estudioCreditos);
		console.log(responseModificarEstudioCreditos);
		if (responseModificarEstudioCreditos["status"] === "success" && responseModificarEstudioCreditos["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseModificarEstudioCreditos["message"], 'success')
				.then(result => {
					this.actualizarEstudioCreditosForm.reset();
					$("#modalFiltro").modal("hide");
					this.listarEstudioCreditos(1);
				}).catch(swal.noop);
		}
		else if (responseModificarEstudioCreditos["status"] === "success" && responseModificarEstudioCreditos["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseModificarEstudioCreditos["message"], 'OK', 'warning');
		}
		else if (responseModificarEstudioCreditos["status"] === "error" && responseModificarEstudioCreditos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseModificarEstudioCreditos['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseModificarEstudioCreditos["message"], 'OK', 'error');
		}
	}

	eliminarEstudioCredito() {
		//console.log("Eliminar estudio credito");
	}

	darColorEstadoEvento(estado: string): string {
		switch (estado) {
			case 'rechazado':
				return 'danger';
				break;
			case 'activo':
				return 'warning';
				break;
			case 'aprobado':
				return 'success';
				break;
			case 'desembolsado':
				return 'info';
				break;
			default:
				return 'default';
				break;
		}
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	fechasValidacion(opc, formatoFecha, fechaTemp: any = null) {
		if (opc == 1) {
			if (formatoFecha != '0000-00-00') {
				//Formatear fecha
				const fechaActualTem = this.datepipe
					.transform(formatoFecha, "yyyy/MM/dd")
					.replace(/\//g, "-");
				return fechaActualTem;
			}
			return formatoFecha;
		}
		else if (opc == 2) {
			if (formatoFecha != '0000-00-00') {
				let fechaLlega = this.fechasValidacion(1, formatoFecha);
				if (fechaTemp.guardar === fechaLlega) {
					return fechaTemp.temp;
				} else {
					const fechaActual = new Date();
					const horasMinSeg = this.datepipe.transform(fechaActual, "H:mm:ss");
					let fechaFormateadaTemp = this.fechasValidacion(1, formatoFecha);
					return fechaFormateadaTemp + ' ' + horasMinSeg;
				}
			}
			return formatoFecha;
		}
		else {
			return '';
		}
	}

	regresar() {
		let idIngreso = localStorage.getItem('idIngreso');
		this._router.navigate(["clientes/ficha-clientes/", idIngreso]);
	}

	validarCampos() {
		let arrayFechasBloqueadas = ['fechaRadicado', 'fechaContestacion'];
		arrayFechasBloqueadas.forEach((element) => {
			if ((document.getElementById(element) as HTMLInputElement).value)
				(document.getElementById(element) as HTMLInputElement).disabled = true;
			else (document.getElementById(element) as HTMLInputElement).disabled = false;
		});
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}
