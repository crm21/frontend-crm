import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarPassComponent } from './actualizar-pass.component';

describe('ActualizarPassComponent', () => {
  let component: ActualizarPassComponent;
  let fixture: ComponentFixture<ActualizarPassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualizarPassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizarPassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
