import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AsesoresRoutes } from './asesores.routing';
import { CrearAsesoresComponent } from './crear-asesores/crear-asesores.component';
import { LoadingModule } from '../shared/loading/loading.module';
import { LoadingSpinnerModule } from '../shared/loading-spinner/loading-spinner.module';
import { ListarAsesoresComponent } from './listar-asesores/listar-asesores.component';

@NgModule({
	declarations: [CrearAsesoresComponent, ListarAsesoresComponent],
	imports: [
		CommonModule,
		RouterModule.forChild(AsesoresRoutes),
		FormsModule,
		MaterialModule,
		ReactiveFormsModule,
		LoadingModule,
		LoadingSpinnerModule
	],
	providers: [DatePipe]
})
export class AsesoresModule { }
