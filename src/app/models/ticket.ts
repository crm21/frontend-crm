export class RegistrarTicket {
    constructor(
        public categoria: string,
        public descripcionSolicitud: string,
        public anexoNombre: string
    ) { }
};

export class TicketAprobado {
    constructor(
        public estado: string,
        public descripcionAprobacion: string
    ) { }
};

export class ReasignarTicketUsuario {
    constructor(
        public idTicket: string,
        public idUsuario: string,
        public descripcion: string
    ) { }
};