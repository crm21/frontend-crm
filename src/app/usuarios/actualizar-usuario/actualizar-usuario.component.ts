import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { DatosCliente, DatosInformacionPersonal, InfoGeneralEmpleado } from '../../interfaces/interfaces';
import { ActualizarCliente, InformacionPersonal } from '../../models/cliente';
import { DatePipe } from "@angular/common";
import swal from "sweetalert2";
import { UsuarioService } from '../../services/usuario/usuario.service';
import { PermisosService } from '../../services/permisos/permisos.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { EmpleadosService } from "src/app/services/empleados/empleados.service";
import { EmpleadoInfoGeneral } from '../../models/empleado';

declare var $: any;
export interface OpcionesArray {
	nombre: string;
	valor: string;
}

@Component({
	selector: 'app-actualizar-usuario',
	templateUrl: './actualizar-usuario.component.html',
	styleUrls: ['./actualizar-usuario.component.css']
})
export class ActualizarUsuarioComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	mensaje: any;
	actualizarClienteForm: FormGroup;
	actualizarInformacionPersonalForm: FormGroup;
	actualizarInformacionGeneralForm: FormGroup;
	rolesExistentes: any;
	idCliente: any;
	opcion: any;

	tipoDocumentosParametros: any;
	estadosParametros: any;
	coloresParametros: any;
	hobbyParametros: any;
	profesionParametros: any;
	tipoDocumentosArray: any[] = [];
	estadosArray: any[] = [];
	coloresArray: any[] = [];
	hobbyArray: any[] = [];
	profesionArray: any[] = [];
	categoriaLicenciaArray: any[] = [];
	claseLibretaMilitarArray: any[] = [];
	colorPreferidoArray: any[] = [];
	estadoCivilArray: any[] = [];
	mostrarActualizarVentana: number = 0;
	idGeneralEmpleado: string = '';
	generoArray: OpcionesArray[] = [
		{ nombre: 'Mujer', valor: 'M' },
		{ nombre: 'Hombre', valor: 'H' },
		{ nombre: 'Otro', valor: 'O' },
	];
	grupoSanguineoArray: OpcionesArray[] = [
		{ nombre: 'A+', valor: 'Ap' },
		{ nombre: 'A-', valor: 'A-' },
		{ nombre: 'B+', valor: 'Bp' },
		{ nombre: 'B-', valor: 'B-' },
		{ nombre: 'O+', valor: 'Op' },
		{ nombre: 'O-', valor: 'O-' },
		{ nombre: 'AB+', valor: 'ABp' },
		{ nombre: 'AB-', valor: 'AB-' }
	];
	testDriveArray = [
		{ nombre: 'Sí', valor: true },
		{ nombre: 'No', valor: false },
	];
	ocultarEdicionCampos: boolean = false;
	tenerLibretaMilitar: boolean = false;
	tenerLicencia: boolean = false;

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _formBuilder: FormBuilder,
		private _usuariosService: UsuarioService,
		private _empleadosService: EmpleadosService,
		private _permisosService: PermisosService,
		private _parametrizacionService: ParametrizacionService,
		private _sweetAlertService: SweetAlertService,
		private datepipe: DatePipe
	) {
		this.idCliente = this._route.snapshot.paramMap.get('idCliente');
		this.opcion = this._route.snapshot.paramMap.get('opc');
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);
			let permisoVerFicha = identityData['data'];

			if (permisoVerFicha.rol.valor == 'A') {
				this.ocultarEdicionCampos = true;
			}
		}
	}

	ngOnInit() {
		if (localStorage.getItem('identity_crm')) {
			this.actualizarClienteForm = this._formBuilder.group({
				tipoDoc: [''],
				documento: ['', [Validators.pattern("[0-9]{5,13}$")]],
				nombres: [''],
				apellidos: [''],
				usuario: [''],
				rol: [''],
				estado: ['']
			});

			this.actualizarInformacionPersonalForm = this._formBuilder.group({
				fechaNacimiento: ['', Validators.required],
				contactoPrincipal: ['', Validators.required],
				contactoSecundario: ['', Validators.required],
				correo: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
				lugarResidencia: ['', Validators.required],
				direccion: ['', Validators.required],
				testDrive: ['', Validators.required],
				color: ['', Validators.required],
				hobby1: ['', Validators.required],
				hobby2: ['', Validators.required],
				profesion: ['', Validators.required],
				genero: ['', Validators.required]
			});

			this.actualizarInformacionGeneralForm = this._formBuilder.group({
				correo: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
				fechaExpedicion: ['', Validators.required],
				fechaNacimiento: [''],
				direccion: [''],
				lugarResidencia: [''],
				telefonoFijo: [''],
				celular: ['', [Validators.required, Validators.pattern("[0-9]{5,13}$")]],
				estadoCivil: ['SAG'],
				aniosExperienciaLaboral: [''],
				tieneLibretaMilitar: ['', [Validators.required]],
				libretaMilitar: [''],
				claseLibretaMilitar: ['SAG'],
				distritoLibretaMilitar: [''],
				tarjetaProfesional: [''],
				grupoSanguineo: ['SAG'],
				tieneLicenciaConduccion: ['', [Validators.required]],
				licenciaConduccion: [''],
				categoriaLicencia: ['SAG'],
				color: ['SAG'],
				hobby1: ['SAG'],
				hobby2: ['SAG'],
				profesion: ['SAG'],
				genero: ['Op'],
			});

			this.buscarId(+this.opcion);
		}
	}

	async cargarInformacionSelectCliente() {
		this.loading = true;
		this.tipoDocumentosParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo Documento');
		this.estadosParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Estado');

		this.tipoDocumentosParametros.data.forEach((element) => {
			this.tipoDocumentosArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		this.estadosParametros.data.forEach((element) => {
			this.estadosArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
		this.listarRolesExistentes();
	}

	async cargarInformacionSelectInfPersonal() {
		this.loading = true;
		this.coloresParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Colores');
		this.hobbyParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Hobby');
		this.profesionParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Profesión');

		this.coloresParametros.data.forEach((element) => {
			this.coloresArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		this.hobbyParametros.data.forEach((element) => {
			this.hobbyArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		this.profesionParametros.data.forEach((element) => {
			this.profesionArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
	}

	async cargarInformacionSelectInfGeneral() {
		this.loading = true;
		let categoriaLicencia = await this._parametrizacionService.listarCategoriaParametrizacion('Licencia de Conducción');
		let claseLibretaMilitar = await this._parametrizacionService.listarCategoriaParametrizacion('Libreta militar');
		let colorPreferido = await this._parametrizacionService.listarCategoriaParametrizacion('Colores');
		let estadoCivil = await this._parametrizacionService.listarCategoriaParametrizacion('Estado Civil');
		let hobby = await this._parametrizacionService.listarCategoriaParametrizacion('Hobby');
		let profesion = await this._parametrizacionService.listarCategoriaParametrizacion('Profesión');

		categoriaLicencia['data'].forEach((element) => {
			this.categoriaLicenciaArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		claseLibretaMilitar['data'].forEach((element) => {
			this.claseLibretaMilitarArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		colorPreferido['data'].forEach((element) => {
			this.colorPreferidoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		estadoCivil['data'].forEach((element) => {
			this.estadoCivilArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		hobby['data'].forEach((element) => {
			this.hobbyArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		profesion['data'].forEach((element) => {
			this.profesionArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		this.loading = false;
	}

	async listarRolesExistentes() {
		this.loading = true;
		const responseRoles = await this._permisosService.listarRoles();
		//console.log(responseRoles);
		this.loading = false;
		if (responseRoles['status'] == "success" && responseRoles['code'] == "200") {
			this.rolesExistentes = responseRoles['data'];
		}
		else if (responseRoles['status'] == "success" && responseRoles['code'] == "300") {
			swal('Advertencia!', responseRoles['message'], 'warning');
		}
		else if (responseRoles["status"] === "error" && responseRoles["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseRoles['message']);
		}
		else {
			swal('¡Error!', responseRoles['message'], 'error');
		}
	}

	/**
	 * 
	 * @param opc 
	 * Permite elegir la opcion de actualizacion de acuerdo al numero que venga en la url
	 */
	buscarId(opc: number) {

		switch (opc) {
			case 1:
				this.cargarInformacionSelectCliente();
				this.buscarIdCliente();
				this.mensaje = 'ACTUALIZACIÓN DEL CLIENTE';
				this.mostrarActualizarVentana = 1;
				break;
			case 2:
				this.cargarInformacionSelectInfPersonal();
				this.buscarIdInformacionPersonal();
				this.mensaje = 'ACTUALIZACIÓN DE LA INFORMACIÓN PERSONAL DEL CLIENTE';
				this.mostrarActualizarVentana = 2;
				break;
			case 3:
				this.cargarInformacionSelectInfGeneral();
				this.buscarInformacionGeneralId();
				this.mensaje = 'ACTUALIZACIÓN DE LA INFORMACIÓN GENERAL DEL USUARIO';
				this.mostrarActualizarVentana = 3;
				this.loading = false;
				break;
			default:
				break;
		}
	}

	/* Actualizacion de clientes e informacion personal */
	async buscarIdCliente() {
		this.loading = true;
		const responseIdCliente = await this._usuariosService.buscarIdCliente(this.idCliente);
		//console.log(responseIdCliente);
		if (responseIdCliente["status"] === "success" && responseIdCliente["code"] === "200") {


			let datosCliente: DatosCliente = responseIdCliente['data'];

			this.actualizarClienteForm.setValue({
				tipoDoc: datosCliente.tipoDocumento.valor,
				documento: datosCliente.documento,
				nombres: datosCliente.nombres,
				apellidos: datosCliente.apellidos,
				usuario: datosCliente.usuario,
				rol: datosCliente.rol.valor,
				estado: datosCliente.estado.valor
			});
			this.loading = false;
		}
		else if (responseIdCliente["status"] === "success" && responseIdCliente["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseIdCliente["message"], 'OK', 'warning');
		}
		else if (responseIdCliente["status"] === "error" && responseIdCliente["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseIdCliente['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseIdCliente["message"], 'OK', 'error');
		}
	}

	async actualizarCliente() {
		//this.mostrarActualizarVentana = 0;
		this.loading = true;
		let actualizarCliente = new ActualizarCliente(
			this.actualizarClienteForm.value.tipoDoc,
			this.actualizarClienteForm.value.documento,
			this.actualizarClienteForm.value.nombres,
			this.actualizarClienteForm.value.apellidos,
			this.actualizarClienteForm.value.usuario,
			this.actualizarClienteForm.value.rol,
			this.actualizarClienteForm.value.estado
		); //console.log(actualizarCliente);

		let responseActualizarCliente = await this._usuariosService.actualizarCliente(this.idCliente, actualizarCliente);
		//console.log(responseActualizarCliente);
		if (responseActualizarCliente["status"] === "success" && responseActualizarCliente["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseActualizarCliente["message"], 'success')
				.then(result => {
					this.actualizarClienteForm.reset();
					this.regresar();
				}).catch(swal.noop);
		}
		else if (responseActualizarCliente["status"] === "success" && responseActualizarCliente["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseActualizarCliente["message"], 'warning');
		}
		else if (responseActualizarCliente["status"] === "error" && responseActualizarCliente["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseActualizarCliente['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseActualizarCliente["message"], 'error');
		}
	}

	/* Actualizar iformacion personal */
	async buscarIdInformacionPersonal() {
		this.loading = true;
		const responseIdInformacionPersonal = await this._usuariosService.buscarIdIformacionPersonal(this.idCliente);
		//console.log(responseIdInformacionPersonal);
		if (responseIdInformacionPersonal["status"] === "success" && responseIdInformacionPersonal["code"] === "200") {
			//this.mostrarActualizarVentana = 2;
			let datosInformacionPersonal: DatosInformacionPersonal = responseIdInformacionPersonal['data'];
			//console.log(datosInformacionPersonal);
			this.idCliente = datosInformacionPersonal.id;

			this.actualizarInformacionPersonalForm.setValue({
				fechaNacimiento: datosInformacionPersonal.fechaNacimiento,
				contactoPrincipal: datosInformacionPersonal.contactoPrincipal,
				contactoSecundario: datosInformacionPersonal.contactoSecundario,
				correo: datosInformacionPersonal.correo,
				lugarResidencia: datosInformacionPersonal.lugarResidencia,
				direccion: datosInformacionPersonal.direccion,
				testDrive: datosInformacionPersonal.testDrive,
				color: datosInformacionPersonal.color.valor,
				hobby1: datosInformacionPersonal.hobby1.valor,
				hobby2: datosInformacionPersonal.hobby2.valor,
				profesion: datosInformacionPersonal.profesion.valor,
				genero: datosInformacionPersonal.genero
			});
			this.loading = false;
		}
		else if (responseIdInformacionPersonal["status"] === "success" && responseIdInformacionPersonal["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseIdInformacionPersonal["message"], 'OK', 'warning');
		}
		else if (responseIdInformacionPersonal["status"] === "error" && responseIdInformacionPersonal["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseIdInformacionPersonal['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseIdInformacionPersonal["message"], 'OK', 'error');
		}
	}

	async actualizarInformacionPersonal() {
		this.loading = true;
		let actualizarInfoPersonal = new InformacionPersonal(
			this.fechasValidacion(this.actualizarInformacionPersonalForm.value.fechaNacimiento),
			this.actualizarInformacionPersonalForm.value.contactoPrincipal,
			this.actualizarInformacionPersonalForm.value.contactoSecundario,
			this.actualizarInformacionPersonalForm.value.correo,
			this.actualizarInformacionPersonalForm.value.lugarResidencia,
			this.actualizarInformacionPersonalForm.value.direccion,
			this.actualizarInformacionPersonalForm.value.testDrive,
			this.actualizarInformacionPersonalForm.value.color,
			this.actualizarInformacionPersonalForm.value.hobby1,
			this.actualizarInformacionPersonalForm.value.hobby2,
			this.actualizarInformacionPersonalForm.value.profesion,
			this.actualizarInformacionPersonalForm.value.genero
		); //console.log(actualizarInfoPersonal);

		let responseActualizarInfoPersonal = await this._usuariosService.actualizarInformacionPersonal(this.idCliente, actualizarInfoPersonal);
		//console.log(responseActualizarInfoPersonal);
		if (responseActualizarInfoPersonal["status"] === "success" && responseActualizarInfoPersonal["code"] === "200") {
			//this.mostrarActualizarVentana = 0;
			this.loading = false;
			swal('¡Bien!', responseActualizarInfoPersonal["message"], 'success')
				.then(result => {
					this.actualizarClienteForm.reset();
					this.regresar();
				}).catch(swal.noop);
		}
		else if (responseActualizarInfoPersonal["status"] === "success" && responseActualizarInfoPersonal["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseActualizarInfoPersonal["message"], 'warning');
		}
		else if (responseActualizarInfoPersonal["status"] === "error" && responseActualizarInfoPersonal["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseActualizarInfoPersonal['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseActualizarInfoPersonal["message"], 'error');
		}
	}

	/**
	 * 
	 * Listar la informacion general del usuario
	 */
	async buscarInformacionGeneralId() {
		this.loading = true;

		const responseInforGeneral = await this._empleadosService.listarInfGeneralEmpleadoPorId(3, this.idCliente);
		//console.log(responseInforGeneral);
		if (responseInforGeneral["status"] === "success" && responseInforGeneral["code"] === "200") {
			this.cargarInformacionGeneralId(responseInforGeneral['data']);
			this.loading = false;
		}
		else if (responseInforGeneral["status"] === "success" && responseInforGeneral["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseInforGeneral['message'] + '<b>', 2000);
		}
		else if (responseInforGeneral["status"] === "error" && responseInforGeneral["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseInforGeneral['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseInforGeneral["message"], 'OK', 'error');
		}
	}

	cargarInformacionGeneralId(datosGenerales: any) {
		//console.log(datosGenerales);
		let datosGeneralesEmpleado: InfoGeneralEmpleado = datosGenerales;
		this.idGeneralEmpleado = datosGenerales.id;
		this.tenerLibretaMilitar = datosGeneralesEmpleado.libretaMilitar == 'No' || datosGeneralesEmpleado.libretaMilitar == null ? false : true;
		this.tenerLicencia = datosGeneralesEmpleado.licenciaConduccion == 'No' || datosGeneralesEmpleado.licenciaConduccion == null ? false : true;

		this.actualizarInformacionGeneralForm.setValue({
			correo: datosGeneralesEmpleado.correo,
			fechaExpedicion: datosGeneralesEmpleado.fechaExpedicion,
			fechaNacimiento: datosGeneralesEmpleado.fechaNacimiento,
			direccion: datosGeneralesEmpleado.direccion,
			lugarResidencia: datosGeneralesEmpleado.lugarResidencia,
			telefonoFijo: datosGeneralesEmpleado.telefonoFijo,
			celular: datosGeneralesEmpleado.celular,
			estadoCivil: datosGeneralesEmpleado.estadoCivil.valor,
			aniosExperienciaLaboral: datosGeneralesEmpleado.aniosExperienciaLaboral,
			tieneLibretaMilitar: datosGeneralesEmpleado.libretaMilitar == 'No' ? datosGeneralesEmpleado.libretaMilitar : 'Si',
			libretaMilitar: datosGeneralesEmpleado.libretaMilitar,
			claseLibretaMilitar: datosGeneralesEmpleado.claseLibretaMilitar.valor,
			distritoLibretaMilitar: datosGeneralesEmpleado.distritoLibretaMilitar,
			tarjetaProfesional: datosGeneralesEmpleado.tarjetaProfesional,
			grupoSanguineo: datosGeneralesEmpleado.grupoSanguineo,
			tieneLicenciaConduccion: datosGeneralesEmpleado.licenciaConduccion == 'No' ? datosGeneralesEmpleado.licenciaConduccion : 'Si',
			licenciaConduccion: datosGeneralesEmpleado.licenciaConduccion,
			categoriaLicencia: datosGeneralesEmpleado.categoriaLicencia.valor,
			color: datosGeneralesEmpleado.colorPreferido.valor,
			hobby1: datosGeneralesEmpleado.hobby1.valor,
			hobby2: datosGeneralesEmpleado.hobby2.valor,
			profesion: datosGeneralesEmpleado.profesion.valor,
			genero: datosGeneralesEmpleado.genero
		});
	}


	async actualizarInformacionGeneral() {
		this.loading = true;
		let actualizarEmpleadoInfGeneral = new EmpleadoInfoGeneral(
			this.actualizarInformacionGeneralForm.value.correo,
			this.actualizarInformacionGeneralForm.value.direccion,
			this.actualizarInformacionGeneralForm.value.lugarResidencia,
			this.actualizarInformacionGeneralForm.value.telefonoFijo,
			this.actualizarInformacionGeneralForm.value.celular,
			this.actualizarInformacionGeneralForm.value.estadoCivil,
			this.actualizarInformacionGeneralForm.value.aniosExperienciaLaboral,
			this.fechasValidacion(this.actualizarInformacionGeneralForm.value.fechaExpedicion),
			this.fechasValidacion(this.actualizarInformacionGeneralForm.value.fechaNacimiento),
			this.tenerLibretaMilitar ? this.actualizarInformacionGeneralForm.value.libretaMilitar : 'No',
			this.tenerLibretaMilitar ? this.actualizarInformacionGeneralForm.value.claseLibretaMilitar : 'SAG',
			this.tenerLibretaMilitar ? this.actualizarInformacionGeneralForm.value.distritoLibretaMilitar : '.',
			this.actualizarInformacionGeneralForm.value.tarjetaProfesional,
			this.actualizarInformacionGeneralForm.value.grupoSanguineo,
			this.tenerLicencia ? this.actualizarInformacionGeneralForm.value.licenciaConduccion : 'No',
			this.tenerLicencia ? this.actualizarInformacionGeneralForm.value.categoriaLicencia : 'SAG',
			this.actualizarInformacionGeneralForm.value.color,
			this.actualizarInformacionGeneralForm.value.hobby1,
			this.actualizarInformacionGeneralForm.value.hobby2,
			this.actualizarInformacionGeneralForm.value.profesion,
			this.actualizarInformacionGeneralForm.value.genero
		);

		let responseActualizarInfGenEmpleado = await this._empleadosService.actualizarInfoGeneralEmpleado(this.idGeneralEmpleado, actualizarEmpleadoInfGeneral);
		//console.log(responseActualizarInfGenEmpleado);
		if (responseActualizarInfGenEmpleado["status"] === "success" && responseActualizarInfGenEmpleado["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseActualizarInfGenEmpleado["message"], 'success')
				.then(result => {
					this.regresar();
				}).catch(swal.noop);
		}
		else if (responseActualizarInfGenEmpleado["status"] === "success" && responseActualizarInfGenEmpleado["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseActualizarInfGenEmpleado["message"], 'warning');
		}
		else if (responseActualizarInfGenEmpleado["status"] === "error" && responseActualizarInfGenEmpleado["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseActualizarInfGenEmpleado['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseActualizarInfGenEmpleado["message"], 'error');
		}
	}

	/**
	 * 
	 * @param opc 
	 * @param valor 
	 * Permite mostrar u ocultar los campos de acuerdo a la eleccion de los combos
	 */
	ocultarInput(opc: any, valor: any) {
		//console.log(opc);
		switch (opc) {
			case 'libreta':
				if (valor == 1) {
					this.tenerLibretaMilitar = true;
				} else {
					this.tenerLibretaMilitar = false;
				}
				break;
			case 'licencia':
				if (valor == 1) {
					this.tenerLicencia = true;
				} else {
					this.tenerLicencia = false;
				}
				break;
		}
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) {
			result = e1 === e2;
		}
		return result;
	}

	fechasValidacion(formatoFecha: any) {
		//console.log(formatoFecha);
		if (formatoFecha) {
			//Formatear fecha
			const fechaActualTem = this.datepipe
				.transform(formatoFecha, "yyyy/MM/dd")
				.replace(/\//g, "-");
			//console.log("fecha formato: "+fechaActualTem);
			return fechaActualTem;
		}
		return '';
	}

	regresar() {
		if (localStorage.getItem('ficha')) {
			let idIngreso = localStorage.getItem('idIngreso');
			localStorage.removeItem('ficha');
			this._router.navigate(["clientes/ficha-clientes/", idIngreso]);
		}
		else { this._router.navigate(["usuarios/listar"]); }
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}