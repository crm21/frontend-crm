import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { Permisos } from '../../models/permisos';
import swal from 'sweetalert2';
import { PermisosService } from '../../services/permisos/permisos.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare const $: any;
@Component({
	selector: 'app-permisos-menu',
	templateUrl: './permisos-menu.component.html',
	styleUrls: ['./permisos-menu.component.css']
})
export class PermisosMenuComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	crearPermisoForm: FormGroup;
	permisosExistentes: any;
	sistemaArray: string[] = ['CRM', 'ERP'];

	constructor(
		private _router: Router,
		private _formBuilder: FormBuilder,
		private _permisosService: PermisosService,
		private _sweetAlertService: SweetAlertService,
	) { }

	ngOnInit() {
		if (localStorage.getItem('identity_crm')) {
			this.crearPermisoForm = this._formBuilder.group({
				titulo: ['', Validators.required],
				url: ['', [Validators.required, Validators.pattern("^[a-z-/]{1,50}$")]],
				icono: ['', Validators.required],
				posicion: ['', Validators.required],
				sistema: ['', Validators.required]
			});
			this.listarPermisosExistentes();
		}
	}

	async onSubmitPermiso() {
		this.loading = true;

		let url = '/' + this.crearPermisoForm.value.url;

		let permiso = new Permisos(
			this.crearPermisoForm.value.titulo,
			url,
			'link',
			this.crearPermisoForm.value.icono,
			this.crearPermisoForm.value.posicion,
			this.crearPermisoForm.value.sistema
		); //console.log(permiso);

		const responsePermiso = await this._permisosService.crearPermisos(permiso);
		//console.log(responsePermiso);
		if (responsePermiso["status"] === "success" && responsePermiso["code"] === "200") {
			this.loading = false;
			this.listarPermisosExistentes();
			swal('¡Bien!!', responsePermiso["message"], 'success')
				.then((result) => {
					this.crearPermisoForm.reset();
					//this.regresar();
				}).catch(swal.noop);
		}
		else if (responsePermiso["status"] === "success" && responsePermiso["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responsePermiso["message"], 'warning');
		}
		else if (responsePermiso["status"] === "error" && responsePermiso["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responsePermiso['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responsePermiso["message"], 'error');
		}
	}

	async listarPermisosExistentes() {
		this.loading = true;
		this.permisosExistentes = null;

		const responsePermisos = await this._permisosService.listarPermisos();
		//console.log(responsePermisos);
		this.loading = false;
		if (responsePermisos['status'] == "success" && responsePermisos['code'] == "200") {
			this.permisosExistentes = responsePermisos['data'];
		}
		else if (responsePermisos['status'] == "success" && responsePermisos['code'] == "300") {
			swal('Advertencia!', responsePermisos['message'], 'warning');
		}
		else if (responsePermisos["status"] === "error" && responsePermisos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responsePermisos['message']);
		}
		else {
			;
			swal('¡Error!', responsePermisos['message'], 'error');
		}
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) {
			result = e1 === e2;
		}
		return result;
	}

	regresar() {
		this._router.navigate(["permisos/listar"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}