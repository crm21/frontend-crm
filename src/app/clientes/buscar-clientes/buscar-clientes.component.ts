import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from "@angular/router";
import swal from "sweetalert2";
import { ClienteService } from '../../services/cliente/cliente.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare var $: any;

@Component({
	selector: 'app-buscar-clientes',
	templateUrl: './buscar-clientes.component.html',
	styleUrls: ['./buscar-clientes.component.css']
})
export class BuscarClientesComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	buscarDocumentoForm: FormGroup;

	constructor(
		private _router: Router,
		private _formBuilder: FormBuilder,
		private _clienteService: ClienteService,
		private _sweetAlertService: SweetAlertService,
	) {
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);
		}
	}

	ngOnInit(): void {
		this.buscarDocumentoForm = this._formBuilder.group({
			documento: ['', [Validators.required, Validators.pattern("[0-9]{4,13}$")]]
		});
	}

	async buscarDocumento() {
		this.loading = true;
		let documento: number = +this.buscarDocumentoForm.value.documento;

		const responseCliente = await this._clienteService.buscarClientePorDocumento(documento);
		//console.log(responseCliente);
		if (responseCliente["status"] === "success" && responseCliente["code"] === "200") {
			this.loading = false;
			if (responseCliente["data"].existe) {
				this.buscarDocumentoForm.reset();
				let idCliente = responseCliente["data"].cliente.id;
				this._router.navigate(["clientes/ingresos/", idCliente]);
			} else {
				this.redireccionarRegistrarCliente(responseCliente["message"], '' + documento);
			}
		}
		else if (responseCliente["status"] === "success" && responseCliente["code"] === "300") {
			this.loading = false;
			swal('¡Error!', responseCliente["message"], 'warning');
		}
		else if (responseCliente["status"] === "error" && responseCliente["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseCliente['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseCliente["message"], 'error');
		}
	}

	redireccionarRegistrarCliente(mensaje: string, documento: string) {
		swal({
			title: 'Lo sentimos!',
			text: mensaje,
			type: 'question',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-warning',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: 'Registrar Cliente',
			cancelButtonText: 'Nueva búsqueda',
			buttonsStyling: true,
			reverseButtons: true
		}).then((result) => {
			if (result.value) {
				localStorage.setItem('documentoCliente', '' + documento);
				this._router.navigate(["clientes/registrar/"]);
			}
		});
	}

	regresar() {
		this._router.navigate(["clientes/listar-clientes"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}