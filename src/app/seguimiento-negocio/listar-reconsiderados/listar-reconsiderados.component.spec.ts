import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarReconsideradosComponent } from './listar-reconsiderados.component';

describe('ListarReconsideradosComponent', () => {
  let component: ListarReconsideradosComponent;
  let fixture: ComponentFixture<ListarReconsideradosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarReconsideradosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarReconsideradosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
