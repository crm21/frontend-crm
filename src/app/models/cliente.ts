export class Cliente {
	constructor(
		public tipoDocumento: string,
		public documento: string,
		public nombres: string,
		public apellidos: string,
		public canalIngreso: string,
		public tarjetaOpciones: string,
		public asesor: string,
		public contactoPrincipal?: string,
		public contactoSecundario?: string,
		public correo?: string,
		public fechaNacimiento?: string,
		public observacion?: string,
		public correoEmpresa?: string,
		public telefonoEmpresa?: string
	) { }
};

export class ActualizarCliente {
	constructor(
		public tipoDocumento: string,
		public documento: string,
		public nombres: string,
		public apellidos: string,
		public usuario: string,
		public rol: string,
		public estado: string
	) { }
};

export class InformacionPersonal {
	constructor(
		public fechaNacimiento: string,
		public contactoPrincipal: string,
		public contactoSecundario: string,
		public correo: string,
		public lugarResidencia: string,
		public direccion: string,
		public testDrive: string,
		public color: string,
		public hobby1: string,
		public hobby2: string,
		public profesion: string,
		public genero: string
	) { }
};


export class ActualizarClienteDatos {
	constructor(
		public tipoDocumento: string,
		public documento: string,
		public nombres: string,
		public apellidos: string
	) { }
};

export class InformacionPersonalDatos {
	constructor(
		public correo: string,
		public fechaNacimiento: string,
		public direccion: string,
		public lugarResidencia: string,
		public contactoPrincipal: string,
		public contactoSecundario: string,
		public color: string,
		public hobby1: string,
		public hobby2: string,
		public profesion: string,
		public genero: string
	) { }
};

export class ClienteNuevoIngreso {
	constructor(
		public canal: string,
		public tarjeta: string,
		public cliente: string,
		public observacion: string,
		public tipoDocumento: string,
		public documento: string
	) { }
};

export class ClienteLeads {
	constructor(
		public tipoDocumento: string,
		public documento: string,
		public nombres: string,
		public apellidos: string,
		public correo: string,
		public telefono: string,
		public canalIngreso: string,
		public observacion: string
	) { }
};

export class ActualizarIngresoCliente {
	constructor(
		public canal: string,
		public fechaIngreso: string
	) { }
};