import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { Login } from '../../models/login';
import { Router, ActivatedRoute } from '@angular/router';
import { UsuarioService } from '../../services/usuario/usuario.service';
import swal from 'sweetalert2';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { environment } from '../../../environments/environment';
import { MatSnackBar } from '@angular/material/snack-bar';

declare var $: any;

@Component({
	selector: 'app-sesion-usuarios',
	templateUrl: './sesion-usuarios.component.html',
	styleUrls: ['./sesion-usuarios.component.css']
})
export class SesionUsuariosComponent implements OnInit {

	private toggleButton: any;
	private sidebarVisible: boolean;
	private nativeElement: Node;
	loading = false;
	test: Date = new Date();
	login: Login;
	hide = true;
	version = environment.version;
	nombresEmpresas: string = '';
	opcionRegistrarse: boolean = false;
	rutaRegistro: string = '';
	imgNombreEmpresa: string = '';

	constructor(
		private element: ElementRef,
		private _router: Router,
		private _usuarioService: UsuarioService,
		private _sweetAlertService: SweetAlertService,
		private _route: ActivatedRoute,
		private _snackBar: MatSnackBar
	) {
		this.nativeElement = element.nativeElement;
		this.sidebarVisible = false;
		this.login = new Login('', '', '', '1');
		let opcionClientes = this._route.snapshot.data.opc;
		let opcionEmpresas = this._route.snapshot.data.business;
		localStorage.setItem('business', opcionEmpresas);
		let opcionRegistro = localStorage.getItem('business');
		this.consultarNombreEmpresa(localStorage.getItem('business'));
		this.mostrarRegistrar(opcionClientes, opcionRegistro);
	}

	ngOnInit() {
		var navbar: HTMLElement = this.element.nativeElement;
		this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
		const body = document.getElementsByTagName('body')[0];
		body.classList.add('login-page');
		body.classList.add('off-canvas-sidebar');
		if (!this.login.empresas) {
			this._router.navigate(['/empresas']);
		} else {
			this.checkSesion();
		}
	}

	sidebarToggle() {
		var toggleButton = this.toggleButton;
		var body = document.getElementsByTagName('body')[0];
		var sidebar = document.getElementsByClassName('navbar-collapse')[0];
		if (this.sidebarVisible == false) {
			setTimeout(function () {
				toggleButton.classList.add('toggled');
			}, 500);
			body.classList.add('nav-open');
			this.sidebarVisible = true;
		} else {
			this.toggleButton.classList.remove('toggled');
			this.sidebarVisible = false;
			body.classList.remove('nav-open');
		}
	}

	ngOnDestroy() {
		const body = document.getElementsByTagName('body')[0];
		body.classList.remove('login-page');
		body.classList.remove('off-canvas-sidebar');
	}

	checkSesion() {
		let checkToken = this._usuarioService.isToken();

		if (checkToken) this._router.navigate(['/dashboard']);
	}

	consultarNombreEmpresa(nombreEmpresa: any) {
		this.login.empresas = nombreEmpresa;
		this.nombresEmpresas = this._usuarioService.nombresEmpresa(nombreEmpresa);
		switch (nombreEmpresa) {
			case 'A':
				this.imgNombreEmpresa = './assets/img/empresa-autodenar-blanco.png';
				break;
			case 'M':
				this.imgNombreEmpresa = './assets/img/empresa-motodenar.png';
				break;
			case 'T':
				this.imgNombreEmpresa = './assets/img/empresa-automotora-blanco.png';
				break;
			case 'D':
				this.imgNombreEmpresa = './assets/img/empresa-autodenar-blanco.png';
				break;
			default:
				this.imgNombreEmpresa = './assets/img/empresa-autodenar-blanco.png';
				break;
		}
	}

	async iniciarSesion(loginForm: any) {
		this.loading = true;
		//console.log(this.login.empresas);
		if (!this.login.empresas) {
			this._router.navigate(['/empresas']);
		} else {
			const responseLogin: any = await this._usuarioService.iniciarSesion(this.login);
			let base_64_data = atob(responseLogin);
			let identityData = JSON.parse(base_64_data);

			if (identityData['status'] == "success") {
				let datosPersonales = identityData['data'];
				const responseDatosLoginToken = await this._usuarioService.iniciarSesion(this.login, true);

				let token: any = responseDatosLoginToken;
				this.loading = false;

				// Guardar token en el localstorage
				localStorage.setItem("system", btoa("crm"));
				localStorage.setItem("token_crm", token);
				localStorage.setItem('identity_crm', responseLogin);
				this._snackBar.open('¡BIENVENIDO! ' + datosPersonales['nombres'] + ' ' + datosPersonales['apellidos'], 'X', {
					duration: 4000,
				});
				this._router.navigate(['/dashboard']);
			} else {
				this.loading = false;
				swal('Advertencia!', identityData['message'], 'warning');
			}
		}
	}

	mostrarRegistrar(opcionClientes: string, opcionRegistro: string) {
		if (opcionClientes == '1') {
			this.opcionRegistrarse = true;
		}
		switch (opcionRegistro) {
			case 'A':
				this.rutaRegistro = '/autodenar/registrarse';
				break;
			case 'M':
				this.rutaRegistro = '/motodenar/registrarse';
				break;
			case 'T':
				this.rutaRegistro = '/automotora/registrarse';
				break;
			default:
				this._router.navigate(["empresas"]);
				break;
		}
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}
}