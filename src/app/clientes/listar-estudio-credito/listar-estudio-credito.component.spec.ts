import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarEstudioCreditoComponent } from './listar-estudio-credito.component';

describe('ListarEstudioCreditoComponent', () => {
  let component: ListarEstudioCreditoComponent;
  let fixture: ComponentFixture<ListarEstudioCreditoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarEstudioCreditoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarEstudioCreditoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
