import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TableroAsignacionRoutes } from './tablero-asignacion.routing';
import { LoadingModule } from '../shared/loading/loading.module';
import { AsignacionAsesoresComponent } from './asignacion-asesores/asignacion-asesores.component';
import { AtencionesComponent } from './atenciones/atenciones.component';
import { ListarDisponibilidadAsesoresComponent } from './listar-disponibilidad-asesores/listar-disponibilidad-asesores.component';
import { ReasignarAsesorComponent } from './reasignar-asesor/reasignar-asesor.component';
import { AtencionesTipificacionComponent } from './atenciones-tipificacion/atenciones-tipificacion.component';

@NgModule({
	declarations: [
		AsignacionAsesoresComponent,
		AtencionesComponent,
		ListarDisponibilidadAsesoresComponent,
		ReasignarAsesorComponent,
		AtencionesTipificacionComponent
	],
	imports: [
		CommonModule,
		RouterModule.forChild(TableroAsignacionRoutes),
		FormsModule,
		MaterialModule,
		ReactiveFormsModule,
		LoadingModule
	],
	providers: [DatePipe]
})
export class TableroAsignacionModule { }
