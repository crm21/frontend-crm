import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SiembraCosechaRoutes } from './siembra-cosecha.routing';
import { LoadingModule } from '../shared/loading/loading.module';
import { LoadingSpinnerModule } from '../shared/loading-spinner/loading-spinner.module';
import { InicioComponent } from './inicio/inicio.component';
import { ClientesScComponent } from './clientes-sc/clientes-sc.component';
import { ClientesSemillaComponent } from './clientes-semilla/clientes-semilla.component';
import { ClientesSembradosComponent } from './clientes-sembrados/clientes-sembrados.component';
import { ClientesCosechadosComponent } from './clientes-cosechados/clientes-cosechados.component';
import { FichaClientesScComponent } from './ficha-clientes-sc/ficha-clientes-sc.component';
import { ClientesProyectadosComponent } from './clientes-proyectados/clientes-proyectados.component';

@NgModule({
	declarations: [
		InicioComponent,
		ClientesScComponent,
		ClientesSemillaComponent,
		ClientesSembradosComponent,
		ClientesCosechadosComponent,
		FichaClientesScComponent,
		ClientesProyectadosComponent
	],
	imports: [
		CommonModule,
		RouterModule.forChild(SiembraCosechaRoutes),
		FormsModule,
		MaterialModule,
		ReactiveFormsModule,
		LoadingModule,
		LoadingSpinnerModule
	],
	providers: [DatePipe]
})
export class SiembraCosechaModule { }