import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarDisponibilidadComponent } from './actualizar-disponibilidad.component';

describe('ActualizarDisponibilidadComponent', () => {
  let component: ActualizarDisponibilidadComponent;
  let fixture: ComponentFixture<ActualizarDisponibilidadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualizarDisponibilidadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizarDisponibilidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
