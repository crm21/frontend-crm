import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from "../../../environments/environment";
import { ClienteService } from '../../services/cliente/cliente.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { UsuarioService } from '../../services/usuario/usuario.service';
import { ClienteNuevoIngreso, ActualizarIngresoCliente } from '../../models/cliente';
import { ActualizarIngreso } from '../../interfaces/interfaces';
import { DatePipe } from "@angular/common";
import swal from "sweetalert2";

declare var $: any;

@Component({
	selector: 'app-ingresos-clientes',
	templateUrl: './ingresos-clientes.component.html',
	styleUrls: ['./ingresos-clientes.component.css']
})
export class IngresosClientesComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	idCliente: any;
	idIngreso: any;
	crearNuevoIngresoForm: FormGroup;
	crearNuevoIngresoDocForm: FormGroup;
	actualizarIngresoForm: FormGroup;
	informacionCliente: any = {
		nombres: null,
		tipoDoc: null,
		documento: null
	};
	historialCliente: any[] = [];
	historiaCliente: any;
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	permisosBotonVerIngreso = ['S', 'CL', 'N'];
	botonVerIngreso = false;
	ocultarBotonRetroceder = true;
	permisoVerFicha: any;
	ocultarAtras = '';
	tipoDocumentosArray: any[] = [];
	tipoCanalIngresoArray: any[] = [];
	tarjetaOpcionesArray: any[] = [];
	localImg = environment.serverImg;
	rutaImg: string = '';
	botonNuevoIngreso: boolean = false;
	camposRequeridos: boolean = false;
	fechaIngresoCliente = {
		temp: null,
		guardar: null
	};

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _formBuilder: FormBuilder,
		private _clienteService: ClienteService,
		private _parametrizacionService: ParametrizacionService,
		private _usuarioService: UsuarioService,
		private _sweetAlertService: SweetAlertService,
		private datepipe: DatePipe
	) {
		this.idCliente = this._route.snapshot.paramMap.get('idCliente');
		if (localStorage.getItem('identity_crm')) {
			this.rutaImg = _usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			this.permisoVerFicha = identityData['data'];
			this.ocultarAtras = this.permisoVerFicha.rol.valor;
			if (this.permisosBotonVerIngreso.includes(this.ocultarAtras)) {
				this.botonVerIngreso = true;
			}

			this.buscarDocumento(this.idCliente, '1');
		}
	}

	ngOnInit(): void {
		if (!localStorage.getItem('ruta')) {
			this.ocultarBotonRetroceder = false;
		}

		this.actualizarIngresoForm = this._formBuilder.group({
			canalIngreso: ['', Validators.required],
			fechaIngreso: ['', Validators.required]
		});

		this.crearNuevoIngresoForm = this._formBuilder.group({
			canalIngreso: ['', Validators.required],
			tarjetaOpciones: ['', Validators.required],
			observacion: ['']
		});

		this.crearNuevoIngresoDocForm = this._formBuilder.group({
			tipoDoc: ['', Validators.required],
			documento: ['', [Validators.required, Validators.pattern("[0-9]{5,13}$")]],
			canalIngreso: ['', Validators.required],
			tarjetaOpciones: ['', Validators.required],
			observacion: ['']
		});
	}

	async buscarDocumento(idCliente: string, page: string) {
		this.loading = true;

		const responseCliente = await this._clienteService.buscarIngresosClientePorID(idCliente, page);
		//console.log(responseCliente);
		if (responseCliente["status"] === "success" && responseCliente["code"] === "200") {
			this.loading = false;
			this.llenarInformacionCliente(responseCliente['data'].informacionCliente);
			this.llenarDatosHistoricos(responseCliente['data'].historialIngresos, page);
		}
		else if (responseCliente["status"] === "success" && responseCliente["code"] === "300") {
			this.loading = false;
			swal('¡Error!', responseCliente["message"], 'warning')
				.then((result) => {
					this._router.navigate(["clientes/buscar"]);
				}).catch(swal.noop);
		}
		else if (responseCliente["status"] === "error" && responseCliente["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseCliente['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseCliente["message"], 'error');
		}
	}

	llenarInformacionCliente(dataCliente: any) {
		this.informacionCliente.nombres = dataCliente.nombres + ' ' + dataCliente.apellidos;
		this.informacionCliente.tipoDoc = dataCliente.tipoDocumento.valor;
		this.informacionCliente.documento = dataCliente.documento;
		if (this.informacionCliente.documento) {
			this.camposRequeridos = true;
		} else {
			this.camposRequeridos = false;
		}
	}

	llenarDatosHistoricos(data: any, page: any) {
		this.historiaCliente = null;
		this.historialCliente = [];
		if (data.ingresosConcesionario) {
			this.historiaCliente = data.ingresosConcesionario;

			this.total_item_count = data.total_item_count;
			this.page_actual = page;

			this.total_pages = data.total_pages;

			if (page >= 2) this.page_prev = page - 1;
			else this.page_prev = 1;

			if (page < data.total_pages) this.page_next = page + 1;
			else {
				if (data.total_pages == 0) this.page_next = 1;
				else this.page_next = data.total_pages;
			}

			this.historiaCliente.forEach((element) => {
				this.botonNuevoIngreso = element.fechaSalida == '0000-00-00' ? false : true;
				this.historialCliente.push({
					id: element.id,
					fechaIngreso: element.fechaIngreso,
					fechaSalida: element.fechaSalida,
					fechaSalidaTemp: element.fechaSalida == '0000-00-00' ? '- - -' : element.fechaSalida,
					canalIngreso: element.canalIngreso,
					tarjetaOpcionesTemp: element.tarjetaOpciones.nombre,
					asesor: element.asesor.nombres + " " + element.asesor.apellidos,
					boton: element.fechaSalida == '0000-00-00' ? true : false
				});

			});
		}
		else this.botonNuevoIngreso = true;
	}

	abrirIngreso(idIngreso: any) {
		swal({
			title: 'Escribir una observación para re-abrir el negocio',
			html: '<div class="form-group">' +
				'<input id="input-field" type="text" class="form-control" />' +
				'</div>',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			buttonsStyling: false
		})
			.then((result) => {
				if (result.value) {
					let observacionAbrirIngreso = $('#input-field').val();
					//console.log(observacionAbrirIngreso);
					this.onSubmitAbrirIngreso(idIngreso, observacionAbrirIngreso);
				}
			}).catch(swal.noop);

	}

	async onSubmitAbrirIngreso(ingresoId: any, observacion: any) {
		//console.log(`idIngreso: ${ingresoId}`);
		this.loading = true;
		const responseInIngreso = await this._clienteService.abrirIngreso(ingresoId, observacion);
		//console.log(responseInIngreso);
		if (responseInIngreso["status"] === "success" && responseInIngreso["code"] === "200") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Bien!', 'El negocio se ha re-abierto', 'OK', 'success');

			this.verFichaCliente(ingresoId);
		}
		else if (responseInIngreso["status"] === "success" && responseInIngreso["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'danger', 'warning', '<b>' + responseInIngreso['message'] + '<b>', 2000);
		}
		else if (responseInIngreso["status"] === "error" && responseInIngreso["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseInIngreso['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseInIngreso['message'] + '<b>', 2000);
		}
	}

	/**
	 * Inicio Nuevo Ingreso
	 */
	async cargarInformacionSelect() {
		this.tipoDocumentosArray = [];
		this.tipoCanalIngresoArray = [];
		this.tarjetaOpcionesArray = [];
		let tipoDocumentosParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo Documento');
		let tipoCanalIngreso = await this._parametrizacionService.listarCategoriaParametrizacion('Canal de Ingreso');
		let tarjetaOpciones = await this._parametrizacionService.listarCategoriaParametrizacion('Tarjeta de Opciones');

		tipoDocumentosParametros['data'].forEach((element) => {
			this.tipoDocumentosArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		tipoCanalIngreso['data'].forEach((element) => {
			this.tipoCanalIngresoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		tarjetaOpciones['data'].forEach((element) => {
			this.tarjetaOpcionesArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
	}

	crearNuevoIngreso() {
		this.cargarInformacionSelect();
		if (this.camposRequeridos) {
			$("#modalNuevoIngreso").modal("show");
		} else {
			$("#modalNuevoIngresoDocumentos").modal("show");
		}
	}

	/* Metodo para registrar un nuevo ingreso sin tipo de documento y documento  */
	async onSubmitCrearNuevoIngresoCliente() {
		this.loading = true;

		let nuevoIngresoCliente = new ClienteNuevoIngreso(
			this.crearNuevoIngresoForm.value.canalIngreso,
			this.crearNuevoIngresoForm.value.tarjetaOpciones,
			this.idCliente,
			this.crearNuevoIngresoForm.value.observacion,
			'',
			''
		); //console.log(nuevoIngresoCliente);

		const responseCliente = await this._clienteService.crearNuevoIngreso(nuevoIngresoCliente);
		//console.log(responseCliente);
		if (responseCliente["status"] === "success" && responseCliente["code"] === "200") {
			this.loading = false;
			this.idIngreso = responseCliente["data"].id
			swal('¡Bien!', responseCliente["message"], 'success')
				.then(result => {
					this.crearNuevoIngresoForm.reset();
					localStorage.setItem('idIngreso', this.idIngreso);
					this.asignarAsesor(this.idCliente);
				}).catch(swal.noop);
		}
		else if (responseCliente["status"] === "success" && responseCliente["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseCliente["message"], 'OK', 'warning');
		}
		else if (responseCliente["status"] === "error" && responseCliente["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseCliente['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseCliente["message"], 'OK', 'error');
		}
	}

	/* Metodo para registrar un nuevo ingreso con tipo de documento y documento  */
	async onSubmitCrearNuevoIngresoClienteDatos() {
		this.loading = true;

		let nuevoIngresoCliente = new ClienteNuevoIngreso(
			this.crearNuevoIngresoDocForm.value.canalIngreso,
			this.crearNuevoIngresoDocForm.value.tarjetaOpciones,
			this.idCliente,
			this.crearNuevoIngresoDocForm.value.observacion,
			this.crearNuevoIngresoDocForm.value.tipoDoc,
			this.crearNuevoIngresoDocForm.value.documento
		); //console.log(nuevoIngresoCliente);

		const responseCliente = await this._clienteService.crearNuevoIngreso(nuevoIngresoCliente);
		//console.log(responseCliente);
		if (responseCliente["status"] === "success" && responseCliente["code"] === "200") {
			this.loading = false;
			this.idIngreso = responseCliente["data"].id
			swal('¡Bien!', responseCliente["message"], 'success')
				.then(result => {
					this.crearNuevoIngresoDocForm.reset();
					localStorage.setItem('idIngreso', this.idIngreso);
					this.asignarAsesor(this.idCliente);
				}).catch(swal.noop);
		}
		else if (responseCliente["status"] === "success" && responseCliente["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseCliente["message"], 'OK', 'warning');
		}
		else if (responseCliente["status"] === "error" && responseCliente["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseCliente['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseCliente["message"], 'OK', 'error');
		}
	}

	asignarAsesor(idCliente) {
		// opc = 1 => vista crear cliente
		let opc = 1;
		this._router.navigate(["atenciones/asignar-asesor/", this.idIngreso, idCliente, opc]);
	}
	/**
	 * Fin Nuevo Ingreso
	 */

	/* Inicia Actualizacion de un ingreso */
	actualizarIngreso(rowIngreso: any) {
		this.cargarInformacionSelect();
		this.idIngreso = rowIngreso.id;
		let ingresoActualizar: ActualizarIngreso = rowIngreso;

		this.fechaIngresoCliente.temp = ingresoActualizar.fechaIngreso;
		this.fechaIngresoCliente.guardar = this.fechasValidacion(1, ingresoActualizar.fechaIngreso);
		this.actualizarIngresoForm.setValue({
			canalIngreso: ingresoActualizar.canalIngreso.valor,
			fechaIngreso: this.fechaIngresoCliente.guardar
		});

		$("#modalActualizarIngreso").modal("show");
	}

	async onSubmitActualizarIngreso() {
		this.loading = true;
		let fechaIngresoTemp = this.fechasValidacion(2, this.actualizarIngresoForm.value.fechaIngreso, this.fechaIngresoCliente);
		let ingresoActualizar = new ActualizarIngresoCliente(
			this.actualizarIngresoForm.value.canalIngreso,
			fechaIngresoTemp
		); //console.log(ingresoActualizar);

		const responseActualizarIngreso = await this._clienteService.actualizarIngresoCliente(this.idIngreso, ingresoActualizar);
		//console.log(responseActualizarIngreso);
		if (responseActualizarIngreso["status"] === "success" && responseActualizarIngreso["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseActualizarIngreso["message"], 'success')
				.then(result => {
					this.actualizarIngresoForm.reset();
					$("#modalActualizarIngreso").modal("hide");
					this.buscarDocumento(this.idCliente, '1');
				}).catch(swal.noop);
		}
		else if (responseActualizarIngreso["status"] === "success" && responseActualizarIngreso["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseActualizarIngreso["message"], 'OK', 'warning');
		}
		else if (responseActualizarIngreso["status"] === "error" && responseActualizarIngreso["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseActualizarIngreso['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseActualizarIngreso["message"], 'OK', 'error');
		}

	}
	/* Finaliza Actualizacion de un ingreso */

	fechasValidacion(opc, formatoFecha, fechaTemp: any = null) {
		if (opc == 1) {
			if (formatoFecha != '0000-00-00') {
				//Formatear fecha
				const fechaActualTem = this.datepipe
					.transform(formatoFecha, "yyyy/MM/dd")
					.replace(/\//g, "-");
				return fechaActualTem;
			}
			return formatoFecha;
		}
		else if (opc == 2) {
			if (formatoFecha != '0000-00-00') {
				let fechaLlega = this.fechasValidacion(1, formatoFecha);
				if (fechaTemp.guardar === fechaLlega) {
					return fechaTemp.temp;
				} else {
					const fechaActual = new Date();
					const horasMinSeg = this.datepipe.transform(fechaActual, "H:mm:ss");
					let fechaFormateadaTemp = this.fechasValidacion(1, formatoFecha);
					return fechaFormateadaTemp + ' ' + horasMinSeg;
				}
			}
			return formatoFecha;
		}
		else {
			return '';
		}
	}

	regresar(opc: number) {
		if (opc == 1) {
			localStorage.removeItem('ruta');
			this._router.navigate(["clientes/buscar"]);
			//window.history.back();
		}
		else if (opc == 2) {
			this._router.navigate(["clientes/listar-clientes"]);
		}
		else {
			if (localStorage.getItem('ruta')) {
				let rutaVolver = localStorage.getItem('ruta');
				//console.log(rutaVolver);
				this._router.navigate([rutaVolver]);
			} else this._router.navigate(["clientes/buscar"]);
		}
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	verFichaCliente(idIngreso: any) {
		this._router.navigate(["clientes/ficha-clientes/", idIngreso]);
	}

	asignar(idIngreso: any) {
		this._router.navigate(["atenciones/reasignar-asesor/", idIngreso]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}