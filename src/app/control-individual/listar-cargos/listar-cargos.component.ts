import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Cargos } from '../../models/cargos';
import { ActualizarCargos } from '../../interfaces/interfaces'
import { ControlIndividualService } from '../../services/control-individual/control-individual.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';

declare var $: any;
declare interface DataTable {
	headerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-listar-cargos',
	templateUrl: './listar-cargos.component.html',
	styleUrls: ['./listar-cargos.component.css']
})
export class ListarCargosComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	consultaVaciaCargarSpinner: boolean = false;
	crearCargoFormGroup: FormGroup;
	actualizarCargoFormGroup: FormGroup;
	dataTable: DataTable;
	listadoCargos: any[] = [];
	periodoPagoArray: any[] = [];
	areasArray: any[] = [];
	dependenciasArray: any[] = [];
	todasDependenciasArray: any[] = [];
	numeroCargosArray: any[] = [];
	ubicacionOrganigramaArray: string[] = ['Eje Estratégico', 'Eje Operativo'];
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	idCargo = null;
	textoExtendido: string = '';

	constructor(
		private _router: Router,
		private _formBuilder: FormBuilder,
		private _controlIndividualService: ControlIndividualService,
		private _sweetAlertService: SweetAlertService,
		private _parametrizacionService: ParametrizacionService
	) {
		if (localStorage.getItem('identity_crm')) {
			this.listarCargos(1, 1);
			this.cargarInformacionSelect();
		}
	}

	ngOnInit() {
		this.dataTable = {
			headerRow: [
				'#',
				"Nombre cargo",
				"Área",
				"Dependencia",
				"Periodo de pago",
				"Número de cargos",
				"Ubicación organigrama",
				"Misión",
				"Acciones"
			],
			dataRows: []
		};
		this.crearCargoFormGroup = this._formBuilder.group({
			nombre: ['', Validators.required],
			area: ['', Validators.required],
			dependencia: ['', Validators.required],
			periodoPago: ['', Validators.required],
			numCargos: ['', Validators.required],
			ubicacionOrganigrama: ['', Validators.required],
			mision: ['', Validators.required]
		});
		this.actualizarCargoFormGroup = this._formBuilder.group({
			nombre: ['', Validators.required],
			area: ['', Validators.required],
			dependencia: ['', Validators.required],
			periodoPago: ['', Validators.required],
			numCargos: ['', Validators.required],
			ubicacionOrganigrama: ['', Validators.required],
			mision: ['', Validators.required]
		});
	}

	async onSubmitCrearCargo() {
		this.loading = true;

		let crearCargo = new Cargos(
			this.crearCargoFormGroup.value.nombre,
			this.crearCargoFormGroup.value.area,
			this.crearCargoFormGroup.value.dependencia,
			this.crearCargoFormGroup.value.periodoPago,
			this.crearCargoFormGroup.value.numCargos,
			this.crearCargoFormGroup.value.ubicacionOrganigrama,
			this.crearCargoFormGroup.value.mision
		); //console.log(crearCargo);
		const responseCrearCargos = await this._controlIndividualService.registrarCargos(crearCargo);
		// console.log(responseCrearCargos);
		if (responseCrearCargos["status"] === "success" && responseCrearCargos["code"] === "200") {
			this.limpiarFormulario();
			this.listarCargos(1, 1);
			this._sweetAlertService.showNotification('top', 'right', 'success', 'check_circle', '<b>' + responseCrearCargos['message'] + '<b>', 2000);
			this.loading = false;
		}
		else if (responseCrearCargos["status"] === "success" && responseCrearCargos["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseCrearCargos['message'] + '<b>', 2000);
		}
		else if (responseCrearCargos["status"] === "error" && responseCrearCargos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseCrearCargos['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseCrearCargos["message"], 'OK', 'error');
		}
	}

	async onSubmitActualizarCargo() {
		this.loading = true;

		let actualizarCargo = new Cargos(
			this.actualizarCargoFormGroup.value.nombre,
			this.actualizarCargoFormGroup.value.area,
			this.actualizarCargoFormGroup.value.dependencia,
			this.actualizarCargoFormGroup.value.periodoPago,
			this.actualizarCargoFormGroup.value.numCargos,
			this.actualizarCargoFormGroup.value.ubicacionOrganigrama,
			this.actualizarCargoFormGroup.value.mision
		); //console.log(crearCargo);
		const responseModificarCargos = await this._controlIndividualService.modificarCargo(this.idCargo, actualizarCargo);
		//console.log(responseModificarCargos);
		if (responseModificarCargos["status"] === "success" && responseModificarCargos["code"] === "200") {
			this.actualizarCargoFormGroup.reset();
			this.listarCargos(1, 1);
			$("#modalCargos").modal("hide");
			this.loading = false;
		}
		else if (responseModificarCargos["status"] === "success" && responseModificarCargos["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseModificarCargos['message'] + '<b>', 2000);
		}
		else if (responseModificarCargos["status"] === "error" && responseModificarCargos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseModificarCargos['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseModificarCargos["message"], 'OK', 'error');
		}
	}

	async listarCargos(opc: any, page: any) {
		this.loading = true;

		const responseListaCargos = await this._controlIndividualService.listarCargosId(opc, '', page);
		//console.log(responseListaCargos);
		if (responseListaCargos["status"] === "success" && responseListaCargos["code"] === "200") {
			this.llenarDatosTabla(page, responseListaCargos['data'], responseListaCargos['data'].cargos);
			this.loading = false;
		}
		else if (responseListaCargos["status"] === "success" && responseListaCargos["code"] === "300") {
			this.loading = false;
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseListaCargos['message'] + '<b>', 2000);
		}
		else if (responseListaCargos["status"] === "error" && responseListaCargos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseListaCargos['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			this._sweetAlertService.alertGeneral('¡Error!', responseListaCargos["message"], 'OK', 'error');
		}
	}

	llenarDatosTabla(page: any, listaCargosResponse: any, cargos: any) {
		this.listadoCargos = cargos;
		this.dataTable.dataRows = [];
		this.total_item_count = listaCargosResponse['total_item_count'];
		this.page_actual = page;

		this.total_pages = listaCargosResponse['total_pages'];

		if (page >= 2) this.page_prev = page - 1;
		else this.page_prev = 1;

		if (page < listaCargosResponse['total_pages']) this.page_next = page + 1;
		else {
			if (listaCargosResponse['total_pages'] == 0) this.page_next = 1;
			else this.page_next = listaCargosResponse['total_pages'];
		}

		let JSONParams: any;

		this.listadoCargos.forEach((element) => {
			JSONParams = {
				id: element.id,
				nombre: element.nombre,
				area: element.area,
				dependencia: element.dependencia,
				periodoPago: element.periodoPago,
				numCargos: element.numCargos,
				ubicacionOrganigrama: element.ubicacionOrganigrama,
				mision: element.mision
			};
			this.dataTable.dataRows.push(JSONParams);
		});

		if (this.total_item_count || this.total_item_count == 0) {
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
		}
	}

	async cargarInformacionSelect() {
		this.loading = true;

		this.periodoPagoArray = [];
		this.areasArray = [];
		this.todasDependenciasArray = [];
		this.numeroCargosArray = [];

		const periodoPagoParametros: any = await this._parametrizacionService.listarCategoriaParametrizacion('Periodo de Pago');
		const areasParametros: any = await this._parametrizacionService.listarCategoriaParametrizacion('Areas');
		const dependenciasParametros: any = await this._parametrizacionService.listarCategoriaParametrizacion('Dependencias');

		periodoPagoParametros['data'].forEach((element) => {
			this.periodoPagoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		areasParametros['data'].forEach((element) => {
			this.areasArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		dependenciasParametros['data'].forEach((element) => {
			this.todasDependenciasArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		for (let index = 1; index < 11; index++) {
			this.numeroCargosArray.push({
				nombre: '' + index,
				valor: '' + index
			});
		}

		this.loading = false;
	}

	limpiarFormulario() {
		this.crearCargoFormGroup.reset();
	}

	listarFuncionesCargo(rowIdCargo: any) {
		this._router.navigate(["control-individual/listar-funciones/", rowIdCargo]);
	}

	actualizarCargo(rowCargo: any) {
		let cargoRow: ActualizarCargos = rowCargo;
		this.idCargo = cargoRow['id'];
		this.cargarDependenciasArea(cargoRow.area.valor);

		this.actualizarCargoFormGroup.setValue({
			nombre: cargoRow.nombre,
			area: cargoRow.area.valor,
			dependencia: cargoRow.dependencia.valor,
			periodoPago: cargoRow.periodoPago.valor,
			numCargos: cargoRow.numCargos,
			ubicacionOrganigrama: cargoRow.ubicacionOrganigrama,
			mision: cargoRow.mision
		});
		$("#modalCargos").modal("show");
	}

	cargarDependenciasArea(areaDatos: string) {
		this.dependenciasArray = [];

		this.todasDependenciasArray.forEach((element) => {
			if (areaDatos === element.descripcion) {
				this.dependenciasArray.push({
					id: element.id,
					nombre: element.nombre,
					valor: element.valor,
					categoria: element.categoria,
					descripcion: element.descripcion
				});
			}
		});
	}

	observarTexto(texto: any) {
		this.textoExtendido = texto;
		$("#modalMision").modal("show");
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}
