import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteClientesLeadsDiaComponent } from './reporte-clientes-leads-dia.component';

describe('ReporteClientesLeadsDiaComponent', () => {
  let component: ReporteClientesLeadsDiaComponent;
  let fixture: ComponentFixture<ReporteClientesLeadsDiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteClientesLeadsDiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteClientesLeadsDiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
