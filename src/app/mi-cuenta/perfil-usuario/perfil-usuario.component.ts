import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActualizarPass, CambiarContrasenia } from '../../models/actualizarContrasena';
import swal from 'sweetalert2';
import { UsuarioService } from '../../services/usuario/usuario.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

@Component({
	selector: 'app-perfil-usuario',
	templateUrl: './perfil-usuario.component.html',
	styleUrls: ['./perfil-usuario.component.css']
})
export class PerfilUsuarioComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	infoUsuario: any;
	dataUsuario: any;
	cambiar = false;
	hide = true;
	modelContrasenia: ActualizarPass;
	correoEmpresa: string = '';
	telefonoEmpresa: string = '';
	urlEmpresaPass: string = '';

	constructor(
		private _router: Router,
		private _usuarioService: UsuarioService,
		private _sweetAlertService: SweetAlertService
	) {
		if (localStorage.getItem('identity_crm')) {
			this.infoUsuario = this.informacionUsuario();
		}
		this.modelContrasenia = new ActualizarPass(null, null, null);
	}

	ngOnInit() {
		this.buscarUsuario();
		let urlAbsoluta = this.getAbsolutePath();
		this.urlEmpresaPass = urlAbsoluta + '#';
		//console.log(this.urlEmpresaPass);
	}

	getAbsolutePath() {
		let loc = window.location;
		let pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
		return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
	}

	informacionUsuario() {
		let base_64_data = atob(localStorage.getItem('identity_crm'));
		let identityData = JSON.parse(base_64_data);
		let data = identityData['data'];
		const informacionUsuario = {
			id: data.sub,
			documento: data.documento,
			nombre: data.nombres + ' ' + data.apellidos,
			rol: data.rol.nombre,
			estado: data.estado.valor
		}
		return informacionUsuario;
	}


	async buscarUsuario() {
		this.loading = true;

		const responseIdCliente = await this._usuarioService.buscarIdCliente(this.infoUsuario.id)
		//console.log(responseIdCliente);
		if (responseIdCliente["status"] === "success" && responseIdCliente["code"] === "200") {
			this.loading = false;
			let usuario = {
				nombres: responseIdCliente['data'].nombres,
				apellidos: responseIdCliente['data'].apellidos,
				correo: responseIdCliente['data'].correo
			}
			this.dataUsuario = usuario;
		}
		else if (responseIdCliente["status"] === "error" && responseIdCliente["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseIdCliente['message']);
		}
		else if (responseIdCliente["status"] === "success" && responseIdCliente["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseIdCliente["message"], 'OK', 'warning');
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseIdCliente["message"], 'OK', 'error');
		}

	}

	cambiarPass() {
		this.cambiar = true;
		let informacionEmpresa: string[] = this._sweetAlertService.datosEmpresas(localStorage.getItem('business'));
		//console.log(informacionEmpresa);
		this.correoEmpresa = informacionEmpresa[0];
		this.telefonoEmpresa = informacionEmpresa[1];
	}

	async cambiarContrasenia(passForm) {
		//console.log(passForm);
		this.loading = true;

		if (passForm.value.nueva === passForm.value.rNueva) {
			let datosContraseniaNueva = new CambiarContrasenia(
				passForm.value.actual,
				passForm.value.nueva,
				this.urlEmpresaPass,
				this.correoEmpresa,
				'' + this.telefonoEmpresa
			); //console.log(datosContraseniaNueva);

			const responseContrasenia = await this._usuarioService.cambiarContrasenia('' + this.infoUsuario.id, datosContraseniaNueva);
			//console.log(responseContrasenia);
			if (responseContrasenia["status"] === "success" && responseContrasenia["code"] === "200") {
				this.loading = false;
				swal('¡Bien!', responseContrasenia['message'], 'success')
					.then(result => {
						this._router.navigate(['/dashboard']);
					}).catch(swal.noop);
			}
			else if (responseContrasenia["status"] === "success" && responseContrasenia["code"] === "300") {
				this.loading = false;
				this._sweetAlertService.alertGeneral('Advertencia!', responseContrasenia["message"], 'OK', 'warning');
			}
			else if (responseContrasenia["status"] === "error" && responseContrasenia["code"] === "100") {
				this.loading = false;
				if (this.validarToken) this.expiracionToken(responseContrasenia['message']);
			}
			else {
				this.loading = false;
				this._sweetAlertService.alertGeneral('¡Error!', responseContrasenia["message"], 'OK', 'error');
				//this._sweetAlertService.showNotification('top', 'right', 'danger', 'warning', '<b>' + responseContrasenia['message'] + '<b>', 2000);
			}
		} else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', 'Los campos: Nueva contraseña y Repetir contraseña, deben ser iguales', 'OK', 'warning');
			//this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b> Nueva contraseña y Repetir contraseña, deben ser iguales <b>', 2000);
		}
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}