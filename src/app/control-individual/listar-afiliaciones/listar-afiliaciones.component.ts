import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Afiliaciones, AfiliacionesSoportes } from '../../models/afiliaciones';
import { ActualizarAfiliaciones } from '../../interfaces/interfaces'
import { DatePipe } from "@angular/common";
import { ControlIndividualService } from '../../services/control-individual/control-individual.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { UsuarioService } from '../../services/usuario/usuario.service';
import swal from 'sweetalert2';

declare var $: any;
declare interface DataTable {
	headerRow: string[];
	dataRows: string[][];
}
declare interface DataTableSoportes {
	headerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-listar-afiliaciones',
	templateUrl: './listar-afiliaciones.component.html',
	styleUrls: ['./listar-afiliaciones.component.css']
})
export class ListarAfiliacionesComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	consultaVaciaCargarSpinner: boolean = false;
	crearAfiliacionForm: FormGroup;
	actualizarAfiliacionForm: FormGroup;
	dataTable: DataTable;
	dataTableSoporte: DataTableSoportes;
	listadoAfiliacionesArray: any[] = [];
	tipoAfiliacionArray: any[] = [];
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	idContrato = null;
	idAfiliacion = null;
	textoExtendido: string = '';
	opcionVentana: any;
	soportesAfiliaciones: string = '';
	afiliacionSoporte = {
		id: null,
		entidad: null,
		fechaAfiliacion: null,
		fechaRetiro: null,
		tipoAfiliacion: null,
		observacion: null,
		idContrato: null,
		soportes: null
	};
	tipoAfiliacionParam: string = 'entidad_';
	idAfiliacionParam: string = '000';
	resetVar: boolean;
	afuConfigSoporte = {
		multiple: true,
		formatsAllowed: ".jpg,.png,.pdf,.jpeg,.zip",
		maxSize: "7",
		uploadAPI: {
			url: this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business')) + '/afiliacionesSoporte' + '?idAfiliacion=' + this.idAfiliacionParam + '&tipoAfiliacion=' + this.tipoAfiliacionParam,
			method: "POST",
			headers: {
				"Authorization": localStorage.getItem("token_crm")
			},
			params: {
				'page': '1'
			},
			responseType: 'blob',
		},
		/* theme: "attachPin", */
		hideProgressBar: false,
		hideResetBtn: true,
		hideSelectBtn: false,
		fileNameIndex: true,
		replaceTexts: {
			selectFileBtn: 'Seleccionar archivos',
			resetBtn: 'Reiniciar',
			uploadBtn: 'Subir archivo',
			dragNDropBox: 'Arrastrar y Soltar',
			attachPinBtn: 'Sube la imagen',
			afterUploadMsg_success: 'Archivo cargado con éxito !',
			afterUploadMsg_error: 'Subida fallida !',
			sizeLimit: 'Límite de tamaño'
		}
	};

	constructor(
		private _router: Router,
		private _datepipe: DatePipe,
		private _route: ActivatedRoute,
		private _formBuilder: FormBuilder,
		private _usuarioService: UsuarioService,
		private _controlIndividualService: ControlIndividualService,
		private _sweetAlertService: SweetAlertService,
		private _parametrizacionService: ParametrizacionService
	) {
		this.idContrato = this._route.snapshot.paramMap.get('idContrato');
		this.opcionVentana = this._route.snapshot.data.opc;
		if (localStorage.getItem('identity_crm')) {
			this.listarAfiliaciones(1);
			this.cargarInformacionSelect();
		}
	}

	ngOnInit() {
		this.dataTable = {
			headerRow: [
				'#',
				"Tipo de Afiliación",
				"Entidad",
				"Fecha de Afiliación",
				"Fecha de Retiro",
				"Observaciones",
				"Soportes",
				"Acciones"
			],
			dataRows: []
		};
		this.dataTableSoporte = {
			headerRow: [
				"#",
				"Soporte",
				"Acciones"
			],
			dataRows: []
		};
		this.crearAfiliacionForm = this._formBuilder.group({
			entidad: ['', Validators.required],
			fechaAfiliacion: ['', Validators.required],
			fechaRetiro: [''],
			tipoAfiliacion: ['', Validators.required],
			observacion: ['']
		});
		this.actualizarAfiliacionForm = this._formBuilder.group({
			entidad: ['', Validators.required],
			fechaAfiliacion: ['', Validators.required],
			fechaRetiro: [''],
			tipoAfiliacion: ['', Validators.required],
			observacion: ['']
		});
	}

	async cargarInformacionSelect() {
		this.loading = true;
		const tipoAfiliacion: any = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo de Afiliación');

		tipoAfiliacion['data'].forEach((element) => {
			this.tipoAfiliacionArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
		this.loading = false;
	}

	/* Inicia Crear nueva afiliación */
	async onSubmitCrearAfiliacion() {
		this.loading = true;
		let crearAfiliaciones = new Afiliaciones(
			this.crearAfiliacionForm.value.entidad,
			this.fechasValidacion(this.crearAfiliacionForm.value.fechaAfiliacion),
			this.fechasValidacion(this.crearAfiliacionForm.value.fechaRetiro),
			this.crearAfiliacionForm.value.tipoAfiliacion,
			this.crearAfiliacionForm.value.observacion,
			this.idContrato
		); //console.log(crearAfiliaciones);
		const responseCrearAfiliaciones = await this._controlIndividualService.registrarAfiliacion(crearAfiliaciones);
		//console.log(responseCrearAfiliaciones);
		if (responseCrearAfiliaciones["status"] === "success" && responseCrearAfiliaciones["code"] === "200") {
			this.limpiarFormulario();
			this._sweetAlertService.showNotification('top', 'right', 'success', 'check_circle', '<b>' + responseCrearAfiliaciones['message'] + '<b>', 2000);
			this.listarAfiliaciones(1);
			this.loading = false;
		}
		else if (responseCrearAfiliaciones["status"] === "success" && responseCrearAfiliaciones["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseCrearAfiliaciones['message'] + '<b>', 2000);
		}
		else if (responseCrearAfiliaciones["status"] === "error" && responseCrearAfiliaciones["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseCrearAfiliaciones['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseCrearAfiliaciones["message"], 'OK', 'error');
		}
	}
	/* Finaliza Crear nueva afiliación */

	/* Inicia Cargar listado de afiliaciones */
	async listarAfiliaciones(page: any) {
		this.loading = true;

		const responseListaAfiliaciones = await this._controlIndividualService.listarAfiliacionesPorContrato(this.idContrato, page);
		//console.log(responseListaAfiliaciones);
		if (responseListaAfiliaciones["status"] === "success" && responseListaAfiliaciones["code"] === "200") {
			this.llenarDatosTabla(page, responseListaAfiliaciones['data'], responseListaAfiliaciones['data'].afiliaciones);
			this.loading = false;
		}
		else if (responseListaAfiliaciones["status"] === "success" && responseListaAfiliaciones["code"] === "300") {
			this.loading = false;
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseListaAfiliaciones['message'] + '<b>', 2000);
		}
		else if (responseListaAfiliaciones["status"] === "error" && responseListaAfiliaciones["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseListaAfiliaciones['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			this._sweetAlertService.alertGeneral('¡Error!', responseListaAfiliaciones["message"], 'OK', 'error');
		}
	}

	llenarDatosTabla(page: any, listaAfiliacionesResponse: any, afiliaciones: any) {
		this.listadoAfiliacionesArray = afiliaciones;
		this.dataTable.dataRows = [];
		this.total_item_count = listaAfiliacionesResponse['total_item_count'];
		this.page_actual = page;

		this.total_pages = listaAfiliacionesResponse['total_pages'];

		if (page >= 2) { this.page_prev = page - 1; }
		else { this.page_prev = 1; }

		if (page < listaAfiliacionesResponse['total_pages']) { this.page_next = page + 1; }
		else {
			if (listaAfiliacionesResponse['total_pages'] == 0) { this.page_next = 1; }
			else { this.page_next = listaAfiliacionesResponse['total_pages']; }
		}

		this.dataTable.dataRows = this.listadoAfiliacionesArray;

		if (this.total_item_count || this.total_item_count == 0) {
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
		}
	}
	/* Finaliza Cargar listado de afiliaciones */

	/* Inicia Actualizar afiliación */
	actualizarCargo(rowAfiliacion: any) {
		let afiliacionRow: ActualizarAfiliaciones = rowAfiliacion;
		this.idAfiliacion = afiliacionRow['id'];

		this.actualizarAfiliacionForm.setValue({
			entidad: afiliacionRow.entidad,
			fechaAfiliacion: afiliacionRow.fechaAfiliacion,
			fechaRetiro: afiliacionRow.fechaRetiro,
			tipoAfiliacion: afiliacionRow.tipoAfiliacion.valor,
			observacion: afiliacionRow.observacion
		});

		$("#modalAfiliaciones").modal("show");
	}

	async onSubmitActualizarAfiliaciones() {
		this.loading = true;

		let actualizarAfiliacion = new Afiliaciones(
			this.actualizarAfiliacionForm.value.entidad,
			this.fechasValidacion(this.actualizarAfiliacionForm.value.fechaAfiliacion),
			this.fechasValidacion(this.actualizarAfiliacionForm.value.fechaRetiro),
			this.actualizarAfiliacionForm.value.tipoAfiliacion,
			this.actualizarAfiliacionForm.value.observacion,
			this.idContrato
		); //console.log(actualizarAfiliacion);
		const responseModificarAfiliaciones = await this._controlIndividualService.modificarAfiliacion(this.idAfiliacion, actualizarAfiliacion);
		//console.log(responseModificarAfiliaciones);
		if (responseModificarAfiliaciones["status"] === "success" && responseModificarAfiliaciones["code"] === "200") {
			this.actualizarAfiliacionForm.reset();
			this.listarAfiliaciones(1);
			$("#modalAfiliaciones").modal("hide");
			this._sweetAlertService.showNotification('top', 'right', 'success', 'warning', '<b>' + responseModificarAfiliaciones['message'] + '<b>', 2000);
			this.loading = false;
		}
		else if (responseModificarAfiliaciones["status"] === "success" && responseModificarAfiliaciones["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseModificarAfiliaciones['message'] + '<b>', 2000);
		}
		else if (responseModificarAfiliaciones["status"] === "error" && responseModificarAfiliaciones["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseModificarAfiliaciones['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseModificarAfiliaciones["message"], 'OK', 'error');
		}
	}
	/* Finaliza Actualizar afiliación */

	limpiarFormulario() {
		this.crearAfiliacionForm.reset();
	}

	listarFuncionesCargo(rowIdContrato: any) {
		this._router.navigate(["control-individual/listar-funciones/", rowIdContrato]);
	}

	/* Inicia Subida de soportes para afiliaciones */
	onSubmitSubirSoporte(rowAfiliaciones: any) {
		this.afiliacionSoporte.soportes = '';
		this.afiliacionSoporte.id = rowAfiliaciones.id;
		this.afiliacionSoporte.entidad = rowAfiliaciones.entidad;
		this.afiliacionSoporte.fechaAfiliacion = rowAfiliaciones.fechaAfiliacion;
		this.afiliacionSoporte.fechaRetiro = rowAfiliaciones.fechaRetiro;
		this.afiliacionSoporte.tipoAfiliacion = rowAfiliaciones.tipoAfiliacion.valor;
		this.afiliacionSoporte.observacion = rowAfiliaciones.observacion;
		this.afiliacionSoporte.idContrato = this.idContrato;
		this.afiliacionSoporte.soportes = rowAfiliaciones.soportes === '' ? '' : rowAfiliaciones.soportes;
		//console.log(this.afiliacionSoporte);
		this.cargarDatosTabla(rowAfiliaciones.soportes);

		$("#modalSubirSoportes").modal("show");
	}

	async subirArchivosAfiliaciones(dataResponse: any) {
		this.loading = true;

		const responseCargarAnexo = JSON.parse(dataResponse.response);
		//console.log(responseCargarAnexo);
		if (responseCargarAnexo["status"] === "success" && responseCargarAnexo["code"] === "200") {
			this.loading = false;
			if (this.afiliacionSoporte.soportes === '') {
				this.soportesAfiliaciones = this.afiliacionSoporte.soportes + responseCargarAnexo["data"];
			} else {
				this.soportesAfiliaciones = this.afiliacionSoporte.soportes + ',' + responseCargarAnexo["data"];
			}
			this.onSubmitActualizarAfiliacionesSoporte();
			this._sweetAlertService.showNotification('top', 'right', 'success', 'success', '<b>' + responseCargarAnexo['message'] + '<b>', 2000);
		}
		else if (responseCargarAnexo["status"] === "success" && responseCargarAnexo["code"] === "300") {
			this.loading = false;
			this.soportesAfiliaciones = '';
			this._sweetAlertService.alertGeneral('Advertencia!', responseCargarAnexo["message"], 'OK', 'warning');
		}
		else if (responseCargarAnexo["status"] === "error" && responseCargarAnexo["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseCargarAnexo['message']);
		}
		else {
			this.loading = false;
			this.soportesAfiliaciones = '';
			this._sweetAlertService.alertGeneral('¡Error!', responseCargarAnexo["message"], 'OK', 'error');
		}
	}

	async onSubmitActualizarAfiliacionesSoporte() {
		this.loading = true;

		let actualizarAfiliacionSoporte = new AfiliacionesSoportes(
			this.afiliacionSoporte.id,
			this.soportesAfiliaciones
		); //console.log(actualizarAfiliacionSoporte);
		const responseModificarAfiliacionesSoportes = await this._controlIndividualService.modificarAfiliacionSoporte(actualizarAfiliacionSoporte);
		//console.log(responseModificarAfiliacionesSoportes);
		if (responseModificarAfiliacionesSoportes["status"] === "success" && responseModificarAfiliacionesSoportes["code"] === "200") {
			this.listarAfiliaciones(1);
			$("#modalSubirSoportes").modal("hide");
			this._sweetAlertService.showNotification('top', 'right', 'success', 'warning', '<b>' + responseModificarAfiliacionesSoportes['message'] + '<b>', 2000);
			this.loading = false;
		}
		else if (responseModificarAfiliacionesSoportes["status"] === "success" && responseModificarAfiliacionesSoportes["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseModificarAfiliacionesSoportes['message'] + '<b>', 2000);
		}
		else if (responseModificarAfiliacionesSoportes["status"] === "error" && responseModificarAfiliacionesSoportes["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseModificarAfiliacionesSoportes['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseModificarAfiliacionesSoportes["message"], 'OK', 'error');
		}
	}
	/* Finaliza Subida de soportes para afiliaciones */

	/* Inicia Cargar archivos en tabla para visualizar */
	cargarDatosTabla(datosSoportes: string) {
		//console.log(datosSoportes);
		this.dataTableSoporte.dataRows = [];

		if (datosSoportes != '') {
			let arraySoportes = datosSoportes.split(",");
			let JSONSoporte: any, indice: number = 1;

			arraySoportes.forEach(element => {
				JSONSoporte = {
					soportesValor: element,
					soportesNombre: this.obtenerNombreArchivoSoporte(element),
					posicion: indice
				};
				this.dataTableSoporte.dataRows.push(JSONSoporte);
				indice++;
			});
			//console.log(this.dataTableSoporte.dataRows);
		} else {
			this.dataTableSoporte.dataRows = [];
		}
	}
	/* Finaliza Cargar archivos en tabla para visualizar */

	/* Inicia Eliminar un soporte de la afiliacion */
	obtenerNombreArchivoSoporte(soporteNombre: string): string {
		let separarArray: string[] = soporteNombre.split('/');
		return separarArray[separarArray.length - 1];
	}

	onSubmitElimarSoporteAfiliacionTabla(soporteRowDelete: any) {
		swal({
			title: 'Eliminar Soporte',
			text: "¿Esta seguro de que desea eliminar el soporte " + soporteRowDelete.soportesNombre + " de esta afiliación?",
			type: 'question',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: 'Si, Eliminar!',
			cancelButtonText: 'Cancelar',
			buttonsStyling: true,
			reverseButtons: true
		}).then((result) => {
			if (result.value) {
				this.eliminarSoporteAfiliacionTabla(soporteRowDelete);
			}
		});
	}

	async eliminarSoporteAfiliacionTabla(soporteRow: any) {
		this.loading = true;
		let posicionSoporte: string = '' + soporteRow.posicion;
		const responseEliminarSoportesAfiliaciones = await this._controlIndividualService.eliminarAfiliacionSoporte('' + this.afiliacionSoporte.id, posicionSoporte);
		//console.log(responseEliminarSoportesAfiliaciones);
		if (responseEliminarSoportesAfiliaciones["status"] === "success" && responseEliminarSoportesAfiliaciones["code"] === "200") {
			this.listarAfiliaciones(1);
			this.soportesAfiliaciones = '';
			this.afiliacionSoporte.soportes = responseEliminarSoportesAfiliaciones["data"];
			this.cargarDatosTabla(responseEliminarSoportesAfiliaciones["data"]);
			this._sweetAlertService.showNotification('top', 'right', 'success', 'warning', '<b>' + responseEliminarSoportesAfiliaciones['message'] + '<b>', 2000);
			this.loading = false;
		}
		else if (responseEliminarSoportesAfiliaciones["status"] === "success" && responseEliminarSoportesAfiliaciones["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseEliminarSoportesAfiliaciones['message'] + '<b>', 2000);
		}
		else if (responseEliminarSoportesAfiliaciones["status"] === "error" && responseEliminarSoportesAfiliaciones["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseEliminarSoportesAfiliaciones['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseEliminarSoportesAfiliaciones["message"], 'OK', 'error');
		}
	}
	/* Finaliza Eliminar un soporte de la afiliacion */

	/* Inicio Descargar Archivo Soporte Afiliacion */
	descargarSoporteAfiliacion(rowSoporte: any) {
		let ruta: string = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		let url = ruta + '/uploads' + rowSoporte.soportesValor;
		let name = 'soporte_';
		let link = document.createElement("a");
		//link.download = name;
		link.target = '_blank';
		link.href = url;
		//console.log(link);
		document.body.appendChild(link);
		link.click();
		document.body.removeChild(link);
	}
	/* Fin Descargar Archivo Soporte Afiliacion */

	observarTexto(texto: any) {
		this.textoExtendido = texto == '' ? '- - -' : texto;
		$("#modalObservacion").modal("show");
	}

	fechasValidacion(formatoFecha) {
		if (formatoFecha) {
			//Formatear fecha
			const fechaActualTem = this._datepipe
				.transform(formatoFecha, "yyyy/MM/dd")
				.replace(/\//g, "-");
			//console.log("fecha formato: "+fechaActualTem);
			return fechaActualTem;
		}
		return '';
	}

	regresar() {
		if (this.opcionVentana == '1') {
			this._router.navigate(["control-individual/ver-contrato-empleado/", this.idContrato]);
		}
		else if (this.opcionVentana == '2') {
			this._router.navigate(["control-individual/registrar-contratos"]);
		}
		else {
			let idEmpleado = localStorage.getItem('empleadoID');
			localStorage.removeItem('empleadoID');
			this._router.navigate(["empleados/ficha-empleados/", idEmpleado]);
		}
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}