import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { SiembraCosechaService } from '../../services/siembra-cosecha/siembra-cosecha.service';
import { ClienteService } from '../../services/cliente/cliente.service';
import swal from 'sweetalert2';

declare var $: any;
declare interface DataTable {
	headerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-clientes-semilla',
	templateUrl: './clientes-semilla.component.html',
	styleUrls: ['./clientes-semilla.component.css']
})
export class ClientesSemillaComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	consultaVaciaCargarSpinner: boolean = false;
	buscarDocumentoForm: FormGroup;
	dataTable: DataTable;
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	clientesArray: any[] = [];

	constructor(
		private _router: Router,
		private _formBuilder: FormBuilder,
		private _clienteService: ClienteService,
		private _sweetAlertService: SweetAlertService,
		private _siembraCosechaService: SiembraCosechaService
	) {
		/* if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);
		} */
	}

	ngOnInit(): void {
		this.dataTable = {
			headerRow: [
				"#",
				"Documento",
				"Nombres Completos",
				"Vehículo",
				"Versión",
				"Modelo",
				"Color Vehículo",
				"Precio Vehículo",
				"Días Transcurridos",
				"Acciones"
			],
			dataRows: []
		};

		this.buscarDocumentoForm = this._formBuilder.group({
			documento: ['', [Validators.required, Validators.pattern("[0-9]{4,13}$")]]
		});

		if (localStorage.getItem('identity_crm')) {
			this.listarClientesSC('1');
		}
	}

	async listarClientesSC(page: string) {
		this.loading = true;
		const responseClientesSC = await this._siembraCosechaService.listarClientesSiembraCosecha(page);
		//console.log(responseClientesSC);
		if (responseClientesSC["status"] === "success" && responseClientesSC["code"] === "200") {
			this.loading = false;
			this.llenarDatosTabla(+page, responseClientesSC['data']);
		}
		else if (responseClientesSC["status"] === "success" && responseClientesSC["code"] === "300") {
			this.loading = false;
			this.clientesArray = [];
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseClientesSC['message'] + '<b>', 2000);
		}
		else if (responseClientesSC["status"] === "error" && responseClientesSC["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseClientesSC['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			this._sweetAlertService.showNotification('top', 'right', 'error', 'warning', '<b>' + responseClientesSC['message'] + '<b>', 2000);
		}
	}

	llenarDatosTabla(page: number, responseClientes: any) {
		this.clientesArray = responseClientes['clientesIngresos'];
		//console.log(responseClientes['clientesIngresos']);
		this.dataTable.dataRows = [];
		this.total_item_count = responseClientes['total_item_count'];
		this.page_actual = page;
		this.total_pages = responseClientes['total_pages'];

		if (page >= 2) this.page_prev = page - 1;
		else this.page_prev = 1;

		if (page < responseClientes['total_pages']) this.page_next = page + 1;
		else {
			if (responseClientes['total_pages'] == 0) this.page_next = 1;
			else this.page_next = responseClientes['total_pages'];
		}

		let JSONParam: any;

		this.clientesArray.forEach(element => {
			let dias: string = element.diasTranscurridos == 0 ? '0 días' : element.diasTranscurridos + ' días';
			let diasNumero: number = element.diasTranscurridos == 0 ? 0 : element.diasTranscurridos;
			JSONParam = {
				id: element.cliente.id,
				tipoDoc: element.cliente.tipoDocumento.valor,
				documento: element.cliente.documento,
				nombres: element.cliente.nombres + ' ' + element.cliente.apellidos,
				vehiculo: element.cotizacion == 0 ? '- - -' : element.cotizacion.vehiculo.nombre,
				modelo: element.cotizacion == 0 ? '- - -' : element.cotizacion.modelo,
				versionId: element.cotizacion == 0 ? '- - -' : element.cotizacion.versionId.nombre,
				colorVehiculo: element.cotizacion == 0 ? '- - -' : element.cotizacion.colorVehiculo.nombre,
				valorVehiculo: element.cotizacion == 0 ? '- - -' : element.cotizacion.valorVehiculo,
				diasTranscurridos: dias,
				colorEstado: this.darColorEstadoEvento(+diasNumero)
			};
			this.dataTable.dataRows.push(JSONParam);
		});
		//console.log(this.usuarioArray);
		if (this.total_item_count || this.total_item_count == 0) {
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
		}
	}

	nuevoCliente() {
		$("#modalBuscarCliente").modal("show");
	}

	async buscarDocumento() {
		this.loading = true;
		let documento: number = +this.buscarDocumentoForm.value.documento;

		const responseCliente = await this._clienteService.buscarClientePorDocumento(documento);
		//console.log(responseCliente);
		if (responseCliente["status"] === "success" && responseCliente["code"] === "200") {
			this.loading = false;
			if (responseCliente["data"].existe) {
				this.buscarDocumentoForm.reset();
				$("#modalBuscarCliente").modal("hide");
				let idCliente = responseCliente["data"].cliente.id;
				this.verClienteFicha('' + idCliente);
			} else {
				this.buscarDocumentoForm.reset();
				$("#modalBuscarCliente").modal("hide");
				this.redireccionarRegistrarCliente(responseCliente["message"]);
			}
		}
		else if (responseCliente["status"] === "success" && responseCliente["code"] === "300") {
			this.loading = false;
			swal('¡Error!', responseCliente["message"], 'warning');
		}
		else if (responseCliente["status"] === "error" && responseCliente["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseCliente['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseCliente["message"], 'error');
		}
	}

	redireccionarRegistrarCliente(mensaje: string) {
		swal({
			title: 'Lo sentimos!',
			text: 'El cliente no se cuentra registrado',
			type: 'question',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-warning',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: 'Registrar Cliente',
			cancelButtonText: 'Nueva búsqueda',
			buttonsStyling: true,
			reverseButtons: true
		}).then((result) => {
			if (result.value) {
				this._router.navigate(["siembra-y-cosecha/registrar-cliente"]);
			} else {
				this.nuevoCliente();
			}
		});
	}

	darColorEstadoEvento(estado: number): string {
		switch (true) {
			case (estado == 0):
				return 'default';
				break;
			case (estado >= 1 && estado < 6):
				return 'info';
				break;
			case (estado >= 6 && estado < 16):
				return 'warning';
				break;
			case (estado >= 16 && estado < 21):
				return 'success';
				break;
			case (estado >= 21):
				return 'danger';
				break;
			default:
				return 'default';
				break;
		}
	}

	verClienteFicha(idCliente: string) {
		localStorage.setItem('rutaSC', 'siembra-y-cosecha/clientes-semilla');
		this._router.navigate(["siembra-y-cosecha/ficha-cliente/", idCliente]);
	}

	regresar() {
		this._router.navigate(["siembra-y-cosecha/clientes"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}
