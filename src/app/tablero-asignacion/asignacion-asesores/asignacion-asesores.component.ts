import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import swal from "sweetalert2";
import { TableroAsignacion, TableroAsignacionEsperar } from "../../models/tableroAsignacion";
import { TableroAsignacionService } from '../../services/tablero-asignacion/tablero-asignacion.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare const $: any;

@Component({
	selector: 'app-asignacion-asesores',
	templateUrl: './asignacion-asesores.component.html',
	styleUrls: ['./asignacion-asesores.component.css']
})
export class AsignacionAsesoresComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	todosAsesoresArray: any[] = [];
	idIngreso: any;
	idCliente: any;
	opcionVentana: any;
	usuarioIdentity: any;
	banderaRolFormulario = 0;
	observacionAsignacion: any;
	botonOcuparArray: string[] = ['A', 'N'];
	botonOcupar: boolean = false;

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _tableroAsignacionService: TableroAsignacionService,
		private _parametrizacionService: ParametrizacionService,
		private _sweetAlertService: SweetAlertService
	) {
		this.idIngreso = this._route.snapshot.paramMap.get('idIngreso');
		this.idCliente = this._route.snapshot.paramMap.get('idCliente');
		this.opcionVentana = this._route.snapshot.paramMap.get('opcionVentana');
		localStorage.setItem('idIngreso', this.idIngreso);
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			this.usuarioIdentity = identityData['data'];
			/* Validacion para que todos los roles excepto el asesor(S) puedan reasignar clientes en espera  */
			if (this.usuarioIdentity['rol'].valor != 'S') {
				this.banderaRolFormulario = 1;
				this.cargarDisponibilidadAsesores();
				if (this.botonOcuparArray.includes(this.usuarioIdentity['rol'].valor)) {
					this.botonOcupar = true;
				}
			}
			else {
				this.asignarAsesorAlCliente();
				this.banderaRolFormulario = 2;
			}
		}
	}

	ngOnInit() { }

	/* Inicio Metodos para el rol asesor */
	async asignarAsesorAlCliente() {
		this.loading = true;
		const disponibilidaAsesor = await this._tableroAsignacionService.consultarDisponibilidadAsesor(this.usuarioIdentity.sub);
		//console.log(disponibilidaAsesor);
		if (disponibilidaAsesor["status"] === "success" && disponibilidaAsesor["code"] === "200") {
			this.loading = false;
			swal({
				title: 'Atención a Clientes',
				text: '¿Desea atender al cliente ahora?',
				type: 'question',
				showCancelButton: true,
				confirmButtonClass: 'btn btn-warning',
				cancelButtonClass: 'btn btn-danger',
				confirmButtonText: 'Sí atender',
				cancelButtonText: 'No atender',
				buttonsStyling: true,
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					swal({
						title: 'Escribir una observación para la asignación',
						html: '<div class="form-group">' +
							'<input id="input-field" type="text" class="form-control" />' +
							'</div>',
						showCancelButton: true,
						confirmButtonClass: 'btn btn-success',
						cancelButtonClass: 'btn btn-danger',
						buttonsStyling: false
					})
						.then((result) => {
							this.observacionAsignacion = $('#input-field').val();
							this.onSubmitAsignarAsesorCliente('');
						}).catch(swal.noop);
				} else {
					this.onSubmitReasignarAsesor(this.usuarioIdentity.sub);
				}
			}).catch(swal.noop);
		}
		else if (disponibilidaAsesor["status"] === "success" && disponibilidaAsesor["code"] === "300") {
			this.loading = false;
			swal('¡Advertencia!', disponibilidaAsesor['message'], 'warning')
				.then((result) => {
					this.regresar();
				});
		}
		else if (disponibilidaAsesor["status"] === "error" && disponibilidaAsesor["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(disponibilidaAsesor['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', disponibilidaAsesor['message'], 'error')
				.then((result) => {
					this.regresar();
				});
		}
	}

	async onSubmitAsignarAsesorCliente(idAsesor) {

		let tableroAsignacion;
		if (this.banderaRolFormulario == 1) {
			swal({
				title: 'Escribir una observación para la asignación',
				html: '<div class="form-group">' +
					'<input id="input-field" type="text" class="form-control" />' +
					'</div>',
				showCancelButton: true,
				confirmButtonClass: 'btn btn-success',
				cancelButtonClass: 'btn btn-danger',
				buttonsStyling: false
			}).then((result) => {
				this.observacionAsignacion = $('#input-field').val();;
				tableroAsignacion = new TableroAsignacion(
					idAsesor,
					this.idIngreso,
					this.observacionAsignacion
				); //console.log(tableroAsignacion);
				if (this.botonOcuparArray.includes(this.usuarioIdentity['rol'].valor)) {
					this.banderaRolFormulario = 2;
				}
				this.asignarAsesorCliente(tableroAsignacion);
			}).catch(swal.noop);
		} else {
			tableroAsignacion = new TableroAsignacion(
				this.usuarioIdentity.sub,
				this.idIngreso,
				this.observacionAsignacion
			); //console.log(tableroAsignacion);
			this.asignarAsesorCliente(tableroAsignacion);
		}
	}

	async asignarAsesorCliente(asignacion: TableroAsignacion) {
		this.loading = true;
		let responseAsignacionAsesor: any;

		if (this.banderaRolFormulario === 1) {
			responseAsignacionAsesor = await this._tableroAsignacionService.registrarAsignacionOcupar(3, asignacion);
		} else {
			responseAsignacionAsesor = await this._tableroAsignacionService.registrarAsignacionOcupar(1, asignacion);
		}

		//console.log(responseAsignacionAsesor);
		if (responseAsignacionAsesor["status"] === "success" && responseAsignacionAsesor["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseAsignacionAsesor["message"], 'success')
				.then(result => {
					if (this.opcionVentana == 1) {
						//this.regresarMisClientes();
						this._router.navigate(["clientes/ficha-clientes/", this.idIngreso]);
					} else this.regresar();
				}).catch(swal.noop);
		}
		else if (responseAsignacionAsesor["status"] === "success" && responseAsignacionAsesor["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseAsignacionAsesor["message"] + ', el cliente se agregará a la lista de espera', 'warning')
				.then(result => {
					this.onSubmitReasignarAsesor(this.usuarioIdentity.sub);
				}).catch(swal.noop);
		}
		else if (responseAsignacionAsesor["status"] === "error" && responseAsignacionAsesor["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseAsignacionAsesor['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseAsignacionAsesor["message"], 'error')
				.then(result => {
					if (this.opcionVentana == 1) {
						this.regresarMisClientes();
					} else this.regresar();
				}).catch(swal.noop);
		}
	}

	async onSubmitReasignarAsesor(idAsesor) {
		this.loading = true;

		const responseReasignacionAsesor = await this._tableroAsignacionService.reasignarAsesor(1, '' + idAsesor, '' + this.idIngreso);
		//console.log(responseReasignacionAsesor);
		if (responseReasignacionAsesor["status"] === "success" && responseReasignacionAsesor["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseReasignacionAsesor["message"], 'success')
				.then(result => {
					this.regresar();
				}).catch(swal.noop);
		}
		else if (responseReasignacionAsesor["status"] === "success" && responseReasignacionAsesor["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseReasignacionAsesor["message"], 'warning')
				.then(result => {
					this.regresar();
				}).catch(swal.noop);
		}
		else if (responseReasignacionAsesor["status"] === "error" && responseReasignacionAsesor["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseReasignacionAsesor['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseReasignacionAsesor["message"], 'error')
				.then(result => {
					this.regresar();
				}).catch(swal.noop);
		}
	}
	/* Inicio Metodos para el rol asesor */

	/* Inicio cargar disponibilidad de todos los asesores disponibles o no
	 * para agregarlos en cards */
	async cargarDisponibilidadAsesores() {
		this.loading = true;
		const responseDisponibilidadAsesores = await this._tableroAsignacionService.disponibilidadAsesores();
		//console.log(responseDisponibilidadAsesores);
		if (responseDisponibilidadAsesores["status"] === "success" && responseDisponibilidadAsesores["code"] === "200") {
			this.loading = false;
			this.cargarInformacionAsesoresCard(responseDisponibilidadAsesores['data']);
		}
		else if (responseDisponibilidadAsesores["status"] === "success" && responseDisponibilidadAsesores["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseDisponibilidadAsesores["message"], 'OK', 'warning');
		}
		else if (responseDisponibilidadAsesores["status"] === "error" && responseDisponibilidadAsesores["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseDisponibilidadAsesores['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseDisponibilidadAsesores["message"], 'OK', 'error');
		}
	}

	cargarInformacionAsesoresCard(asesores) {
		let asesoresDisponiblesArray: any[] = [];
		let asesoresOcupadosArray: any[] = [];

		this.todosAsesoresArray = [];

		if (asesores.asesoresDisponibles.length > 0) {
			asesores.asesoresDisponibles.forEach((element) => {
				asesoresDisponiblesArray.push({
					id: element.id,
					nombre: element.nombre,
					disponible: 1,
					icono: 'verde'
				});
			});
			//console.log(asesoresDisponiblesArray);
		}
		if (asesores.asesoresOcupados.length > 0) {
			asesores.asesoresOcupados.forEach((element) => {
				asesoresOcupadosArray.push({
					id: element.id,
					nombre: element.nombre,
					disponible: 2,
					icono: 'rojo'
				});
			});
			//console.log(asesoresOcupadosArray);
		}

		this.todosAsesoresArray = asesoresDisponiblesArray.concat(asesoresOcupadosArray);
	}
	/* Fin cargar disponibilidad de todos los asesores disponibles o no
	 * para agregarlos en cards */

	regresar() {
		this._router.navigate(["clientes/ficha-clientes/", this.idIngreso]);
	}

	regresarValidacion() {
		swal({
			title: 'Asignar Asesor',
			text: 'El cliente registrado quedará sin asesor',
			type: 'warning',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-warning',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: 'Continuar',
			cancelButtonText: 'Cancelar',
			buttonsStyling: true,
			reverseButtons: true
		}).then((result) => {
			if (result.value) {
				this._router.navigate(["clientes/ficha-clientes/", this.idIngreso]);
			}
		}).catch(swal.noop);
	}

	regresarMisClientes() {
		this._router.navigate(["clientes/listar-clientes"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}