import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SeguimientoNegocioRoutes } from './seguimiento-negocio.routing';
import { LoadingModule } from '../shared/loading/loading.module';
import { LoadingSpinnerModule } from '../shared/loading-spinner/loading-spinner.module';
import { ListarEstudiosCreditoComponent } from './listar-estudios-credito/listar-estudios-credito.component';
import { ListarProcesosVentaComponent } from './listar-procesos-venta/listar-procesos-venta.component';
import { ListarReconsideradosComponent } from './listar-reconsiderados/listar-reconsiderados.component';

@NgModule({
	declarations: [
		ListarEstudiosCreditoComponent,
		ListarProcesosVentaComponent,
		ListarReconsideradosComponent
	],
	imports: [
		CommonModule,
		RouterModule.forChild(SeguimientoNegocioRoutes),
		FormsModule,
		MaterialModule,
		ReactiveFormsModule,
		LoadingModule,
		LoadingSpinnerModule
	],
	providers: [DatePipe]
})
export class SeguimientoNegocioModule { }