import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientesCosechadosComponent } from './clientes-cosechados.component';

describe('ClientesCosechadosComponent', () => {
  let component: ClientesCosechadosComponent;
  let fixture: ComponentFixture<ClientesCosechadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientesCosechadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientesCosechadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
