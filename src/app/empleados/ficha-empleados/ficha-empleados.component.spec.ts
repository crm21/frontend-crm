import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FichaEmpleadosComponent } from './ficha-empleados.component';

describe('FichaEmpleadosComponent', () => {
  let component: FichaEmpleadosComponent;
  let fixture: ComponentFixture<FichaEmpleadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FichaEmpleadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FichaEmpleadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
