import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientesSemillaComponent } from './clientes-semilla.component';

describe('ClientesSemillaComponent', () => {
  let component: ClientesSemillaComponent;
  let fixture: ComponentFixture<ClientesSemillaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientesSemillaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientesSemillaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
