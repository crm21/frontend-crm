import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ClientesRoutes } from './clientes.routing';
import { CrearClientesComponent } from './crear-clientes/crear-clientes.component';
import { LoadingModule } from '../shared/loading/loading.module';
import { LoadingSpinnerModule } from '../shared/loading-spinner/loading-spinner.module';
import { BuscarClientesComponent } from './buscar-clientes/buscar-clientes.component';
import { FichaClientesComponent } from './ficha-clientes/ficha-clientes.component';
import { SolicitarCotizacionComponent } from './solicitar-cotizacion/solicitar-cotizacion.component';
import { ListarCotizacionComponent } from './listar-cotizacion/listar-cotizacion.component';
import { CrearEstudioCreditoComponent } from './crear-estudio-credito/crear-estudio-credito.component';
import { ListarEstudioCreditoComponent } from './listar-estudio-credito/listar-estudio-credito.component';
import { CrearReconsideradoComponent } from './crear-reconsiderado/crear-reconsiderado.component';
import { ListarReconsideradoComponent } from './listar-reconsiderado/listar-reconsiderado.component';
import { ListarProcesoVentasComponent } from './listar-proceso-ventas/listar-proceso-ventas.component';
import { MisClientesComponent } from './mis-clientes/mis-clientes.component';
import { MiPerfilComponent } from './mi-perfil/mi-perfil.component';
import { ClientesContactCenterComponent } from './clientes-contact-center/clientes-contact-center.component';
import { IngresosClientesComponent } from './ingresos-clientes/ingresos-clientes.component';
import { ContestacionComponent } from './contestacion/contestacion.component';

@NgModule({
	declarations: [
		CrearClientesComponent,
		BuscarClientesComponent,
		FichaClientesComponent,
		SolicitarCotizacionComponent,
		ListarCotizacionComponent,
		CrearEstudioCreditoComponent,
		ListarEstudioCreditoComponent,
		CrearReconsideradoComponent,
		ListarReconsideradoComponent,
		ListarProcesoVentasComponent,
		MisClientesComponent,
		MiPerfilComponent,
		ClientesContactCenterComponent,
		IngresosClientesComponent,
		ContestacionComponent
	],
	imports: [
		CommonModule,
		RouterModule.forChild(ClientesRoutes),
		FormsModule,
		MaterialModule,
		ReactiveFormsModule,
		LoadingModule,
		LoadingSpinnerModule
	],
	providers: [DatePipe]
})
export class ClientesModule { }
