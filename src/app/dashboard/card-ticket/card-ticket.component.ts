import { Component, OnInit } from '@angular/core';
import { TicketsService } from '../../services/tickets/tickets.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-card-ticket',
	templateUrl: './card-ticket.component.html',
	styleUrls: ['./card-ticket.component.css']
})
export class CardTicketComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	nombreRolValor: string = '';
	verTicketsInOut: boolean = false;
	ticketsAbiertosNum: any = '----';
	ticketsCerradosNum: any = '----';

	constructor(
		private _router: Router,
		private _ticketsService: TicketsService,
		private _sweetAlertService: SweetAlertService
	) {
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			this.nombreRolValor = identityData['data'].rol.valor;
			this.mostrarTicketsRoles();
		}
	}

	ngOnInit(): void { }

	mostrarTicketsRoles() {
		switch (this.nombreRolValor) {
			case 'A':
				this.verTicketsInOut = true;
				this.ticketsAbiertosCerradosReporte();
				break;
			case 'GG':
				this.verTicketsInOut = true;
				this.ticketsAbiertosCerradosReporte();
				break;
			case 'GA':
				this.verTicketsInOut = true;
				this.ticketsAbiertosCerradosReporte();
				break;
			case 'LBDC':
				this.verTicketsInOut = true;
				this.ticketsAbiertosCerradosReporte();
				break;
			case 'AD':
				this.verTicketsInOut = true;
				this.ticketsAbiertosCerradosReporte();
				break;
			case 'N':
				this.verTicketsInOut = true;
				this.ticketsAbiertosCerradosReporte();
				break;
			default:
				this.verTicketsInOut = false;
				break;
		}
	}

	async ticketsAbiertosCerradosReporte() {
		this.loading = true;
		const responseTicketsOpenClose = await this._ticketsService.ticketsAbiertosCerrados();
		//console.log(responseTicketsOpenClose);
		if (responseTicketsOpenClose["status"] === "success" && responseTicketsOpenClose["code"] === "200") {
			this.ticketsAbiertosNum = +responseTicketsOpenClose['data'].abiertos;
			this.ticketsCerradosNum = +responseTicketsOpenClose['data'].cerrados;
			this.loading = false;
		}
		else if (responseTicketsOpenClose["status"] === "success" && responseTicketsOpenClose["code"] === "300") {
			this.ticketsAbiertosNum = '0';
			this.ticketsCerradosNum = '0';
			this.loading = false;
		}
		else if (responseTicketsOpenClose["status"] === "error" && responseTicketsOpenClose["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseTicketsOpenClose['message']);
		}
		else {
			this.loading = false;
		}
	}

	onSubmitVerTicketsReporte() {
		this._router.navigate(["tickets/listar"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}

}
