export class ProcesoVentas {
    constructor(
		public fechaFacturacion?: string,
		public fechaEnvioMatricula?: string,
		public fechaRecepcionMatricula?: string,
		public fechaSoat?: string,
		public fechaActivacionChevy?: string,
		public fechaEntrega?: string,
		public precioFinalVenta?: string,
		public observacion?: string,
		public fechaFirmaContrato?: string,
		public fechaConsignacion?: string,
		public placa?: string,
		public vin?: string,
		public kilometraje?: string
    ) { }
}
