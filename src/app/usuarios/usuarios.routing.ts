import { Routes } from '@angular/router';

import { CrearUsuariosComponent } from './crear-usuarios/crear-usuarios.component';
import { ListarUsuariosComponent } from './listar-usuarios/listar-usuarios.component';
import { ActualizarUsuarioComponent } from './actualizar-usuario/actualizar-usuario.component';

export const UsuariosRoutes: Routes = [

	{
		path: '',
		children: [ {
			path: 'registrar',
			component: CrearUsuariosComponent
		},{
			path: 'listar',
			component: ListarUsuariosComponent
		},{
			path: 'actualizar-informacion/:idCliente/:opc',
			component: ActualizarUsuarioComponent
		}]
	}
];