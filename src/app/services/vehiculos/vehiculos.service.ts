import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Vehiculo } from '../../models/vehiculo';
import { UsuarioService } from '../usuario/usuario.service';

@Injectable({
	providedIn: 'root'
})
export class VehiculosService {

	headers: any;
	url: string = '';
	token: string;

	constructor(
		private http: HttpClient,
		private _usuarioService: UsuarioService
	) {
		console.log('[Vehiculos services is ready]...');
	}

	async listarVehiculosCliente(opc: any, idCliente: string, pagination: string) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			let params = "?idCliente=" + idCliente + "&pagination" + pagination;
			this.http.get(this.url + "/vehiculo/" + opc + params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async actualizarVehiculoCliente(idVehiculo: string, vehiculo: Vehiculo) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			const json = JSON.stringify(vehiculo);
			let params = "json=" + json + "&idVehiculo=" + idVehiculo;
			this.http.put(this.url + "/vehiculo", params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async registrarVehiculoCliente(vehiculo: Vehiculo) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			const json = JSON.stringify(vehiculo);
			let params = "json=" + json;
			this.http.post(this.url + "/vehiculo", params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}
}