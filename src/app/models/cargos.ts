export class Cargos {
    constructor(
        public nombre: string,
        public area: string,
        public dependencia: string,
        public periodoPago: string,
        public numCargos: string,
        public ubicacionOrganigrama: string,
        public mision: string
    ) { }
};