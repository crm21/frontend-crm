import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteVehiculosVendidosComponent } from './reporte-vehiculos-vendidos.component';

describe('ReporteVehiculosVendidosComponent', () => {
  let component: ReporteVehiculosVendidosComponent;
  let fixture: ComponentFixture<ReporteVehiculosVendidosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteVehiculosVendidosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteVehiculosVendidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
