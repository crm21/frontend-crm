import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteClientesConCotizacionesComponent } from './reporte-clientes-con-cotizaciones.component';

describe('ReporteClientesConCotizacionesComponent', () => {
  let component: ReporteClientesConCotizacionesComponent;
  let fixture: ComponentFixture<ReporteClientesConCotizacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteClientesConCotizacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteClientesConCotizacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
