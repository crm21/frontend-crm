export class TableroAsignacion {
    constructor(
        public asesor: string,
        public ingreso: string,
        public observacion?: string
    ) { }
}

export class TableroAsignacionEsperar {
    constructor(
        public asesor: string
    ) { }
}