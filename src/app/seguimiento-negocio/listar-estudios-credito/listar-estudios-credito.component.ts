import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { EstadisticasService } from '../../services/estadisticas/estadisticas.service';
import { TableroAsignacionService } from '../../services/tablero-asignacion/tablero-asignacion.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { EstudioCreditoService } from '../../services/estudio-credito/estudio-credito.service';
import { DatePipe } from "@angular/common";

declare var $: any;
declare interface TableData {
	headerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-listar-estudios-credito',
	templateUrl: './listar-estudios-credito.component.html',
	styleUrls: ['./listar-estudios-credito.component.css']
})
export class ListarEstudiosCreditoComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	identityUsuario: any;
	idUsuario: any;
	dataTable: TableData;
	buscarFiltrosForm: FormGroup;
	buscarUsuarioFormGroup: FormGroup;
	reporteEstadisticaArray: any[] = [];
	todosAsesoresArray: any[] = [];
	financieraArray: any[] = [];
	contestacionArray: any[] = [];
	total_item_count: number = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	consultaVaciaCargarSpinner: boolean = false;
	downloadMostrar: boolean = false;
	ocultarCampoAsesor: boolean = true;
	fechaIni: string = '';
	fechaFin: string = '';
	busquedaFiltros: any = {
		asesor: '',
		financiera: '',
		contestacion: ''
	};
	nombreArchivo: string = '';
	documentoCliente: string = '';
	cambiarPaginador: string = 'T';
	edicionEstudioCredito: boolean = true;
	contestacionForm: string = '';

	constructor(
		private _router: Router,
		private _datepipe: DatePipe,
		private _formBuilder: FormBuilder,
		private _sweetAlertService: SweetAlertService,
		private _estadisticasService: EstadisticasService,
		private _estudioCreditoService: EstudioCreditoService,
		private _tableroAsignacionService: TableroAsignacionService,
		private _parametrizacionService: ParametrizacionService
	) {
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			this.identityUsuario = identityData['data'];
			this.idUsuario = this.identityUsuario.sub;
			if (this.identityUsuario.rol.valor == 'S') {
				this.ocultarCampoAsesor = false;
			}
		}
		this.datosTabla();
	}

	ngOnInit() {
		this.buscarFiltrosForm = this._formBuilder.group({
			fechaInicial: ['', Validators.required],
			fechaFinal: ['', Validators.required]
		});
		this.buscarUsuarioFormGroup = this._formBuilder.group({
			filtroUsuario: ['', Validators.required]
		});
		if (localStorage.getItem('identity_crm')) {
			this.cargarDisponibilidadAsesores();
			this.listarEstadisticas(1);
		}
	}

	datosTabla() {
		this.dataTable = {
			headerRow: [
				"#",
				"Documento Cliente",
				"Nombres Completos",
				"Fecha Contestación",
				"Banco",
				"Contestación",
				"Plan",
				"Fecha Desembolso",
				"Asesor",
				"Acciones"
			],
			dataRows: []
		};
	}

	onSubmitBuscarReporte() {
		let _fechaIni: string, _fechaFin: string, asesor: string, financiera: string;
		let comprobarFecha: boolean = true;
		_fechaIni = this.buscarFiltrosForm.value.fechaInicial;
		_fechaFin = this.buscarFiltrosForm.value.fechaFinal;

		if (this.identityUsuario.rol.valor == 'S') this.busquedaFiltros.asesor = this.identityUsuario.sub;

		if (!_fechaIni || _fechaIni == '' || !_fechaFin || _fechaFin == '') {
			this.fechaIni = '';
			this.fechaFin = '';
		} else {
			this.fechaIni = this.fechasValidacion(this.buscarFiltrosForm.value.fechaInicial);
			this.fechaFin = this.fechasValidacion(this.buscarFiltrosForm.value.fechaFinal);
			if (this.validarFechasSuperior(this.fechaIni, this.fechaFin)) comprobarFecha = true;
			else comprobarFecha = false;
		}

		if (comprobarFecha) {
			//console.log(`${this.busquedaFiltros.asesor}, ${this.busquedaFiltros.financiera}, ${this.fechaIni}, ${this.fechaFin}`);
			this.listarEstadisticas(1);
		} else {
			//console.log('La fecha inicial debe ser inferior a la fecha final.');
			this._sweetAlertService.showNotification('top', 'right', 'danger', 'warning', '<b>' + 'La fecha inicial debe ser inferior a la fecha final.' + '<b>', 2000);
		}
	}

	async listarEstadisticas(page: any) {
		this.loading = true;
		//console.log(`${this.busquedaFiltros.asesor}, ${this.busquedaFiltros.financiera}, ${this.fechaIni}, ${this.fechaFin}, ${page}`);
		const responseEstadisticas = await this._estudioCreditoService.listarEstudiosDeCreditoPermiso(this.busquedaFiltros.asesor, this.busquedaFiltros.financiera, this.fechaIni, this.fechaFin, page);
		//console.log(responseEstadisticas);
		if (responseEstadisticas["status"] === "success" && responseEstadisticas["code"] === "200") {
			this.loading = false;
			this.cambiarPaginador = 'T';
			this.llenarDatosTabla(page, responseEstadisticas['data'], '1');
		}
		else if (responseEstadisticas["status"] === "success" && responseEstadisticas["code"] === "300") {
			this.loading = false;
			this.downloadMostrar = false;
			this.cambiarPaginador = 'T';
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseEstadisticas['message'] + '<b>', 2000);
		}
		else if (responseEstadisticas["status"] === "error" && responseEstadisticas["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseEstadisticas['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			this.downloadMostrar = false;
			this.cambiarPaginador = 'T';
			this._sweetAlertService.showNotification('top', 'right', 'error', 'danger', '<b>' + responseEstadisticas['message'] + '<b>', 2000);
		}
	}

	onSubmitClientePorDocumento(page: string) {
		this.documentoCliente = this.buscarUsuarioFormGroup.value.filtroUsuario;
		this.listarClientePorDocumento(page);
	}

	async listarClientePorDocumento(page: string) {
		this.loading = true;
		const responseUsuarios = await this._estadisticasService.estadisticasEstudioCreditoPorDocumento(this.documentoCliente, page);
		//console.log(responseUsuarios);
		if (responseUsuarios["status"] === "success" && responseUsuarios["code"] === "200") {
			this.loading = false;
			this.downloadMostrar = false;
			this.cambiarPaginador = 'PD';
			this.buscarUsuarioFormGroup.reset();
			this.llenarDatosTabla(page, responseUsuarios['data'], '2');
		}
		else if (responseUsuarios["status"] === "success" && responseUsuarios["code"] === "300") {
			this.loading = false;
			this.downloadMostrar = false;
			this.cambiarPaginador = 'PD';
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseUsuarios['message'] + '<b>', 2000);
		}
		else if (responseUsuarios["status"] === "error" && responseUsuarios["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseUsuarios['message']);
		}
		else {
			this.loading = false;
			this.downloadMostrar = false;
			this.cambiarPaginador = 'PD';
			this.dataTable.dataRows = [];
			this._sweetAlertService.showNotification('top', 'right', 'error', 'warning', '<b>' + responseUsuarios['message'] + '<b>', 2000);
		}
	}

	llenarDatosTabla(page: any, responseEstadisticas: any, tipoFiltro: string) {
		//console.log(responseEstadisticas);
		if (tipoFiltro === '1') {
			this.reporteEstadisticaArray = responseEstadisticas['estudios'];
		} else {
			this.reporteEstadisticaArray = responseEstadisticas['estudiosCreditos'];
		}
		this.dataTable.dataRows = [];
		this.total_item_count = responseEstadisticas['total_item_count'];
		this.page_actual = page;

		this.total_pages = responseEstadisticas['total_pages'];

		if (page >= 2) this.page_prev = page - 1;
		else this.page_prev = 1;

		if (page < responseEstadisticas['total_pages']) this.page_next = page + 1;
		else {
			if (responseEstadisticas['total_pages'] == 0) this.page_next = 1;
			else this.page_next = responseEstadisticas['total_pages'];
		}

		let JSONParam: any;

		this.reporteEstadisticaArray.forEach(element => {
			JSONParam = {
				id: element.id,
				idIngreso: element.cotizacion.ingresoConcesionario.id,
				idPersona: element.cotizacion.ingresoConcesionario.persona.id,
				tipoDocPersona: element.cotizacion.ingresoConcesionario.persona.tipoDocumento.valor,
				documentoPersona: element.cotizacion.ingresoConcesionario.persona.documento,
				nombresPersona: element.cotizacion.ingresoConcesionario.persona.nombres,
				apellidosPersona: element.cotizacion.ingresoConcesionario.persona.apellidos,
				negocioAbierto: element.cotizacion.ingresoConcesionario.fechaSalida == '0000-00-00' ? true : false,
				fechaContestacion: element.fechaContestacion == '0000-00-00' ? '- - -' : element.fechaContestacion,
				fechaDesembolso: element.fechaDesembolso == '0000-00-00' ? '- - -' : element.fechaDesembolso,
				banco: element.banco.nombre,
				contestacion: element.contestacion.nombre == 'Desembolsado' ? 'Aprobado <br>' + element.contestacion.nombre : element.contestacion.nombre,
				colorContestacion: this.darColorEstadoEvento(element.contestacion.valor),
				editarEstudioCredito: this.edicionEstudioCredito,
				plan: element.plan.nombre,
				asesor: element.cotizacion.ingresoConcesionario.asesor.nombres + ' ' + element.cotizacion.ingresoConcesionario.asesor.apellidos,
				cotizacionId: element.cotizacion.id
			};
			this.dataTable.dataRows.push(JSONParam);
		});
		//console.log(this.reporteEstadisticaArray);
		if (this.total_item_count || this.total_item_count == 0) {
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
		}
	}

	onSubmitBuscarReporteContestacion() {
		//console.log(this.busquedaFiltros.contestacion);
		this.contestacionForm = this.busquedaFiltros.contestacion;
		this.listarClientePorContestacion(1);
	}

	async listarClientePorContestacion(page: any) {
		this.loading = true;
		const responseECContestacion = await this._estudioCreditoService.listarEstudiosDeCreditoPermisoContestacion(this.contestacionForm, '' + page);
		//console.log(responseECContestacion);
		if (responseECContestacion["status"] === "success" && responseECContestacion["code"] === "200") {
			this.loading = false;
			this.cambiarPaginador = 'PC';
			this.llenarDatosTabla(page, responseECContestacion['data'], '1');
		}
		else if (responseECContestacion["status"] === "success" && responseECContestacion["code"] === "300") {
			this.loading = false;
			
			this.downloadMostrar = false;
			this.cambiarPaginador = 'PC';
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseECContestacion['message'] + '<b>', 2000);
		}
		else if (responseECContestacion["status"] === "error" && responseECContestacion["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseECContestacion['message']);
		}
		else {
			this.loading = false;
			
			this.downloadMostrar = false;
			this.cambiarPaginador = 'PC';
			this._sweetAlertService.showNotification('top', 'right', 'error', 'warning', '<b>' + responseECContestacion['message'] + '<b>', 2000);
		}
	}

	async descargarArchivoEstadisticas() {
		this.loading = true;
		const responseFileDown = await this._estadisticasService.downloadFilesEstadisticas();
		//console.log(responseFileDown);
		if (responseFileDown["status"] === "success" && responseFileDown["code"] === "200") {
			this.loading = false;
		}
		else if (responseFileDown["status"] === "success" && responseFileDown["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseFileDown['message'] + '<b>', 2000);
		}
		else if (responseFileDown["status"] === "error" && responseFileDown["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseFileDown['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'error', 'danger', '<b>' + responseFileDown['message'] + '<b>', 2000);
		}
	}

	async cargarDisponibilidadAsesores() {
		this.loading = true;
		const responseDisponibilidadAsesores = await this._tableroAsignacionService.disponibilidadAsesores();
		//console.log(responseDisponibilidadAsesores);
		if (responseDisponibilidadAsesores["status"] === "success" && responseDisponibilidadAsesores["code"] === "200") {
			this.loading = false;
			this.cargarInformacionSelect(responseDisponibilidadAsesores['data']);
		}
		else if (responseDisponibilidadAsesores["status"] === "success" && responseDisponibilidadAsesores["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseDisponibilidadAsesores["message"], 'OK', 'warning');
		}
		else if (responseDisponibilidadAsesores["status"] === "error" && responseDisponibilidadAsesores["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseDisponibilidadAsesores['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseDisponibilidadAsesores["message"], 'OK', 'error');
		}
	}

	async cargarInformacionSelect(asesores: any) {
		//console.log(asesores);
		let disponibilidadAsesorArray: any[] = [];
		this.todosAsesoresArray = [];

		if (asesores.asesoresDisponibles.length > 0) {
			disponibilidadAsesorArray = asesores.asesoresDisponibles;
		}
		if (asesores.asesoresOcupados.length >= 0) {
			this.todosAsesoresArray = disponibilidadAsesorArray.concat(asesores.asesoresOcupados);
		}

		const bancosParametros: any = await this._parametrizacionService.listarCategoriaParametrizacion('Bancos');
		const contestacionParametros: any = await this._parametrizacionService.listarCategoriaParametrizacion('Contestación');

		bancosParametros['data'].forEach((element) => {
			this.financieraArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		contestacionParametros['data'].forEach((element) => {
			this.contestacionArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
		this.contestacionArray.push({
			id: '',
			nombre: 'Desembolsado',
			valor: 'desembolsado',
			categoria: 'Contestación Estudio Crédito',
			descripcion: ''
		});
	}

	darColorEstadoEvento(estado: string): string {
		switch (estado) {
			case 'rechazado':
				if (this.identityUsuario.rol.valor == 'A') {
					this.edicionEstudioCredito = true;
				} else {
					this.edicionEstudioCredito = false;
				}
				return 'danger';
				break;
			case 'activo':
				this.edicionEstudioCredito = true;
				return 'warning';
				break;
			case 'aprobado':
				this.edicionEstudioCredito = true;
				return 'success';
				break;
			case 'desembolsado':
				if (this.identityUsuario.rol.valor == 'A') {
					this.edicionEstudioCredito = true;
				} else {
					this.edicionEstudioCredito = false;
				}
				return 'success';
				break;
			case 'SAG':
				this.edicionEstudioCredito = true;
				return 'default';
				break;
			default:
				if (this.identityUsuario.rol.valor == 'A') {
					this.edicionEstudioCredito = true;
				} else {
					this.edicionEstudioCredito = false;
				}
				return 'default';
				break;
		}
	}

	buscarIdCliente(rowId: any) {
		localStorage.setItem('rutaSN', 'seguimiento-del-negocio/estudios-de-credito');
		//this._router.navigate(["clientes/ingresos/", rowId]);
		this._router.navigate(["clientes/ficha-clientes/", rowId]);
	}

	editarEstudioCredito(rowId: string, rowCotizacion: string) {
		localStorage.setItem('rutaSeguimiento', 'seguimiento-del-negocio/estudios-de-credito');
		this._router.navigate(["clientes/contestacion/", rowId, rowCotizacion, 'EC']);
	}

	fechasValidacion(formatoFecha: any) {
		if (formatoFecha) {
			//Formatear fecha
			const fechaActualTem = this._datepipe
				.transform(formatoFecha, "yyyy/MM/dd")
				.replace(/\//g, "-");
			//console.log("fecha formato: "+fechaActualTem);
			return fechaActualTem;
		}
		return '';
	}

	validarFechasSuperior(fechaStart: any, fechaEnd: any) {
		const fechaStart1 = new Date(fechaStart);
		const fechaEnd1 = new Date(fechaEnd);

		const diaInicio = fechaStart1.getDate();
		const mesInicio = fechaStart1.getMonth() + 1;
		const anioInicio = fechaStart1.getFullYear();
		const fechaStart1Tem = diaInicio + "-" + mesInicio + "-" + anioInicio;

		const diaFin = fechaEnd1.getUTCDate();
		const mesFin = fechaEnd1.getUTCMonth() + 1;
		const anioFin = fechaEnd1.getUTCFullYear();
		const fechaEnd1Tem = diaFin + "-" + mesFin + "-" + anioFin;

		//console.log("fechaInicio: "+fechaStart1Tem);
		//console.log("fechaFinal: "+fechaEnd1Tem);

		if (!(anioFin > anioInicio)) {
			if (anioFin < anioInicio) return false;

			if (!(mesFin > mesInicio)) {
				if (mesFin < mesInicio) return false;

				if (!(diaFin > diaInicio)) return false;
				return true;
			}
			return true;
		}
		return true;
	}

	limpiarFiltros() {
		this.cambiarPaginador = 'T';
		this.buscarFiltrosForm.reset();
		this.buscarFiltrosForm.value.fechaInicial = null;
		this.buscarFiltrosForm.value.fechaFinal = null;
		this.fechaIni = '';
		this.fechaFin = '';
		if (this.identityUsuario.rol.valor != 'S') {
			this.busquedaFiltros.asesor = '';
			(document.getElementById('asesor') as HTMLInputElement).value = '';
		}
		this.busquedaFiltros.financiera = '';
		this.busquedaFiltros.contestacion = '';
		(document.getElementById('contestacion') as HTMLInputElement).value = '';
		this.listarEstadisticas(1);
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	regresar() {
		this._router.navigate(["dashboard"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}