export class Afiliaciones {
    constructor(
        public entidad: string,
        public fechaAfiliacion: string,
        public fechaRetiro: string,
        public tipoAfiliacion: string,
        public observacion: string,
        public contrato: string
    ) { }
};

export class AfiliacionesSoportes {
    constructor(
        public idAfiliacion: string,
        public soportes: string
    ) { }
};