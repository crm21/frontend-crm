import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElegirEmpresasComponent } from './elegir-empresas.component';

describe('ElegirEmpresasComponent', () => {
  let component: ElegirEmpresasComponent;
  let fixture: ComponentFixture<ElegirEmpresasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElegirEmpresasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElegirEmpresasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
