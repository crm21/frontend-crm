import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from "@angular/router";
import { ProcesoVentas } from '../../models/procesoVentas';
import swal from "sweetalert2";
import { ActualizarProcesoVentas } from '../../interfaces/interfaces';
import { DatePipe } from "@angular/common";
import { ProcesoVentasService } from '../../services/proceso-ventas/proceso-ventas.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare var $: any;

@Component({
	selector: 'app-listar-proceso-ventas',
	templateUrl: './listar-proceso-ventas.component.html',
	styleUrls: ['./listar-proceso-ventas.component.css']
})
export class ListarProcesoVentasComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	idIngreso: any = 0;
	idCotizacion: any = 0;
	ingresoFinalizado: any = 0;
	botonMostrar: any;
	idProcesoVentas: any;
	procesoVentasForm: FormGroup;
	listadoProcesoVentasArray: any[] = [];
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	permisosRolValor: string = '';
	datosCliente = {
		nombres: null,
		documento: null
	};
	permisosBotonEdicion = ['A', 'GC', 'DN', 'S'];
	permisosEdicion: boolean = false;
	camposProcesoVentasChevy: boolean = false;

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _formBuilder: FormBuilder,
		private _procesoVentaService: ProcesoVentasService,
		private _sweetAlertService: SweetAlertService,
		private datepipe: DatePipe
	) {
		this.idCotizacion = this._route.snapshot.paramMap.get('idCotizacion');
		this.idIngreso = this._route.snapshot.paramMap.get('idIngreso');
		this.ingresoFinalizado = this._route.snapshot.paramMap.get('ingresoFinalizado');

		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			let permisoVerFicha = identityData['data'];
			this.permisosRolValor = permisoVerFicha.rol.valor;
			this.botonMostrar = this.ingresoFinalizado == 1 ? true : false;
			if (this.permisosBotonEdicion.includes(this.permisosRolValor)) {
				this.permisosEdicion = true;
			}
		}
	}

	ngOnInit() {
		if (localStorage.getItem('identity_crm')) {
			this.procesoVentasForm = this._formBuilder.group({
				fechaFacturacion: [''],
				fechaEnvioMatricula: [''],
				fechaRecepcionMatricula: [''],
				fechaSoat: [''],
				fechaActivacionChevy: [''],
				fechaEntrega: [''],
				precioFinalVenta: ['', [Validators.pattern("[0-9]{4,25}$")]],
				observacion: [''],
				fechaFirmaContrato: [''],
				fechaConsignacion: ['']
			});
			this.listarProcesoVentasDatos(1);
		}
	}

	async listarProcesoVentasDatos(page: any) {
		this.loading = true;
		let responseListadoProcesoVentas = await this._procesoVentaService.listarProcesoVentas('2', this.idCotizacion, page);
		//console.log(responseListadoProcesoVentas);
		if (responseListadoProcesoVentas["status"] === "success" && responseListadoProcesoVentas["code"] === "200") {
			this.llenarDatosCliente(responseListadoProcesoVentas["data"]);
			this.llenarDatosProcesoVentas(responseListadoProcesoVentas["data"], page);
			this.loading = false;
			$(".main-panel").scrollTop(0);
		}
		else if (responseListadoProcesoVentas["status"] === "success" && responseListadoProcesoVentas["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseListadoProcesoVentas['message'] + '<b>', 2000);
			this.datosCliente.nombres = 'Datos no registrados';
			this.datosCliente.documento = 'Datos no registrados';
		}
		else if (responseListadoProcesoVentas["status"] === "error" && responseListadoProcesoVentas["code"] === "100") {
			this.expiracionToken(responseListadoProcesoVentas['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseListadoProcesoVentas["message"], 'error')
				.then(result => {
					this._router.navigate(["clientes/buscar"]);
				}).catch(swal.noop);
		}
	}

	llenarDatosCliente(data: any) {
		if (data.total_item_count > 0) {
			let nombre = data['procesos'][0].cotizacionId.ingresoConcesionario.persona.nombres;
			let apellido = data['procesos'][0].cotizacionId.ingresoConcesionario.persona.apellidos;
			this.datosCliente.nombres = nombre + ' ' + apellido;
			this.datosCliente.documento = data['procesos'][0].cotizacionId.ingresoConcesionario.persona.documento;
		} else {
			this.datosCliente.nombres = 'Datos no registrados';
			this.datosCliente.documento = 'Datos no registrados';
		}
	}

	llenarDatosProcesoVentas(data: any, page: any) {
		if (data.total_item_count > 0) {
			this.listadoProcesoVentasArray = [];
			let datosProcesoVentas: any = data['procesos'];

			let letraEmpresa = localStorage.getItem('business');
			switch (letraEmpresa) {
				case 'A':
					this.camposProcesoVentasChevy = true;
					break;
				case 'M':
					this.camposProcesoVentasChevy = false;
					break;
				case 'T':
					this.camposProcesoVentasChevy = false;
					break;
				default:
					this.camposProcesoVentasChevy = false;
					break;
			}

			datosProcesoVentas.forEach((element) => {
				this.listadoProcesoVentasArray.push({
					id: element.id,
					cotizacionId: element.cotizacionId.numCotizacion,
					fechaInicioProceso: element.fechaInicioProceso,
					fechaFacturacion: element.fechaFacturacion == null ? '- - -' : element.fechaFacturacion,
					fechaEnvioMatricula: element.fechaEnvioMatricula == null ? '- - -' : element.fechaEnvioMatricula,
					fechaRecepcionMatricula: element.fechaRecepcionMatricula == null ? '- - -' : element.fechaRecepcionMatricula,
					fechaSoat: element.fechaSoat == null ? '- - -' : element.fechaSoat,
					fechaActivacionChevy: element.fechaActivacionChevyOnStar == null ? '- - -' : element.fechaActivacionChevyOnStar,
					fechaEntrega: element.fechaEntrega == null ? '- - -' : element.fechaEntrega,
					precioFinalVenta: element.precioFinalVenta,
					observacion: element.observacion == null ? '- - -' : element.observacion,
					fechaFirmaContrato: element.fechaFirmaContrato == null ? '- - -' : element.fechaFirmaContrato,
					fechaConsignacion: element.fechaConsignacion == null ? '- - -' : element.fechaConsignacion,
					placa: element.placa == null ? '- - -' : element.placa,
					vin: element.vin == null ? '- - -' : element.vin,
					kilometraje: element.kilometrajeInicial == null ? '- - -' : element.kilometrajeInicial
				});
			});

			this.total_item_count = data.total_item_count;
			this.page_actual = page;

			this.total_pages = data.total_pages;

			if (page >= 2) this.page_prev = page - 1;
			else this.page_prev = 1;

			if (page < data.total_pages) this.page_next = page + 1;
			else {
				if (data.total_pages == 0) this.page_next = 1;
				else this.page_next = data.total_pages;
			}
		} else {
			this.listadoProcesoVentasArray = [];
		}

	}

	modificarProcesoVenta(procesoVentas) {
		let rowProcesoVentas: ActualizarProcesoVentas = procesoVentas;
		this.idProcesoVentas = procesoVentas.id;

		this.procesoVentasForm.setValue({
			fechaFacturacion: rowProcesoVentas.fechaFacturacion,
			fechaEnvioMatricula: rowProcesoVentas.fechaEnvioMatricula,
			fechaRecepcionMatricula: rowProcesoVentas.fechaRecepcionMatricula,
			fechaSoat: rowProcesoVentas.fechaSoat,
			fechaActivacionChevy: rowProcesoVentas.fechaActivacionChevy,
			fechaEntrega: rowProcesoVentas.fechaEntrega,
			precioFinalVenta: rowProcesoVentas.precioFinalVenta,
			observacion: '',
			fechaFirmaContrato: rowProcesoVentas.fechaFirmaContrato,
			fechaConsignacion: rowProcesoVentas.fechaConsignacion
		});
		if (this.permisosRolValor == 'S') {
			this.validarCampos();
		}
		$("#modalFiltro").modal("show");
	}

	async guardarDatosProcesoVentas() {
		this.loading = true;

		let procesoVenta = new ProcesoVentas(
			this.fechasValidacion(this.procesoVentasForm.value.fechaFacturacion),
			this.fechasValidacion(this.procesoVentasForm.value.fechaEnvioMatricula),
			this.fechasValidacion(this.procesoVentasForm.value.fechaRecepcionMatricula),
			this.fechasValidacion(this.procesoVentasForm.value.fechaSoat),
			this.fechasValidacion(this.procesoVentasForm.value.fechaActivacionChevy),
			this.fechasValidacion(this.procesoVentasForm.value.fechaEntrega),
			this.procesoVentasForm.value.precioFinalVenta,
			this.procesoVentasForm.value.observacion,
			this.fechasValidacion(this.procesoVentasForm.value.fechaFirmaContrato),
			this.fechasValidacion(this.procesoVentasForm.value.fechaConsignacion)
		); //console.log(procesoVenta);

		let responseActualizacionProcesoVentas = await this._procesoVentaService.actualizarProcesoVentas(this.idProcesoVentas, procesoVenta);
		//console.log(responseActualizacionProcesoVentas);
		if (responseActualizacionProcesoVentas["status"] === "success" && responseActualizacionProcesoVentas["code"] === "200") {
			this.procesoVentasForm.reset();
			this.loading = false;
			$("#modalFiltro").modal("hide");
			swal('Bien!', responseActualizacionProcesoVentas["message"], 'success')
				.then(result => {
					this.listarProcesoVentasDatos(1);
					$(".main-panel").scrollTop(0);
				}).catch(swal.noop);
		}
		else if (responseActualizacionProcesoVentas["status"] === "success" && responseActualizacionProcesoVentas["code"] === "300") {
			this.loading = false;
			swal('Advertencia!', responseActualizacionProcesoVentas["message"], 'warning');
		}
		else if (responseActualizacionProcesoVentas["status"] === "error" && responseActualizacionProcesoVentas["code"] === "100") {
			this.expiracionToken(responseActualizacionProcesoVentas['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseActualizacionProcesoVentas["message"], 'error');
		}
	}

	eliminarProcesoVenta(rowId) {
		//console.log(rowId);
	}

	regresar() {
		let idIngreso = localStorage.getItem('idIngreso');
		this._router.navigate(["clientes/ficha-clientes/", idIngreso]);
	}

	fechasValidacion(formatoFecha: string) {
		if (formatoFecha != '- - -') {
			//Formatear fecha
			const fechaActualTem = this.datepipe
				.transform(formatoFecha, "yyyy/MM/dd")
				.replace(/\//g, "-");
			//console.log("fecha formato: "+fechaActualTem);
			return fechaActualTem;
		}
		return '';
	}

	formatoValorEntero(dato: string) {
		if (dato != '') {
			dato = dato.replace(/COP/gi, '');
			dato = dato.replace(/[-+()\s.]/g, '');
			dato = dato.replace(',00', '');
			return dato;
		}
		return '';
	}

	colocarFormatoMoneda(numero: any) {
		if (numero) {
			return new Intl.NumberFormat("COP", { style: "currency", currency: "COP" }).format(numero);
		}
		return '';
	}

	formatearMoneda(evento: any) {
		let numero: number = parseInt((document.getElementById('precioFinalVenta') as HTMLInputElement).value);
		if (numero) {
			let formateado: any = new Intl.NumberFormat("COP", { style: "currency", currency: "COP" }).format(numero);
			(document.getElementById('precioFinalVenta') as HTMLInputElement).value = formateado;
			//console.log(formateado);
		}
	}

	validarCampos() {
		let arrayFechasBloqueadas = ['fechaFacturacion', 'fechaEnvioMatricula', 'fechaRecepcionMatricula',
			'fechaSoat', 'fechaActivacionChevy', 'fechaEntrega',
			'fechaFirmaContrato', 'fechaConsignacion', 'precioFinalVenta'];
		arrayFechasBloqueadas.forEach((element) => {
			if ((document.getElementById(element) as HTMLInputElement).value)
				(document.getElementById(element) as HTMLInputElement).disabled = true;
			else (document.getElementById(element) as HTMLInputElement).disabled = false;
		});
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}