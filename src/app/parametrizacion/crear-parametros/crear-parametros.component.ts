import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { Parametrizacion } from "../../models/parametrizacion";
import swal from "sweetalert2";
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare var $: any;

@Component({
	selector: 'app-crear-parametros',
	templateUrl: './crear-parametros.component.html',
	styleUrls: ['./crear-parametros.component.css']
})
export class CrearParametrosComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	tipoDocumentos: any;
	categoriasArray: any[] = [];
	categoriaValor: any;
	parametros: Parametrizacion;
	permisoRol: string = '';

	constructor(
		private _parametrizacionService: ParametrizacionService,
		private _sweetAlertService: SweetAlertService,
		private _router: Router
	) {
		this.parametros = new Parametrizacion('', '', '', '');
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);
			let permiso = identityData['data'];

			this.permisoRol = permiso.rol.valor;
		}
	}

	ngOnInit() {
		if (localStorage.getItem('identity_crm')) {
			this.listarCategorias('');
		}
	}

	async onSubmitCrearParametrizacion(datosParametrizacionForm) {
		this.loading = true;
		//console.log(this.parametros);

		const responseParametros = await this._parametrizacionService.registrarParametrizacion(this.parametros);
		//console.log(responseParametros);
		if (responseParametros["status"] === "success" && responseParametros["code"] === "200") {
			this.loading = false;
			swal('Bien!', responseParametros["message"], 'success')
				.then(result => {
					this.regresar();
				}).catch(swal.noop);
		}
		else if (responseParametros["status"] === "success" && responseParametros["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseParametros["message"], 'OK', 'warning');
		}
		else if (responseParametros["status"] === "error" && responseParametros["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseParametros['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseParametros["message"], 'OK', 'error');
		}
	}

	selectListCategorias() {
		document.getElementById("myDropdown").classList.toggle("show");
	}

	filtroCategorias(x: any) {
		let input, filter, ul, li, a, i;
		input = document.getElementById("categoria");
		filter = input.value.toUpperCase();
		let div = document.getElementById("myDropdown");
		a = div.getElementsByTagName("a");

		for (i = 0; i < a.length; i++) {
			let txtValue = a[i].textContent || a[i].innerText;
			if (txtValue.toUpperCase().indexOf(filter) > -1) {
				a[i].style.display = "";
			} else {
				a[i].style.display = "none";
			}
		}
	}

	async listarCategorias(categoria) {
		const categoriasRol = ['Hobby', 'Profesión', 'Colores'];
		const listaCategoria = await this._parametrizacionService.listarCategoriaParametrizacionLike('');
		//console.log(listaCategoria);
		if (listaCategoria["status"] === "success" && listaCategoria["code"] === "200") {
			if (this.permisoRol == 'A') {
				listaCategoria['data'].forEach((element) => {
					this.categoriasArray.push({
						categoria: element.categoria
					});
				});
			} else {
				listaCategoria['data'].forEach((element) => {
					if (categoriasRol.includes(element.categoria)) {
						this.categoriasArray.push({
							categoria: element.categoria
						});
					}
				});
			}
			this.loading = false;
		}
		else if (listaCategoria["status"] === "success" && listaCategoria["code"] === "300") {
			this.categoriasArray = [];
			this._sweetAlertService.alertGeneral('Advertencia!', listaCategoria["message"], 'OK', 'warning');
			this.loading = false;
		}
		else if (listaCategoria["status"] === "error" && listaCategoria["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(listaCategoria['message']);
		}
		else {
			this.categoriasArray = [];
			this._sweetAlertService.alertGeneral('¡Error!', listaCategoria["message"], 'OK', 'error');
			this.loading = false;
		}
		//console.log(this.categoriasArray);
	}

	seleccionarCategorias(categoria: any) {
		this.parametros.categoria = categoria;
		document.getElementById("myDropdown").classList.toggle("show");
		(document.getElementById('categoria') as HTMLInputElement).value = categoria;
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	regresar() {
		this._router.navigate(["parametrizacion/listar"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}
