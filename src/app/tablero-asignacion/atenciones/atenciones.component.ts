import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from "@angular/router";
import { TableroAsignacion } from "../../models/tableroAsignacion";
import { DatePipe } from "@angular/common";
import swal from "sweetalert2";
import { TableroAsignacionService } from '../../services/tablero-asignacion/tablero-asignacion.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare var $: any;
declare interface TableData {
	headerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-actualizar-disponibilidad',
	templateUrl: './atenciones.component.html',
	styleUrls: ['./atenciones.component.css']
})
export class AtencionesComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	dataTable: TableData;
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	atencionesAsesorArray: any[] = [];
	objecionesArray: any[] = [];
	tipificacionArray: any[] = [];
	consultaVacia = false;
	rolValor = '';
	botonFiltradoFechaActual = 0;
	fechaBusqueda: string = '';
	buscarFechaForm: FormGroup;
	finalizarAtencionForm: FormGroup;
	disponibilidadAsesor: any;
	disponibilidadAsesorArray: any[] = [];
	idAsesor: any;
	ventanaAsesoresDisponibles = true;
	actualizarDisponibilidad = true;
	idAtencion: number = 0;
	textoExtendido: string = '';

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _formBuilder: FormBuilder,
		private _tableroAsignacionService: TableroAsignacionService,
		private _parametrizacionService: ParametrizacionService,
		private _sweetAlertService: SweetAlertService,
		private datepipe: DatePipe
	) {
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);
			let identity = identityData['data'];

			this.rolValor = identity.rol.valor;
			this.idAsesor = identity.sub;
			this.datosTabla();
		}
	}

	ngOnInit() {
		if (localStorage.getItem('identity_crm')) {
			this.buscarFechaForm = this._formBuilder.group({
				fechaEspecifica: ['', Validators.required],
			});
			this.finalizarAtencionForm = this._formBuilder.group({
				objecion: ['NN', Validators.required],
				tipificacion: ['Ninguno'],
				observacion: ['']
			});
			let fechaActual = new Date();
			if (this.rolValor == 'S') {
				this.botonFiltradoFechaActual = 1;
				this.listarAtencionesPorAsesorToken(2, this.idAsesor, '');
			} else {
				this.botonFiltradoFechaActual = 0;
				this.listarAtencionesPorFecha(1, this.fechasValidacion(fechaActual));
			}
		}
	}

	datosTabla() {
		this.dataTable = {
			headerRow: [
				"#",
				"Asesor",
				"Cliente",
				"Fecha Inicial",
				"Fecha Final",
				"Tiempo de Atención",
				"Objeción",
				"Observación",
				"Disponibilidad"
			],
			dataRows: []
		};
	}

	async listarAtencionesPorAsesorToken(opc: any, idAsesor: any, fecha: any) {
		let fechaActual_ = this.fechasValidacion(fecha)
		//console.log(`Parametros de busqueda: ${opc}, ${idAsesor}, ${fechaActual_}`);
		this.loading = true;
		const responseAtencionesPorAsesorToken = await this._tableroAsignacionService.atencionesPorFecha(opc, idAsesor, fechaActual_);
		//console.log(responseAtencionesPorAsesorToken);
		if (responseAtencionesPorAsesorToken['status'] == "success" && responseAtencionesPorAsesorToken['code'] == "200") {
			this.loading = false;
			this.consultaVacia = false;
			this.llenarDatosTabla(responseAtencionesPorAsesorToken['data']);
		}
		else if (responseAtencionesPorAsesorToken['status'] == "success" && responseAtencionesPorAsesorToken['code'] == "300") {
			this.loading = false;
			this.consultaVacia = true;
		}
		else if (responseAtencionesPorAsesorToken["status"] === "error" && responseAtencionesPorAsesorToken["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseAtencionesPorAsesorToken['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseAtencionesPorAsesorToken['message'], 'error');
		}
	}

	async listarAtencionesPorFecha(opc: any, fecha: any) {
		this.loading = true;
		this.fechaBusqueda = fecha;
		//console.log(`Parametros de busqueda: ${opc}, ${this.fechaBusqueda}`);
		const responseAtencionesPorAsesor = await this._tableroAsignacionService.atencionesPorFecha(opc, '', fecha);
		//console.log(responseAtencionesPorAsesor);
		if (responseAtencionesPorAsesor['status'] == "success" && responseAtencionesPorAsesor['code'] == "200") {
			this.loading = false;
			this.consultaVacia = false;
			this.llenarDatosTabla(responseAtencionesPorAsesor['data']);
		}
		else if (responseAtencionesPorAsesor['status'] == "success" && responseAtencionesPorAsesor['code'] == "300") {
			this.loading = false;
			this.consultaVacia = true;
		}
		else if (responseAtencionesPorAsesor["status"] === "error" && responseAtencionesPorAsesor["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseAtencionesPorAsesor['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseAtencionesPorAsesor['message'], 'error');
		}
	}

	llenarDatosTabla(responseAtencionAsesorData: any) {
		//console.log(responseAtencionAsesorData);
		this.atencionesAsesorArray = responseAtencionAsesorData;
		this.dataTable.dataRows = [];

		let JSONParam;

		this.atencionesAsesorArray.forEach((element) => {
			JSONParam = {
				id: element.id,
				tipoDocumentoAsesor: element.asesor.tipoDocumento.valor,
				documentoAsesor: element.asesor.documento,
				asesorId: element.asesor.id,
				asesor: element.asesor.nombres + ' ' + element.asesor.apellidos,
				tipoDocumentoCliente: element.ingreso.persona.tipoDocumento.valor,
				documentoCliente: element.ingreso.persona.documento,
				cliente: element.ingreso.persona.nombres + ' ' + element.ingreso.persona.apellidos,
				fechaInicial: element.fechaInicial,
				fechaFinal: element.fechaFinal,
				tiempoAtencion: element.tiempoAtencion,
				objecion: element.objecion.nombre,
				observacion: element.observacion
			};
			this.dataTable.dataRows.push(JSONParam);
		});
	}

	finalizarAtencion(idAtencion) {
		this.idAtencion = idAtencion;
		this.cargarInformacionSelect();
		$("#modalFinalizarAtencion").modal("show");
	}

	async onSubmitFinalizarAtencion() {
		let objecion_ = this.finalizarAtencionForm.value.objecion;
		//let tipificacion_ = this.finalizarAtencionForm.value.tipificacion;
		let tipificacion_ = 'Ninguno';
		let observacion_ = this.finalizarAtencionForm.value.observacion;
		observacion_ = observacion_ == null ? '' : observacion_;
		//console.log(`${this.idAtencion}, ${objecion_}, ${observacion_}`);
		this.loading = true;
		const responseFinalizarAtencion = await this._tableroAsignacionService.finalizarAtencionAsesor(this.idAtencion, objecion_, tipificacion_, observacion_);
		//console.log(responseFinalizarAtencion);
		if (responseFinalizarAtencion["status"] === "success" && responseFinalizarAtencion["code"] === "200") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'success', 'success', '<b>' + responseFinalizarAtencion['message'] + '<b>', 2000);
			swal('¡Bien!', responseFinalizarAtencion['message'], 'success')
				.then((result) => {
					if (this.rolValor == 'S') {
						this.botonFiltradoFechaActual = 1;
						this.listarAtencionesPorAsesorToken(2, this.idAsesor, '');
					} else {
						this.botonFiltradoFechaActual = 0;
						this.listarAtencionesPorFecha(1, '');
					}
					$("#modalFinalizarAtencion").modal("hide");
					this.finalizarAtencionForm.reset();
				}).catch(swal.noop);
		}
		else if (responseFinalizarAtencion["status"] === "success" && responseFinalizarAtencion["code"] === "300") {
			this.loading = false;
			$("#modalFinalizarAtencion").modal("hide");
			this.finalizarAtencionForm.reset();
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseFinalizarAtencion['message'] + '<b>', 2000);
			swal('Advertencia!', responseFinalizarAtencion['message'], 'warning')
				.then((result) => {
					if (this.rolValor == 'S') {
						this.botonFiltradoFechaActual = 1;
						this.listarAtencionesPorAsesorToken(2, this.idAsesor, '');
					} else {
						this.botonFiltradoFechaActual = 0;
						this.listarAtencionesPorFecha(1, '');
					}
				}).catch(swal.noop);
		}
		else if (responseFinalizarAtencion["status"] === "error" && responseFinalizarAtencion["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseFinalizarAtencion['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseFinalizarAtencion['message'], 'error');
		}
	}

	async cargarInformacionSelect() {
		this.objecionesArray = [];
		this.tipificacionArray = [];
		const objecionesParametros: Object = await this._parametrizacionService.listarCategoriaParametrizacion('Objeciones');
		const tipificacionParametros: Object = await this._parametrizacionService.listarCategoriaParametrizacion('Tipificación Clientes');

		objecionesParametros['data'].forEach((element) => {
			this.objecionesArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
		tipificacionParametros['data'].forEach((element) => {
			this.tipificacionArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
	}

	onSubmitFiltroAtencionesPorFecha(event: any) {
		this.fechaBusqueda = this.fechasValidacion(event.value);
		if (this.rolValor == 'S') {
			this.botonFiltradoFechaActual = 1;
			this.listarAtencionesPorAsesorToken(2, this.idAsesor, this.fechaBusqueda);
		} else {
			this.botonFiltradoFechaActual = 0;
			this.listarAtencionesPorFecha(1, this.fechaBusqueda);
		}
	}

	limpiarFiltros() {
		let fechaActual = new Date();
		(document.getElementById('fecha') as HTMLInputElement).value = '';
		if (this.rolValor == 'S') {
			this.botonFiltradoFechaActual = 1;
			this.listarAtencionesPorAsesorToken(2, this.idAsesor, '');
		} else {
			this.botonFiltradoFechaActual = 0;
			this.listarAtencionesPorFecha(1, this.fechasValidacion(fechaActual));
		}
	}

	regresar() {
		let idIngreso = localStorage.getItem('idIngreso');
		this._router.navigate(["clientes/ficha-clientes/", idIngreso]);
	}

	fechasValidacion(formatoFecha) {
		if (formatoFecha) {
			//Formatear fecha
			const fechaActualTem = this.datepipe
				.transform(formatoFecha, "yyyy/MM/dd")
				.replace(/\//g, "-");
			//console.log("fecha formato: "+fechaActualTem);
			return fechaActualTem;
		}
		return '';
	}

	observarTexto(texto: any) {
		this.textoExtendido = texto;
		$("#modalObservacion").modal("show");
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}