import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteClientesLeadsMesComponent } from './reporte-clientes-leads-mes.component';

describe('ReporteClientesLeadsMesComponent', () => {
  let component: ReporteClientesLeadsMesComponent;
  let fixture: ComponentFixture<ReporteClientesLeadsMesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteClientesLeadsMesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteClientesLeadsMesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
