import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FuncionesCargos } from '../../models/funcionesCargo';
import { ActualizarFuncionesCargos } from '../../interfaces/interfaces'
import { ControlIndividualService } from '../../services/control-individual/control-individual.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';

declare var $: any;
declare interface DataTable {
	headerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-listar-funciones',
	templateUrl: './listar-funciones.component.html',
	styleUrls: ['./listar-funciones.component.css']
})
export class ListarFuncionesComponent implements OnInit, OnDestroy {

	loading: boolean = false;
	validarToken: boolean = true;
	consultaVaciaCargarSpinner: boolean = false;
	crearFuncionesCargoFormGroup: FormGroup;
	actualizarFuncionCargoFormGroup: FormGroup;
	dataTable: DataTable;
	listadoFuncionesCargos: any[] = [];
	periodoPagoArray: any[] = [];
	numeroCargosArray: any[] = [];
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	idCargo = null;
	idFuncion = null;
	textoExtendido: string = '';
	nombreCargoVariable: string = '';
	cargoEmpleado: any = {
		id: null,
		nombre: null,
		area: null,
		dependencia: null,
		periodoPago: null,
		numCargos: null,
		ubicacionOrganigrama: null,
		mision: null
	};
	opcionVentana: any;

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _formBuilder: FormBuilder,
		private _controlIndividualService: ControlIndividualService,
		private _sweetAlertService: SweetAlertService,
		private _parametrizacionService: ParametrizacionService
	) {
		this.idCargo = this._route.snapshot.paramMap.get('idCargo');
		this.opcionVentana = this._route.snapshot.data.opc;
		if (localStorage.getItem('identity_crm')) {
			this.listarFuncionesCargos(1);
			this.nombreCargo();
			this.listarCargoPorId();
		}
	}

	ngOnInit() {
		this.dataTable = {
			headerRow: [
				'#',
				"Función",
				"Acción específica",
				"Resultados obtenidos",
				"Tiempo de ejecución",
				"Recursos utilizados",
				"Acciones"
			],
			dataRows: []
		};
		this.crearFuncionesCargoFormGroup = this._formBuilder.group({
			titulo: ['', Validators.required],
			accionEspecifica: ['', Validators.required],
			resultadosObtenidos: ['', Validators.required],
			tiempoEjecucion: ['', Validators.required],
			recursosUtilizados: ['', Validators.required]
		});
		this.actualizarFuncionCargoFormGroup = this._formBuilder.group({
			titulo: ['', Validators.required],
			accionEspecifica: ['', Validators.required],
			resultadosObtenidos: ['', Validators.required],
			tiempoEjecucion: ['', Validators.required],
			recursosUtilizados: ['', Validators.required]
		});
	}

	ngOnDestroy() {
		localStorage.removeItem('empleadoID');
	}

	crearNuevaFuncion() {
		$("#modalFuncionesCargosCrear").modal("show");
	}

	async onSubmitCrearFuncionCargo() {
		this.loading = true;
		let crearFuncionCargo = new FuncionesCargos(
			this.idCargo,
			this.crearFuncionesCargoFormGroup.value.titulo,
			this.crearFuncionesCargoFormGroup.value.accionEspecifica,
			this.crearFuncionesCargoFormGroup.value.resultadosObtenidos,
			this.crearFuncionesCargoFormGroup.value.tiempoEjecucion,
			this.crearFuncionesCargoFormGroup.value.recursosUtilizados
		); //console.log(crearFuncionCargo);
		const responseCrearFuncionCargos = await this._controlIndividualService.registrarFuncionesCargo(crearFuncionCargo);
		//console.log(responseCrearFuncionCargos);
		if (responseCrearFuncionCargos["status"] === "success" && responseCrearFuncionCargos["code"] === "200") {
			this.limpiarFormulario();
			this.listarFuncionesCargos(1);
			$("#modalFuncionesCargosCrear").modal("hide");
			this._sweetAlertService.showNotification('top', 'right', 'success', 'check_circle', '<b>' + responseCrearFuncionCargos['message'] + '<b>', 2000);
			this.loading = false;
		}
		else if (responseCrearFuncionCargos["status"] === "success" && responseCrearFuncionCargos["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseCrearFuncionCargos['message'] + '<b>', 2000);
		}
		else if (responseCrearFuncionCargos["status"] === "error" && responseCrearFuncionCargos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseCrearFuncionCargos['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseCrearFuncionCargos["message"], 'OK', 'error');
		}
	}

	async onSubmitActualizarFuncionesCargo() {
		this.loading = true;
		let actualizarFuncionCargo = new FuncionesCargos(
			this.idCargo,
			this.actualizarFuncionCargoFormGroup.value.titulo,
			this.actualizarFuncionCargoFormGroup.value.accionEspecifica,
			this.actualizarFuncionCargoFormGroup.value.resultadosObtenidos,
			this.actualizarFuncionCargoFormGroup.value.tiempoEjecucion,
			this.actualizarFuncionCargoFormGroup.value.recursosUtilizados
		); //console.log(crearCargo);
		const responseModificarCargos = await this._controlIndividualService.modificarFuncionesCargo(this.idFuncion, actualizarFuncionCargo);
		//console.log(responseModificarCargos);
		if (responseModificarCargos["status"] === "success" && responseModificarCargos["code"] === "200") {
			this.actualizarFuncionCargoFormGroup.reset();
			this.listarFuncionesCargos(1);
			$("#modalFuncionesCargos").modal("hide");
			this._sweetAlertService.showNotification('top', 'right', 'success', 'warning', '<b>' + responseModificarCargos['message'] + '<b>', 2000);
			this.loading = false;
		}
		else if (responseModificarCargos["status"] === "success" && responseModificarCargos["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseModificarCargos['message'] + '<b>', 2000);
		}
		else if (responseModificarCargos["status"] === "error" && responseModificarCargos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseModificarCargos['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseModificarCargos["message"], 'OK', 'error');
		}
	}

	async listarCargoPorId() {
		this.loading = true;

		const responseListaFuncionesCargos = await this._controlIndividualService.listarCargosId(2, this.idCargo, '');
		//console.log(responseListaFuncionesCargos);
		if (responseListaFuncionesCargos["status"] === "success" && responseListaFuncionesCargos["code"] === "200") {
			let datosCargo = responseListaFuncionesCargos['data'];
			this.cargoEmpleado.id = datosCargo.id;
			this.cargoEmpleado.nombre = datosCargo.nombre;
			this.cargoEmpleado.area = datosCargo.area.nombre;
			this.cargoEmpleado.dependencia = datosCargo.dependencia.nombre;
			this.cargoEmpleado.periodoPago = datosCargo.periodoPago.nombre;
			this.cargoEmpleado.numCargos = datosCargo.numCargos;
			this.cargoEmpleado.ubicacionOrganigrama = datosCargo.ubicacionOrganigrama;
			this.cargoEmpleado.mision = datosCargo.mision;
			this.loading = false;
		}
		else if (responseListaFuncionesCargos["status"] === "success" && responseListaFuncionesCargos["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseListaFuncionesCargos['message'] + '<b>', 2000);
		}
		else if (responseListaFuncionesCargos["status"] === "error" && responseListaFuncionesCargos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseListaFuncionesCargos['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseListaFuncionesCargos["message"], 'OK', 'error');
		}
	}

	async listarFuncionesCargos(page: any) {
		this.loading = true;

		const responseListaFuncionesCargos = await this._controlIndividualService.listarFuncionesCargo(this.idCargo, page);
		//console.log(responseListaFuncionesCargos);
		if (responseListaFuncionesCargos["status"] === "success" && responseListaFuncionesCargos["code"] === "200") {
			this.llenarDatosTabla(page, responseListaFuncionesCargos['data'], responseListaFuncionesCargos['data'].funciones);
			this.loading = false;
		}
		else if (responseListaFuncionesCargos["status"] === "success" && responseListaFuncionesCargos["code"] === "300") {
			this.loading = false;
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseListaFuncionesCargos['message'] + '<b>', 2000);
		}
		else if (responseListaFuncionesCargos["status"] === "error" && responseListaFuncionesCargos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseListaFuncionesCargos['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			this._sweetAlertService.alertGeneral('¡Error!', responseListaFuncionesCargos["message"], 'OK', 'error');
		}
	}

	llenarDatosTabla(page: any, listaFuncionesCargosResponse: any, funciones: any) {
		this.listadoFuncionesCargos = funciones;
		this.dataTable.dataRows = [];
		this.total_item_count = listaFuncionesCargosResponse['total_item_count'];
		this.page_actual = page;

		this.total_pages = listaFuncionesCargosResponse['total_pages'];

		if (page >= 2) this.page_prev = page - 1;
		else this.page_prev = 1;

		if (page < listaFuncionesCargosResponse['total_pages']) this.page_next = page + 1;
		else {
			if (listaFuncionesCargosResponse['total_pages'] == 0) this.page_next = 1;
			else this.page_next = listaFuncionesCargosResponse['total_pages'];
		}

		this.dataTable.dataRows = this.listadoFuncionesCargos;

		if (this.total_item_count || this.total_item_count == 0) {
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
		}
	}

	async cargarInformacionSelect() {
		this.loading = true;
		const periodoPago: any = await this._parametrizacionService.listarCategoriaParametrizacion('Periodo de Pago');

		periodoPago['data'].forEach((element) => {
			this.periodoPagoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
		for (let index = 1; index < 11; index++) {
			this.numeroCargosArray.push({
				nombre: index,
				valor: index
			});
		}
		this.loading = false;
	}

	async nombreCargo() {
		this.loading = true;

		const responseNombreCargo = await this._controlIndividualService.listarNombreCargo(3, this.idCargo);
		//console.log(responseNombreCargo);
		if (responseNombreCargo["status"] === "success" && responseNombreCargo["code"] === "200") {
			this.nombreCargoVariable = responseNombreCargo["data"].nombre;
			this.loading = false;
		}
		else if (responseNombreCargo["status"] === "success" && responseNombreCargo["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseNombreCargo['message'] + '<b>', 2000);
		}
		else if (responseNombreCargo["status"] === "error" && responseNombreCargo["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseNombreCargo['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseNombreCargo["message"], 'OK', 'error');
		}
	}

	limpiarFormulario() {
		this.crearFuncionesCargoFormGroup.reset();
	}

	listarFuncionesCargo(rowIdCargo: any) {
		this._router.navigate(["control-individual/listar-funciones/", rowIdCargo]);
	}

	actualizarCargo(rowFuncionCargo: any) {
		let cargoRow: ActualizarFuncionesCargos = rowFuncionCargo;
		this.idFuncion = cargoRow['id'];

		this.actualizarFuncionCargoFormGroup.setValue({
			titulo: cargoRow.titulo,
			accionEspecifica: cargoRow.accionEspecifica,
			resultadosObtenidos: cargoRow.resultadosObtenidos,
			tiempoEjecucion: cargoRow.tiempoEjecucion,
			recursosUtilizados: cargoRow.recursosUtilizados
		});

		$("#modalFuncionesCargos").modal("show");
	}

	observarTexto(texto: any) {
		this.textoExtendido = texto;
		$("#modalAccionEspecifica").modal("show");
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	regresar() {
		if (this.opcionVentana == '1') {
			let idEmpleado = localStorage.getItem('empleadoID');
			this._router.navigate(["empleados/ficha-empleados/", idEmpleado]);

		} else {
			this._router.navigate(["control-individual/listar-cargos"]);
		}
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}