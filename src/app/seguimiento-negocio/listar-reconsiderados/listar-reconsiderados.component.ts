import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import swal from "sweetalert2";
import { ReconsideradoService } from '../../services/reconsiderado/reconsiderado.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare var $: any;
declare interface TableData {
	headerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-listar-reconsiderados',
	templateUrl: './listar-reconsiderados.component.html',
	styleUrls: ['./listar-reconsiderados.component.css']
})
export class ListarReconsideradosComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	consultaVacia: boolean = false;
	dataTable: TableData;
	listadoReconsideradosArray: any[] = [];
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	rolValor: string = '';
	textoExtendido: string = '';

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _reconsideradoService: ReconsideradoService,
		private _sweetAlertService: SweetAlertService
	) {
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			let permisoVerFicha = identityData['data'];
			this.rolValor = permisoVerFicha.rol.valor;
			this.listarReconsiderados(1);
		}
	}

	ngOnInit() {
		this.datosTabla();
	}

	datosTabla() {
		this.dataTable = {
			headerRow: [
				"#",
				"Documento Cliente",
				"Nombres Completos",
				"Fecha Reconsiderado",
				"Reconsiderado",
				"Observaciones",
				"Acciones"
			],
			dataRows: []
		};
	}

	async listarReconsiderados(page: number) {
		this.loading = true;
		let responseListadoReconsiderados = await this._reconsideradoService.listarReconsideradosTotal('' + page);
		//console.log(responseListadoReconsiderados);
		if (responseListadoReconsiderados["status"] === "success" && responseListadoReconsiderados["code"] === "200") {
			this.llenarDatosReconsiderados(responseListadoReconsiderados['data'], page);
			this.consultaVacia = false;
			this.loading = false;
		}
		else if (responseListadoReconsiderados["status"] === "success" && responseListadoReconsiderados["code"] === "300") {
			this.loading = false;
			this.consultaVacia = true;
			this.dataTable.dataRows = [];
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseListadoReconsiderados['message'] + '<b>', 2000);
		}
		else if (responseListadoReconsiderados["status"] === "error" && responseListadoReconsiderados["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseListadoReconsiderados['message']);
		}
		else {
			this.loading = false;
			this.consultaVacia = true;
			this._sweetAlertService.showNotification('top', 'right', 'error', 'danger', '<b>' + responseListadoReconsiderados['message'] + '<b>', 2000);
		}
	}

	llenarDatosReconsiderados(responseReconsiderado: any, page: number) {
		this.listadoReconsideradosArray = [];
		this.consultaVacia = false;
		this.listadoReconsideradosArray = responseReconsiderado['reconsiderados'];
		this.dataTable.dataRows = [];
		this.total_item_count = responseReconsiderado['total_item_count'];
		this.page_actual = page;

		if (this.total_item_count == 0) this.consultaVacia = true; // Ocultar Tabla por ausencia de datos

		this.total_pages = responseReconsiderado['total_pages'];

		if (page >= 2) this.page_prev = page - 1;
		else this.page_prev = 1;

		if (page < responseReconsiderado['total_pages']) this.page_next = page + 1;
		else {
			if (responseReconsiderado['total_pages'] == 0) this.page_next = 1;
			else this.page_next = responseReconsiderado['total_pages'];
		}

		let JSONParam: any;

		this.listadoReconsideradosArray.forEach(element => {
			JSONParam = {
				id: element.id,
				idPersona: element.estudioCredito.ingresoConcesionario.persona.id,
				tipoDocPersona: element.estudioCredito.ingresoConcesionario.persona.tipoDocumento.valor,
				documentoPersona: element.estudioCredito.ingresoConcesionario.persona.documento,
				nombresPersona: element.estudioCredito.ingresoConcesionario.persona.nombres,
				apellidosPersona: element.estudioCredito.ingresoConcesionario.persona.apellidos,
				negocioAbierto: element.estudioCredito.ingresoConcesionario.fechaSalida == '0000-00-00' ? true : false,
				fechaReconsiderado: element.fechaReconsiderado,
				reconsiderado: element.reconsiderado.nombre,
				observacion: element.observacion == null ? '' : element.observacion
			};
			this.dataTable.dataRows.push(JSONParam);
		});
	}

	observarTexto(texto: any) {
		this.textoExtendido = texto;
		$("#modalObservacion").modal("show");
	}

	buscarIdCliente(rowId: any) {
		localStorage.setItem('ruta', 'seguimiento-del-negocio/reconsiderados');
		this._router.navigate(["clientes/ingresos/", rowId]);
	}

	editarReconsiderado(rowId: string) {
		localStorage.setItem('rutaSeguimiento', 'seguimiento-del-negocio/reconsiderados');
		this._router.navigate(["clientes/contestacion/", rowId, 'C', 'R']);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}