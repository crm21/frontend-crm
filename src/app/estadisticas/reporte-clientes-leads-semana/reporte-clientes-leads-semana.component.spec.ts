import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteClientesLeadsSemanaComponent } from './reporte-clientes-leads-semana.component';

describe('ReporteClientesLeadsSemanaComponent', () => {
  let component: ReporteClientesLeadsSemanaComponent;
  let fixture: ComponentFixture<ReporteClientesLeadsSemanaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteClientesLeadsSemanaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteClientesLeadsSemanaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
