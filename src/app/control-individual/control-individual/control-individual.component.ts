import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ControlIndividualService } from '../../services/control-individual/control-individual.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare var $: any;
declare interface DataTable {
	headerRow: string[];
	footerRow: string[];
	dataRows: string[][];
}
export interface OpcionesArray {
	nombre: string;
	valor: string;
}

@Component({
	selector: 'app-control-individual',
	templateUrl: './control-individual.component.html',
	styleUrls: ['./control-individual.component.css']
})
export class ControlIndividualComponent implements OnInit, OnDestroy {

	loading: boolean = false;
	validarToken: boolean = true;
	buscarEmpleadoFormGroup: FormGroup;
	empleadoId: any;
	dataTable: any;
	generoArray: OpcionesArray[] = [
		{ nombre: 'Mujer', valor: 'M' },
		{ nombre: 'Hombre', valor: 'H' },
		{ nombre: 'Otro', valor: 'O' }
	];

	/* Cargar información en variables para mostrar */
	contratoId: any;
	filtroBusqueda = '';
	empleado: any = {
		id: null,
		tipoDocumento: null,
		documento: null,
		nombres: null,
		apellidos: null,
		estado: null
	};
	infoGeneral: any = {
		id: null,
		fechaExpedicion: null,
		grupoSanguineo: null,
		fechaNacimiento: null,
		telefonoFijo: null,
		celular: null,
		correo: null,
		direccion: null,
		lugarResidencia: null,
		profesion: null,
		hobby1: null,
		hobby2: null,
		colorPreferido: null,
		genero: null,
		estadoCivil: null,
		aniosExperienciaLaboral: null,
		libretaMilitar: null,
		claseLibretaMilitar: null,
		distritoLibretaMilitar: null,
		tarjetaProfesional: null,
		licenciaConduccion: null,
		categoriaLicencia: null
	};
	contrato: any = {
		id: null,
		fechaFirmaContrato: null,
		fechaDesvinculacion: null,
		descripcionSalario: null,
		valorSalario: null,
		fechaIniciacionLabores: null,
		observacion: null,
		tipoContrato: null,
		tipoDesvinculacion: null
	};
	cargoEmpleado: any = {
		id: null,
		nombre: null,
		dependencia: null,
		periodoPago: null,
		numCargos: null,
		ubicacionOrganigrama: null,
		mision: null
	};
	afiliacionesArray: any[] = [];
	textoExtendido: string = '';

	/* Variables booleanas para saber si se registra o no la información */
	existeContrato: boolean = true;
	existeCargo: boolean = true;
	existeAfiliacion: boolean = true;
	consultaExistente: boolean = true;
	opcionVentana: any;
	documentEmpleado: any;

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _formBuilder: FormBuilder,
		private _controlIndividualService: ControlIndividualService,
		private _sweetAlertService: SweetAlertService
	) {
		this.opcionVentana = this._route.snapshot.data.opc;
		if (localStorage.getItem('identity_crm')) {
			if (this.opcionVentana == '2' && localStorage.getItem('controlId')) {
				this.documentEmpleado = localStorage.getItem('controlId');
				this.consultaExistente = false;
				this.listarControlIndividualEmpleado(this.documentEmpleado);
			}
		}
	}

	ngOnInit() {
		this.dataTable = {
			headerRow: [
				"Tipo de Afiliación",
				"Entidad",
				"Fecha de Afiliación",
				"Fecha de Retiro",
				"Observaciones"
			],
			dataRows: []
		};
		this.buscarEmpleadoFormGroup = this._formBuilder.group({
			filtroEmpleado: ['', Validators.required]
		});
	}

	ngOnDestroy() {
		localStorage.removeItem('controlId');
	}

	onSubmitBuscarEmpleado() {
		this.filtroBusqueda = this.buscarEmpleadoFormGroup.value.filtroEmpleado;
		this.listarControlIndividualEmpleado(this.filtroBusqueda);
	}

	async listarControlIndividualEmpleado(idEmpleado: any) {
		this.loading = true;
		const responseControlIndividual = await this._controlIndividualService.listarControlIndividual(2, idEmpleado);
		//console.log(responseControlIndividual);
		if (responseControlIndividual["status"] === "success" && responseControlIndividual["code"] === "200") {
			this.consultaExistente = false;
			this.cargarInformacionPersonal(responseControlIndividual['data'].empleado);
			this.cargarInformacionGenearal(responseControlIndividual['data'].infoGeneral);
			this.cargarInformacionContrato(responseControlIndividual['data'].contrato);
			this.cargarInformacionCargo(responseControlIndividual['data'].cargo);
			this.cargarAfiliaciones(responseControlIndividual['data'].afiliaciones);
			this.loading = false;
		}
		else if (responseControlIndividual["status"] === "success" && responseControlIndividual["code"] === "300") {
			this.consultaExistente = true;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseControlIndividual['message'] + '<b>', 2000);
			this.loading = false;
		}
		else if (responseControlIndividual["status"] === "error" && responseControlIndividual["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseControlIndividual['message']);
		}
		else {
			this.consultaExistente = true;
			this._sweetAlertService.alertGeneral('¡Error!', responseControlIndividual["message"], 'OK', 'error');
			this.loading = false;
		}
	}

	cargarInformacionPersonal(datosPersonales: any) {
		if (datosPersonales != '0') {
			this.empleadoId = datosPersonales.id;
			this.empleado.id = datosPersonales.id;
			this.empleado.tipoDocumento = datosPersonales.tipoDocumento.valor;
			this.empleado.documento = datosPersonales.documento;
			this.empleado.nombres = datosPersonales.nombres;
			this.empleado.apellidos = datosPersonales.apellidos;
			this.empleado.estado = datosPersonales.estado.nombre;
			//console.log(this.empleado);
		}
	}

	cargarInformacionGenearal(datosGenerales: any) {
		if (datosGenerales != '0') {
			this.empleado.id = datosGenerales.id;
			this.infoGeneral.id = datosGenerales.id;
			this.infoGeneral.fechaExpedicion = datosGenerales.fechaExpedicion;
			this.infoGeneral.grupoSanguineo = this.validacionCamposVisibles('grupoSanguineo', datosGenerales.grupoSanguineo);
			this.infoGeneral.fechaNacimiento = datosGenerales.fechaNacimiento;
			this.infoGeneral.telefonoFijo = datosGenerales.telefonoFijo;
			this.infoGeneral.celular = datosGenerales.celular;
			this.infoGeneral.correo = datosGenerales.correo;
			this.infoGeneral.direccion = datosGenerales.direccion;
			this.infoGeneral.lugarResidencia = datosGenerales.lugarResidencia;
			this.infoGeneral.profesion = datosGenerales.profesion.nombre;
			this.infoGeneral.hobby1 = datosGenerales.hobby1.nombre;
			this.infoGeneral.hobby2 = datosGenerales.hobby2.nombre;
			this.infoGeneral.colorPreferido = datosGenerales.colorPreferido.nombre;
			this.infoGeneral.genero = this.validacionCamposVisibles('genero', datosGenerales.genero);
			this.infoGeneral.estadoCivil = datosGenerales.estadoCivil.nombre;
			this.infoGeneral.aniosExperienciaLaboral = datosGenerales.aniosExperienciaLaboral;
			this.infoGeneral.libretaMilitar = datosGenerales.libretaMilitar;
			this.infoGeneral.claseLibretaMilitar = datosGenerales.claseLibretaMilitar.nombre;
			this.infoGeneral.distritoLibretaMilitar = datosGenerales.distritoLibretaMilitar;
			this.infoGeneral.tarjetaProfesional = datosGenerales.tarjetaProfesional;
			this.infoGeneral.licenciaConduccion = datosGenerales.licenciaConduccion;
			this.infoGeneral.categoriaLicencia = datosGenerales.categoriaLicencia.nombre;
			//console.log(this.infoGeneral);
		}
	}

	cargarInformacionContrato(datosContrato: any) {
		if (datosContrato == '0') this.existeContrato = false;
		else {
			this.contratoId = datosContrato.id;
			this.contrato.id = datosContrato.id;
			this.contrato.fechaFirmaContrato = datosContrato.fechaFirmaContrato;
			this.contrato.fechaDesvinculacion = datosContrato.fechaDesvinculacion;
			this.contrato.descripcionSalario = datosContrato.descripcionSalario;
			this.contrato.valorSalario = datosContrato.valorSalario;
			this.contrato.fechaIniciacionLabores = datosContrato.fechaIniciacionLabores;
			this.contrato.observacion = datosContrato.observacion == '' ? '- - -' : datosContrato.observacion;
			this.contrato.tipoContrato = datosContrato.tipoContrato.nombre;
			this.contrato.tipoDesvinculacion = datosContrato.tipoDesvinculacion.nombre;
			this.existeContrato = true;
			//console.log(this.contrato);
		}
	}

	cargarInformacionCargo(datosCargo: any) {
		if (datosCargo == '0') this.existeCargo = false;
		else {
			this.cargoEmpleado.id = datosCargo.id;
			this.cargoEmpleado.nombre = datosCargo.nombre;
			this.cargoEmpleado.dependencia = datosCargo.dependencia;
			this.cargoEmpleado.periodoPago = datosCargo.periodoPago.nombre;
			this.cargoEmpleado.numCargos = datosCargo.numCargos;
			this.cargoEmpleado.ubicacionOrganigrama = datosCargo.ubicacionOrganigrama;
			this.cargoEmpleado.mision = datosCargo.mision;
			this.existeCargo = true;
			//console.log(this.cargoEmpleado);
		}
	}

	cargarAfiliaciones(datosAfiliacion: any) {
		this.afiliacionesArray = [];
		if (datosAfiliacion == '0') {
			this.existeAfiliacion = false;
		}
		else {
			this.afiliacionesArray = datosAfiliacion;
			this.dataTable.dataRows = this.afiliacionesArray;
			this.existeAfiliacion = true;
			//console.log(this.afiliacionesArray);
		}
	}

	validacionCamposVisibles(opc: any, valorBuscar: any) {
		let devolverCadena: string = '';
		switch (opc) {
			case 'genero':
				const generoNombre = this.generoArray.find(element => element.valor == valorBuscar);

				if (generoNombre) devolverCadena = generoNombre.nombre;
				else devolverCadena = valorBuscar;
				break;
			case 'grupoSanguineo':
				let gruposSanguineo = [
					{ nombre: 'A+', valor: 'Ap' },
					{ nombre: 'B+', valor: 'Bp' },
					{ nombre: 'O+', valor: 'Op' },
					{ nombre: 'AB+', valor: 'ABp' }
				];
				const grupoSan = gruposSanguineo.find(element => element.valor == valorBuscar);

				if (grupoSan) devolverCadena = grupoSan.nombre;
				else devolverCadena = valorBuscar;
				break;
		}
		return devolverCadena;
	}

	observarTexto(texto: any) {
		this.textoExtendido = texto == '' ? '- - -' : texto;
		$("#modalObservacion").modal("show");
	}

	limpiarFormulario() {
		this.buscarEmpleadoFormGroup.reset();
		this.dataTable.dataRows = [];
		this.afiliacionesArray = [];
		this.consultaExistente = true;
	}

	regresar() {
		this._router.navigate(["empleados/ficha-empleados/", this.empleadoId]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}
