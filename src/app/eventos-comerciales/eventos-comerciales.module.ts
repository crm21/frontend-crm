import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { EventosComercialesRoutes } from './eventos-comerciales.routing';
import { LoadingModule } from '../shared/loading/loading.module';
import { LoadingSpinnerModule } from '../shared/loading-spinner/loading-spinner.module';
import { RegistrarEventoComponent } from './registrar-evento/registrar-evento.component';
import { ListarAgendamientoComponent } from './listar-agendamiento/listar-agendamiento.component';
import { CalendarioEventosComponent } from './calendario-eventos/calendario-eventos.component';

@NgModule({
	declarations: [
		RegistrarEventoComponent,
		ListarAgendamientoComponent,
		CalendarioEventosComponent
	],
	imports: [
		CommonModule,
		RouterModule.forChild(EventosComercialesRoutes),
		FormsModule,
		MaterialModule,
		ReactiveFormsModule,
		LoadingModule,
		LoadingSpinnerModule
	],
	providers: [DatePipe]
})
export class EventosComercialesModule { }