import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketsRevisionComponent } from './tickets-revision.component';

describe('TicketsRevisionComponent', () => {
  let component: TicketsRevisionComponent;
  let fixture: ComponentFixture<TicketsRevisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketsRevisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketsRevisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
