import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteClientesMesComponent } from './reporte-clientes-mes.component';

describe('ReporteClientesMesComponent', () => {
  let component: ReporteClientesMesComponent;
  let fixture: ComponentFixture<ReporteClientesMesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteClientesMesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteClientesMesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
