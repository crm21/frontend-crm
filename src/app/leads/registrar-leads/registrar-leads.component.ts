import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from "@angular/router";
import { ClienteService } from '../../services/cliente/cliente.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { Cliente, ClienteLeads } from '../../models/cliente';
import swal from "sweetalert2";
import { LeadsService } from '../../services/leads/leads.service';

declare var $: any;
@Component({
	selector: 'app-registrar-leads',
	templateUrl: './registrar-leads.component.html',
	styleUrls: ['./registrar-leads.component.css']
})
export class RegistrarLeadsComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	infoUsuario: any;
	crearClienteForm: FormGroup;
	tipoDocumentosArray: any[] = [];
	tipoCanalIngresoArray: any[] = [];

	constructor(
		private _router: Router,
		private _formBuilder: FormBuilder,
		private _leadsService: LeadsService,
		private _parametrizacionService: ParametrizacionService,
		private _sweetAlertService: SweetAlertService
	) {
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			this.infoUsuario = identityData['data'];
		}
	}

	ngOnInit() {
		this.crearClienteForm = this._formBuilder.group({
			tipoDoc: [''],
			documento: ['', [Validators.pattern("[0-9]{5,13}$")]],
			nombres: ['', [Validators.required, Validators.pattern("^[a-zA-ZñÑ ]{1,80}$")]],
			apellidos: ['', [Validators.required, Validators.pattern("^[a-zA-ZñÑ ]{1,80}$")]],
			canalIngreso: ['', Validators.required],
			correo: ['', [Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
			telefono: ['', [Validators.pattern("[0-9]{1,15}$")]],
			observacion: ['']
		});
		if (localStorage.getItem('identity_crm')) {
			this.cargarInformacionSelect();
		}
	}

	async cargarInformacionSelect() {
		this.loading = true;
		this.tipoDocumentosArray = [];
		this.tipoCanalIngresoArray = [];
		
		let tipoDocumentosParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo Documento');
		let tipoCanalIngresoParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Canal de Ingreso');
		
		tipoDocumentosParametros['data'].forEach((element) => {
			this.tipoDocumentosArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		tipoCanalIngresoParametros['data'].forEach((element) => {
			this.tipoCanalIngresoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
		this.loading = false;
	}

	async onSubmitCrearCliente() {
		this.loading = true;

		let clienteLeadsDatos = new ClienteLeads(
			this.crearClienteForm.value.tipoDoc,
			this.crearClienteForm.value.documento,
			this.crearClienteForm.value.nombres,
			this.crearClienteForm.value.apellidos,
			this.crearClienteForm.value.correo,
			this.crearClienteForm.value.telefono,
			this.crearClienteForm.value.canalIngreso,
			this.crearClienteForm.value.observacion
		); //console.log(clienteLeadsDatos);

		const responseClienteLeads = await this._leadsService.registrarClienteLeads(clienteLeadsDatos);
		//console.log(responseClienteLeads);
		if (responseClienteLeads["status"] === "success" && responseClienteLeads["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseClienteLeads["message"], 'success')
				.then(result => {
					this.crearClienteForm.reset();
					//this.regresar();
				}).catch(swal.noop);
		}
		else if (responseClienteLeads["status"] === "success" && responseClienteLeads["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseClienteLeads['message'] + '<b>', 2000);
		}
		else if (responseClienteLeads["status"] === "error" && responseClienteLeads["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseClienteLeads['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseClienteLeads["message"], 'OK', 'error');
		}
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	regresar() {
		this._router.navigate(["leads"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}