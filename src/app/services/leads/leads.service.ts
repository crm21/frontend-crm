import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UsuarioService } from '../usuario/usuario.service';
import { ClienteLeads } from '../../models/cliente';

@Injectable({
	providedIn: 'root'
})
export class LeadsService {

	headers: any;
	url: string = '';
	token: string;

	constructor(
		private http: HttpClient,
		private _usuarioService: UsuarioService
	) {
		console.log('[Leads services is ready]...');
	}

	async listarLeadsDatos(pagination: string) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			let params = "?pagination=" + pagination;
			this.http.get(this.url + "/leads" + params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async registrarClienteLeads(clienteLeads: ClienteLeads) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			const json = JSON.stringify(clienteLeads);
			let params = "json=" + json;
			this.http.post(this.url + "/registrarLead", params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async asignarClienteAsesorLeads(idAsesor: string, idRegistro: string) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			let params = "idAsesor=" + idAsesor + "&idRegistro=" + idRegistro;
			this.http.post(this.url + "/asesorLead", params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async modificarClienteLeads(idLead: string, clienteLeadsActualizar: ClienteLeads) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			const json = JSON.stringify(clienteLeadsActualizar);
			let params = "json=" + json + "&idLead=" + idLead;
			this.http.put(this.url + "/actualizarLead", params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}
}