import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { from } from 'rxjs';
import swal from 'sweetalert2';
import { UsuarioService } from '../usuario/usuario.service';
import { environment } from '../../../environments/environment';
declare var $: any;

@Injectable({
	providedIn: 'root'
})
export class SweetAlertService {

	resp: boolean;

	constructor(
		private _router: Router,
		private _usuarioService: UsuarioService
	) { }

	datosEmpresas(empresas: string): string[] {
		let datoEmpresa: string[] = [];

		switch (empresas) {
			case 'A':
				datoEmpresa = [environment.correoEmpresaAutodenar, environment.telefonoEmpresaAutodenar];
				return datoEmpresa;
				break;
			case 'M':
				datoEmpresa = [environment.correoEmpresaMotodenar, environment.telefonoEmpresaMotodenar];
				return datoEmpresa;
				break;
			case 'T':
				datoEmpresa = [environment.correoEmpresaAutomotora, environment.telefonoEmpresaAutomotora];
				return datoEmpresa;
				break;
			default:
				datoEmpresa = [environment.correoEmpresaAutodenar, environment.telefonoEmpresaAutodenar];
				return datoEmpresa;
				break;
		}
	}

	descargarEstadisticas(nombreArchivo: string, tipoEstadistica: string, documento: string) {
		let ruta: string = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		let url = ruta + '/excel/' + nombreArchivo + '.xlsx';
		let name = 'estadistica' + tipoEstadistica + documento + '.xlsx';
		let link = document.createElement("a");

		link.download = name;
		link.href = url;
		document.body.appendChild(link);
		link.click();
		document.body.removeChild(link);
	}

	fondoNegro(opc: number) {
		let classEtiqueta = document.getElementsByClassName('container-token-black')[0];
		if (opc == 1) {
			setTimeout(function () {
				classEtiqueta.classList.add('token-black');
				//console.log(classEtiqueta);
			}, 100);
		} else {
			setTimeout(function () {
				classEtiqueta.classList.remove('token-black');
				//console.log(classEtiqueta);
			}, 500);
		}
	}

	cerrarSesionVenceToken(mensaje: string) {
		this.fondoNegro(1);
		swal(
			"Su sesión expiró",
			mensaje,
			"error"
		).then((result: any) => {
			let opcionEmpresa = localStorage.getItem('business');
			localStorage.clear();
			localStorage.setItem('business', opcionEmpresa);
			localStorage.setItem('identity_crm', null);
			this.fondoNegro(2);
			if (opcionEmpresa == 'A') {
				this._router.navigate(['/autodenar']);
			}
			else if (opcionEmpresa == 'M') {
				this._router.navigate(['/motodenar']);
			}
			else if (opcionEmpresa == 'T') {
				this._router.navigate(['/automotora']);
			}
			else {
				this._router.navigate(['/empresas']);
			}
		}).catch(swal.noop);
	}

	alertInput(titulo, textoHtml, textoBtnOK, textoBtnCancelar) {
		swal({
			title: titulo,
			html: '<div class="form-group">' +
				'<label class="text-dark" for="FormControlLabel">' + textoHtml + '</label>' +
				'<input id="input-field" placeholder="Observaciones..." type="text" class="form-control" />' +
				'</div>',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: textoBtnOK,
			cancelButtonText: textoBtnCancelar,
			buttonsStyling: false
		}).then((result) => {
			if (result.value) { }
			else { }
		}).catch(swal.noop);
	}

	alertConfirmacion() {
		swal({
			title: '¿Esta seguro de eliminar?',
			text: "Tenga en cuenta que esta acción no se podrá revertir",
			type: 'warning',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: 'Si, eliminar!',
			cancelButtonText: 'Cancelar',
			buttonsStyling: true,
			reverseButtons: true
		}).then((result) => {
		}).catch(swal.noop);
	}

	alertGeneral(titulo: string = "Bien!", texto: any, textoBtn: string = "OK", tipo) {
		swal({
			title: titulo,
			text: texto,
			buttonsStyling: false,
			confirmButtonClass: "btn btn-primary",
			confirmButtonText: textoBtn,
			type: tipo
		}).catch(swal.noop)
	}

	alertCloseAutomatico(titulo: string = 'close', descripcion: string = 'Ok!', tiempo: number = 2000) {
		swal({
			title: titulo,
			text: descripcion,
			timer: tiempo,
			showConfirmButton: false
		}).catch(swal.noop);
	}

	showNotification(from: any, align: any, type: any, icon: any, message: any, timer) {
		// const type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary'];
		$.notify({
			icon: icon,
			message: message,
		}, {
			type: type,
			timer: timer,
			placement: {
				from: from,
				align: align
			},
			template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0} alert-with-icon" role="alert">' +
				'<button mat-raised-button type="button" aria-hidden="true" class="close" data-notify="dismiss">' +
				'<i class="material-icons">close</i>' +
				'</button>' +
				'<i class="material-icons" data-notify="icon">notifications</i> ' +
				'<span data-notify="title">{1}</span> ' +
				'<span data-notify="message">{2}</span>' +
				'<div class="progress" data-notify="progressbar">' +
				'<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
				'</div>' +
				'<a href="{3}" target="{4}" data-notify="url"></a>' +
				'</div>'
		});
	}
}