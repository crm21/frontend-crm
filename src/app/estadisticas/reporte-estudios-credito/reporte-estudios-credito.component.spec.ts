import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteEstudiosCreditoComponent } from './reporte-estudios-credito.component';

describe('ReporteEstudiosCreditoComponent', () => {
  let component: ReporteEstudiosCreditoComponent;
  let fixture: ComponentFixture<ReporteEstudiosCreditoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteEstudiosCreditoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteEstudiosCreditoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
