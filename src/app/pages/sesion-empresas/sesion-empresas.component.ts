import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { Login } from '../../models/login';
import { Router, ActivatedRoute } from '@angular/router';
import { UsuarioService } from '../../services/usuario/usuario.service';
import swal from 'sweetalert2';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { environment } from '../../../environments/environment';

declare var $: any;

@Component({
	selector: 'app-sesion-empresas',
	templateUrl: './sesion-empresas.component.html',
	styleUrls: ['./sesion-empresas.component.css']
})
export class SesionEmpresasComponent implements OnInit {

	private toggleButton: any;
	private sidebarVisible: boolean;
	private nativeElement: Node;
	loading = false;
	test: Date = new Date();
	login: Login;
	hide = true;
	version = environment.version;
	nombresEmpresas: string = '';
	letraEmpresa: string = '';
	imgNombreEmpresa: string = '';

	constructor(
		private element: ElementRef,
		private _router: Router,
		private _usuarioService: UsuarioService,
		private _sweetAlertService: SweetAlertService,
		private _route: ActivatedRoute
	) {

		this.nativeElement = element.nativeElement;
		this.sidebarVisible = false;
		let opcionEmpresa = this._route.snapshot.data.opc;
		localStorage.setItem("business", opcionEmpresa);
		this.consultarNombreEmpresa(localStorage.getItem('business'));
		this.letraEmpresa = localStorage.getItem('business');
	}

	ngOnInit() {
		var navbar: HTMLElement = this.element.nativeElement;
		this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
		const body = document.getElementsByTagName('body')[0];
		body.classList.add('login-page');
		body.classList.add('off-canvas-sidebar');
		this.checkSesion();
	}

	sidebarToggle() {
		var toggleButton = this.toggleButton;
		var body = document.getElementsByTagName('body')[0];
		var sidebar = document.getElementsByClassName('navbar-collapse')[0];
		if (this.sidebarVisible == false) {
			setTimeout(function () {
				toggleButton.classList.add('toggled');
			}, 500);
			body.classList.add('nav-open');
			this.sidebarVisible = true;
		} else {
			this.toggleButton.classList.remove('toggled');
			this.sidebarVisible = false;
			body.classList.remove('nav-open');
		}
	}

	ngOnDestroy() {
		const body = document.getElementsByTagName('body')[0];
		body.classList.remove('login-page');
		body.classList.remove('off-canvas-sidebar');
	}

	checkSesion() {
		let checkToken = this._usuarioService.isToken();

		if (checkToken) this._router.navigate(['/dashboard']);
	}

	consultarNombreEmpresa(nombreEmpresa: any) {
		this.nombresEmpresas = this._usuarioService.nombresEmpresa(nombreEmpresa);
		switch (nombreEmpresa) {
			case 'A':
				this.imgNombreEmpresa = './assets/img/empresa-autodenar-blanco.png';
				break;
			case 'M':
				this.imgNombreEmpresa = './assets/img/empresa-motodenar.png';
				break;
			case 'T':
				this.imgNombreEmpresa = './assets/img/empresa-automotora-blanco.png';
				break;
			case 'D':
				this.imgNombreEmpresa = './assets/img/empresa-autodenar-blanco.png';
				break;
			default:
				this.imgNombreEmpresa = './assets/img/empresa-autodenar-blanco.png';
				break;
		}
	}

	ngAfterViewInit() {
		const breakCards = true;
		if (breakCards === true) {
			// We break the cards headers if there is too much stress on them :-)
			$('[data-header-animation="true"]').each(function () {
				const $fix_button = $(this);
				const $card = $(this).parent('.card');
				$card.find('.fix-broken-card').click(function () {
					const $header = $(this).parent().parent().siblings('.card-header, .card-image');
					$header.removeClass('hinge').addClass('fadeInDown');

					$card.attr('data-count', 0);

					setTimeout(function () {
						$header.removeClass('fadeInDown animate');
					}, 480);
				});
			});
		}
	}

	onClickSesionUsuario(numero: number) {
		switch (this.letraEmpresa) {
			case 'A':
				if (numero === 1) {
					this._router.navigate(["autodenar/clientes"]);
				}
				else if (numero === 2) {
					this._router.navigate(["autodenar/colaboradores"]);
				}
				else if (numero === 3) {
					this._router.navigate(["autodenar/aseguradora"]);
				}
				break;
			case 'M':
				if (numero === 1) {
					this._router.navigate(["motodenar/clientes"]);
				}
				else if (numero === 2) {
					this._router.navigate(["motodenar/colaboradores"]);
				}
				else if (numero === 3) {
					this._router.navigate(["motodenar/aseguradora"]);
				}
				break;
			case 'T':
				if (numero === 1) {
					this._router.navigate(["automotora/clientes"]);
				}
				else if (numero === 2) {
					this._router.navigate(["automotora/colaboradores"]);
				}
				else if (numero === 3) {
					this._router.navigate(["automotora/aseguradora"]);
				}
				break;
			default:
				this._router.navigate(["empresas"]);
				break;
		}
	}
}