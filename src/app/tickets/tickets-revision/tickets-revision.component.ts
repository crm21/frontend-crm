import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';
import swal from 'sweetalert2';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { TicketsService } from '../../services/tickets/tickets.service';
import { TicketAprobado, ReasignarTicketUsuario } from '../../models/ticket';
import { PermisosService } from '../../services/permisos/permisos.service';
import { UsuarioService } from '../../services/usuario/usuario.service';

declare var $: any;

@Component({
	selector: 'app-tickets-revision',
	templateUrl: './tickets-revision.component.html',
	styleUrls: ['./tickets-revision.component.css']
})
export class TicketsRevisionComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	mostraBotonContestacion: boolean = false;
	estadoTicket: boolean = false;
	contestarTicketUsuarioForm: FormGroup;
	estadosArray: any[] = [];
	rolesExistentes: any[] = [];
	usuarioArray: any[] = [];
	textoExtendido: string = '';
	idTicket: string;
	permisosContestacion: any[] = ['A', 'GA', 'GG'];
	permisosCerrarTicket: any[] = ['A'];
	ticketUsuario = {
		id: null,
		consecutivo: null,
		categoriaSolicitud: null,
		solicitadoPor: null,
		fechaSolicitud: null,
		descripcionSolicitud: null,
		aprobadoPor: null,
		fechaAprobacion: null,
		anexo: null,
		descripcionAprobacion: null,
		estado: null,
		estadoValor: null,
		colorEstado: null,
		asignadoA: null
	};
	buscarRol: any = {
		rolUsuario: null
	};
	reasignacionT = {
		idUsuario: null,
		nombresUsuario: null,
		idTicket: null,
		descripcionReasignacion: null
	};
	rolValor: string = '';
	documento: string = '';
	botonCerrarTicket: boolean = false;

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _formBuilder: FormBuilder,
		private _ticketsService: TicketsService,
		private _permisosService: PermisosService,
		private _usuariosService: UsuarioService,
		private _sweetAlertService: SweetAlertService,
		private _parametrizacionService: ParametrizacionService
	) {
		this.idTicket = this._route.snapshot.paramMap.get('idTicket');
		this.reasignacionT.idTicket = this.idTicket;
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);
			this.rolValor = identityData['data'].rol.valor;

			if (this.permisosContestacion.includes(this.rolValor)) {
				this.mostraBotonContestacion = true;
			}
		}
	}

	ngOnInit() {
		this.contestarTicketUsuarioForm = this._formBuilder.group({
			estado: ['', Validators.required],
			descripcionContestacion: ['', Validators.required]
		});
		if (localStorage.getItem('identity_crm')) {
			if (this.permisosCerrarTicket.includes(this.rolValor)) {
				this.botonCerrarTicket = true;
			}
			this.listarTickets();
		}
	}

	/* Inicia Listar Tickets */
	async listarTickets() {
		this.loading = true;

		const responseListarTickets = await this._ticketsService.listarTickets('2', '', this.idTicket);
		//console.log(responseListarTickets);
		if (responseListarTickets['status'] == "success" && responseListarTickets['code'] == "200") {
			this.loading = false;
			this.llenarTabla(responseListarTickets['data']);
		}
		else if (responseListarTickets['status'] == "success" && responseListarTickets['code'] == "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'success', 'warning', '<b>' + responseListarTickets['message'] + '<b>', 2000);
		}
		else if (responseListarTickets["status"] === "error" && responseListarTickets["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseListarTickets['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseListarTickets['message'], 'error');
		}
	}

	llenarTabla(tickets: any) {
		//console.log(tickets);
		this.ticketUsuario.id = tickets.id;
		this.ticketUsuario.consecutivo = tickets.consecutivoTicket;
		this.ticketUsuario.categoriaSolicitud = tickets.categoriaSolicitud.nombre;
		this.ticketUsuario.solicitadoPor = tickets.solicitadoPor.nombres + ' ' + tickets.solicitadoPor.apellidos;
		this.ticketUsuario.fechaSolicitud = tickets.fechaSolicitud;
		this.ticketUsuario.descripcionSolicitud = tickets.descripcionSolicitud;
		this.ticketUsuario.aprobadoPor = tickets.aprobadoPor == null ? '- - -' : tickets.aprobadoPor.nombres + ' ' + tickets.aprobadoPor.apellidos;
		this.ticketUsuario.fechaAprobacion = tickets.fechaAprobacion == null ? '- - -' : tickets.fechaAprobacion;
		this.ticketUsuario.anexo = tickets.anexo;
		this.ticketUsuario.descripcionAprobacion = tickets.descripcionAprobacion == null ? '- - -' : tickets.descripcionAprobacion;
		this.ticketUsuario.estado = tickets.estado.valor == 'cerradoT' ? 'aprobado <br>' + tickets.estado.nombre : tickets.estado.nombre;
		this.ticketUsuario.estadoValor = tickets.estado.valor;
		this.ticketUsuario.colorEstado = this.darColorEstadoEvento(tickets.estado.valor);
		this.ticketUsuario.asignadoA = tickets.asignadoA.nombres + ' ' + tickets.asignadoA.apellidos;
		//console.log(this.ticketUsuario);
	}
	/* Finaliza Listar Tickets */

	/* Inicia cargar select */
	async cargarInformacionSelect() {
		this.estadosArray = [];
		let estadosParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Estado Ticket');

		estadosParametros['data'].forEach((element) => {
			this.estadosArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
	}
	/* Finaliza cargar select */

	/* Inicia Responder Ticket */
	responderTicket() {
		this.cargarInformacionSelect();
		$("#modalResponder").modal("show");
	}

	async onSubmitContestarTicketsUsuario() {
		this.loading = true;
		let contestarTicket = new TicketAprobado(
			this.contestarTicketUsuarioForm.value.estado,
			this.contestarTicketUsuarioForm.value.descripcionContestacion
		); //console.log(contestarTicket);

		const responseCancelarTicket = await this._ticketsService.aprobadoTicketUsuario(this.idTicket, contestarTicket);
		//console.log(responseCancelarTicket);
		if (responseCancelarTicket["status"] === "success" && responseCancelarTicket["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseCancelarTicket["message"], 'success')
				.then(result => {
					this.contestarTicketUsuarioForm.reset();
					$("#modalResponder").modal("hide");
					this.listarTickets();
				}).catch(swal.noop);
		}
		else if (responseCancelarTicket["status"] === "success" && responseCancelarTicket["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseCancelarTicket["message"], 'OK', 'warning');
		}
		else if (responseCancelarTicket["status"] === "error" && responseCancelarTicket["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseCancelarTicket['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseCancelarTicket["message"], 'OK', 'error');
		}
	}
	/* Finaliza Responder Ticket */

	/* Inicia Reasignacion de Ticket */
	reasignarTicket() {
		this.listarRolesExistentes();
		$("#modalReasignarTicket").modal("show");
	}

	async listarRolesExistentes() {
		this.loading = true;
		const responseRoles = await this._permisosService.listarRoles();
		//console.log(responseRoles);
		this.loading = false;
		if (responseRoles['status'] == "success" && responseRoles['code'] == "200") {
			this.rolesExistentes = responseRoles['data'];
		}
		else if (responseRoles['status'] == "success" && responseRoles['code'] == "300") {
			swal('Advertencia!', responseRoles['message'], 'warning');
		}
		else if (responseRoles["status"] === "error" && responseRoles["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseRoles['message']);
		}
		else {
			swal('¡Error!', responseRoles['message'], 'error');
		}
	}

	onSubmitBuscarUsuariosFiltros() {
		let filtroBusqueda = this.buscarRol.rolUsuario;
		let opcion: number = 4;
		//console.log(`Buscar: ${opcion},  ${filtroBusqueda}`);
		this.listarUsuarios(opcion, filtroBusqueda);
	}

	async listarUsuarios(opcionBusqueda: number, filtroBusqueda: string) {
		this.loading = true;
		const responseUsuarios = await this._usuariosService.listarUsuarios('' + opcionBusqueda, '1', filtroBusqueda);
		//console.log(responseUsuarios);
		if (responseUsuarios["status"] === "success" && responseUsuarios["code"] === "200") {
			this.loading = false;
			this.usuarioArray = responseUsuarios['data'];
		}
		else if (responseUsuarios["status"] === "success" && responseUsuarios["code"] === "300") {
			this.loading = false;
			this.usuarioArray = [];
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseUsuarios['message'] + '<b>', 2000);
		}
		else if (responseUsuarios["status"] === "error" && responseUsuarios["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseUsuarios['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'error', 'warning', '<b>' + responseUsuarios['message'] + '<b>', 2000);
		}
	}

	selectListarUsuarios() {
		document.getElementById("myDropdown").classList.toggle("show");
	}

	filtroUsuarios(x: any) {
		let input, filter, ul, li, a, i;
		input = document.getElementById("usuarioReaginacion");
		filter = input.value.toUpperCase();
		let div = document.getElementById("myDropdown");
		a = div.getElementsByTagName("a");

		for (i = 0; i < a.length; i++) {
			let txtValue = a[i].textContent || a[i].innerText;
			if (txtValue.toUpperCase().indexOf(filter) > -1) {
				a[i].style.display = "";
			} else {
				a[i].style.display = "none";
			}
		}
	}

	seleccionarUsuario(usuario: any) {
		this.reasignacionT.idUsuario = '' + usuario.id;
		this.reasignacionT.nombresUsuario = usuario.nombres;
		document.getElementById("myDropdown").classList.toggle("show");
		(document.getElementById('usuarioReaginacion') as HTMLInputElement).value = usuario.nombres;
	}

	async onSubmitReasignarTicket() {
		this.loading = true;

		let ticketReasignar = new ReasignarTicketUsuario(
			this.reasignacionT.idTicket,
			this.reasignacionT.idUsuario,
			this.reasignacionT.descripcionReasignacion
		); //console.log(ticketReasignar);

		const responseReasignarTicket = await this._ticketsService.reasignarTicketUsuarios(ticketReasignar);
		//console.log(responseReasignarTicket);
		if (responseReasignarTicket["status"] === "success" && responseReasignarTicket["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseReasignarTicket["message"], 'success')
				.then(result => {
					this.limpiarCamposReasignarTicket();
					$("#modalReasignarTicket").modal("hide");
					//this._router.navigate(["tickets/revision/", this.idTicket]);
					this.ngOnInit();
				}).catch(swal.noop);
		}
		else if (responseReasignarTicket["status"] === "success" && responseReasignarTicket["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseReasignarTicket["message"], 'OK', 'warning');
		}
		else if (responseReasignarTicket["status"] === "error" && responseReasignarTicket["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseReasignarTicket['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseReasignarTicket["message"], 'OK', 'error');
		}
	}

	limpiarCamposReasignarTicket() {
		this.buscarRol.rolUsuario = '';
		this.reasignacionT.idUsuario = null;
		this.reasignacionT.nombresUsuario = null;
		this.reasignacionT.descripcionReasignacion = '';
		this.rolesExistentes = [];
		(document.getElementById('usuarioReaginacion') as HTMLInputElement).value = '';
		(document.getElementById('rolUsuario') as HTMLInputElement).value = '';
		(document.getElementById('descripcionReasignacion') as HTMLInputElement).value = '';
	}
	/* Finaliza Reasignacion de Ticket */

	/* Inicia Cerrar Ticket */
	cerrarTicket() {
		swal({
			title: 'Escribir una observación cerrar el ticket',
			html: '<div class="form-group">' +
				'<input id="input-field" type="text" class="form-control" />' +
				'</div>',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			buttonsStyling: false
		})
			.then((result) => {
				if (result.value) {
					let observacionCierre = $('#input-field').val();
					//console.log(observacionCierre);
					this.cerrarTicketAdmin(observacionCierre);
				}
			}).catch(swal.noop);
	}

	async cerrarTicketAdmin(observacion: string) {
		this.loading = true;
		const responseCierreTicket = await this._ticketsService.cerrarTicketUsuarios('' + this.idTicket, 'cerradoT', observacion);
		//console.log(responseCierreTicket);
		if (responseCierreTicket["status"] === "success" && responseCierreTicket["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseCierreTicket["message"], 'success')
				.then(result => {
					this.ngOnInit();
				}).catch(swal.noop);
		}
		else if (responseCierreTicket["status"] === "success" && responseCierreTicket["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseCierreTicket["message"], 'OK', 'warning');
		}
		else if (responseCierreTicket["status"] === "error" && responseCierreTicket["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseCierreTicket['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseCierreTicket["message"], 'OK', 'error');
		}
	}
	/* Finalizar Cerrar Ticket */

	/* Inicio Descargar Archivo Anexo */
	descargarAnexoTicket() {
		let ruta: string = this._usuariosService.comprobarEmpresaServicios(localStorage.getItem('business'));
		let url = ruta + '/uploads/' + this.ticketUsuario.anexo;
		let name = 'anexo';
		let link = document.createElement("a");

		//link.download = name;
		link.target = '_blank';
		link.href = url;
		document.body.appendChild(link);
		link.click();
		document.body.removeChild(link);
	}
	/* Fin Descargar Archivo Anexo */
	
	observarTexto(texto: any) {
		this.textoExtendido = texto;
		$("#modalObservacion").modal("show");
	}

	darColorEstadoEvento(estado: string): string {
		switch (estado) {
			case 'canceladoT':
				this.estadoTicket = false;
				return 'optimus-black';
				break;
			case 'rechazadoT':
				this.estadoTicket = false;
				return 'danger';
				break;
			case 'aprobadoT':
				this.estadoTicket = false;
				return 'success';
				break;
			case 'cerradoT':
				this.estadoTicket = false;
				return 'success';
				break;
			case 'enRevisionT':
				this.estadoTicket = true;
				return 'warning';
				break;
			case 'pendienteRevisionT':
				this.estadoTicket = true;
				return 'default';
				break;
			default:
				this.estadoTicket = false;
				return 'default';
				break;
		}
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	revisarTicket(rowId: string) {
		this._router.navigate(["tickets/revision/", rowId]);
	}

	regresar() {
		this._router.navigate(["tickets/listar"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}