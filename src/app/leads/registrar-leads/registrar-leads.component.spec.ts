import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarLeadsComponent } from './registrar-leads.component';

describe('RegistrarLeadsComponent', () => {
  let component: RegistrarLeadsComponent;
  let fixture: ComponentFixture<RegistrarLeadsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarLeadsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarLeadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
