import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarAgendaClienteComponent } from './registrar-agenda-cliente.component';

describe('RegistrarAgendaClienteComponent', () => {
  let component: RegistrarAgendaClienteComponent;
  let fixture: ComponentFixture<RegistrarAgendaClienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarAgendaClienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarAgendaClienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
