import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarDisponibilidadAsesoresComponent } from './listar-disponibilidad-asesores.component';

describe('ListarDisponibilidadAsesoresComponent', () => {
  let component: ListarDisponibilidadAsesoresComponent;
  let fixture: ComponentFixture<ListarDisponibilidadAsesoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarDisponibilidadAsesoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarDisponibilidadAsesoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
