import { Routes } from '@angular/router';

import { DashboardComponent } from './dashboard.component';

export const DashboardRoutes: Routes = [
	{

		path: '',
		children: [
			{
				path: 'dashboard',
				component: DashboardComponent,
				data: {
					opc: 1
				}
			}, {
				path: 'dashboard-2/:valorCambio',
				component: DashboardComponent,
				data: {
					opc: 2
				}
			}, {
				path: 'dashboard-3/:valorCambio',
				component: DashboardComponent,
				data: {
					opc: 3
				}
			}
		]
	}
];
