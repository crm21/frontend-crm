import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarAgendaTallerComponent } from './registrar-agenda-taller.component';

describe('RegistrarAgendaTallerComponent', () => {
  let component: RegistrarAgendaTallerComponent;
  let fixture: ComponentFixture<RegistrarAgendaTallerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarAgendaTallerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarAgendaTallerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
