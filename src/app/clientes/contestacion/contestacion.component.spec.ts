import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContestacionComponent } from './contestacion.component';

describe('ContestacionComponent', () => {
  let component: ContestacionComponent;
  let fixture: ComponentFixture<ContestacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContestacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContestacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
