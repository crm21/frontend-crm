import { Component, OnInit, Renderer2, ViewChild, ElementRef, Directive } from '@angular/core';
import { ROUTES } from '../.././sidebar/sidebar.component';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Location, LocationStrategy, PathLocationStrategy, UpperCasePipe } from '@angular/common';
import { environment } from '../../../environments/environment';
import swal from "sweetalert2";
import { UsuarioService } from '../../services/usuario/usuario.service';
import { Login } from '../../models/login';

const misc: any = {
	navbar_menu_visible: 0,
	active_collapse: true,
	disabled_collapse_init: 0,
};

declare var $: any;
@Component({
	selector: 'app-navbar-cmp',
	templateUrl: 'navbar.component.html'
})

export class NavbarComponent implements OnInit {
	public cargaLoading: boolean = false;
	private listTitles: any[];
	location: Location;
	mobile_menu_visible: any = 0;
	private nativeElement: Node;
	private toggleButton: any;
	private sidebarVisible: boolean;
	private _router: Subscription;

	public testMode = environment.project;
	public nombreEmpresa = '';
	public permisoCambiarEmpresa = true;
	public permisoCambiarEmpresaArray = ['A', 'GG', 'GA'];
	public permisoSistema = {
		crm: null,
		erp: null
	};
	private login: Login;

	@ViewChild('app-navbar-cmp', { static: false }) button: any;

	constructor(
		location: Location,
		private renderer: Renderer2,
		private element: ElementRef,
		private router: Router,
		public _routerPag: Router,
		public _usuarioService: UsuarioService
	) {
		this.login = new Login('', '', '', '2');
		this.location = location;
		this.nativeElement = element.nativeElement;
		this.sidebarVisible = false;
		this.nombreEmpresa = _usuarioService.nombresEmpresa(localStorage.getItem('business'));
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);
			let rolUsuario = identityData['data'].rol.valor;
			this.permisoSistema.crm = identityData['data'].permisos.crm.acceso;
			this.permisoSistema.erp = identityData['data'].permisos.erp.acceso;

			if (this.permisoCambiarEmpresaArray.includes(rolUsuario)) {
				this.permisoCambiarEmpresa = true;
			}
			else this.permisoCambiarEmpresa = false;
		}
	}
	minimizeSidebar() {
		const body = document.getElementsByTagName('body')[0];

		if (misc.sidebar_mini_active === true) {
			body.classList.remove('sidebar-mini');
			misc.sidebar_mini_active = false;

		} else {
			setTimeout(function () {
				body.classList.add('sidebar-mini');

				misc.sidebar_mini_active = true;
			}, 300);
		}

		// we simulate the window Resize so the charts will get updated in realtime.
		const simulateWindowResize = setInterval(function () {
			window.dispatchEvent(new Event('resize'));
		}, 180);

		// we stop the simulation of Window Resize after the animations are completed
		setTimeout(function () {
			clearInterval(simulateWindowResize);
		}, 1000);
	}
	hideSidebar() {
		const body = document.getElementsByTagName('body')[0];
		const sidebar = document.getElementsByClassName('sidebar')[0];

		if (misc.hide_sidebar_active === true) {
			setTimeout(function () {
				body.classList.remove('hide-sidebar');
				misc.hide_sidebar_active = false;
			}, 300);
			setTimeout(function () {
				sidebar.classList.remove('animation');
			}, 600);
			sidebar.classList.add('animation');

		} else {
			setTimeout(function () {
				body.classList.add('hide-sidebar');
				// $('.sidebar').addClass('animation');
				misc.hide_sidebar_active = true;
			}, 300);
		}

		// we simulate the window Resize so the charts will get updated in realtime.
		const simulateWindowResize = setInterval(function () {
			window.dispatchEvent(new Event('resize'));
		}, 180);

		// we stop the simulation of Window Resize after the animations are completed
		setTimeout(function () {
			clearInterval(simulateWindowResize);
		}, 1000);
	}

	ngOnInit() {

		this.listTitles = ROUTES.filter(listTitle => listTitle);

		const navbar: HTMLElement = this.element.nativeElement;
		const body = document.getElementsByTagName('body')[0];
		this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
		if (body.classList.contains('sidebar-mini')) {
			misc.sidebar_mini_active = true;
		}
		if (body.classList.contains('hide-sidebar')) {
			misc.hide_sidebar_active = true;
		}
		this._router = this.router.events.filter(event => event instanceof NavigationEnd).subscribe((event: NavigationEnd) => {
			this.sidebarClose();

			const $layer = document.getElementsByClassName('close-layer')[0];
			if ($layer) {
				$layer.remove();
			}
		});
	}
	onResize(event) {
		if ($(window).width() > 991) {
			return false;
		}
		return true;
	}
	sidebarOpen() {
		var $toggle = document.getElementsByClassName('navbar-toggler')[0];
		const toggleButton = this.toggleButton;
		const body = document.getElementsByTagName('body')[0];
		setTimeout(function () {
			toggleButton.classList.add('toggled');
		}, 500);
		body.classList.add('nav-open');
		setTimeout(function () {
			$toggle.classList.add('toggled');
		}, 430);

		var $layer = document.createElement('div');
		$layer.setAttribute('class', 'close-layer');


		if (body.querySelectorAll('.main-panel')) {
			document.getElementsByClassName('main-panel')[0].appendChild($layer);
		} else if (body.classList.contains('off-canvas-sidebar')) {
			document.getElementsByClassName('wrapper-full-page')[0].appendChild($layer);
		}

		setTimeout(function () {
			$layer.classList.add('visible');
		}, 100);

		$layer.onclick = function () { //asign a function
			body.classList.remove('nav-open');
			this.mobile_menu_visible = 0;
			this.sidebarVisible = false;

			$layer.classList.remove('visible');
			setTimeout(function () {
				$layer.remove();
				$toggle.classList.remove('toggled');
			}, 400);
		}.bind(this);

		body.classList.add('nav-open');
		this.mobile_menu_visible = 1;
		this.sidebarVisible = true;
	};
	sidebarClose() {
		var $toggle = document.getElementsByClassName('navbar-toggler')[0];
		const body = document.getElementsByTagName('body')[0];
		this.toggleButton.classList.remove('toggled');
		var $layer = document.createElement('div');
		$layer.setAttribute('class', 'close-layer');

		this.sidebarVisible = false;
		body.classList.remove('nav-open');
		// $('html').removeClass('nav-open');
		body.classList.remove('nav-open');
		if ($layer) {
			$layer.remove();
		}

		setTimeout(function () {
			$toggle.classList.remove('toggled');
		}, 400);

		this.mobile_menu_visible = 0;
	};
	sidebarToggle() {
		if (this.sidebarVisible === false) {
			this.sidebarOpen();
		} else {
			this.sidebarClose();
		}
	}

	getTitle() {
		var titlee = this.location.prepareExternalUrl(this.location.path());
		if (titlee.charAt(0) === '#') {
			titlee = titlee.slice(1);
		}
		for (let i = 0; i < this.listTitles.length; i++) {
			if (this.listTitles[i].type === "link" && this.listTitles[i].path === titlee) {
				return this.listTitles[i].title;
			} else if (this.listTitles[i].type === "sub") {
				for (let j = 0; j < this.listTitles[i].children.length; j++) {
					let subtitle = this.listTitles[i].path + '/' + this.listTitles[i].children[j].path;
					// console.log(subtitle)
					// console.log(titlee)
					if (subtitle === titlee) {
						return this.listTitles[i].children[j].title;
					}
				}
			}
		}
		return 'Dashboard';
	}
	getPath() {
		return this.location.prepareExternalUrl(this.location.path());
	}

	cambiarSistema(valorSistema: any) {
		let sistema = localStorage.getItem('system');
		sistema = atob(sistema);

		if (valorSistema == sistema) {
			let nombre = valorSistema == 'crm' ? 'CRM' : 'ERP';
			swal(
				"Advertencia!",
				"Usted ya se encuentra en el sistema " + nombre,
				"warning"
			);
		} else {
			swal({
				title: "Cambiar de Sistema",
				text: "¿Esta seguro de que desea cambiar de sistema?",
				type: "info",
				showCancelButton: true,
				confirmButtonClass: "btn btn-success",
				cancelButtonClass: "btn btn-danger",
				confirmButtonText: "Si",
				cancelButtonText: "Cancelar",
				buttonsStyling: true,
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					this.cargaLoading = true;
					this._routerPag.navigate(['/dashboard']);
					localStorage.setItem("system", btoa(valorSistema));
					setTimeout(function () {
						window.location.reload();
						this.cargaLoading = false;
					}, 2500);

				}
			});
		}
	}

	cambiarEmpresa(valorEmpresa: any) {
		let empresa = localStorage.getItem('business');

		if (valorEmpresa == empresa) {
			swal(
				"Advertencia!",
				"Usted ya se encuentra en la empresa " + this.nombreEmpresa,
				"warning"
			);
		} else {
			swal({
				title: "Cambiar de Empresa",
				text: "¿Esta seguro de que desea cambiar de Empresa?",
				type: "info",
				showCancelButton: true,
				confirmButtonClass: "btn btn-success",
				cancelButtonClass: "btn btn-danger",
				confirmButtonText: "Si",
				cancelButtonText: "Cancelar",
				buttonsStyling: true,
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					this.cargaLoading = true;
					this._routerPag.navigate(['/dashboard']);
					localStorage.setItem("business", valorEmpresa);
					this.login.empresas = localStorage.getItem("business");
					this.iniciarSesionEmpresa();
					setTimeout(function () {
						window.location.reload();
						this.cargaLoading = false;
					}, 2500);

				}
			});
		}
	}

	async iniciarSesionEmpresa() {
		
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			this.login.password = '' + identityData['data'].documento;
			this.login.opc = '2';
						
			if (!this.login.empresas) {
				this.router.navigate(['/empresas']);
			} else {
				const responseLogin: any = await this._usuarioService.iniciarSesion(this.login);
				let base_64_data = atob(responseLogin);
				let identityData = JSON.parse(base_64_data);
				//console.log(identityData);
				if (identityData['status'] == "success") {
					const responseDatosLoginToken = await this._usuarioService.iniciarSesion(this.login, true);
	
					let token: any = responseDatosLoginToken;
	
					// Guardar token en el localstorage
					localStorage.setItem("system", btoa("crm"));
					localStorage.setItem("token_crm", token);
					localStorage.setItem('identity_crm', responseLogin);
					
					this.router.navigate(['/dashboard']);
				} else {
					swal('Advertencia!', identityData['message'], 'warning');
				}
			}
		}
	}
}
