export class RegistroCliente {
    constructor(
        public nombres: string,
        public apellidos: string,
        public correo: string,
        public password: string,
        public confirmPass: string,
        public correoEmpresa: string,
        public telefonoEmpresa: string
    ) { }
}