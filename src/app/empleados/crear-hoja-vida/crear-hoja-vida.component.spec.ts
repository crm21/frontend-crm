import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearHojaVidaComponent } from './crear-hoja-vida.component';

describe('CrearHojaVidaComponent', () => {
  let component: CrearHojaVidaComponent;
  let fixture: ComponentFixture<CrearHojaVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearHojaVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearHojaVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
