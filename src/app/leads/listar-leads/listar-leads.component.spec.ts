import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarLeadsComponent } from './listar-leads.component';

describe('ListarLeadsComponent', () => {
  let component: ListarLeadsComponent;
  let fixture: ComponentFixture<ListarLeadsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarLeadsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarLeadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
