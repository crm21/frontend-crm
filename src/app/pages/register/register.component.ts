import { Component, OnInit, OnDestroy } from '@angular/core';
import { environment } from '../../../environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { RegistroCliente } from '../../models/registroCliente';
import swal from 'sweetalert2';
import { ClienteService } from '../../services/cliente/cliente.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare var $: any;

@Component({
	selector: 'app-register-cmp',
	templateUrl: './register.component.html'
})

export class RegisterComponent implements OnInit, OnDestroy {
	test: Date = new Date();
	version = environment.version;
	registroCliente: RegistroCliente;
	loading = false;
	check: any;
	hide: boolean = true;

	constructor(
		private _router: Router,
		private _clienteService: ClienteService,
		private _sweetAlertService: SweetAlertService,
		private _route: ActivatedRoute,
	) {
		$("#terminosCondiciones").attr('checked', false);
		this.registroCliente = new RegistroCliente('', '', '', '', '', '', '');
		let opcionEmpresa = this._route.snapshot.data.opc;
		localStorage.setItem('business', opcionEmpresa);
	}
	
	ngOnInit() {
		const body = document.getElementsByTagName('body')[0];
		body.classList.add('register-page');
		body.classList.add('off-canvas-sidebar');
		let informacionEmpresa: string[] = this._sweetAlertService.datosEmpresas(localStorage.getItem('business'));
		//console.log(informacionEmpresa);
		this.registroCliente.correoEmpresa = informacionEmpresa[0];
		this.registroCliente.telefonoEmpresa = informacionEmpresa[1];
	}

	ngOnDestroy() {
		const body = document.getElementsByTagName('body')[0];
		body.classList.remove('register-page');
		body.classList.remove('off-canvas-sidebar');
	}

	async onSubmitRegistrarse(registrarClienteForm: RegistroCliente) {
		//console.log(this.registroCliente);
		if (this.check) {
			if (this.registroCliente.password === this.registroCliente.confirmPass) {
				this.loading = true;
				const responseRegistrarCliente = await this._clienteService.registrarClientesSinToken(this.registroCliente);
				//console.log(responseRegistrarCliente);
				if (responseRegistrarCliente["status"] === "success" && responseRegistrarCliente["code"] === "200") {
					this.loading = false;
					swal('¡Bien!', responseRegistrarCliente['message'], 'success');
					this.regresarLogin();
				}
				else if (responseRegistrarCliente["status"] === "success" && responseRegistrarCliente["code"] === "300") {
					this.loading = false;
					this._sweetAlertService.alertGeneral('Advertencia!', responseRegistrarCliente["message"], 'OK', 'warning');
				}
				else {
					this.loading = false;
					this._sweetAlertService.alertGeneral('¡Error!', responseRegistrarCliente["message"], 'OK', 'error');
				}
			} else {
				this.loading = false;
				this._sweetAlertService.alertGeneral('Advertencia!', 'Los campos: contraseña y confirmar contraseña, deben ser iguales', 'OK', 'warning');
			}
		} else {
			swal('Nos hace falta un paso  <i class="material-icons md-36">how_to_reg</i>',
				'Debes aceptar los terminos y condiciones de uso para continuar con el proceso de registro',
				'warning');
		}
	}

	aceptarAcuerdo() {
		this.check = true;
		$("#terminosCondiciones").attr('checked', true);
		$('#modalAceptarTerminos').modal('hide');
	}

	checkTerminos(event: any) {
		this.check = event.currentTarget.checked;
	}

	regresarLogin() {
		let opcionRegistro = localStorage.getItem('business');
		switch (opcionRegistro) {
			case 'A':
				this._router.navigate(['autodenar/clientes']);
				break;
			case 'M':
				this._router.navigate(['motodenar/clientes']);
				break;
			case 'T':
				this._router.navigate(['automotora/clientes']);
				break;
			default:
				this._router.navigate(["empresas"]);
				break;
		}
	}
}