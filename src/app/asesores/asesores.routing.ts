import { Routes } from '@angular/router';

import { CrearAsesoresComponent } from './crear-asesores/crear-asesores.component';
import { ListarAsesoresComponent } from './listar-asesores/listar-asesores.component';

export const AsesoresRoutes: Routes = [
    {
        path: '',
        redirectTo: 'listar',
        pathMatch: 'full',
    },
    {
        path: '',
        children: [ {
            path: 'registrar',
            component: CrearAsesoresComponent
        },{
            path: 'listar',
            component: ListarAsesoresComponent
        }]
    }
];
