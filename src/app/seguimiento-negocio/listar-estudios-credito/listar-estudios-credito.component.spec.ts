import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarEstudiosCreditoComponent } from './listar-estudios-credito.component';

describe('ListarEstudiosCreditoComponent', () => {
  let component: ListarEstudiosCreditoComponent;
  let fixture: ComponentFixture<ListarEstudiosCreditoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarEstudiosCreditoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarEstudiosCreditoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
