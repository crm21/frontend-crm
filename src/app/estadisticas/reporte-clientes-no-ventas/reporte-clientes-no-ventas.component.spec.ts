import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteClientesNoVentasComponent } from './reporte-clientes-no-ventas.component';

describe('ReporteClientesNoVentasComponent', () => {
  let component: ReporteClientesNoVentasComponent;
  let fixture: ComponentFixture<ReporteClientesNoVentasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteClientesNoVentasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteClientesNoVentasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
