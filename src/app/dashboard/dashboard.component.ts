import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from '../services/usuario/usuario.service';
import { SweetAlertService } from '../services/sweet-alert/sweet-alert.service';
import swal from 'sweetalert2';

declare var $: any;

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	cargaLoading: boolean = false;
	homeCliente: boolean = false;
	datosUsuario: any;
	nombresEmpresas: any = '';
	opcionVentana: any = '';
	cambioSistema: string = '';
	valorCambio: string = '';
	rolUsuario: string = '';

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _usuarioService: UsuarioService,
		private _sweetAlertService: SweetAlertService
	) {
		this.opcionVentana = this._route.snapshot.data.opc;
		this.valorCambio = this._route.snapshot.paramMap.get('valorCambio');;
		if (localStorage.getItem('identity_crm')) {

			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);
			let dataUsuario = identityData['data'];
			this.rolUsuario = dataUsuario.rol.valor;
			let urlAbsoluta = this.getAbsolutePath();
			urlAbsoluta = urlAbsoluta + '#/dashboard';

			if (this.opcionVentana == 2) {
				this.cargaLoading = true;
				localStorage.setItem("system", btoa(this.valorCambio));
				setTimeout(function () {
					window.location.href = urlAbsoluta;
					window.location.reload();
					this.cargaLoading = false;
				}, 2500);
			}

			if (this.opcionVentana == 3) {
				//console.log(this.valorCambio);
				this.cargaLoading = true;
				localStorage.setItem("business", this.valorCambio);
				setTimeout(function () {
					window.location.href = urlAbsoluta;
					window.location.reload();
					this.cargaLoading = false;
				}, 2500);
			}

			if (localStorage.getItem('system')) {
				let sistema = localStorage.getItem('system');
				sistema = atob(sistema);

				if (sistema == 'crm') this.cambioSistema = 'crm';
				else this.cambioSistema = 'erp';
			} else {
				localStorage.setItem("system", btoa("crm"));
				window.location.reload();
			}
		}
		else
			if (this.validarToken) this.expiracionToken('Usuario no autenticado');
	}

	ngOnInit() { }

	consultarNombreEmpresa(nombreEmpresa: any) {
		this.nombresEmpresas = this._usuarioService.nombresEmpresa(nombreEmpresa);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}

	getAbsolutePath() {
		let loc = window.location;
		let pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
		return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
	}
}
