import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtencionesTipificacionComponent } from './atenciones-tipificacion.component';

describe('AtencionesTipificacionComponent', () => {
  let component: AtencionesTipificacionComponent;
  let fixture: ComponentFixture<AtencionesTipificacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtencionesTipificacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtencionesTipificacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
