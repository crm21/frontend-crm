import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FichaClientesScComponent } from './ficha-clientes-sc.component';

describe('FichaClientesScComponent', () => {
  let component: FichaClientesScComponent;
  let fixture: ComponentFixture<FichaClientesScComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FichaClientesScComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FichaClientesScComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
