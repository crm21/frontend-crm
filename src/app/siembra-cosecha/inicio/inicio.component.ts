import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario/usuario.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { Router } from '@angular/router';

declare var $: any;

@Component({
	selector: 'app-inicio',
	templateUrl: './inicio.component.html',
	styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	nombres: string;
	nombre: string = 'Usuario';
	nombresEmpresas: string = '';
	imgNombreEmpresa: string = '';

	constructor(
		private _router: Router,
		private _usuarioService: UsuarioService,
		private _sweetAlertService: SweetAlertService
	) {
		if (localStorage.getItem('identity_crm')) {
			this.consultarNombreEmpresa(localStorage.getItem('business'));

			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			let dataUsuario = identityData['data'];
			this.nombres = dataUsuario.nombres + ' ' + dataUsuario.apellidos;
			this.nombre = dataUsuario.rol.nombre;
		}
	}

	ngOnInit() { }

	consultarNombreEmpresa(nombreEmpresa: any) {
		this.nombresEmpresas = this._usuarioService.nombresEmpresa(nombreEmpresa);
		switch (nombreEmpresa) {
			case 'A':
				this.imgNombreEmpresa = './assets/img/empresa-autodenar.png';
				break;
			case 'M':
				this.imgNombreEmpresa = './assets/img/empresa-motodenar.png';
				break;
			case 'T':
				this.imgNombreEmpresa = './assets/img/empresa-automotora.png';
				break;
			case 'D':
				this.imgNombreEmpresa = './assets/img/empresa-autodenar.png';
				break;
			default:
				this.imgNombreEmpresa = './assets/img/empresa-autodenar.png';
				break;
		}
	}

	onSubmitClientes() {
		this._router.navigate(["siembra-y-cosecha/clientes"]);
	}

	onSubmitaAgendamiento() {
		this._router.navigate(["siembra-y-cosecha/clientes"]);
	}

	onSubmitEstadisticas() {
		this._router.navigate(["siembra-y-cosecha/clientes"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}