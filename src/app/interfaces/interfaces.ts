export interface GeneralResponse {
	status: string;
	code: number;
	message: string;
	data: any[];
}

export interface DatosCliente {
	tipoDocumento: TipoDoc;
	documento: string;
	nombres: string;
	apellidos: string;
	usuario: string;
	rol: Rol;
	estado: Estado;
}

export interface ActualizarDatosCliente {
	tipoDocumento: TipoDoc;
	documento: string;
	nombres: string;
	apellidos: string;
}

export interface DatosInformacionPersonal {
	id: number;
	fechaNacimiento: string;
	contactoPrincipal: string;
	contactoSecundario: string;
	correo: string;
	lugarResidencia: string;
	direccion: string;
	testDrive: boolean;
	color: Colores;
	hobby1: Hobby;
	hobby2: Hobby;
	profesion: Profesion;
	genero: Genero;
}

export interface ActualizarDatosInformacionPersonal {
	id: number;
	correo: string;
	fechaNacimiento: string;
	direccion: string;
	lugarResidencia: string;
	contactoPrincipal: string;
	contactoSecundario: string;
	color: Colores;
	hobby1: Hobby;
	hobby2: Hobby;
	profesion: Profesion;
	genero: Genero;
}

export interface ActualizarCotizacion {
	numCotizacion: string;
	vehiculo: TipoVehiculo;
	modelo: string;
	cuotaInicial: string;
	metodoPago: MetodoPago;
	versionId: VersionId;
	colorVehiculo: ColorVehiculo;
	valorVehiculo: number;
	ultimoDigito: string;
	lugarMatricula: string;
	estado: EstadoNegocio;
}

export interface ActualizarProcesoVentas {
	fechaFacturacion: string;
	fechaEnvioMatricula: string;
	fechaRecepcionMatricula: string;
	fechaSoat: string;
	fechaActivacionChevy: string;
	fechaEntrega: string;
	precioFinalVenta: string;
	observacion: string;
	fechaFirmaContrato: string;
	fechaConsignacion: string;
}

export interface ActualizarProcesoVentasContestacion {
	fechaFacturacion: string;
	fechaEnvioMatricula: string;
	fechaRecepcionMatricula: string;
	fechaSoat: string;
	fechaActivacionChevyOnStar: string;
	fechaEntrega: string;
	precioFinalVenta: string;
	observacion: string;
	fechaFirmaContrato: string;
	fechaConsignacion: string;
	placa: string;
	vin: string;
	kilometrajeInicial: string;
}

export interface ActualizarReconsideradoContestacion {
	reconsiderado: Contestacion;
	observacion: string;
}

export interface modificarEstudioCredito {
	fechaRadicado: string;
	fechaContestacion: string;
	fechaDesembolso: string;
	contestacion: Contestacion;
	banco: Bancos;
	plan: Plan;
	observacion: string;
}

export interface ModificarPermisos {
	titulo: string;
	url: string;
	posicion: PosicionPermiso;
	icono: string;
	sistema: string;
}

export interface ModificarPermisosRol {
	estado: Estado;
}

/* Informacion Empleado */
export interface InfoPersonalEmpleado {
	id: number;
	tipoDocumento: TipoDoc;
	documento: string;
	nombres: string;
	apellidos: string;
	estado: Estado;
}

export interface InfoGeneralEmpleado {
	idGeneral: string;
	correo: string;
	direccion: string;
	lugarResidencia: string;
	telefonoFijo: string;
	celular: string;
	estadoCivil: EstadoCivil;
	aniosExperienciaLaboral: string;
	fechaExpedicion: string;
	fechaNacimiento: string;
	libretaMilitar: string;
	claseLibretaMilitar: ClaseLibretaMilitar;
	distritoLibretaMilitar: string;
	tarjetaProfesional: string;
	grupoSanguineo: GrupoSanguineo;
	licenciaConduccion: string;
	categoriaLicencia: CategoriaLicencia;
	colorPreferido: Colores;
	hobby1: Hobby;
	hobby2: Hobby;
	profesion: Profesion;
	genero: Genero;
}

export interface InfoEmpleadoEmpleado {
	idEmpleado: string;
	trabajaActualmente: string;
	empresaActual: string;
	tipoEmpleado: TipoEmpleado;
	tipoContrato: TipoContrato;
	trabajoEmpresa: string;
	empleoAntes: string;
	fechaEmpleoAntes: string;
	recomendado: string;
	nombreRecomendado: string;
	dependenciaRecomendado: string;
	parientesEmpresa: string;
	nombrePariente: string;
	dependenciaPariente: string;
	conocimientoVacante: string;
	ciudadVivido: string;
	ciudadesTrabajado: string;
	trabajoOtraCiudad: string;
	tipoVivienda: TipoVivienda;
	nombreArrendador: string;
	telefonoArrendador: string;
	tiempoVivienda: string;
	ingresosAdicionales: string;
	valorIngresosAdicionales: string;
	valorObligaciones: string;
	conceptoObligaciones: string;
	aspiracionSalarial: string;
	reconocimientos: string;
	perteneceGrupo: string;
	hobbies: string;
	deportes: TipoDeportes;
	expectativas: string;
}

export interface InfoFamiliarEmpleado {
	idFamiliar: string;
	nombrePareja: string;
	profesionPareja: Profesion;
	empresaPareja: string;
	cargoActualPareja: string;
	direccion: string;
	contacto: string;
	ciudad: string;
	numHermanos: string;
}

export interface InfoEducacionEmpleado {
	idEducacion: string;
	tipoEstudio: TipoEstudio;
	anioFinalizacion: string;
	aniosCursados: string;
	duracionMeses: string;
	tituloObtenido: string;
	institucion: string;
	ciudad: string;
	intensidadHoraria: string;
	estadoCurso: TipoCursos;
	anioSemestreCursa: string;
	observacion: string;
}

export interface interfaceVehiculo {
	id: string;
	placa: string;
	modelo: string;
	version: VersionId;
	cliente: string;
	vin: string;
	tipoAceite: TipoAceite;
}

/* Informacion Empleado */

export interface TipoDoc {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}

export interface Rol {
	id: number;
	nombre: string;
	valor: string;
}

export interface Estado {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}

export interface Hobby {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}

export interface Colores {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}

export interface Profesion {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}

export interface Genero {
	nombre: string;
	valor: string;
}

export interface TipoVehiculo {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}

export interface MetodoPago {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}

export interface VersionId {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}

export interface ColorVehiculo {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}

export interface EstadoNegocio {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}

export interface Bancos {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}

export interface Contestacion {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}

export interface Plan {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}

export interface EstadoCivil {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}

/* Selects hoja de vida */
export interface ClaseLibretaMilitar {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}

export interface CategoriaLicencia {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}

export interface GrupoSanguineo {
	nombre: string;
	valor: string;
}

export interface TipoEmpleado {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}

export interface TipoContrato {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}

export interface TipoVivienda {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}

export interface TipoDeportes {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}

export interface TipoEstudio {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}

export interface TipoCursos {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}

export interface ConocimientoVacante {
	nombre: string;
	valor: string;
}
/* Selects hoja de vida */

/* Cargos */
export interface ActualizarCargos {
	nombre: string;
	area: Areas;
	dependencia: Dependencias;
	periodoPago: PeriodoPago,
	numCargos: NumeroCargos;
	ubicacionOrganigrama: string;
	mision: string;
};

export interface PeriodoPago {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}
export interface NumeroCargos {
	nombre: string;
	valor: string;
}

/* Funciones Cargos */
export interface ActualizarFuncionesCargos {
	cargo: string;
	titulo: string;
	accionEspecifica: string;
	resultadosObtenidos: string;
	tiempoEjecucion: string;
	recursosUtilizados: string;
};
/* Afiliaciones */
export interface ActualizarAfiliaciones {
	entidad: string;
	fechaAfiliacion: string;
	fechaRetiro: string;
	tipoAfiliacion: TipoAfiliacion;
	observacion: string;
	contrato: string;
};
export interface TipoAfiliacion {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}
/* Contratos */
export interface ActualizarContratos {
	fechaFirmaContrato: string;
	fechaIniciacionLabores: string;
	descripcionSalario: string;
	valorSalario: string;
	tipoContrato: TipoContrato;
	fechaDesvinculacion: string;
	tipoDesvinculacion: TipoDesvinculacion;
	observacion: string;
	cargo: CargoNombre;
};
export interface TipoContrato {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}
export interface TipoDesvinculacion {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}
export interface CargoNombre {
	valor: string;
	nombre: string;
}

/* Permisos */
export interface PosicionPermiso {
	id: string;
	titulo: string;
	posicion: string;
}

/* Cargos */
export interface Areas {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}
export interface Dependencias {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}

/**
 * Cliente Siembra y Cosecha
 */
export interface ClienteSiembraCosecha {
	tipoDocumento: TipoDoc;
	documento: string;
	nombres: string;
	apellidos: string;
	correo: string;
	tipoEntrada: TipoEntrada;
}

export interface TipoEntrada {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}

/* vehiculo */
export interface TipoAceite {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}

/* Proyeccion */
export interface ProyectarKm {
	kmActual: string;
	kmProyectado: string;
}

/* Actualizacion de Ingresos de un Cliente */
export interface ActualizarIngreso {
	canalIngreso: TarjetaOpciones;
	fechaIngreso: string;
}
export interface TarjetaOpciones {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}
/* Actualizar leads */
export interface ActualizarLeads {
	tipoDocumento: TipoDoc;
	documentoPersona: string;
	nombresPersona: string;
	apellidosPersona: string;
	canalIngreso: CanalIngreso;
	correo: string;
	contactoPrincipal: string;
}
export interface CanalIngreso {
	id: number;
	nombre: string;
	valor: string;
	categoria: string;
	descripcion: string;
}