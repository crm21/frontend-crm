import { Routes } from '@angular/router';

import { ListarLeadsComponent } from './listar-leads/listar-leads.component';
import { RegistrarLeadsComponent } from './registrar-leads/registrar-leads.component';

export const LeadsRoutes: Routes = [
	{
		path: '',
		redirectTo: 'listar',
		pathMatch: 'full',
	},
	{
		path: '',
		children: [
			{
				path: '',
				component: ListarLeadsComponent
			}, {
				path: '*',
				component: ListarLeadsComponent
			}, {
				path: 'listar',
				component: ListarLeadsComponent
			}, {
				path: 'registrar',
				component: RegistrarLeadsComponent
			}
		]
	}
];