import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';
import { EventoComercial } from '../../models/eventoComercial';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { AgendaService } from '../../services/agenda/agenda.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { EventosComercialesService } from '../../services/eventos-comerciales/eventos-comerciales.service';
import { DatePipe } from '@angular/common';
import swal from 'sweetalert2';

declare var $: any;

@Component({
	selector: 'app-registrar-evento',
	templateUrl: './registrar-evento.component.html',
	styleUrls: ['./registrar-evento.component.css']
})
export class RegistrarEventoComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	checkEnvioCorreo: boolean = true;
	inputEnvioCorreo: boolean = false;
	crearEventoComercial: EventoComercial;
	idCliente: string;
	horasArray: any[] = [];
	tipoAgendamientoArray: any[] = [];
	horaEscodiga: string = '';
	correoEmpresa: string = '';
	telefonoEmpresa: string = '';

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _sweetAlertService: SweetAlertService,
		private _agendaService: AgendaService,
		private _eventosComercialesService: EventosComercialesService,
		private _parametrizacionService: ParametrizacionService,
		private _datepipe: DatePipe
	) {
		$("#confirmarEnvioCorreo").attr('checked', false);
		this.idCliente = this._route.snapshot.paramMap.get('idCliente');
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);
			let permisoVerFicha = identityData['data'];

			this.cargarInformacionSelect();
		}
		this.crearEventoComercial = new EventoComercial(+this.idCliente, '', '', '', '', '', false, '', '');
	}

	ngOnInit() {
		let informacionEmpresa: string[] = this._sweetAlertService.datosEmpresas(localStorage.getItem('business'));
		//console.log(informacionEmpresa);
		this.correoEmpresa = informacionEmpresa[0];
		this.telefonoEmpresa = informacionEmpresa[1];
	}

	async cargarInformacionSelect() {
		this.tipoAgendamientoArray = [];
		const tipoAgendamientoParametro: any = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo Evento');

		tipoAgendamientoParametro['data'].forEach((element) => {
			this.tipoAgendamientoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
		//console.log(this.tipoAgendamientoArray);
	}

	disponibilidaHorasFecha(evento: any) {
		//console.log(evento.value);
		let fechaAgenda = this.fechasValidacion(1, evento.value);
		//console.log(fechaAgenda);
		let valor: boolean = this.validarFechasSuperior(fechaAgenda);
		//console.log(valor);
		if (valor) {
			this.horasArray = [];
			this.cargarHorasPorFechas(fechaAgenda);
		} else {
			this.horasArray = [];
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b> La fecha elegida debe ser superio a la fecha actual.<b>', 2000);
		}
	}

	async cargarHorasPorFechas(fechaConsulta: string) {
		this.loading = true;

		const responseHorasDisponibles = await this._eventosComercialesService.listarHorasDisponibles(fechaConsulta);
		//console.log(responseHorasDisponibles);
		if (responseHorasDisponibles["status"] === "success" && responseHorasDisponibles["code"] === "200") {
			this.horasArray = responseHorasDisponibles['data'].horas;
			this.loading = false;
		}
		else if (responseHorasDisponibles["status"] === "success" && responseHorasDisponibles["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseHorasDisponibles['message'] + '<b>', 2000);
		}
		else if (responseHorasDisponibles["status"] === "error" && responseHorasDisponibles["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseHorasDisponibles['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseHorasDisponibles["message"], 'OK', 'error');
		}
	}

	onSubmitAgendarCitaComercial(crearEventoComercialForm: any) {
		let eventoComercialCrear = new EventoComercial(
			+this.idCliente,
			this.crearEventoComercial.titulo,
			this.crearEventoComercial.evento,
			this.fechasValidacion(1, this.crearEventoComercial.fechaEvento),
			this.crearEventoComercial.hora,
			this.crearEventoComercial.observacion,
			this.crearEventoComercial.confirmarEnvioCorreo,
			this.correoEmpresa,
			this.telefonoEmpresa
		); //console.log(eventoComercialCrear);

		let fechaString = this.fechasValidacion(2, eventoComercialCrear.fechaEvento);
		let correo = eventoComercialCrear.confirmarEnvioCorreo ? 'Sí' : 'No';

		swal({
			title: '¿Está seguro de guardar el agendamiento?',
			html: '<p>Se guardará el siguiente agendamiento:</p>' +
				'<div class="row justify-content-center text-left">' +
				'<div class="col-sm-4"><strong> Fecha: </strong></div>' +
				'<div class="col-sm-7 text-capitalize">' + fechaString + '</div>' +
				'<div class="col-sm-4"> <strong> Hora: </strong> </div>' +
				'<div class="col-sm-7">' + this.horaEscodiga + '</div>' +
				'<div class="col-sm-4"><strong> Evento: </strong></div>' +
				'<div class="col-sm-7">' + eventoComercialCrear.evento + '</div>' +
				'<div class="col-sm-4"><strong> Enviar correo: </strong></div>' +
				'<div class="col-sm-7">' + correo + '</div>' +
				'</div>',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			buttonsStyling: false
		})
			.then((result) => {
				//console.log(result);
				if (result.value) {
					this.guardarEventoComercial(eventoComercialCrear, crearEventoComercialForm);
				}
			}).catch(swal.noop);
	}

	async guardarEventoComercial(eventoComercialCrear: EventoComercial, crearEventoComercialForm: any) {
		this.loading = true;
		const responseAgendarCita = await this._eventosComercialesService.registrarEventoComercial(eventoComercialCrear);
		//console.log(responseAgendarCita);
		if (responseAgendarCita["status"] === "success" && responseAgendarCita["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseAgendarCita["message"], 'success')
				.then(result => {
					crearEventoComercialForm.reset();
					this.regresar();
				}).catch(swal.noop);
		}
		else if (responseAgendarCita["status"] === "success" && responseAgendarCita["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseAgendarCita['message'] + '<b>', 2000);
		}
		else if (responseAgendarCita["status"] === "error" && responseAgendarCita["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseAgendarCita['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'error', 'warning', '<b>' + responseAgendarCita['message'] + '<b>', 2000);
		}
	}

	campoEnvioCorreo(tipoEvento: string) {
		if (tipoEvento === 'Agendar') {
			this.inputEnvioCorreo = true;
		} else {
			this.inputEnvioCorreo = false;
		}
	}

	confirmarEnvio(event: any) {
		this.checkEnvioCorreo = event.currentTarget.checked;
		this.crearEventoComercial.confirmarEnvioCorreo = this.checkEnvioCorreo;
	}

	guardarHoraEscogida(event: any) {
		this.horaEscodiga = event;
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	filtroDiasBloqueados = (d: Date | null): boolean => {
		const day = (d || new Date()).getDay();
		// Prevent Saturday and Sunday from being selected.
		//return day !== 0 && day !== 6;
		return day !== 0;
	}

	validarFechasSuperior(fechaCita: string) {
		const fechaActual = new Date();
		const fechaIngresada = new Date(fechaCita);

		const diaInicio = fechaActual.getDate();
		const mesInicio = fechaActual.getMonth() + 1;
		const anioInicio = fechaActual.getFullYear();
		const fechaActualTem = diaInicio + "-" + mesInicio + "-" + anioInicio;

		const diaFin = fechaIngresada.getUTCDate();
		const mesFin = fechaIngresada.getUTCMonth() + 1;
		const anioFin = fechaIngresada.getUTCFullYear();
		const fechaIngresadaTem = diaFin + "-" + mesFin + "-" + anioFin;

		/* console.log("fechaActual: "+fechaActualTem);
		console.log("fechaIngresada: "+fechaIngresadaTem); */

		if (!(anioFin > anioInicio)) {
			if (anioFin < anioInicio) return false;

			if (!(mesFin > mesInicio)) {
				if (mesFin < mesInicio) return false;

				if (!(diaFin > diaInicio)) return false;
				return true;
			}
			return true;
		}
		return true;
	}

	fechasValidacion(opc: number, formatoFecha: any) {
		switch (opc) {
			case 1:
				if (formatoFecha) {
					//Formatear fecha
					const fechaActualTem = this._datepipe
						.transform(formatoFecha, "yyyy/MM/dd")
						.replace(/\//g, "-");
					//console.log("fecha formato: "+fechaActualTem);
					return fechaActualTem;
				}
				break;
			case 2:
				if (formatoFecha) {
					//Formatear fecha
					const fechaActualTem = this._datepipe
						.transform(formatoFecha, "MMMM d, y");
					//console.log("fecha formato: " + fechaActualTem);
					return fechaActualTem;
				}
				break;
			default:
				return '';
				break;
		}
	}

	regresar() {
		let idIngreso = localStorage.getItem('idIngreso');
		this._router.navigate(["clientes/ficha-clientes/", idIngreso]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}