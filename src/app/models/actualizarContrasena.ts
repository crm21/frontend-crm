export class ActualizarPass {
    constructor(
        public actual: string,
        public nueva: string,
        public rNueva: string
    ) { }
}

export class RecordarCuenta {
    constructor(
        public correo: string,
    ) { }
}

export class CambiarContrasenia {
    constructor(
        public oldPass: string,
        public newPass: string,
        public ruta: string,
        public correoEmpresa: string,
        public telefonoEmpresa: string,
    ) { }
}

export class ConfirmarContrasenia {
    constructor(
        public id: string,
        public newPass: string
    ) { }
}

export class UsuariosCambiarContrasenia {
    constructor(
        public documento: string,
        public password: string,
        public checkPassword: boolean
    ) { }
}