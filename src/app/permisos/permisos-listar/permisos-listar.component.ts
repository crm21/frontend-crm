import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import { ModificarPermisos } from '../../interfaces/interfaces';
import { Permisos } from '../../models/permisos';
import swal from 'sweetalert2';
import { PermisosService } from '../../services/permisos/permisos.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';

declare var $: any;
declare interface DataTable {
	headerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-permisos-listar',
	templateUrl: './permisos-listar.component.html',
	styleUrls: ['./permisos-listar.component.css']
})
export class PermisosListarComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	consultaVaciaCargarSpinner: boolean = false;
	actualizarPermisoRolForm: FormGroup;
	dataTable: DataTable;
	permisosArray: any[] = [];
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	idPermiso: any;
	permisosExistentes: any[] = [];
	sistemaArray: string[] = ['CRM', 'ERP'];

	constructor(
		private _router: Router,
		private _formBuilder: FormBuilder,
		private _permisosService: PermisosService,
		private _sweetAlertService: SweetAlertService,
		private _parametrizacionService: ParametrizacionService
	) {
		//if (localStorage.getItem('identity_crm')) {}
		this.actualizarPermisoRolForm = this._formBuilder.group({
			titulo: ['', Validators.required],
			url: ['', [Validators.required, Validators.pattern("^[a-z-/]{1,50}$")]],
			icono: ['', Validators.required],
			posicion: ['', Validators.required],
			sistema: ['', Validators.required]
		});
	}

	ngOnInit() {
		this.dataTable = {
			headerRow: [
				'#',
				'Título',
				'URL',
				'Posición',
				'Icono',
				'Sistema',
				'Acciones'
			],
			dataRows: []
		};
		if (localStorage.getItem('identity_crm')) {
			this.listarRolesPermisos(1);
		}
	}

	async listarRolesPermisos(page) {
		this.loading = true;

		const responseRolPermisos = await this._permisosService.listarPermisosTodos(page);
		//console.log(responseRolPermisos);
		if (responseRolPermisos['status'] == "success" && responseRolPermisos['code'] == "200") {
			this.loading = false;
			this.llenarTabla(responseRolPermisos['data'], page);
		}
		else if (responseRolPermisos['status'] == "success" && responseRolPermisos['code'] == "300") {
			this.loading = false;
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			swal('Advertencia!', responseRolPermisos['message'], 'warning');
		}
		else if (responseRolPermisos["status"] === "error" && responseRolPermisos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseRolPermisos['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			swal('¡Error!', responseRolPermisos['message'], 'error');
		}
	}

	llenarTabla(permisos: any, page: any) {
		//console.log(permisos);
		this.permisosArray = permisos['permisos'];
		this.dataTable.dataRows = [];
		this.dataTable.dataRows = this.permisosArray;
		this.total_item_count = permisos['total_item_count'];
		this.page_actual = page;

		this.total_pages = permisos['total_pages'];

		if (page >= 2) this.page_prev = page - 1;
		else this.page_prev = 1;

		if (page < permisos['total_pages']) this.page_next = page + 1;
		else {
			if (permisos['total_pages'] == 0) this.page_next = 1;
			else this.page_next = permisos['total_pages'];
		}

		if (this.total_item_count || this.total_item_count == 0) {
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
		}
	}

	async listarPermisosExistentes() {
		this.loading = true;
		this.permisosExistentes = [];

		const responsePermisos = await this._permisosService.listarPermisos();
		//console.log(responsePermisos);
		this.loading = false;
		if (responsePermisos['status'] == "success" && responsePermisos['code'] == "200") {
			let arrayPermiso: any[] = responsePermisos['data'];
			let JSONParam;

			arrayPermiso.forEach(element => {
				JSONParam = {
					id: '' + element.id,
					titulo: element.titulo + ' - ' + element.sistema,
					posicion: element.posicion
				};
				this.permisosExistentes.push(JSONParam);
			});
			//console.log(this.permisosExistentes);
		}
		else if (responsePermisos['status'] == "success" && responsePermisos['code'] == "300") {
			swal('Advertencia!', responsePermisos['message'], 'warning');
		}
		else if (responsePermisos["status"] === "error" && responsePermisos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responsePermisos['message']);
		}
		else {
			swal('¡Error!', responsePermisos['message'], 'error');
		}
	}

	editarPermisos(permiso) {
		//console.log(permiso);
		this.listarPermisosExistentes();
		this.idPermiso = permiso.id;
		let rowPermisos: ModificarPermisos = permiso;

		this.actualizarPermisoRolForm.setValue({
			titulo: rowPermisos.titulo,
			url: rowPermisos.url,
			posicion: rowPermisos.posicion,
			icono: rowPermisos.icono,
			sistema: rowPermisos.sistema
		});

		$("#modalActualizar").modal("show");
	}

	async onSubmitActualizarPermisoRol() {
		this.loading = true;
		let permiso = new Permisos(
			this.actualizarPermisoRolForm.value.titulo,
			this.actualizarPermisoRolForm.value.url,
			'link',
			this.actualizarPermisoRolForm.value.icono,
			this.buscarIdPermisos(this.actualizarPermisoRolForm.value.posicion),
			this.actualizarPermisoRolForm.value.sistema
		); //console.log(permiso);

		const responseModificarPermisos = await this._permisosService.modificarPermisos(this.idPermiso, permiso);
		//console.log(responseModificarPermisos);
		if (responseModificarPermisos["status"] === "success" && responseModificarPermisos["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseModificarPermisos["message"], 'success')
				.then(result => {
					this.actualizarPermisoRolForm.reset();
					$("#modalActualizar").modal("hide");
					this.listarRolesPermisos(1);
				}).catch(swal.noop);
		}
		else if (responseModificarPermisos["status"] === "success" && responseModificarPermisos["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseModificarPermisos["message"], 'OK', 'warning');
		}
		else if (responseModificarPermisos["status"] === "error" && responseModificarPermisos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseModificarPermisos['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseModificarPermisos["message"], 'OK', 'error');
		}
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	buscarIdPermisos(posicion: string) {
		let devolverId: string = '';
		const permisosArray = this.permisosExistentes.find(element => element.posicion == posicion);

		if (permisosArray) devolverId = permisosArray.id;
		else devolverId = posicion;

		return devolverId;
	}

	crearPermisos() {
		this._router.navigate(["permisos/menu"]);
	}

	crearPermisosRoles() {
		this._router.navigate(["permisos/rol"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}