import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteClientesVehiculosUsadosComponent } from './reporte-clientes-vehiculos-usados.component';

describe('ReporteClientesVehiculosUsadosComponent', () => {
  let component: ReporteClientesVehiculosUsadosComponent;
  let fixture: ComponentFixture<ReporteClientesVehiculosUsadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteClientesVehiculosUsadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteClientesVehiculosUsadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
