import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarReconsideradoComponent } from './listar-reconsiderado.component';

describe('ListarReconsideradoComponent', () => {
  let component: ListarReconsideradoComponent;
  let fixture: ComponentFixture<ListarReconsideradoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarReconsideradoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarReconsideradoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
