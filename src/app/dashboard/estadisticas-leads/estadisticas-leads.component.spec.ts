import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadisticasLeadsComponent } from './estadisticas-leads.component';

describe('EstadisticasLeadsComponent', () => {
  let component: EstadisticasLeadsComponent;
  let fixture: ComponentFixture<EstadisticasLeadsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadisticasLeadsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadisticasLeadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
