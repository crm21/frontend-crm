import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteClientesEsperaComponent } from './reporte-clientes-espera.component';

describe('ReporteClientesEsperaComponent', () => {
  let component: ReporteClientesEsperaComponent;
  let fixture: ComponentFixture<ReporteClientesEsperaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteClientesEsperaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteClientesEsperaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
