import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {
	EmpleadoInfoPersonal,
	EmpleadoInfoGeneral,
	EmpleadoInfoEmpleado,
	EmpleadoInfoFamiliar,
	EmpleadoInfoEducacion,
	ActualizarEmpleadoInfoPersonal
} from '../../models/empleado';
import { UsuarioService } from '../usuario/usuario.service';

@Injectable({
	providedIn: 'root'
})
export class EmpleadosService {

	headers: any;
	url: string = '';
	token: string;

	constructor(
		private http: HttpClient,
		private _usuarioService: UsuarioService
	) {
		console.log('[Empleados services is ready]...');
	}

	async listarInfoGeneral(pagination: any) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			let params = "?pagination=" + pagination;
			this.http.get(this.url + "/infoGeneral" + params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async listarEmpleados(opc: any, idEmpleado: any, filtro: any, pagination: any) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			let params = "?idEmpleado=" + idEmpleado + "&filtro=" + filtro + "&pagination=" + pagination;
			this.http.get(this.url + "/empleadosPorFiltros/" + opc + params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async listarEmpleadosConContrato(opc: any, idEmpleado: any) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			let params = "?idEmpleado=" + idEmpleado;
			this.http.get(this.url + "/empleadosPorFiltros/" + opc + params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async registrarEmpleado(
		empleado: EmpleadoInfoPersonal,
		infoGeneral: EmpleadoInfoGeneral,
		infoEmpleado: EmpleadoInfoEmpleado,
		infoFamiliar: EmpleadoInfoFamiliar,
		infoEducacion: any[]
	) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			const json = '{"empleado":' + JSON.stringify(empleado) +
				',"infoGeneral":' + JSON.stringify(infoGeneral) +
				',"infoEmpleado":' + JSON.stringify(infoEmpleado) +
				',"infoFamiliar":' + JSON.stringify(infoFamiliar) +
				',"educacion":' + JSON.stringify(infoEducacion) +
				'}';
			let params = "json=" + json;
			this.http.post(this.url + "/registrarEmpleados", params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async listarInfGeneralEmpleadoPorId(opc: any, idEmpleado: any) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			let params = "?idEmpleado=" + idEmpleado;
			this.http.get(this.url + "/infoGeneral/" + opc + params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async listarInfEmpleadoEmpleadoPorId(opc: any, idInfoGeneral: any) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			let params = "?idInfoGeneral=" + idInfoGeneral;
			this.http.get(this.url + "/infoEmpleado/" + opc + params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async listarInfFamiliarEmpleadoPorId(opc: any, idInfoEmpleado: any) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			let params = "?idInfoEmpleado=" + idInfoEmpleado;
			this.http.get(this.url + "/infoFamiliar/" + opc + params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async listarEducacionEmpleadoPorId(opc: any, idEmpleado: any) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			let params = "?idEmpleado=" + idEmpleado;
			this.http.get(this.url + "/educacion/" + opc + params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async actualizarInfoPersonalEmpleado(idEmpleado: any, actualizarEmpleadoInfPers: ActualizarEmpleadoInfoPersonal) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			const json = JSON.stringify(actualizarEmpleadoInfPers);
			let params = "json=" + json;
			this.http.put(this.url + "/clientes/" + idEmpleado, params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async actualizarInfoGeneralEmpleado(idInformacion: any, actualizarEmpleadoInfGene: EmpleadoInfoGeneral) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			const json = JSON.stringify(actualizarEmpleadoInfGene);
			let params = "json=" + json + "&idInformacion=" + idInformacion;
			this.http.put(this.url + "/infoGeneral", params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async actualizarInfoEmpleadoEmpleado(idInformacion: any, actualizarEmpleadoInfEmple: EmpleadoInfoEmpleado) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			const json = JSON.stringify(actualizarEmpleadoInfEmple);
			let params = "json=" + json + "&idInformacion=" + idInformacion;
			this.http.put(this.url + "/infoEmpleado", params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async actualizarInfoFamiliarEmpleado(idInformacion: any, actualizarEmpleadoInfFamili: EmpleadoInfoFamiliar) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			const json = JSON.stringify(actualizarEmpleadoInfFamili);
			let params = "json=" + json + "&idInformacion=" + idInformacion;
			this.http.put(this.url + "/infoFamiliar", params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async modificarTituloEducacionEmpleado(idEducacion: any, tituloEducacionEmpleado: EmpleadoInfoEducacion) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			const json = JSON.stringify(tituloEducacionEmpleado);
			let params = "json=" + json + "&idEducacion=" + idEducacion;
			this.http.put(this.url + "/educacion", params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async eliminarTituloEducacionEmpleado(opc: any, idEmpleado: any) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			let params = "?idEmpleado=" + idEmpleado;
			this.http.get(this.url + "/educacion/" + opc + params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}

	async registrarTitulosEducacionEmpleado(idEmpleado: any, infoEducacion: any[]) {
		this.url = this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business'));
		this.token = await this._usuarioService.getToken();
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.set('Authorization', this.token);

		return await new Promise((resolve, reject) => {
			const json = JSON.stringify(infoEducacion);
			let params = "json=" + json + "&idEmpleado=" + idEmpleado;
			this.http.post(this.url + "/educacion", params, { headers: this.headers }).subscribe(
				(response: Object) => {
					resolve(response);
				},
				(error: any) => {
					reject(error);
				}
			);
		}).catch((error: any) => {
			console.log(error);
			throw error;
		});
	}
}