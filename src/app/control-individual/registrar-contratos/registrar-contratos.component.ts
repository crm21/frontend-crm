import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Contratos } from '../../models/contratos';
import { ActualizarContratos } from '../../interfaces/interfaces';
import swal from 'sweetalert2';
import { DatePipe } from "@angular/common";
import { EmpleadosService } from '../../services/empleados/empleados.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { ControlIndividualService } from '../../services/control-individual/control-individual.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare var $: any;

@Component({
	selector: 'app-registrar-contratos',
	templateUrl: './registrar-contratos.component.html',
	styleUrls: ['./registrar-contratos.component.css']
})
export class RegistrarContratosComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	buscarEmpleadoFormGroup: FormGroup;
	crearContratoForm: FormGroup;
	empleadoContrato: any[] = [];
	tipoDesvinculacionArray: any[] = [];
	cargoArray: any[] = [];
	tipoContratoArray: any[] = [];
	descripcionSalarioArray: string[] = ['Fijo', 'Variable'];
	filtroBusqueda = '';
	opcion = 1;
	empleadoEncontrado: boolean = false;
	empleado = {
		id: null,
		identificacion: null,
		nombres: null
	};
	contrato: any = {
		id: null,
		fechaFirmaContrato: null,
		fechaIniciacionLabores: null,
		descripcionSalario: null,
		valorSalario: null,
		tipoContrato: null,
		fechaDesvinculacion: null,
		tipoDesvinculacion: null,
		observacion: null,
		cargo: null
	};
	estadoContrato: boolean = true;
	mensajeBoton: string = 'Registrar';

	constructor(
		private _router: Router,
		private _datepipe: DatePipe,
		private _formBuilder: FormBuilder,
		private _empleadosService: EmpleadosService,
		private _parametrizacionService: ParametrizacionService,
		private _controlIndividualService: ControlIndividualService,
		private _sweetAlertService: SweetAlertService
	) {
		if (localStorage.getItem('identity_crm')) {
			this.cargarInformacionSelect();
		}
	}

	ngOnInit() {
		this.buscarEmpleadoFormGroup = this._formBuilder.group({
			filtroEmpleado: ['', [Validators.required, Validators.pattern("[0-9]{5,13}$")]]
		});
		this.crearContratoForm = this._formBuilder.group({
			fechaFirmaContrato: ['', [Validators.required]],
			fechaDesvinculacion: [''],
			descripcionSalario: ['', [Validators.required]],
			valorSalario: ['', [Validators.required]],
			fechaIniciacionLabores: ['', [Validators.required]],
			observacion: [''],
			tipoContrato: ['', [Validators.required]],
			tipoDesvinculacion: [''],
			cargo: ['', [Validators.required]]
		});
	}

	onSubmitBuscarEmpleado() {
		this.filtroBusqueda = this.buscarEmpleadoFormGroup.value.filtroEmpleado;
		this.opcion = 4;
		this.listarEmpleadosPorFiltro(this.opcion, this.filtroBusqueda);
	}

	async listarEmpleadosPorFiltro(opc: any, filtro: any) {
		this.loading = true;

		const responseEmpleados = await this._empleadosService.listarEmpleadosConContrato(opc, filtro);
		//console.log(responseEmpleados);
		if (responseEmpleados["status"] === "success" && responseEmpleados["code"] === "200") {
			this.empleadoEncontrado = true;
			this.llenarDatos(responseEmpleados['data']);
			this._sweetAlertService.showNotification('top', 'right', 'success', 'success', '<b>' + responseEmpleados['message'] + '<b>', 2000);
			this.loading = false;
		}
		else if (responseEmpleados["status"] === "success" && responseEmpleados["code"] === "300") {
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseEmpleados['message'] + '<b>', 2000);
			this.empleadoEncontrado = false;
			this.loading = false;
		}
		else if (responseEmpleados["status"] === "error" && responseEmpleados["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseEmpleados['message']);
		}
		else {
			this._sweetAlertService.alertGeneral('¡Error!', responseEmpleados["message"], 'OK', 'error');
			this.empleadoEncontrado = false;
			this.loading = false;
		}
	}

	llenarDatos(data: any) {
		//console.log(data);
		this.crearContratoForm.reset();
		this.mensajeBoton = 'Registrar';
		let datosEmpleado = data.empleado;
		let datosContrato = data.contrato;
		this.empleado.id = datosEmpleado.id;
		this.empleado.identificacion = datosEmpleado.documento;
		this.empleado.nombres = datosEmpleado.nombres + ' ' + datosEmpleado.apellidos;
		this.estadoContrato = data.estadoContrato;
		if (data.estadoContrato) {
			this.cargarInformacionContrato(datosContrato);
		}
	}

	cargarInformacionContrato(datosContrato: any) {
		this.mensajeBoton = 'Modificar';
		this.contrato.id = datosContrato.id;
		this.contrato.fechaFirmaContrato = datosContrato.fechaFirmaContrato;
		this.contrato.fechaIniciacionLabores = datosContrato.fechaIniciacionLabores;
		this.contrato.descripcionSalario = datosContrato.descripcionSalario;
		this.contrato.valorSalario = datosContrato.valorSalario;
		this.contrato.tipoContrato = datosContrato.tipoContrato;
		this.contrato.fechaDesvinculacion = datosContrato.fechaDesvinculacion;
		this.contrato.tipoDesvinculacion = datosContrato.tipoDesvinculacion;
		this.contrato.observacion = datosContrato.observacion;
		this.contrato.cargo = {
			valor: '' + datosContrato.cargo.id,
			nombre: datosContrato.cargo.nombre
		};

		let contratoActualizar: ActualizarContratos = this.contrato;
		this.crearContratoForm.setValue({
			fechaFirmaContrato: contratoActualizar.fechaFirmaContrato,
			fechaIniciacionLabores: contratoActualizar.fechaIniciacionLabores,
			descripcionSalario: contratoActualizar.descripcionSalario,
			valorSalario: contratoActualizar.valorSalario,
			tipoContrato: contratoActualizar.tipoContrato.valor,
			fechaDesvinculacion: contratoActualizar.fechaDesvinculacion,
			tipoDesvinculacion: contratoActualizar.tipoDesvinculacion.valor,
			observacion: contratoActualizar.observacion,
			cargo: contratoActualizar.cargo.valor
		});
	}

	async cargarInformacionSelect() {
		this.loading = true;
		let tipoDesvinculacion = await this._parametrizacionService.listarCategoriaParametrizacion('Tipo Desvinculación');
		let tipoContrato = await this._parametrizacionService.listarCategoriaParametrizacion('Clase de Contrato');

		tipoDesvinculacion['data'].forEach((element) => {
			this.tipoDesvinculacionArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		tipoContrato['data'].forEach((element) => {
			this.tipoContratoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		this.listarCargosContrato();
	}

	async listarCargosContrato() {
		const responseNombreCargo = await this._controlIndividualService.listarNombreCargo(4, '');
		//console.log(responseNombreCargo);
		if (responseNombreCargo["status"] === "success" && responseNombreCargo["code"] === "200") {
			let cargosLista = responseNombreCargo["data"];
			cargosLista.forEach((element) => {
				this.cargoArray.push({
					valor: '' + element.id,
					nombre: element.nombre
				});
			});
			this.loading = false;
		}
		else if (responseNombreCargo["status"] === "success" && responseNombreCargo["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseNombreCargo['message'] + '<b>', 2000);
		}
		else if (responseNombreCargo["status"] === "error" && responseNombreCargo["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseNombreCargo['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseNombreCargo["message"], 'OK', 'error');
		}
	}

	async onSubmitCrearContrato() {
		let responseCrearContrato;
		let nuevoContrato = new Contratos(
			this.fechasValidacion(this.crearContratoForm.value.fechaFirmaContrato),
			this.fechasValidacion(this.crearContratoForm.value.fechaDesvinculacion),
			this.crearContratoForm.value.descripcionSalario,
			this.crearContratoForm.value.valorSalario,
			this.fechasValidacion(this.crearContratoForm.value.fechaIniciacionLabores),
			this.crearContratoForm.value.observacion,
			this.crearContratoForm.value.tipoContrato,
			this.crearContratoForm.value.tipoDesvinculacion,
			this.crearContratoForm.value.cargo,
			this.empleado.id,
		); //console.log(nuevoContrato);
		if (this.estadoContrato) {
			responseCrearContrato = await this._controlIndividualService.modificarContrato(this.contrato.id, nuevoContrato);
		} else {
			responseCrearContrato = await this._controlIndividualService.registrarContrato(nuevoContrato);
		}
		//console.log(responseCrearContrato);
		if (responseCrearContrato["status"] === "success" && responseCrearContrato["code"] === "200") {
			let idContrato = responseCrearContrato["data"].id;
			this.empleadoEncontrado = false;
			$('.main-panel').scrollTop(0);
			this.crearContratoForm.reset();
			this.buscarEmpleadoFormGroup.reset();
			swal('¡Bien!', responseCrearContrato['message'], 'success')
				.then((result) => {
					swal({
						title: '¡Bien!',
						text: ' ¿Desea registrar afiliaciones al empleado?',
						type: 'info',
						showCancelButton: true,
						confirmButtonClass: 'btn btn-warning',
						cancelButtonClass: 'btn btn-danger',
						confirmButtonText: 'Registrar Afiliación',
						cancelButtonText: 'No',
						buttonsStyling: true,
						reverseButtons: true
					}).then((result) => {
						if (result.value) {
							this.registrarAfiliacion(idContrato);
						}
					}).catch(swal.noop);
				}).catch(swal.noop);

			this.loading = false;
		}
		else if (responseCrearContrato["status"] === "success" && responseCrearContrato["code"] === "300") {
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseCrearContrato['message'] + '<b>', 2000);
			this.empleadoEncontrado = true;
			this.loading = false;
		}
		else if (responseCrearContrato["status"] === "error" && responseCrearContrato["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseCrearContrato['message']);
		}
		else {
			this._sweetAlertService.alertGeneral('¡Error!', responseCrearContrato["message"], 'OK', 'error');
			this.empleadoEncontrado = true;
			this.loading = false;
		}

	}

	limpiarFormulario() {
		this.empleadoEncontrado = false;
		this.crearContratoForm.reset();
		this.buscarEmpleadoFormGroup.reset();
	}

	buscarIdCliente(rowIdEmpleado) {
		// opc = 2 para modificar empleado
		this._router.navigate(["empleados/actualizar/", rowIdEmpleado]);
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	fechasValidacion(formatoFecha) {
		if (formatoFecha) {
			//Formatear fecha
			const fechaActualTem = this._datepipe
				.transform(formatoFecha, "yyyy/MM/dd")
				.replace(/\//g, "-");
			//console.log("fecha formato: "+fechaActualTem);
			return fechaActualTem;
		}
		return '';
	}

	registrarAfiliacion(idContrato) {
		this._router.navigate(["control-individual/listar-afiliaciones", idContrato]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}