import { Component, OnInit } from '@angular/core';
import PerfectScrollbar from 'perfect-scrollbar';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import swal from "sweetalert2";
import { UsuarioService } from '../services/usuario/usuario.service';
import { Login } from '../models/login';

declare const $: any;

//Metadata
export interface RouteInfo {
	path: string;
	title: string;
	type: string;
	icontype: string;
	collapse?: string;
	children?: ChildrenItems[];
}

export interface ChildrenItems {
	path: string;
	title: string;
	ab: string;
	type?: string;
}

//Menu Items
export const ROUTES: RouteInfo[] = [];

@Component({
	selector: 'app-sidebar-cmp',
	templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
	public menuItems: any[];
	ps: any;
	public nombres = "Invitado";
	public apellidos = "Invitado";
	public infoUsuario: any;
	public menuPermisos = [];

	public urlImg = environment.localImag;
	public nombreEmpresa = '';
	public nombreSistema = '';
	public rolUsuario: any;
	public permisoCambiarEmpresa = false;
	public permisoCambiarEmpresaArray = ['A', 'GG', 'GA'];
	public permisoSistema = {
		crm: null,
		erp: null
	};
	private login: Login;

	constructor(public _usuarioService: UsuarioService, public _routerPag: Router,) {

		this.login = new Login('', '', '', '2');
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);
			let dataUsuario = identityData['data'];
			this.rolUsuario = identityData['data'].rol.valor;

			this.permisoSistema.crm = identityData['data'].permisos.crm.acceso;
			this.permisoSistema.erp = identityData['data'].permisos.erp.acceso;
			//console.log(dataUsuario.permisos);
			this.menuPermisos = [];
			this.nombreEmpresa = _usuarioService.nombresEmpresa(localStorage.getItem('business'));
			this.infoUsuario = dataUsuario;
			this.nombres = this.infoUsuario.nombres;
			this.apellidos = this.infoUsuario.apellidos;
			if (localStorage.getItem('system')) {
				let sistema = localStorage.getItem('system');
				sistema = atob(sistema);

				if (sistema == 'crm') {
					this.menuPermisos = this.infoUsuario.permisos.crm.permisos;
					this.nombreSistema = 'CRM';
				}
				else {
					this.nombreSistema = 'ERP';
					this.menuPermisos = this.infoUsuario.permisos.erp.permisos;
				}

			} else {
				localStorage.setItem("system", btoa("crm"));
				this.menuPermisos = this.infoUsuario.permisos.crm.permisos;
			}
		}
	}

	isMobileMenu() {
		if ($(window).width() > 991) {
			return false;
		}
		return true;
	};

	ngOnInit() {
		if (this.permisoCambiarEmpresaArray.includes(this.rolUsuario)) {
			this.permisoCambiarEmpresa = true;
		}
		else this.permisoCambiarEmpresa = false;
		//this.menuItems = ROUTES.filter(menuItem => menuItem);
		this.menuItems = this.menuPermisos;
		let rutasMenu = this.menuPermisos;
		if (rutasMenu.length > 0) {
			rutasMenu.forEach((element) => {
				ROUTES.push({
					path: element.path,
					title: element.title,
					type: element.type,
					icontype: element.icontype
				});
			});
		}

		if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
			const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
			this.ps = new PerfectScrollbar(elemSidebar);
		}
	}
	updatePS(): void {
		if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
			this.ps.update();
		}
	}
	isMac(): boolean {
		let bool = false;
		if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
			bool = true;
		}
		return bool;
	}

	cambiarSistema(valorSistema: any) {
		let sistema = localStorage.getItem('system');
		sistema = atob(sistema);

		if (valorSistema == sistema) {
			swal(
				"Advertencia!",
				"Usted ya se encuentra en el sistema " + this.nombreSistema,
				"warning"
			);
		} else {
			swal({
				title: "Cambiar de Sistema",
				text: "¿Esta seguro de que desea cambiar de sistema?",
				type: "info",
				showCancelButton: true,
				confirmButtonClass: "btn btn-success",
				cancelButtonClass: "btn btn-danger",
				confirmButtonText: "Si",
				cancelButtonText: "Cancelar",
				buttonsStyling: true,
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					this._routerPag.navigate(['/dashboard-2/', valorSistema]);
				}
			});
		}
	}

	cambiarEmpresa(valorEmpresa: any) {
		let empresa = localStorage.getItem('business');

		if (valorEmpresa == empresa) {
			swal(
				"Advertencia!",
				"Usted ya se encuentra en la empresa " + this.nombreEmpresa,
				"warning"
			);
		} else {
			swal({
				title: "Cambiar de Empresa",
				text: "¿Esta seguro de que desea cambiar de Empresa?",
				type: "info",
				showCancelButton: true,
				confirmButtonClass: "btn btn-success",
				cancelButtonClass: "btn btn-danger",
				confirmButtonText: "Si",
				cancelButtonText: "Cancelar",
				buttonsStyling: true,
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					localStorage.setItem("business", valorEmpresa);
					this.login.empresas = localStorage.getItem("business");
					this.iniciarSesionEmpresa();
					this._routerPag.navigate(['/dashboard-3/', valorEmpresa]);
				}
			});
		}
	}

	async iniciarSesionEmpresa() {

		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			this.login.password = '' + identityData['data'].documento;
			this.login.opc = '2';

			if (!this.login.empresas) {
				this._routerPag.navigate(['/empresas']);
			} else {
				const responseLogin: any = await this._usuarioService.iniciarSesion(this.login);
				let base_64_data = atob(responseLogin);
				let identityData = JSON.parse(base_64_data);
				//console.log(identityData);
				if (identityData['status'] == "success") {
					const responseDatosLoginToken = await this._usuarioService.iniciarSesion(this.login, true);

					let token: any = responseDatosLoginToken;

					// Guardar token en el localstorage
					localStorage.setItem("system", btoa("crm"));
					localStorage.setItem("token_crm", token);
					localStorage.setItem('identity_crm', responseLogin);

					this._routerPag.navigate(['/dashboard']);
				} else {
					swal('Advertencia!', identityData['message'], 'warning');
				}
			}
		}
	}
}
