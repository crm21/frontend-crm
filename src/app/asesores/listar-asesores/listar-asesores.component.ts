import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioService } from '../../services/usuario/usuario.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { TableroAsignacionService } from '../../services/tablero-asignacion/tablero-asignacion.service';
import swal from 'sweetalert2';

declare var $: any;
declare interface DataTable {
	headerRow: string[];
	footerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-listar-asesores',
	templateUrl: './listar-asesores.component.html',
	styleUrls: ['./listar-asesores.component.css']
})
export class ListarAsesoresComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	consultaVaciaCargarSpinner: boolean = false;
	dataTable: any;
	registros: any[] = [];
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	paginacionUsuarios = true;
	reasignarClientesAsesoresForm: FormGroup;
	usuario = {
		sub: null,
		rol: null

	};
	todosAsesoresArray: any[] = [];

	constructor(
		private _router: Router,
		private _formBuilder: FormBuilder,
		private _usuariosService: UsuarioService,
		private _sweetAlertService: SweetAlertService,
		private _tableroAsignacionService: TableroAsignacionService,
	) {
		if (localStorage.getItem('identity_crm')) {
			this.listarUsuariosFiltro(1);
			this.cargarTotalAsesores();
		}
	}

	ngOnInit() {
		this.dataTable = {
			headerRow: [
				'#',
				"Documento Cliente",
				"Nombres Completos",
				"Usuario",
				"Rol",
				"Última Sesión",
				"# Sesiones",
				"Estado",
				"Acciones"
			],
			dataRows: []
		};
		this.reasignarClientesAsesoresForm = this._formBuilder.group({
			asesorAnterior: ['', Validators.required],
			asesorNuevo: ['', Validators.required]
		});
	}

	async listarUsuariosFiltro(page) {
		this.loading = true;
		// Metodo para buscar desde los formularios
		// ocularPaginado = 0 es el paginado de todos los usuarios
		// ocularPaginado = 1 es el paginado de los filtros
		// page = 1 es el numero de la pagina inicial
		// primeraBusqueda = 1 es desde el formulario de busquedas
		// primeraBusqueda = 2 es desde del paginado
		// opc = 1 lista todos los usuarios
		// opc = 2 lista por filtro de roles
		// opc = 3 lista por filtro de documento
		// opc = 4 lista por filtro de nombre

		const responseUsuarios = await this._usuariosService.listarUsuarios(2, page, 'S');
		//console.log(responseUsuarios);
		if (responseUsuarios["status"] === "success" && responseUsuarios["code"] === "200") {
			this.llenarDatosTabla(page, responseUsuarios['data'], responseUsuarios['data'].usuarios);
			this.loading = false;
		}
		else if (responseUsuarios["status"] === "success" && responseUsuarios["code"] === "300") {
			this.loading = false;
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseUsuarios['message'] + '<b>', 2000);
		}
		else if (responseUsuarios["status"] === "error" && responseUsuarios["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseUsuarios['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			this._sweetAlertService.alertGeneral('¡Error!', responseUsuarios["message"], 'OK', 'error');
		}
	}

	llenarDatosTabla(page: any, responseUsuarios: any, datosUsuarios: any) {
		this.registros = datosUsuarios;
		this.dataTable.dataRows = [];
		this.total_item_count = responseUsuarios['total_item_count'];
		this.page_actual = page;

		this.total_pages = responseUsuarios['total_pages'];

		if (page >= 2) this.page_prev = page - 1;
		else this.page_prev = 1;

		if (page < responseUsuarios['total_pages']) this.page_next = page + 1;
		else {
			if (responseUsuarios['total_pages'] == 0) this.page_next = 1;
			else this.page_next = responseUsuarios['total_pages'];
		}

		let jsonRegistro;

		this.registros.forEach(element => {
			if (element.usuario != 'sinasignar') {
				jsonRegistro = {
					id: element.id,
					tipoDoc: element.tipoDocumento.valor,
					documento: element.documento,
					nombres: element.nombres,
					apellidos: element.apellidos,
					usuario: element.usuario,
					rol: element.rol.nombre,
					ultimaSesion: element.ultimaSesion == '0000-00-00' ? '- - -' : element.ultimaSesion,
					sesiones: element.sesiones == 0 ? 'Aún no ingresa' : element.sesiones,
					estado: element.estado.nombre
				};
				this.dataTable.dataRows.push(jsonRegistro);
			}
		});

		if (this.total_item_count || this.total_item_count == 0) {
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
		}
	}

	reasignarAsesores() {
		//		this.cargarTotalAsesores();
		$("#modalReasignarClientesAsesores").modal("show");
	}

	async cargarTotalAsesores() {
		this.loading = true;
		const responseTotalAsesores = await this._tableroAsignacionService.disponibilidadAsesores();
		//console.log(responseTotalAsesores);
		if (responseTotalAsesores["status"] === "success" && responseTotalAsesores["code"] === "200") {
			this.loading = false;
			this.cargarInformacionSelect(responseTotalAsesores['data']);
		}
		else if (responseTotalAsesores["status"] === "success" && responseTotalAsesores["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseTotalAsesores['message'] + '<b>', 2000);
		}
		else if (responseTotalAsesores["status"] === "error" && responseTotalAsesores["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseTotalAsesores['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseTotalAsesores["message"], 'OK', 'error');
		}
	}

	async cargarInformacionSelect(asesores: any) {
		let disponibilidadAsesorArray: any[] = [];
		this.todosAsesoresArray = [];

		if (asesores.asesoresDisponibles.length > 0) {
			disponibilidadAsesorArray = asesores.asesoresDisponibles;
		}
		if (asesores.asesoresOcupados.length >= 0) {
			this.todosAsesoresArray = disponibilidadAsesorArray.concat(asesores.asesoresOcupados);
		}
	}

	onSubmitReasignarClientesAsesores() {
		let asesorAnterior: string = '' + this.reasignarClientesAsesoresForm.value.asesorAnterior;
		let asesorNuevo: string = '' + this.reasignarClientesAsesoresForm.value.asesorNuevo;

		if (asesorAnterior === asesorNuevo) {
			this._sweetAlertService.alertGeneral('Advertencia!', 'Seleccione asesores diferentes para poder reasignar los clientes', 'OK', 'warning');
		} else {
			this.reasignarClientesAsesoresId(asesorAnterior, asesorNuevo);
		}
	}

	async reasignarClientesAsesoresId(asesorAnterior: string, asesorNuevo: string) {
		this.loading = true;

		const responseReasignacionAsesores = await this._tableroAsignacionService.reasignarAsesor(2, '', '', asesorAnterior, asesorNuevo);
		//console.log(responseReasignacionAsesores);
		if (responseReasignacionAsesores["status"] === "success" && responseReasignacionAsesores["code"] === "200") {
			this.loading = false;
			$("#modalReasignarClientesAsesores").modal("hide");
			this.reasignarClientesAsesoresForm.reset();
			this._sweetAlertService.showNotification('top', 'right', 'success', 'success', '<b>' + responseReasignacionAsesores['message'] + '<b>', 2000);
		}
		else if (responseReasignacionAsesores["status"] === "success" && responseReasignacionAsesores["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseReasignacionAsesores['message'] + '<b>', 2000);
		}
		else if (responseReasignacionAsesores["status"] === "error" && responseReasignacionAsesores["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseReasignacionAsesores['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseReasignacionAsesores["message"], 'error').then(result => { }).catch(swal.noop);
		}
	}

	buscarIdCliente(rowDocumento) {
		//console.log("buscar");
	}

	crearAsesor() {
		this._router.navigate(["asesores/registrar"]);
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) {
			result = e1 === e2;
		}
		return result;
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}
