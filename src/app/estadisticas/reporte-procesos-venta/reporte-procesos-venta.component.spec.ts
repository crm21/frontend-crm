import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteProcesosVentaComponent } from './reporte-procesos-venta.component';

describe('ReporteProcesosVentaComponent', () => {
  let component: ReporteProcesosVentaComponent;
  let fixture: ComponentFixture<ReporteProcesosVentaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteProcesosVentaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteProcesosVentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
