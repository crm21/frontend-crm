import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Reconsiderado } from "../../models/reconsiderado";
import swal from "sweetalert2";
import { ReconsideradoService } from '../../services/reconsiderado/reconsiderado.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare var $: any;

@Component({
	selector: 'app-crear-reconsiderado',
	templateUrl: './crear-reconsiderado.component.html',
	styleUrls: ['./crear-reconsiderado.component.css']
})
export class CrearReconsideradoComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	idCotizacion: any;
	ingresoFinalizado: any;
	botonMostrar: any;
	datosReconsideradoForm: FormGroup;
	tipoReconsideradoParametros: any;
	tipoReconsideradoArray: any[] = [];

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _reconsideradoService: ReconsideradoService,
		private _parametrizacionService: ParametrizacionService,
		private _sweetAlertService: SweetAlertService
	) {
		this.idCotizacion = this._route.snapshot.paramMap.get('idCotizacion');
		this.ingresoFinalizado = this._route.snapshot.paramMap.get('ingresoFinalizado');
		this.botonMostrar = this.ingresoFinalizado == 1 ? true : false;
	}

	async ngOnInit() {
		if (localStorage.getItem('identity_crm')) {
			this.datosReconsideradoForm = new FormGroup({
				reconsiderado: new FormControl(null, Validators.required),
				observacion: new FormControl(null),
			});
			this.loading = true;
			this.cargarInformacionSelect();
			this.loading = false;
		}
	}

	async onSubmitCrearReconsiderado() {
		this.loading = true;

		let reconsiderado = new Reconsiderado(
			this.idCotizacion,
			this.datosReconsideradoForm.value.reconsiderado,
			this.datosReconsideradoForm.value.observacion,
		); //console.log(reconsiderado);
		const responseReconsiderado = await this._reconsideradoService.registrarReconsiderado(reconsiderado);
		//console.log(responseReconsiderado);
		if (responseReconsiderado["status"] === "success" && responseReconsiderado["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseReconsiderado["message"], 'success')
				.then(result => {
					this.datosReconsideradoForm.reset();
					this._router.navigate(["clientes/listar-reconsiderado/", this.idCotizacion, this.ingresoFinalizado]);
				}).catch(swal.noop);
		}
		else if (responseReconsiderado["status"] === "success" && responseReconsiderado["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseReconsiderado["message"], 'OK', 'warning');
		}
		else if (responseReconsiderado["status"] === "error" && responseReconsiderado["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseReconsiderado['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseReconsiderado["message"], 'OK', 'error');
		}
	}

	async cargarInformacionSelect() {
		this.tipoReconsideradoParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Reconsiderado');

		this.tipoReconsideradoParametros.data.forEach((element) => {
			this.tipoReconsideradoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	regresar() {
		let idIngreso = localStorage.getItem('idIngreso');
		this._router.navigate(["clientes/ficha-clientes/", idIngreso]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}
