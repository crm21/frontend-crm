import { Routes } from '@angular/router';

import { RegistrarEventoComponent } from './registrar-evento/registrar-evento.component';
import { ListarAgendamientoComponent } from './listar-agendamiento/listar-agendamiento.component';
import { CalendarioEventosComponent } from './calendario-eventos/calendario-eventos.component';

export const EventosComercialesRoutes: Routes = [
	{
		path: '',
		children: [
			{
				path: 'listar',
				component: ListarAgendamientoComponent
			}, {
				path: 'registrar/:idCliente',
				component: RegistrarEventoComponent
			}, {
				path: 'calendario',
				component: CalendarioEventosComponent
			}
		]
	}
];