import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { ControlIndividualService } from '../../services/control-individual/control-individual.service';

declare const $: any;

@Component({
	selector: 'app-ficha-empleados',
	templateUrl: './ficha-empleados.component.html',
	styleUrls: ['./ficha-empleados.component.css']
})
export class FichaEmpleadosComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;

	/* Cargar información en variables para mostrar */
	empleado: any = {
		id: null,
		tipoDocumento: null,
		documento: null,
		nombres: null,
		apellidos: null,
		estado: null
	};
	empleadoId: any;
	contratoId: any;
	cargoEmpleadoId: any;
	nombreCargo: string = '';

	/* Variables booleanas para saber si se registra o no la información */
	existeContrato: boolean = true;
	existeCargo: boolean = true;
	existeAfiliacion: boolean = true;
	botonHabilitar: boolean = true;

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _sweetAlertService: SweetAlertService,
		private _controlIndividualService: ControlIndividualService
	) {
		this.empleadoId = this._route.snapshot.paramMap.get('idEmpleado');
		if (localStorage.getItem('identity_crm')) {
			this.listarFichaEmpleado(this.empleadoId);
		}
	}

	ngOnInit() { }

	async listarFichaEmpleado(idEmpleado: any) {
		this.loading = true;
		const responseControlIndividual = await this._controlIndividualService.listarControlIndividual(1, idEmpleado);
		//console.log(responseControlIndividual);
		if (responseControlIndividual["status"] === "success" && responseControlIndividual["code"] === "200") {
			this.botonHabilitar = false;
			this.cargarInformacionPersonalId(responseControlIndividual['data'].empleado);
			this.cargarInformacionContratoId(responseControlIndividual['data'].contrato);
			this.cargarInformacionCargoId(responseControlIndividual['data'].cargo);
			this.cargarAfiliacionesId(responseControlIndividual['data'].afiliaciones);
			this.loading = false;
		}
		else if (responseControlIndividual["status"] === "success" && responseControlIndividual["code"] === "300") {
			this.botonHabilitar = true;
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseControlIndividual['message'] + '<b>', 2000);
			this.loading = false;
		}
		else if (responseControlIndividual["status"] === "error" && responseControlIndividual["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseControlIndividual['message']);
		}
		else {
			this.botonHabilitar = true;
			this._sweetAlertService.alertGeneral('¡Error!', responseControlIndividual["message"], 'OK', 'error');
			this.loading = false;
		}
	}

	cargarInformacionPersonalId(datosPersonales: any) {
		if (datosPersonales != '0') {
			this.empleado.id = datosPersonales.id;
			this.empleado.tipoDocumento = datosPersonales.tipoDocumento.valor;
			this.empleado.documento = datosPersonales.documento;
			this.empleado.nombres = datosPersonales.nombres;
			this.empleado.apellidos = datosPersonales.apellidos;
			this.empleado.estado = datosPersonales.estado.nombre;
			//console.log(datosPersonales);
		}
	}

	cargarInformacionContratoId(datosContrato: any) {
		if (datosContrato == '0') this.existeContrato = false;
		else {
			this.contratoId = datosContrato.id;
			this.existeContrato = true;
		}
	}

	cargarInformacionCargoId(datosCargo: any) {
		if (datosCargo == '0') {
			this.existeCargo = false;
			this.nombreCargo = '';
		}
		else {
			this.cargoEmpleadoId = datosCargo.id;
			this.nombreCargo = datosCargo.nombre;
			this.existeCargo = true;
		}
	}

	cargarAfiliacionesId(datosAfiliacion: any) {
		if (datosAfiliacion == '0') this.existeAfiliacion = false;
		else this.existeAfiliacion = true;
	}

	regresar() {
		this._router.navigate(["empleados/listar"]);
	}

	controlIndividual(documento) {
		localStorage.setItem('controlId', documento);
		this._router.navigate(["control-individual/listar-control-individual"]);
	}

	buscarIdCliente(rowIdEmpleado) {
		// opc = 2 para modificar empleado
		this._router.navigate(["empleados/actualizar/", rowIdEmpleado]);
	}

	gestionContratos(opcion: any, valorFiltro: any) {
		if (opcion == '1') {
			localStorage.setItem('empleadoID', this.empleadoId);
			this._router.navigate(["control-individual/ver-contrato-empleado/", valorFiltro]);
		}
		else {
			this._router.navigate(["control-individual/registrar-contrato-empleado/", valorFiltro]);
		}
	}

	verCargoFunciones() {
		localStorage.setItem('empleadoID', this.empleadoId);
		this._router.navigate(["control-individual/listar-funcion/", this.cargoEmpleadoId]);
	}

	verAfiliacion() {
		localStorage.setItem('empleadoID', this.empleadoId);
		this._router.navigate(["control-individual/listar-afiliaciones-empleado", this.contratoId]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}