import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { TicketsService } from '../../services/tickets/tickets.service';
import { RegistrarTicket, TicketAprobado } from '../../models/ticket';
import { UsuarioService } from '../../services/usuario/usuario.service';

declare var $: any;
declare interface DataTable {
	headerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-tickets-listar',
	templateUrl: './tickets-listar.component.html',
	styleUrls: ['./tickets-listar.component.css']
})
export class TicketsListarComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	mostraBotonContestacion: boolean = false;
	consultaVaciaCargarSpinner: boolean = false;
	crearTicketUsuarioForm: FormGroup;
	cancelarTicketUsuarioForm: FormGroup;
	dataTable: DataTable;
	estadosArray: any[] = [];
	categoriaArray: any[] = [];
	ticketReporteArray: any[] = [];
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	textoExtendido: string = '';
	idTicket: string;
	permisosContestacion: any[] = ['A', 'GA', 'GG'];
	busquedaFiltros: any = {
		estado: ''
	};
	resetVar: boolean;
	nombreArchivoTicket: string = '';
	afuConfig = {
		multiple: false,
		formatsAllowed: ".jpg,.png,.pdf,.docx,.txt,.jpeg,.xlsx,.doc,.xls,.zip",
		maxSize: "7",
		uploadAPI: {
			url: this._usuarioService.comprobarEmpresaServicios(localStorage.getItem('business')) + '/ticketAnexo',
			method: "POST",
			headers: {
				"Authorization": localStorage.getItem("token_crm")
			},
			params: {
				'page': '1'
			},
			responseType: 'blob',
		},
		/* theme: "attachPin", */
		hideProgressBar: false,
		hideResetBtn: true,
		hideSelectBtn: false,
		fileNameIndex: true,
		replaceTexts: {
			selectFileBtn: 'Seleccionar archivos',
			resetBtn: 'Reiniciar',
			uploadBtn: 'Subir archivo',
			dragNDropBox: 'Arrastrar y Soltar',
			attachPinBtn: 'Sube la imagen',
			afterUploadMsg_success: 'Archivo cargado con éxito !',
			afterUploadMsg_error: 'Subida fallida !',
			sizeLimit: 'Límite de tamaño'
		}
	};

	constructor(
		private _router: Router,
		private _formBuilder: FormBuilder,
		private _usuarioService: UsuarioService,
		private _ticketsService: TicketsService,
		private _sweetAlertService: SweetAlertService,
		private _parametrizacionService: ParametrizacionService
	) {
		if (localStorage.getItem('identity_crm')) {
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			if (this.permisosContestacion.includes(identityData['data'].rol.valor)) {
				this.mostraBotonContestacion = true;
			}
			
			this.cargarInformacionSelect();
		}
	}

	ngOnInit() {
		this.dataTable = {
			headerRow: [
				'#',
				'Consecutivo Ticket',
				'Categoría de Solicitud',
				'Solicitado Por',
				'Fecha de Solicitud',
				'Descripción de la Solicitud',
				'Contestado Por',
				'Fecha de Contestación',
				'Descripción de la Contestación',
				'Estado',
				'Acciones'
			],
			dataRows: []
		};
		this.crearTicketUsuarioForm = this._formBuilder.group({
			categoria: ['', Validators.required],
			descripcionSolicitud: ['', Validators.required],
			anexo: ['']
		});
		this.cancelarTicketUsuarioForm = this._formBuilder.group({
			descripcionCancelacion: ['', Validators.required]
		});
		if (localStorage.getItem('identity_crm')) {
			this.listarTickets(1);
		}
	}

	async listarTickets(page: any) {
		this.loading = true;

		const responseListarTickets = await this._ticketsService.listarTickets('1', page, '', this.busquedaFiltros.estado);
		//console.log(responseListarTickets);
		if (responseListarTickets['status'] == "success" && responseListarTickets['code'] == "200") {
			this.loading = false;
			this.llenarTabla(responseListarTickets['data'], page);
		}
		else if (responseListarTickets['status'] == "success" && responseListarTickets['code'] == "300") {
			this.loading = false;
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			this._sweetAlertService.showNotification('top', 'right', 'success', 'warning', '<b>' + responseListarTickets['message'] + '<b>', 2000);
		}
		else if (responseListarTickets["status"] === "error" && responseListarTickets["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseListarTickets['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			swal('¡Error!', responseListarTickets['message'], 'error');
		}
	}

	llenarTabla(tickets: any, page: any) {
		//console.log(tickets);
		this.ticketReporteArray = tickets['tickets'];
		this.dataTable.dataRows = [];
		this.total_item_count = tickets['total_item_count'];
		this.page_actual = page;

		this.total_pages = tickets['total_pages'];

		if (page >= 2) this.page_prev = page - 1;
		else this.page_prev = 1;

		if (page < tickets['total_pages']) this.page_next = page + 1;
		else {
			if (tickets['total_pages'] == 0) this.page_next = 1;
			else this.page_next = tickets['total_pages'];
		}

		let JSONParam;

		this.ticketReporteArray.forEach(element => {
			JSONParam = {
				id: element.id,
				consecutivo: element.consecutivoTicket,
				categoriaSolicitud: element.categoriaSolicitud.nombre,
				solicitadoPor: element.solicitadoPor.nombres + ' ' + element.solicitadoPor.apellidos,
				fechaSolicitud: element.fechaSolicitud,
				descripcionSolicitud: element.descripcionSolicitud,
				aprobadoPor: element.aprobadoPor == null ? '- - -' : element.aprobadoPor.nombres + ' ' + element.aprobadoPor.apellidos,
				fechaAprobacion: element.fechaAprobacion == null ? '- - -' : element.fechaAprobacion,
				descripcionAprobacion: element.descripcionAprobacion == null ? '- - -' : element.descripcionAprobacion,
				estado: element.estado.valor == 'cerradoT' ? 'aprobado <br>' + element.estado.nombre : element.estado.nombre,
				validarCancelarTicket: element.estado.valor == 'pendienteRevisionT' ? true : false,
				valorEstadoTicket: element.estado.valor,
				colorEstado: this.darColorEstadoEvento(element.estado.valor)
			};
			this.dataTable.dataRows.push(JSONParam);
		});
		//console.log(this.dataTable.dataRows);
		if (this.total_item_count || this.total_item_count == 0) {
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
		}
	}

	async cargarInformacionSelect() {
		this.categoriaArray = [];
		let categoriaParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Tipos de Solicitud Tickets');
		let estadosParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Estado Ticket');

		categoriaParametros['data'].forEach((element) => {
			this.categoriaArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});

		estadosParametros['data'].forEach((element) => {
			this.estadosArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
	}

	registraTicketVentana() {
		$("#modalRegistrar").modal("show");
	}

	cancelarSolicitud(rowTicket: any) {
		this.idTicket = rowTicket.id;
		$("#modalCancelar").modal("show");
	}

	async onSubmitCrearTicketsUsuario() {
		this.loading = true;

		let crearTicket = new RegistrarTicket(
			this.crearTicketUsuarioForm.value.categoria,
			this.crearTicketUsuarioForm.value.descripcionSolicitud,
			this.nombreArchivoTicket
		); //console.log(crearTicket);
		
		const responseCrearTicket = await this._ticketsService.registrarTicketUsuario(crearTicket);
		//console.log(responseCrearTicket);
		if (responseCrearTicket["status"] === "success" && responseCrearTicket["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseCrearTicket["message"], 'success')
				.then(result => {
					this.crearTicketUsuarioForm.reset();
					this.nombreArchivoTicket = '';
					$("#modalRegistrar").modal("hide");
					this.listarTickets(1);
				}).catch(swal.noop);
		}
		else if (responseCrearTicket["status"] === "success" && responseCrearTicket["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseCrearTicket["message"], 'OK', 'warning');
		}
		else if (responseCrearTicket["status"] === "error" && responseCrearTicket["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseCrearTicket['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseCrearTicket["message"], 'OK', 'error');
		}
	}

	async onSubmitCancelarTicketsUsuario() {
		this.loading = true;
		let cancelarTicket = new TicketAprobado(
			'canceladoT',
			this.cancelarTicketUsuarioForm.value.descripcionCancelacion
		); //console.log(cancelarTicket);

		const responseCancelarTicket = await this._ticketsService.aprobadoTicketUsuario(this.idTicket, cancelarTicket);
		//console.log(responseCancelarTicket);
		if (responseCancelarTicket["status"] === "success" && responseCancelarTicket["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseCancelarTicket["message"], 'success')
				.then(result => {
					this.cancelarTicketUsuarioForm.reset();
					$("#modalCancelar").modal("hide");
					this.listarTickets(1);
				}).catch(swal.noop);
		}
		else if (responseCancelarTicket["status"] === "success" && responseCancelarTicket["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseCancelarTicket["message"], 'OK', 'warning');
		}
		else if (responseCancelarTicket["status"] === "error" && responseCancelarTicket["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseCancelarTicket['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseCancelarTicket["message"], 'OK', 'error');
		}

	}

	confirmarRevision(rowId: string, nombreConsecutivo: string) {
		swal({
			title: 'Revisión Ticket',
			text: "Iniciar revisión del ticket " + nombreConsecutivo,
			type: 'question',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: 'Si, Revisar!',
			cancelButtonText: 'Cancelar',
			buttonsStyling: true,
			reverseButtons: true
		}).then((result) => {
			if (result.value) {
				this.cambiarARevision(rowId);
			}
		});
	}

	async cambiarARevision(rowId: string) {
		this.loading = true;

		const responseRevisionTicket = await this._ticketsService.revisionTicketUsuario(rowId);
		//console.log(responseRevisionTicket);
		if (responseRevisionTicket['status'] == "success" && responseRevisionTicket['code'] == "200") {
			this.loading = false;
			this.revisarTicket(rowId);
		}
		else if (responseRevisionTicket['status'] == "success" && responseRevisionTicket['code'] == "300") {
			this.loading = false;
			this.listarTickets(1);
			this._sweetAlertService.showNotification('top', 'right', 'success', 'warning', '<b>' + responseRevisionTicket['message'] + '<b>', 2000);
		}
		else if (responseRevisionTicket["status"] === "error" && responseRevisionTicket["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseRevisionTicket['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseRevisionTicket['message'], 'error')
				.then((result) => {
					this.listarTickets(1);
				});
		}
	}

	darColorEstadoEvento(estado: string): string {
		switch (estado) {
			case 'canceladoT':
				return 'optimus-black';
				break;
			case 'rechazadoT':
				return 'danger';
				break;
			case 'aprobadoT':
				return 'success';
				break;
			case 'cerradoT':
				return 'success';
				break;
			case 'enRevisionT':
				return 'warning';
				break;
			case 'pendienteRevisionT':
				return 'default';
				break;
			default:
				return 'default';
				break;
		}
	}

	async subirArchivosTicket(dataResponse: any) {
		this.loading = true;
		const responseCargarAnexo = JSON.parse(dataResponse.response);
		//console.log(responseCargarAnexo);
		if (responseCargarAnexo["status"] === "success" && responseCargarAnexo["code"] === "200") {
			this.loading = false;
			this.nombreArchivoTicket = responseCargarAnexo["data"];
			this._sweetAlertService.showNotification('top', 'right', 'success', 'success', '<b>' + responseCargarAnexo['message'] + '<b>', 2000);
		}
		else if (responseCargarAnexo["status"] === "success" && responseCargarAnexo["code"] === "300") {
			this.loading = false;
			this.nombreArchivoTicket = '';
			this._sweetAlertService.alertGeneral('Advertencia!', responseCargarAnexo["message"], 'OK', 'warning');
		}
		else if (responseCargarAnexo["status"] === "error" && responseCargarAnexo["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseCargarAnexo['message']);
		}
		else {
			this.loading = false;
			this.nombreArchivoTicket = '';
			this._sweetAlertService.alertGeneral('¡Error!', responseCargarAnexo["message"], 'OK', 'error');
		}
	}

	limpiarFiltros() {
		this.busquedaFiltros.estado = '';
		(document.getElementById('estado') as HTMLInputElement).value = '';
		this.listarTickets(1);
	}

	observarTexto(texto: any) {
		this.textoExtendido = texto;
		$("#modalObservacion").modal("show");
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	revisarTicket(rowId: string) {
		this._router.navigate(["tickets/revision/", rowId]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}