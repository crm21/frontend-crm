import { Routes } from '@angular/router';

import { InicioComponent } from './inicio/inicio.component';
import { ClientesScComponent } from './clientes-sc/clientes-sc.component';
import { ClientesSemillaComponent } from './clientes-semilla/clientes-semilla.component';
import { ClientesSembradosComponent } from './clientes-sembrados/clientes-sembrados.component';
import { ClientesCosechadosComponent } from './clientes-cosechados/clientes-cosechados.component';
import { FichaClientesScComponent } from './ficha-clientes-sc/ficha-clientes-sc.component';
import { ClientesProyectadosComponent } from './clientes-proyectados/clientes-proyectados.component';

export const SiembraCosechaRoutes: Routes = [
	{
		path: '',
		redirectTo: 'dashboard',
		pathMatch: 'full',
	},
	{
		path: '',
		children: [
			{
				path: '',
				component: InicioComponent
			},{
				path: '*',
				component: InicioComponent
			},{
				path: 'dashboard',
				component: InicioComponent
			}, {
				path: 'clientes',
				component: ClientesScComponent
			}, {
				path: 'clientes-semilla',
				component: ClientesSemillaComponent
			}, {
				path: 'clientes-proyectados',
				component: ClientesProyectadosComponent
			}, {
				path: 'clientes-sembrados',
				component: ClientesSembradosComponent
			}, {
				path: 'clientes-cosechados',
				component: ClientesCosechadosComponent
			}, {
				path: 'registrar-cliente',
				component: FichaClientesScComponent,
				data: {
					opc: 1
				}
			}, {
				path: 'ficha-cliente/:idCliente',
				component: FichaClientesScComponent,
				data: {
					opc: 2
				}
			}
		]
	}
];