import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { AuditoriaServices } from '../../services/auditoria/auditoria.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

declare var $: any;
declare interface DataTable {
	headerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-auditoria',
	templateUrl: './auditoria.component.html',
	styleUrls: ['./auditoria.component.css']
})
export class AuditoriaComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	consultaVaciaCargarSpinner: boolean = false;
	dataTable: DataTable;
	registros: any[] = [];
	separacionParametros: string[] = [];
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	textoExtendido: string = '';

	constructor(
		private _router: Router,
		private _auditoriaService: AuditoriaServices,
		private _sweetAlertService: SweetAlertService
	) { }

	ngOnInit() {
		if (localStorage.getItem('identity_crm')) {
			this.dataTable = {
				headerRow: [
					'#',
					'ID Usuario',
					'Documento Usuario',
					'Nombre Usuario',
					'Tabla',
					'Valores Relevantes',
					'Acción',
					'Fecha Creación',
					'ID Elemento',
					'Origen'
				],
				dataRows: []
			};
			this.listarRegistrosAuditoria(1);
		}
	}

	async listarRegistrosAuditoria(page: any) {
		this.loading = true;

		const responseAuditoria = await this._auditoriaService.listarAuditoria(page);
		//console.log(responseAuditoria);
		if (responseAuditoria['status'] == "exito" && responseAuditoria['code'] == "200") {
			this.loading = false;
			this.registros = responseAuditoria['auditoria'];
			this.dataTable.dataRows = [];
			this.total_item_count = responseAuditoria['total_item_count'];
			this.page_actual = page;


			this.total_pages = responseAuditoria['total_pages'];
			if (page >= 2) this.page_prev = page - 1;
			else this.page_prev = 1;

			if (page < responseAuditoria['total_pages']) this.page_next = page + 1;
			else {
				if (responseAuditoria['total_pages'] == 0) this.page_next = 1;
				else this.page_next = responseAuditoria['total_pages'];
			}

			let jsonRegistro;

			this.registros.forEach(element => {
				jsonRegistro = {
					id: element.id,
					idUsuario: element.idUsuario,
					documentoUsuario: element.documentoUsuario,
					nombreUsuario: element.nombreUsuario,
					tabla: element.tabla,
					valoresRelevantes: element.valoresRelevantes,
					accion: element.accion,
					creacionAuditoria: element.creacionAuditoria,
					idElemento: element.idElemento,
					origenAuditoria: element.origenAuditoria
				};
				this.dataTable.dataRows.push(jsonRegistro);
			});

			if (this.total_item_count || this.total_item_count == 0) {
				this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			}
		}
		else if (responseAuditoria['status'] == "exito" && responseAuditoria['code'] == "300") {
			this.loading = false;
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			swal('Advertencia!', responseAuditoria['message'], 'warning');
		}
		else if (responseAuditoria["status"] === "error" && responseAuditoria["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseAuditoria['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			swal('¡Error!', responseAuditoria['message'], 'error');
		}
	}

	listarRegistros(opc: number) {
		if (opc == 1) {
			this._router.navigate(["registros-sistema/auditoria"]);
		} else {
			this._router.navigate(["registros-sistema/excepciones"]);
		}
	}

	observarTexto(texto: any) {
		this.separacionParametros = texto.split(",");
		//console.log(texto);
		//console.log(this.separacionParametros);
		this.textoExtendido = texto;
		$("#modalValorRelevante").modal("show");
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}