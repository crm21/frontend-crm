import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContratosEmpleadoComponent } from './contratos-empleado.component';

describe('ContratosEmpleadoComponent', () => {
  let component: ContratosEmpleadoComponent;
  let fixture: ComponentFixture<ContratosEmpleadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContratosEmpleadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContratosEmpleadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
