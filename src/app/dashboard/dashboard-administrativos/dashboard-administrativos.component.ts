import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { UsuarioService } from '../../services/usuario/usuario.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';


@Component({
	selector: 'app-dashboard-administrativos',
	templateUrl: './dashboard-administrativos.component.html',
	styleUrls: ['./dashboard-administrativos.component.css']
})
export class DashboardAdministrativosComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	nombres: string;
	nombre: string = 'Usuario';
	nombresEmpresas: string = '';
	imgNombreEmpresa: string = '';

	constructor(
		private _usuarioService: UsuarioService,
		private _sweetAlertService: SweetAlertService,
		private _router: Router
	) {
		if (localStorage.getItem('identity_crm')) {
			this.consultarNombreEmpresa(localStorage.getItem('business'));

			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			let dataUsuario = identityData['data'];
			this.nombres = dataUsuario.nombres + ' ' + dataUsuario.apellidos;
			this.nombre = dataUsuario.rol.nombre;
		}
	}

	ngOnInit() { }

	consultarNombreEmpresa(nombreEmpresa: any) {
		this.nombresEmpresas = this._usuarioService.nombresEmpresa(nombreEmpresa);
		switch (nombreEmpresa) {
			case 'A':
				this.imgNombreEmpresa = './assets/img/empresa-autodenar.png';
				break;
			case 'M':
				this.imgNombreEmpresa = './assets/img/empresa-motodenar.png';
				break;
			case 'T':
				this.imgNombreEmpresa = './assets/img/empresa-automotora.png';
				break;
			case 'D':
				this.imgNombreEmpresa = './assets/img/empresa-autodenar.png';
				break;
			default:
				this.imgNombreEmpresa = './assets/img/empresa-autodenar.png';
				break;
		}
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}