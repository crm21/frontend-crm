import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteClientesLeadsAnioComponent } from './reporte-clientes-leads-anio.component';

describe('ReporteClientesLeadsAnioComponent', () => {
  let component: ReporteClientesLeadsAnioComponent;
  let fixture: ComponentFixture<ReporteClientesLeadsAnioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteClientesLeadsAnioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteClientesLeadsAnioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
