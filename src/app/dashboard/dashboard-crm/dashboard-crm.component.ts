import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from "@angular/router";
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { TableroAsignacionService } from '../../services/tablero-asignacion/tablero-asignacion.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';
import { EstadisticasService } from '../../services/estadisticas/estadisticas.service';
import { UsuarioService } from '../../services/usuario/usuario.service';
//import { TicketsService } from '../../services/tickets/tickets.service';
import Chart from 'chart.js';
import swal from 'sweetalert2';

declare var $: any;

@Component({
	selector: 'app-dashboard-crm',
	templateUrl: './dashboard-crm.component.html',
	styleUrls: ['./dashboard-crm.component.css']
})
export class DashboardCrmComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	nombres: string;
	nombre: string = 'Usuario';
	nombreRolValor: string = '';
	nombresEmpresas: string = '';

	finalizarAtencionForm: FormGroup;
	ventanaAsesorDisponible: boolean = false;
	estadisticasLeads: boolean = false;
	ocultarVentanaAsesor: any;
	mostraAtencionAsesor: boolean = false;
	//verTicketsInOut: boolean = false;
	verProcesosVentaVaciosCompletos: boolean = false;
	disponibilidaAsesor: any;
	nombreCliente: any;
	documento: any;
	fechaInicial: any;
	idAtencion: any;
	idIngreso: string = '';
	idUsuario: number = 0;
	objecionesArray: any[] = [];
	tipificacionArray: any[] = [];
	permisosAtenciones: any[] = ['N', 'CL'];

	/* Variables estadistica */
	clientesDiaNum: any = '----';
	clientesSemanaNum: any = '----';
	clientesMesNum: any = '----';
	clientesAnioNum: any = '----';
	vehiculosVendidosNum: any = '----';
	clientesNoVentasNum: any = '----';
	clientesConCotizacionNum: any = '----';
	clientesSinCotizacionNum: any = '----';
	clientesVehiculoUsadoNum: any = '----';
	clientesEsperaNum: any = '----';
	estudiosCreditoNum: any = '----';
	ingresosActivosNum: any = '----';
	procesosVentaNum: any = '----';
	//ticketsAbiertosNum: any = '----';
	//ticketsCerradosNum: any = '----';
	procesosVentaVaciosNum: any = '----';
	procesosVentaCompletosNum: any = '----';
	imgNombreEmpresa: string = '';
	labelsDatos: string[] = [];
	categoriasEstadisticasEventos: string[] = [];
	nombreDiagramaBarraEmpresa = {
		param1: 'Vehículos',
		param2: 'Entregados',
	};

	constructor(
		private _tableroAsignacionService: TableroAsignacionService,
		private _parametrizacionService: ParametrizacionService,
		private _estadisticastService: EstadisticasService,
		private _sweetAlertService: SweetAlertService,
		private _usuarioService: UsuarioService,
		//private _ticketsService: TicketsService,
		private _formBuilder: FormBuilder,
		private _router: Router
	) {
		if (localStorage.getItem('identity_crm')) {
			this.consultarNombreEmpresa(localStorage.getItem('business'));

			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			this.ocultarVentanaAsesor = identityData['data'];
			this.nombres = this.ocultarVentanaAsesor.nombres + ' ' + this.ocultarVentanaAsesor.apellidos;
			this.nombre = this.ocultarVentanaAsesor.rol.nombre;
			this.nombreRolValor = this.ocultarVentanaAsesor.rol.valor;
			this.idUsuario = this.ocultarVentanaAsesor.sub;
			this.mostrarProcesosVentaVaciosCompletos();
			//this.mostrarTicketsRoles();
			this.mostrarEstadisticasLeadsRoles();

			if (!this.permisosAtenciones.includes(this.ocultarVentanaAsesor.rol.valor)) {
				this.mostraAtencionAsesor = true;
				this.cargarInformacionSelect();
				this.consultarDisponibilidadAsesor(this.idUsuario);
			}
		}
	}

	ngOnInit() {
		this.finalizarAtencionForm = this._formBuilder.group({
			objecion: ['NN', Validators.required],
			tipificacion: ['Ninguno'],
			observacion: ['']
		});
		if (localStorage.getItem('identity_crm')) {
			this.diagramaPastelEventos();
			this.diagramaBarrasVehiculos();
			this.diagramaBarrasDesembolsos();
			this.clientesDiaReporte(this.idUsuario);
			this.clientesSemanaReporte(this.idUsuario);
			this.clientesMesReporte(this.idUsuario);
			this.clientesAnioReporte(this.idUsuario);
			this.clientesIngresosActivos(this.idUsuario);
			this.clientesProcesosVentas(this.idUsuario);
			this.vehiculosVendidosReporte(this.idUsuario);
			this.clientesNoVentasReporte(this.idUsuario);
			this.clientesConCotizacionReporte(this.idUsuario);
			this.clientesSinCotizacionReporte(this.idUsuario);
			this.clientesVehiculoUsadoReporte(this.idUsuario);
			this.reporteEstudiosCredito(this.idUsuario);
			this.clientesEsperaReporte(this.idUsuario);
		}
	}

	ngOnDestroy() {
		localStorage.removeItem('ruta');
	}

	/* Inicia Validaciones para mostrar/ocultar card de estadisticas de acuerdo a ciertos parametros */
	mostrarProcesosVentaVaciosCompletos() {
		switch (this.nombreRolValor) {
			case 'A':
				this.verProcesosVentaVaciosCompletos = true;
				this.listarProcesosVentaVaciosCompletos();
				break;
			case 'GG':
				this.verProcesosVentaVaciosCompletos = true;
				this.listarProcesosVentaVaciosCompletos();
				break;
			case 'GA':
				this.verProcesosVentaVaciosCompletos = true;
				this.listarProcesosVentaVaciosCompletos();
				break;
			case 'GC':
				this.verProcesosVentaVaciosCompletos = true;
				this.listarProcesosVentaVaciosCompletos();
				break;
			case 'CB':
				this.verProcesosVentaVaciosCompletos = true;
				this.listarProcesosVentaVaciosCompletos();
				break;
			case 'DN':
				this.verProcesosVentaVaciosCompletos = true;
				this.listarProcesosVentaVaciosCompletos();
				break;
			case 'DN':
				this.verProcesosVentaVaciosCompletos = true;
				this.listarProcesosVentaVaciosCompletos();
				break;
			default:
				this.verProcesosVentaVaciosCompletos = false;
				break;
		}
	}

	/* mostrarTicketsRoles() {
		switch (this.nombreRolValor) {
			case 'A':
				this.verTicketsInOut = true;
				this.ticketsAbiertosCerradosReporte();
				break;
			case 'GG':
				this.verTicketsInOut = true;
				this.ticketsAbiertosCerradosReporte();
				break;
			case 'GA':
				this.verTicketsInOut = true;
				this.ticketsAbiertosCerradosReporte();
				break;
			default:
				this.verTicketsInOut = false;
				break;
		}
	} */

	mostrarEstadisticasLeadsRoles() {
		switch (this.nombreRolValor) {
			case 'A':
				this.estadisticasLeads = true;
				break;
			case 'DT':
				this.estadisticasLeads = true;
				break;
			case 'GG':
				this.estadisticasLeads = true;
				break;
			case 'GA':
				this.estadisticasLeads = true;
				break;
			case 'GC':
				this.estadisticasLeads = true;
				break;
			case 'CM':
				this.estadisticasLeads = true;
				break;
			case 'DC':
				this.estadisticasLeads = true;
				break;
			default:
				this.estadisticasLeads = false;
				break;
		}
	}
	/* Inicia Validaciones para mostrar/ocultar card de estadisticas de acuerdo a ciertos parametros */

	/* Funcion para iniciar los diagramas de barras */
	startAnimationForBarChart(chart: any) {
		let seq2: any, delays2: any, durations2: any;
		seq2 = 0;
		delays2 = 10;
		durations2 = 500;
		chart.on('draw', function (data: any) {
			if (data.type === 'bar') {
				seq2++;
				data.element.animate({
					opacity: {
						begin: seq2 * delays2,
						dur: durations2,
						from: 0,
						to: 1,
						easing: 'ease'
					}
				});
			}
		});

		seq2 = 0;
	}

	consultarNombreEmpresa(nombreEmpresa: any) {
		this.nombresEmpresas = this._usuarioService.nombresEmpresa(nombreEmpresa);
		switch (nombreEmpresa) {
			case 'A':
				this.imgNombreEmpresa = './assets/img/empresa-autodenar.png';
				this.nombreDiagramaBarraEmpresa.param1 = 'Vehículos';
				this.nombreDiagramaBarraEmpresa.param2 = 'Entregados';
				break;
			case 'M':
				this.imgNombreEmpresa = './assets/img/empresa-motodenar.png';
				this.nombreDiagramaBarraEmpresa.param1 = 'Motos';
				this.nombreDiagramaBarraEmpresa.param2 = 'Entregadas';
				break;
			case 'T':
				this.imgNombreEmpresa = './assets/img/empresa-automotora.png';
				this.nombreDiagramaBarraEmpresa.param1 = 'Vehículos';
				this.nombreDiagramaBarraEmpresa.param2 = 'Entregados';
				break;
			case 'D':
				this.imgNombreEmpresa = './assets/img/empresa-autodenar.png';
				break;
			default:
				this.imgNombreEmpresa = './assets/img/empresa-autodenar.png';
				this.nombreDiagramaBarraEmpresa.param1 = 'Vehículos';
				this.nombreDiagramaBarraEmpresa.param2 = 'Entregados';
				break;
		}
	}

	async consultarDisponibilidadAsesor(idAsesor: any) {
		this.loading = true;
		this.disponibilidaAsesor = await this._tableroAsignacionService.consultarDisponibilidadAsesor(idAsesor);
		//console.log(this.disponibilidaAsesor);
		if (this.disponibilidaAsesor["status"] === "success" && this.disponibilidaAsesor["code"] === "200") {
			if (this.disponibilidaAsesor.data.disponibilidad.estado == true) {
				const responseAtenciones = this.atencionesAsesorArray(this.disponibilidaAsesor.data.disponibilidad.atenciones);
				//console.log(responseAtenciones);
				this.idAtencion = responseAtenciones.id;
				this.idIngreso = responseAtenciones.idIngreso;
				this.nombreCliente = responseAtenciones.cliente;
				this.documento = responseAtenciones.documento;
				this.fechaInicial = responseAtenciones.fechaInicial;
				this.ventanaAsesorDisponible = true;
			}
			else {
				this.ventanaAsesorDisponible = false;
			}
			this.loading = false;
		}
		else if (this.disponibilidaAsesor["status"] === "error" && this.disponibilidaAsesor["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(this.disponibilidaAsesor['message']);
		}
		else {
			this.ventanaAsesorDisponible = false;
			this.loading = false;
		}
	}

	atencionesAsesorArray(atenciones): any {
		let atencionesAsesor = {
			id: null,
			idIngreso: null,
			cliente: null,
			documento: null,
			fechaInicial: null
		};
		atenciones.forEach((element) => {
			if (element.fechaFinal == '0000-00-00') {
				atencionesAsesor.id = element.id;
				atencionesAsesor.idIngreso = '' + element.ingreso.id;
				atencionesAsesor.cliente = element.ingreso.persona.nombres + ' ' + element.ingreso.persona.apellidos;
				atencionesAsesor.documento = element.ingreso.persona.documento;
				atencionesAsesor.fechaInicial = element.fechaInicial;
			}
		});

		return atencionesAsesor;
	}

	async cargarInformacionSelect() {
		this.objecionesArray = [];
		this.tipificacionArray = [];
		const objecionesParametros: Object = await this._parametrizacionService.listarCategoriaParametrizacion('Objeciones');
		const tipificacionParametros: Object = await this._parametrizacionService.listarCategoriaParametrizacion('Tipificación Clientes');

		objecionesParametros['data'].forEach((element) => {
			this.objecionesArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
		tipificacionParametros['data'].forEach((element) => {
			this.tipificacionArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
	}

	finalizarAtencion(idAtencion) {
		$("#modalFinalizarAtencion").modal("show");
	}

	async onSubmitFinalizarAtencion() {
		this.loading = true;
		let objecion_ = this.finalizarAtencionForm.value.objecion;
		//let tipificacion_ = this.finalizarAtencionForm.value.tipificacion;
		let tipificacion_ = 'Ninguno';
		let observacion_ = this.finalizarAtencionForm.value.observacion;
		observacion_ = observacion_ == null ? '' : observacion_;
		//console.log(`${this.idAtencion}, ${objecion_}, ${observacion_}`);
		const responseFinalizarAtencion = await this._tableroAsignacionService.finalizarAtencionAsesor(this.idAtencion, objecion_, tipificacion_, observacion_);
		//console.log(responseFinalizarAtencion);
		if (responseFinalizarAtencion["status"] === "success" && responseFinalizarAtencion["code"] === "200") {
			this.consultarDisponibilidadAsesor(this.ocultarVentanaAsesor.sub);
			$("#modalFinalizarAtencion").modal("hide");
			this.finalizarAtencionForm.reset();
			this._sweetAlertService.showNotification('top', 'right', 'success', 'success', '<b>' + responseFinalizarAtencion['message'] + '<b>', 2000);
			this.loading = false;
		}
		else if (responseFinalizarAtencion["status"] === "error" && responseFinalizarAtencion["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseFinalizarAtencion['message']);
		}
		else {
			this.ventanaAsesorDisponible = false;
			$("#modalFinalizarAtencion").modal("hide");
			this.finalizarAtencionForm.reset();
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseFinalizarAtencion['message'] + '<b>', 2000);
			this.loading = false;
		}
	}

	verFichaCliente(idIngreso: string) {
		this._router.navigate(["clientes/ficha-clientes/", idIngreso]);
	}

	/* Estadisticas */
	async diagramaPastelEventos() {
		this.loading = true;
		const responseDiagramaPastelEventos = await this._estadisticastService.indicadorPastelEventos();
		//console.log(responseDiagramaPastelEventos);
		if (responseDiagramaPastelEventos["status"] === "success" && responseDiagramaPastelEventos["code"] === "200") {
			this.llenarDatosDiagramaPastelEventos(responseDiagramaPastelEventos["data"]);
			this.loading = false;
		}
		else if (responseDiagramaPastelEventos["status"] === "success" && responseDiagramaPastelEventos["code"] === "300") {
			this.loading = false;
		} else if (responseDiagramaPastelEventos["status"] === "error" && responseDiagramaPastelEventos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseDiagramaPastelEventos['message']);
		}
		else {
			this.loading = false;
		}
	}

	llenarDatosDiagramaPastelEventos(dataDiagrama: any) {
		this.labelsDatos = dataDiagrama.labels;
		let seriesDatos = dataDiagrama.serie;
		this.categoriasEstadisticasEventos = dataDiagrama.categoria;

		var ctx = document.getElementById('agendamientosEventos');
		var myChart = new Chart(ctx, {
			type: 'pie',
			data: {
				datasets: [{
					data: seriesDatos,
					backgroundColor: [
						'rgba(69, 61, 63, 0.2)',
						'rgba(209, 121, 5, 0.2)',
						'rgba(255, 152, 0, 0.2)',
						'rgba(244, 67, 54, 0.2)',
						'rgba(0, 188, 212, 0.2)'
					],
					borderColor: [
						'rgba(69, 61, 63, 1)',
						'rgba(209, 121, 5, 1)',
						'rgba(255, 152, 0, 1)',
						'rgba(244, 67, 54, 1)',
						'rgba(0, 188, 212, 1)'
					],
					borderWidth: 1,
					label: 'Agendamientos'
				}],

				labels: this.categoriasEstadisticasEventos
			},
			options: {
				responsive: true
			}
		});
	}

	async diagramaBarrasVehiculos() {
		this.loading = true;
		const responseDiagramaPastelEventos = await this._estadisticastService.graficaBarrasVehiculosEntregadosMes();
		//console.log(responseDiagramaPastelEventos);
		if (responseDiagramaPastelEventos["status"] === "success" && responseDiagramaPastelEventos["code"] === "200") {
			this.llenarDatosDiagramaBarrasVehiculos(responseDiagramaPastelEventos["data"]);
			this.loading = false;
		}
		else if (responseDiagramaPastelEventos["status"] === "success" && responseDiagramaPastelEventos["code"] === "300") {
			this.loading = false;
		} else if (responseDiagramaPastelEventos["status"] === "error" && responseDiagramaPastelEventos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseDiagramaPastelEventos['message']);
		}
		else {
			this.loading = false;
		}
	}

	llenarDatosDiagramaBarrasVehiculos(dataDiagrama: any) {
		let datosMesSerie = dataDiagrama;

		var ctx = document.getElementById('vehiculosEntregados');
		var myChart = new Chart(ctx, {
			type: 'bar',
			data: {
				labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
				datasets: [{
					label: '# ' + this.nombreDiagramaBarraEmpresa.param1,
					data: datosMesSerie,
					backgroundColor: [
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)'
					],
					borderColor: [
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)'
					],
					borderWidth: 1
				}]
			},
			options: {
				responsive: true,
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});
	}

	async diagramaBarrasDesembolsos() {
		this.loading = true;
		const responseDiagramaPastelDesembolsos = await this._estadisticastService.graficaBarrasDesembolsosMes();
		//console.log(responseDiagramaPastelDesembolsos);
		if (responseDiagramaPastelDesembolsos["status"] === "success" && responseDiagramaPastelDesembolsos["code"] === "200") {
			this.llenarDatosDiagramaBarrasDesembolsos(responseDiagramaPastelDesembolsos["data"]);
			this.loading = false;
		}
		else if (responseDiagramaPastelDesembolsos["status"] === "success" && responseDiagramaPastelDesembolsos["code"] === "300") {
			this.loading = false;
		} else if (responseDiagramaPastelDesembolsos["status"] === "error" && responseDiagramaPastelDesembolsos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseDiagramaPastelDesembolsos['message']);
		}
		else {
			this.loading = false;
		}
	}

	llenarDatosDiagramaBarrasDesembolsos(dataDiagrama: any) {
		let datosMesSerie = dataDiagrama;

		var ctx = document.getElementById('creditosDesembolsados');
		var myChart = new Chart(ctx, {
			type: 'bar',
			data: {
				labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
				datasets: [{
					label: '# Créditos Desembolsados',
					data: datosMesSerie,
					backgroundColor: [
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(153, 102, 255, 0.2)'
					],
					borderColor: [
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(153, 102, 255, 1)'
					],
					borderWidth: 1
				}]
			},
			options: {
				responsive: true,
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});
	}

	async clientesDiaReporte(idUsuario: any) {
		this.loading = true;
		const clientesDiaDatos = await this._estadisticastService.reporteClientesDia(idUsuario);
		//console.log(clientesDiaDatos);
		if (clientesDiaDatos["status"] === "success" && clientesDiaDatos["code"] === "200") {
			this.clientesDiaNum = clientesDiaDatos['data'].numeroTotal;
			this.loading = false;
		}
		else if (clientesDiaDatos["status"] === "success" && clientesDiaDatos["code"] === "300") {
			this.clientesDiaNum = clientesDiaDatos['data'].numeroTotal;
			this.loading = false;
		} else if (clientesDiaDatos["status"] === "error" && clientesDiaDatos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(clientesDiaDatos['message']);
		}
		else {
			this.loading = false;
		}
	}

	async clientesSemanaReporte(idUsuario: any) {
		this.loading = true;
		const clientesSemanaDatos = await this._estadisticastService.reporteClientesSemana(idUsuario);
		//console.log(clientesSemanaDatos);
		if (clientesSemanaDatos["status"] === "success" && clientesSemanaDatos["code"] === "200") {
			this.clientesSemanaNum = clientesSemanaDatos['data'].numeroTotal;
			this.loading = false;
		}
		else if (clientesSemanaDatos["status"] === "success" && clientesSemanaDatos["code"] === "300") {
			this.clientesSemanaNum = clientesSemanaDatos['data'].numeroTotal;
			this.loading = false;
		} else if (clientesSemanaDatos["status"] === "error" && clientesSemanaDatos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(clientesSemanaDatos['message']);
		}
		else {
			this.loading = false;
		}
	}

	async clientesMesReporte(idUsuario: any) {
		this.loading = true;
		const clientesMesDatos = await this._estadisticastService.reporteClientesMes(idUsuario);
		//console.log(clientesMesDatos);
		if (clientesMesDatos["status"] === "success" && clientesMesDatos["code"] === "200") {
			this.clientesMesNum = clientesMesDatos['data'].numeroTotal;
			this.loading = false;
		}
		else if (clientesMesDatos["status"] === "success" && clientesMesDatos["code"] === "300") {
			this.clientesMesNum = clientesMesDatos['data'].numeroTotal;
			this.loading = false;
		} else if (clientesMesDatos["status"] === "error" && clientesMesDatos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(clientesMesDatos['message']);
		}
		else {
			this.loading = false;
		}
	}

	async clientesAnioReporte(idUsuario: any) {
		this.loading = true;
		const clientesAnioDatos = await this._estadisticastService.reporteClientesAnio(idUsuario);
		//console.log(clientesAnioDatos);
		if (clientesAnioDatos["status"] === "success" && clientesAnioDatos["code"] === "200") {
			this.clientesAnioNum = clientesAnioDatos['data'].numeroTotal;
			this.loading = false;
		}
		else if (clientesAnioDatos["status"] === "success" && clientesAnioDatos["code"] === "300") {
			this.clientesAnioNum = clientesAnioDatos['data'].numeroTotal;
			this.loading = false;
		} else if (clientesAnioDatos["status"] === "error" && clientesAnioDatos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(clientesAnioDatos['message']);
		}
		else {
			this.loading = false;
		}
	}

	async clientesIngresosActivos(idUsuario: any) {
		this.loading = true;
		const clientesClientesInActivosTotal = await this._estadisticastService.reporteClientesIngresosActivos(idUsuario);
		//console.log(clientesClientesInActivosTotal);
		if (clientesClientesInActivosTotal["status"] === "success" && clientesClientesInActivosTotal["code"] === "200") {
			this.ingresosActivosNum = clientesClientesInActivosTotal['data'].numeroTotal;
			this.loading = false;
		}
		else if (clientesClientesInActivosTotal["status"] === "success" && clientesClientesInActivosTotal["code"] === "300") {
			this.ingresosActivosNum = clientesClientesInActivosTotal['data'].numeroTotal;
			this.loading = false;
		}
		else if (clientesClientesInActivosTotal["status"] === "error" && clientesClientesInActivosTotal["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(clientesClientesInActivosTotal['message']);
		}
		else {
			this.loading = false;
		}
	}

	async clientesProcesosVentas(idUsuario: any) {
		this.loading = true;
		const clientesProcesosVentaTotal = await this._estadisticastService.reporteClientesProcesosVenta(idUsuario);
		//console.log(clientesProcesosVentaTotal);
		if (clientesProcesosVentaTotal["status"] === "success" && clientesProcesosVentaTotal["code"] === "200") {
			this.procesosVentaNum = +clientesProcesosVentaTotal['data'];
			this.loading = false;
		}
		else if (clientesProcesosVentaTotal["status"] === "success" && clientesProcesosVentaTotal["code"] === "300") {
			this.procesosVentaNum = +clientesProcesosVentaTotal['data'];
			this.loading = false;
		}
		else if (clientesProcesosVentaTotal["status"] === "error" && clientesProcesosVentaTotal["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(clientesProcesosVentaTotal['message']);
		}
		else {
			this.loading = false;
		}
	}

	async vehiculosVendidosReporte(idUsuario: any) {
		this.loading = true;
		const vehiculosVendidosDatos = await this._estadisticastService.reporteVehiculosVendidos(idUsuario);
		//console.log(vehiculosVendidosDatos);
		if (vehiculosVendidosDatos["status"] === "success" && vehiculosVendidosDatos["code"] === "200") {
			this.vehiculosVendidosNum = vehiculosVendidosDatos['data'].numeroTotal;
			this.loading = false;
		}
		else if (vehiculosVendidosDatos["status"] === "success" && vehiculosVendidosDatos["code"] === "300") {
			this.vehiculosVendidosNum = vehiculosVendidosDatos['data'].numeroTotal;
			this.loading = false;
		} else if (vehiculosVendidosDatos["status"] === "error" && vehiculosVendidosDatos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(vehiculosVendidosDatos['message']);
		}
		else {
			this.loading = false;
		}
	}

	async clientesNoVentasReporte(idUsuario: any) {
		this.loading = true;
		const clientesNoVentasDatos = await this._estadisticastService.reporteClientesNoVentas(idUsuario);
		//console.log(clientesNoVentasDatos);
		if (clientesNoVentasDatos["status"] === "success" && clientesNoVentasDatos["code"] === "200") {
			this.clientesNoVentasNum = clientesNoVentasDatos['data'].numeroTotal;
			this.loading = false;
		}
		else if (clientesNoVentasDatos["status"] === "success" && clientesNoVentasDatos["code"] === "300") {
			this.clientesNoVentasNum = clientesNoVentasDatos['data'].numeroTotal;
			this.loading = false;
		} else if (clientesNoVentasDatos["status"] === "error" && clientesNoVentasDatos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(clientesNoVentasDatos['message']);
		}
		else {
			this.loading = false;
		}
	}

	async clientesConCotizacionReporte(idUsuario: any) {
		this.loading = true;
		const clientesConCotizacionDatos = await this._estadisticastService.reporteClientesConCotizacion(idUsuario);
		//console.log(clientesConCotizacionDatos);
		if (clientesConCotizacionDatos["status"] === "success" && clientesConCotizacionDatos["code"] === "200") {
			this.clientesConCotizacionNum = clientesConCotizacionDatos['data'].numeroTotal;
			this.loading = false;
		}
		else if (clientesConCotizacionDatos["status"] === "success" && clientesConCotizacionDatos["code"] === "300") {
			this.clientesConCotizacionNum = clientesConCotizacionDatos['data'].numeroTotal;
			this.loading = false;
		}
		else if (clientesConCotizacionDatos["status"] === "error" && clientesConCotizacionDatos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(clientesConCotizacionDatos['message']);
		}
		else {
			this.loading = false;
		}
	}

	async clientesSinCotizacionReporte(idUsuario: any) {
		this.loading = true;
		const clientesSinCotizacionDatos = await this._estadisticastService.reporteClientesSinCotizacion(idUsuario);
		//console.log(clientesSinCotizacionDatos);
		if (clientesSinCotizacionDatos["status"] === "success" && clientesSinCotizacionDatos["code"] === "200") {
			this.clientesSinCotizacionNum = clientesSinCotizacionDatos['data'].numeroTotal;
			this.loading = false;
		}
		else if (clientesSinCotizacionDatos["status"] === "success" && clientesSinCotizacionDatos["code"] === "300") {
			this.clientesSinCotizacionNum = clientesSinCotizacionDatos['data'].numeroTotal;
			this.loading = false;
		}
		else if (clientesSinCotizacionDatos["status"] === "error" && clientesSinCotizacionDatos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(clientesSinCotizacionDatos['message']);
		}
		else {
			this.loading = false;
		}
	}

	async clientesVehiculoUsadoReporte(idUsuario: any) {
		this.loading = true;
		const clientesVehiculoUsadoDatos = await this._estadisticastService.reporteClientesVehiculoUsado(idUsuario);
		//console.log(clientesVehiculoUsadoDatos);
		if (clientesVehiculoUsadoDatos["status"] === "success" && clientesVehiculoUsadoDatos["code"] === "200") {
			this.clientesVehiculoUsadoNum = clientesVehiculoUsadoDatos['data'].numeroTotal;
			this.loading = false;
		}
		else if (clientesVehiculoUsadoDatos["status"] === "success" && clientesVehiculoUsadoDatos["code"] === "300") {
			this.clientesVehiculoUsadoNum = clientesVehiculoUsadoDatos['data'].numeroTotal;
			this.loading = false;
		}
		else if (clientesVehiculoUsadoDatos["status"] === "error" && clientesVehiculoUsadoDatos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(clientesVehiculoUsadoDatos['message']);
		}
		else {
			this.loading = false;
		}
	}

	async reporteEstudiosCredito(idUsuario: any) {
		this.loading = true;
		const clientesEstudioCreditoTotal = await this._estadisticastService.reporteTotalEstudiosCredito(idUsuario);
		//console.log(clientesEstudioCreditoTotal);
		if (clientesEstudioCreditoTotal["status"] === "success" && clientesEstudioCreditoTotal["code"] === "200") {
			this.estudiosCreditoNum = clientesEstudioCreditoTotal['data'].numeroTotal;
			this.loading = false;
		}
		else if (clientesEstudioCreditoTotal["status"] === "success" && clientesEstudioCreditoTotal["code"] === "300") {
			this.estudiosCreditoNum = clientesEstudioCreditoTotal['data'].numeroTotal;
			this.loading = false;
		}
		else if (clientesEstudioCreditoTotal["status"] === "error" && clientesEstudioCreditoTotal["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(clientesEstudioCreditoTotal['message']);
		}
		else {
			this.loading = false;
		}
	}

	async clientesEsperaReporte(idUsuario: any) {
		this.loading = true;
		const clientesEsperaDatos = await this._estadisticastService.reporteClientesEspera(idUsuario);
		//console.log(clientesEsperaDatos);
		if (clientesEsperaDatos["status"] === "success" && clientesEsperaDatos["code"] === "200") {
			this.clientesEsperaNum = +clientesEsperaDatos['data'].numeroTotal;
			this.loading = false;
		}
		else if (clientesEsperaDatos["status"] === "success" && clientesEsperaDatos["code"] === "300") {
			this.clientesEsperaNum = +clientesEsperaDatos['data'].numeroTotal;
			this.loading = false;
		}
		else if (clientesEsperaDatos["status"] === "error" && clientesEsperaDatos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(clientesEsperaDatos['message']);
		}
		else {
			this.loading = false;
		}
	}

	/* async ticketsAbiertosCerradosReporte() {
		this.loading = true;
		const responseTicketsOpenClose = await this._ticketsService.ticketsAbiertosCerrados();
		//console.log(responseTicketsOpenClose);
		if (responseTicketsOpenClose["status"] === "success" && responseTicketsOpenClose["code"] === "200") {
			this.ticketsAbiertosNum = +responseTicketsOpenClose['data'].abiertos;
			this.ticketsCerradosNum = +responseTicketsOpenClose['data'].cerrados;
			this.loading = false;
		}
		else if (responseTicketsOpenClose["status"] === "success" && responseTicketsOpenClose["code"] === "300") {
			this.ticketsAbiertosNum = '0';
			this.ticketsCerradosNum = '0';
			this.loading = false;
		}
		else if (responseTicketsOpenClose["status"] === "error" && responseTicketsOpenClose["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseTicketsOpenClose['message']);
		}
		else {
			this.loading = false;
		}
	} */
	
	async listarProcesosVentaVaciosCompletos() {
		this.loading = true;
		const responseProcesosVentaVacioCompleto = await this._estadisticastService.reporteProcesosVentaVaciosCompletosEstadisticas();
		//console.log(responseProcesosVentaVacioCompleto);
		if (responseProcesosVentaVacioCompleto["status"] === "success" && responseProcesosVentaVacioCompleto["code"] === "200") {
			this.procesosVentaVaciosNum = +responseProcesosVentaVacioCompleto['data'].abiertos;
			this.procesosVentaCompletosNum = +responseProcesosVentaVacioCompleto['data'].cerrados;
			this.loading = false;
		}
		else if (responseProcesosVentaVacioCompleto["status"] === "success" && responseProcesosVentaVacioCompleto["code"] === "300") {
			this.procesosVentaVaciosNum = '0';
			this.procesosVentaCompletosNum = '0';
			this.loading = false;
		}
		else if (responseProcesosVentaVacioCompleto["status"] === "error" && responseProcesosVentaVacioCompleto["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseProcesosVentaVacioCompleto['message']);
		}
		else {
			this.loading = false;
		}
	}

	/* Listado de Estadisticas por cada uno de los card */
	onSubmitClientesDiaReporte() {
		this._router.navigate(["estadisticas/clientes-dia"]);
	}

	onSubmitClientesSemanaReporte() {
		this._router.navigate(["estadisticas/clientes-semana"]);
	}

	onSubmitClientesMesReporte() {
		this._router.navigate(["estadisticas/clientes-mes"]);
	}

	onSubmitClientesAnioReporte() {
		this._router.navigate(["estadisticas/clientes-anio"]);
	}

	onSubmitVehiculosVendidosReporte() {
		this._router.navigate(["estadisticas/vehiculos-vendidos"]);
	}

	onSubmitClientesNoVentasReporte() {
		this._router.navigate(["estadisticas/clientes-no-ventas"]);
	}

	onSubmitClientesConCotizacionReporte() {
		this._router.navigate(["estadisticas/clientes-con-cotizaciones"]);
	}

	onSubmitClientesSinCotizacionReporte() {
		this._router.navigate(["estadisticas/clientes-sin-cotizaciones"]);
	}

	onSubmitClientesVehiculoUsadoReporte() {
		this._router.navigate(["estadisticas/clientes-vehiculos-usados"]);
	}

	onSubmitClientesEsperaReporte() {
		this._router.navigate(["estadisticas/clientes-espera"]);
	}

	onSubmitReporteEstudiosCredito() {
		this._router.navigate(["estadisticas/estudios-de-credito"]);
	}

	onSubmitReporteClientesIngresosActivos() {
		this._router.navigate(["estadisticas/clientes-ingresos-activos"]);
	}

	onSubmitProcesosVentaReporte() {
		this._router.navigate(["estadisticas/procesos-de-venta"]);
	}

	/* onSubmitVerTicketsReporte() {
		this._router.navigate(["tickets/listar"]);
	} */
	
	onSubmitVerProcesosVentaVaciosCompletosReporte() {
		this._router.navigate(["seguimiento-del-negocio/procesos-de-venta"]);
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}