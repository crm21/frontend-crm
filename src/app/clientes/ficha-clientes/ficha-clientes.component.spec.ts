import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FichaClientesComponent } from './ficha-clientes.component';

describe('FichaClientesComponent', () => {
  let component: FichaClientesComponent;
  let fixture: ComponentFixture<FichaClientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FichaClientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FichaClientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
