export class EmpleadoInfoPersonal {
	constructor(
		public tipoDocumento: string,
		public documento: string,
		public nombres: string,
		public apellidos: string,
		public rol: string
	) { }
};

export class EmpleadoInfoGeneral {
	constructor(
		public correo: string,
		public direccion?: string,
		public lugarResidencia?: string,
		public telefonoFijo?: string,
		public celular?: string,
		public estadoCivil?: string,
		public aniosExperienciaLaboral?: string,
		public fechaExpedicion?: string,
		public fechaNacimiento?: string,
		public libretaMilitar?: string,
		public claseLibretaMilitar?: string,
		public distritoLibretaMilitar?: string,
		public tarjetaProfesional?: string,
		public grupoSanguineo?: string,
		public licenciaConduccion?: string,
		public categoriaLicencia?: string,
		public colorPreferido?: string,
		public hobby1?: string,
		public hobby2?: string,
		public profesion?: string,
		public genero?: string
	) { }
};

export class EmpleadoInfoEmpleado {
	constructor(
		public trabajaActualmente?: string,
		public empresaActual?: string,
		public tipoEmpleado?: string,
		public tipoContrato?: string,
		public trabajoEmpresa?: string,
		public empleoAntes?: string,
		public fechaEmpleoAntes?: string,
		public recomendado?: string,
		public nombreRecomendado?: string,
		public dependenciaRecomendado?: string,
		public parientesEmpresa?: string,
		public nombrePariente?: string,
		public dependenciaPariente?: string,
		public conocimientoVacante?: string,
		public ciudadVivido?: string,
		public ciudadesTrabajado?: string,
		public trabajoOtraCiudad?: string,
		public tipoVivienda?: string,
		public nombreArrendador?: string,
		public telefonoArrendador?: string,
		public tiempoVivienda?: string,
		public ingresosAdicionales?: string,
		public valorIngresosAdicionales?: string,
		public valorObligaciones?: string,
		public conceptoObligaciones?: string,
		public aspiracionSalarial?: string,
		public reconocimientos?: string,
		public perteneceGrupo?: string,
		public hobbies?: string,
		public deportes?: string,
		public expectativas?: string
	) { }
};

export class EmpleadoInfoFamiliar {
	constructor(
		public nombrePareja: string,
		public profesionPareja: string,
		public empresaPareja: string,
		public cargoActualPareja: string,
		public direccion: string,
		public contacto: string,
		public ciudad: string,
		public numHermanos: string
	) { }
};

export class EmpleadoInfoEducacion {
	constructor(
		public id: string,
		public tipoEstudio: string,
		public anioFinalizacion: string,
		public aniosCursados: string,
		public duracionMeses: string,
		public tituloObtenido: string,
		public institucion: string,
		public ciudad: string,
		public intensidadHoraria: string,
		public estadoCurso: string,
		public anioSemestreCursa: string,
		public observacion: string,
		public eliminar?: string
	) { }
};

export class ActualizarEmpleadoInfoPersonal {
	constructor(
		public tipoDocumento: string,
		public documento: string,
		public nombres: string,
		public apellidos: string
	) { }
};

export class ActualizarEmpleadoInfoEducacion {
	constructor(
		public id: string,
		public tipoEstudio: string,
		public anioFinalizacion: string,
		public aniosCursados: string,
		public duracionMeses: string,
		public tituloObtenido: string,
		public institucion: string,
		public ciudad: string,
		public intensidadHoraria: string,
		public estadoCurso: string,
		public anioSemestreCursa: string,
		public observacion: string,
		public infoEmpleado: string
	) { }
};
