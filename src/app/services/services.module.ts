import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { LoginGuard } from './guards/login.guard';
import { SweetAlertService } from './sweet-alert/sweet-alert.service';
import { UsuarioService } from './usuario/usuario.service';
import { AsesorService } from './asesor/asesor.service';
import { AuditoriaServices } from './auditoria/auditoria.service';
import { ClienteService } from './cliente/cliente.service';
import { ParametrizacionService } from './parametrizacion/parametrizacion.service';
import { EstudioCreditoService } from './estudio-credito/estudio-credito.service';
import { ReconsideradoService } from './reconsiderado/reconsiderado.service';
import { PermisosService } from './permisos/permisos.service';
import { TableroAsignacionService } from './tablero-asignacion/tablero-asignacion.service';
import { ProcesoVentasService } from './proceso-ventas/proceso-ventas.service';
import { EstadisticasService } from './estadisticas/estadisticas.service';
import { ReporteBdcService } from './reporte-bdc/reporte-bdc.service';
import { EmpleadosService } from './empleados/empleados.service';
import { ReporteClientesService } from './reporte-clientes/reporte-clientes.service';
import { ControlIndividualService } from './control-individual/control-individual.service';
import { VehiculosService } from './vehiculos/vehiculos.service';
import { AgendaService } from './agenda/agenda.service';
import { EventosComercialesService } from './eventos-comerciales/eventos-comerciales.service';
import { TicketsService } from './tickets/tickets.service';
import { SiembraCosechaService } from './siembra-cosecha/siembra-cosecha.service';
import { LeadsService } from './leads/leads.service';
import { ProyeccionService } from './proyeccion/proyeccion.service';

@NgModule({
	declarations: [],
	imports: [
		CommonModule,
		HttpClientModule
	],
	providers: [
		LoginGuard,
		SweetAlertService,
		UsuarioService,
		AsesorService,
		AuditoriaServices,
		ClienteService,
		ParametrizacionService,
		EstudioCreditoService,
		ReconsideradoService,
		PermisosService,
		TableroAsignacionService,
		ProcesoVentasService,
		EstadisticasService,
		ReporteBdcService,
		EmpleadosService,
		ReporteClientesService,
		ControlIndividualService,
		VehiculosService,
		AgendaService,
		EventosComercialesService,
		TicketsService,
		SiembraCosechaService,
		LeadsService,
		ProyeccionService
	]
})
export class ServicesModule { }