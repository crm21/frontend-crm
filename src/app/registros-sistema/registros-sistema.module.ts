import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuditoriaComponent } from './auditoria/auditoria.component';
import { ExcepcionesComponent } from './excepciones/excepciones.component';
import { RouterModule } from '@angular/router';
import { RegistrosSistemaRoutes } from './registros-sistema.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { LoadingModule } from '../shared/loading/loading.module';
import { LoadingSpinnerModule } from '../shared/loading-spinner/loading-spinner.module';

@NgModule({
  declarations: [AuditoriaComponent, ExcepcionesComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(RegistrosSistemaRoutes),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    LoadingModule,
    LoadingSpinnerModule
  ]
})
export class RegistrosSistemaModule { }
