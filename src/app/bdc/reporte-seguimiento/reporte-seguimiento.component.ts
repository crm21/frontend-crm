import { Component, OnInit } from "@angular/core";
import { DatePipe } from "@angular/common";
import { Router } from "@angular/router";
import swal from "sweetalert2";
import { ReporteBdcService } from '../../services/reporte-bdc/reporte-bdc.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { TableroAsignacionService } from '../../services/tablero-asignacion/tablero-asignacion.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';

declare var $: any;
declare interface TableData {
	headerRow: string[];
	dataRows: string[][];
}

@Component({
	selector: 'app-reporte-seguimiento',
	templateUrl: './reporte-seguimiento.component.html',
	styleUrls: ['./reporte-seguimiento.component.css']
})
export class ReporteSeguimientoComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	dataTable: TableData;
	reporteArray: any[] = [];
	objecionesArray: any[] = [];
	diasArray = [
		{ valor: 1, nombre: 'Hasta hoy' },
		{ valor: 2, nombre: '2' },
		{ valor: 4, nombre: '4' },
		{ valor: 6, nombre: '6' }
	];
	todosAsesoresArray: any[] = [];
	totalAtencionesArray: any[] = [];
	totalAtenciones: number;
	nombresCliente: string = '';
	fecha: string = '';
	ocultarCampoAsesor: boolean = true;
	consultaVaciaCargarSpinner: boolean = false;
	identityUsuario: any = null;
	textoExtendido: string = '';
	busquedaFiltros: any = {
		dia: '',
		asesor: '',
		objecion: ''
	};

	constructor(
		private _reporteBdcService: ReporteBdcService,
		private _sweetAlertService: SweetAlertService,
		private _tableroAsignacionService: TableroAsignacionService,
		private _parametrizacionService: ParametrizacionService,
		private _datepipe: DatePipe,
		private _router: Router
	) { this.datosTabla(); }

	ngOnInit() {
		if (localStorage.getItem('identity_crm')) {
			let fechaActual = new Date();
			let asesorId;
			let base_64_data = atob(localStorage.getItem('identity_crm'));
			let identityData = JSON.parse(base_64_data);

			this.identityUsuario = identityData['data'];
			if (this.identityUsuario.rol.valor == 'S') {
				this.ocultarCampoAsesor = false;
				asesorId = this.identityUsuario.sub;
			}
			else asesorId = '';

			this.listarReportesBdc(this.fechasValidacion(fechaActual), '', asesorId, '');
			this.cargarDisponibilidadAsesores();
		}
	}

	datosTabla() {
		this.dataTable = {
			headerRow: [
				"#",
				"Documento",
				"Cliente",
				"Fecha Ingreso",
				"Duración Última Atención",
				"Objeción",
				"Observaciones",
				"Asesor",
				"Total Atenciones",
				"Atenciones"
			],
			dataRows: []
		};
	}

	onSubmitBuscarReporte(event: any, opc: number) {
		let dia, asesor, objecion;

		if (opc == 1) {
			this.fecha = this.fechasValidacion(event.value);
		}

		dia = this.busquedaFiltros.dia;
		if (this.identityUsuario.rol.valor == 'S') asesor = this.identityUsuario.sub;
		else asesor = this.busquedaFiltros.asesor; //asesor = this.busquedaFiltros.asesor;
		objecion = this.busquedaFiltros.objecion;
		//console.log(`${this.fecha}, ${dia}, ${asesor}, ${objecion}`);
		this.listarReportesBdc(this.fecha, dia, asesor, objecion);
	}

	async listarReportesBdc(fecha: string, dia: string, asesor: string, objecion: string) {
		this.loading = true; //console.log(`${fecha}, ${dia}, ${asesor}, ${objecion}`);
		const responseReporteBdc = await this._reporteBdcService.listarReporteBdc(fecha, dia, asesor, objecion);
		//console.log(responseReporteBdc);
		if (responseReporteBdc['status'] == "success" && responseReporteBdc['code'] == "200") {
			this.llenarDatosTabla(responseReporteBdc);
			this.loading = false;
		}
		else if (responseReporteBdc['status'] == "success" && responseReporteBdc['code'] == "300") {
			this.loading = false;
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			this._sweetAlertService.showNotification('top', 'right', 'warning', 'warning', '<b>' + responseReporteBdc['message'] + '<b>', 2000);
		}
		else if (responseReporteBdc["status"] === "error" && responseReporteBdc["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseReporteBdc['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			swal('¡Error!', responseReporteBdc['message'], 'error');
		}
	}

	async cargarDisponibilidadAsesores() {
		this.loading = true;
		const responseDisponibilidadAsesores = await this._tableroAsignacionService.disponibilidadAsesores();
		//console.log(responseDisponibilidadAsesores);
		if (responseDisponibilidadAsesores["status"] === "success" && responseDisponibilidadAsesores["code"] === "200") {
			this.loading = false;
			this.cargarInformacionSelect(responseDisponibilidadAsesores['data']);
		}
		else if (responseDisponibilidadAsesores["status"] === "success" && responseDisponibilidadAsesores["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseDisponibilidadAsesores["message"], 'OK', 'warning');
		}
		else if (responseDisponibilidadAsesores["status"] === "error" && responseDisponibilidadAsesores["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseDisponibilidadAsesores['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseDisponibilidadAsesores["message"], 'OK', 'error');
		}
	}

	llenarDatosTabla(reporteBdc: any) {
		//console.log(reporteBdc);
		this.dataTable.dataRows = [];
		this.reporteArray = reporteBdc['data'];
		let JSONParam, tiempoTem, objecionTem, asesorTem, observacionTem;

		this.reporteArray.forEach(element => {
			if ((element.ultimaAtencion == 0) || (element.ultimaAtencion == null)) {
				tiempoTem = '---';
				objecionTem = '---';
				//asesorTem = '---';
				observacionTem = '---';
			} else {
				tiempoTem = element.ultimaAtencion.tiempoAtencion;
				objecionTem = element.ultimaAtencion.objecion.nombre;
				//asesorTem = element.asesor.nombres + ' ' + element.asesor.apellidos;
				observacionTem = element.ultimaAtencion.observacion;
			}
			JSONParam = {
				id: element.idIngreso,
				tipoDoc: element.cliente.tipoDocumento.valor,
				documento: element.cliente.documento,
				cliente: element.cliente.nombres + ' ' + element.cliente.apellidos,
				fechaIngreso: element.fechaIngreso,
				tiempo: tiempoTem,
				objecion: objecionTem,
				asesor: element.asesor.nombres + ' ' + element.asesor.apellidos,
				observacion: observacionTem,
				atenciones: element.atenciones,
				total: element.total
			};
			this.dataTable.dataRows.push(JSONParam);
		});
		//console.log(this.dataTable.dataRows);
	}

	async cargarInformacionSelect(asesores) {
		//console.log(asesores);
		let disponibilidadAsesorArray: any[] = [];
		this.todosAsesoresArray = [];

		if (asesores.asesoresDisponibles.length > 0) {
			disponibilidadAsesorArray = asesores.asesoresDisponibles;
		}
		if (asesores.asesoresOcupados.length >= 0) {
			this.todosAsesoresArray = disponibilidadAsesorArray.concat(asesores.asesoresOcupados);
		}

		const objeciones: any = await this._parametrizacionService.listarCategoriaParametrizacion('Objeciones');

		objeciones['data'].forEach((element) => {
			this.objecionesArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
		//console.log(this.todosAsesoresArray);
		//console.log(this.objecionesArray);
	}

	visualizarAtenciones(atenciones, cliente, totalAtenciones) {
		this.totalAtencionesArray = atenciones;
		this.nombresCliente = cliente;
		this.totalAtenciones = totalAtenciones;
		$("#modalFiltro").modal("show");
	}

	limpiarFiltros() {
		let fechaActual = new Date();
		let idAsesor = '';
		if (this.identityUsuario.rol.valor == 'S') {
			this.busquedaFiltros.dia = '';
			this.busquedaFiltros.asesor = '';
			this.busquedaFiltros.objecion = '';
			this.fecha = '';
			(document.getElementById('fecha') as HTMLInputElement).value = '';
			(document.getElementById('dia') as HTMLInputElement).value = '';
			(document.getElementById('objecion') as HTMLInputElement).value = '';
			idAsesor = this.identityUsuario.sub;
		} else {
			this.busquedaFiltros.dia = '';
			this.busquedaFiltros.asesor = '';
			this.busquedaFiltros.objecion = '';
			this.fecha = '';
			(document.getElementById('fecha') as HTMLInputElement).value = '';
			(document.getElementById('dia') as HTMLInputElement).value = '';
			(document.getElementById('asesor') as HTMLInputElement).value = '';
			(document.getElementById('objecion') as HTMLInputElement).value = '';
		}
		this.listarReportesBdc(this.fechasValidacion(fechaActual), '', idAsesor, '');
	}

	observarTexto(texto: any) {
		this.textoExtendido = texto;
		$("#modalObservacion").modal("show");
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) result = e1 === e2;
		return result;
	}

	fechasValidacion(formatoFecha: any) {
		if (formatoFecha) {
			//Formatear fecha
			const fechaActualTem = this._datepipe
				.transform(formatoFecha, "yyyy/MM/dd")
				.replace(/\//g, "-");
			//console.log("fecha formato: "+fechaActualTem);
			return fechaActualTem;
		}
		return '';
	}

	verFichaCliente(idIngreso: any) {
		localStorage.setItem('ruta', 'bdc/reporte');
		this._router.navigate(["clientes/ficha-clientes/", idIngreso]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}