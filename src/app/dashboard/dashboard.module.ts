import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdModule } from '../md/md.module';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutes } from './dashboard.routing';
import { LoadingModule } from '../shared/loading/loading.module';
import { LoadingSistemaModule } from '../shared/loading-sistema/loading-sistema.module';
import { DashboardCrmComponent } from './dashboard-crm/dashboard-crm.component';
import { DashboardErpComponent } from './dashboard-erp/dashboard-erp.component';
import { DashboardClienteComponent } from './dashboard-cliente/dashboard-cliente.component';
import { DasboardContactCenterComponent } from './dasboard-contact-center/dasboard-contact-center.component';
import { EstadisticasLeadsComponent } from './estadisticas-leads/estadisticas-leads.component';
import { CardTicketComponent } from './card-ticket/card-ticket.component';
import { DashboardAdministrativosComponent } from './dashboard-administrativos/dashboard-administrativos.component';

@NgModule({
    declarations: [
        DashboardComponent,
        DashboardCrmComponent,
        DashboardErpComponent,
        DashboardClienteComponent,
        DasboardContactCenterComponent,
        EstadisticasLeadsComponent,
        CardTicketComponent,
        DashboardAdministrativosComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(DashboardRoutes),
        FormsModule,
        MaterialModule,
        ReactiveFormsModule,
        MdModule,
        LoadingModule,
        LoadingSistemaModule
    ],
    providers: [DatePipe]
})

export class DashboardModule { }
