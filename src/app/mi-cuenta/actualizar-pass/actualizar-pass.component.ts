import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmarContrasenia } from '../../models/actualizarContrasena';
import swal from 'sweetalert2';
import { UsuarioService } from '../../services/usuario/usuario.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

@Component({
	selector: 'app-actualizar-pass',
	templateUrl: './actualizar-pass.component.html',
	styleUrls: ['./actualizar-pass.component.css']
})
export class ActualizarPassComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	infoUsuario: any;
	dataUsuario: any;

	private id: string;
	private newPass: string;

	constructor(
		private _router: Router,
		private _route: ActivatedRoute,
		private _usuarioService: UsuarioService,
		private _sweetAlertService: SweetAlertService
	) {
		this.id = this._route.snapshot.paramMap.get('idUsuario');
		this.newPass = this._route.snapshot.paramMap.get('newPass');
	}

	ngOnInit() {
		if (localStorage.getItem('identity_crm')) {
			this.cambiarContrasenia();
		}
	}

	async cambiarContrasenia() {
		this.loading = true;

		let dataNewPass = new ConfirmarContrasenia(
			this.id,
			this.newPass
		); //console.log(dataNewPass);


		const responseContrasenia = await this._usuarioService.confirmarContrasenia(dataNewPass);
		//console.log(responseContrasenia);
		if (responseContrasenia["status"] === "success" && responseContrasenia["code"] === "200") {
			this.loading = false;
			localStorage.clear();
			swal('¡Bien!', responseContrasenia['message'], 'success')
				.then((result: any) => {
					this.cerrarSesionCambioContrasenia();
				});
		}
		else if (responseContrasenia["status"] === "success" && responseContrasenia["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseContrasenia["message"], 'OK', 'warning');
		}
		else if (responseContrasenia["status"] === "error" && responseContrasenia["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseContrasenia['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseContrasenia["message"], 'OK', 'error');
		}
	}

	cerrarSesionCambioContrasenia() {
		let opcionEmpresa = localStorage.getItem('business');
		localStorage.clear();
		localStorage.setItem('business', opcionEmpresa);
		localStorage.setItem('identity_crm', null);

		if (opcionEmpresa == 'A') {
			this._router.navigate(['/autodenar']);
		}
		else if (opcionEmpresa == 'M') {
			this._router.navigate(['/motodenar']);
		}
		else if (opcionEmpresa == 'T') {
			this._router.navigate(['/automotora']);
		}
		else {
			this._router.navigate(['/empresas']);
		}
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}