import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ReportesClientesRoutes } from './reportes-clientes.routing';
import { LoadingModule } from '../shared/loading/loading.module';
import { IngresoClientesComponent } from './ingreso-clientes/ingreso-clientes.component';
import { EstudiosCreditoComponent } from './estudios-credito/estudios-credito.component';

@NgModule({
	declarations: [IngresoClientesComponent, EstudiosCreditoComponent],
	imports: [
		CommonModule,
		RouterModule.forChild(ReportesClientesRoutes),
		FormsModule,
		MaterialModule,
		ReactiveFormsModule,
		LoadingModule
	],
	providers: [DatePipe]
})
export class ReportesClientesModule { }
