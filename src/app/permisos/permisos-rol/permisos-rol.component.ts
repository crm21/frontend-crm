import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { PermisosPorRoles } from '../../models/permisos';
import { ModificarPermisosRol } from '../../interfaces/interfaces';
import swal from 'sweetalert2';
import { PermisosService } from '../../services/permisos/permisos.service';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';
import { ParametrizacionService } from '../../services/parametrizacion/parametrizacion.service';

declare var $: any;
declare interface DataTable {
	headerRow: string[];
	dataRows: string[][];
}
declare interface Rol {
	id: number;
	nombre: string;
	valor: string;
}

@Component({
	selector: 'app-permisos-rol',
	templateUrl: './permisos-rol.component.html',
	styleUrls: ['./permisos-rol.component.css']
})
export class PermisosRolComponent implements OnInit {

	loading: boolean = false;
	validarToken: boolean = true;
	errorCheck: boolean = true;
	consultaVaciaCargarSpinner: boolean = false;
	crearRolPermisoForm: FormGroup;
	permisosCrm: any[] = [];
	permisosErp: any[] = [];
	rolesExistentes: Rol[];

	actualizarPermisoRolForm: FormGroup;
	dataTable: DataTable;
	rolPermisosArray: any[] = [];
	total_item_count = 0;
	page_prev: any;
	page_next: any;
	page_actual: any;
	total_pages: any;
	estadoParametros: any;
	estadoArray: any[] = [];
	idPermiso: any;

	constructor(
		private fb: FormBuilder,
		private _router: Router,
		private _permisosService: PermisosService,
		private _sweetAlertService: SweetAlertService,
		private _parametrizacionService: ParametrizacionService
	) { }

	ngOnInit() {
		this.crearRolPermisoForm = this.fb.group({
			rol: ['', Validators.required],
			permisoCrm: this.fb.array([], Validators.required),
			permisoErp: this.fb.array([], Validators.required)
		});

		this.actualizarPermisoRolForm = new FormGroup({
			estado: new FormControl(null, Validators.required),
		});
		if (localStorage.getItem('identity_crm')) {
			/* this.crearRolPermisoForm = new FormGroup({
				rol: new FormControl(null, Validators.required),
				permiso: this.fb.array([], Validators.required),
			}); */

			this.dataTable = {
				headerRow: [
					'#',
					'Rol',
					'Título',
					'URL',
					'Posición',
					'Sistema',
					'Estado',
					'Acciones'
				],
				dataRows: []
			};

			this.listarRolesPermisos(1);
			this.listarRolesExistentes();
			this.listarPermisosExistentes();
		}
	}

	async listarRolesExistentes() {
		this.loading = true;
		const responseRoles = await this._permisosService.listarRoles();
		//console.log(responseRoles);
		if (responseRoles['status'] == "success" && responseRoles['code'] == "200") {
			this.loading = false;
			this.rolesExistentes = responseRoles['data'];
		}
		else if (responseRoles['status'] == "success" && responseRoles['code'] == "300") {
			this.loading = false;
			swal('Advertencia!', responseRoles['message'], 'warning');
		}
		else if (responseRoles["status"] === "error" && responseRoles["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseRoles['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responseRoles['message'], 'error');
		}
	}

	async listarPermisosExistentes() {
		this.loading = true;
		const responsePermisosExistentes = await this._permisosService.listarPermisosPorSistema();
		//console.log(responsePermisosExistentes);
		if (responsePermisosExistentes['status'] == "success" && responsePermisosExistentes['code'] == "200") {
			this.loading = false;
			this.permisosCrm = responsePermisosExistentes['data'].crm;
			this.permisosErp = responsePermisosExistentes['data'].erp;
		}
		else if (responsePermisosExistentes['status'] == "success" && responsePermisosExistentes['code'] == "300") {
			this.loading = false;
			swal('Advertencia!', responsePermisosExistentes['message'], 'warning');
		}
		else if (responsePermisosExistentes["status"] === "error" && responsePermisosExistentes["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responsePermisosExistentes['message']);
		}
		else {
			this.loading = false;
			swal('¡Error!', responsePermisosExistentes['message'], 'error');
		}
	}

	onCheckboxChangeCrm(e) {
		const checkArray: FormArray = this.crearRolPermisoForm.get('permisoCrm') as FormArray;

		if (e.target.checked) {
			checkArray.push(new FormControl(e.target.value));
			this.errorCheck = false;
		} else {
			let i: number = 0;
			checkArray.controls.forEach((item: any) => {
				if (item.value == e.target.value) {
					checkArray.removeAt(i);
					return;
				}
				i++;
			});

		}
	}

	onCheckboxChangeErp(e) {
		const checkArray: FormArray = this.crearRolPermisoForm.get('permisoErp') as FormArray;

		if (e.target.checked) {
			checkArray.push(new FormControl(e.target.value));
			this.errorCheck = false;
		} else {
			let i: number = 0;
			checkArray.controls.forEach((item: any) => {
				if (item.value == e.target.value) {
					checkArray.removeAt(i);
					return;
				}
				i++;
			});
		}
	}

	async onSubmitAsignarPermisoRol() {
		//this.loading = true;
		let crmPermisos: string[] = this.crearRolPermisoForm.value.permisoCrm;
		let erpPermisos: string[] = this.crearRolPermisoForm.value.permisoErp;
		let totalPermisos: string[] = crmPermisos.concat(erpPermisos);
		//console.log(totalPermisos);

		if (totalPermisos.length != 0 && this.crearRolPermisoForm.value.rol) {
			this.errorCheck = false;
			let rolPermiso = new PermisosPorRoles(
				this.crearRolPermisoForm.value.rol,
				totalPermisos
			); //console.log(rolPermiso);

			const responseRolPermiso = await this._permisosService.crearRolPermisos(rolPermiso);
			//console.log(responseRolPermiso);
			if (responseRolPermiso['status'] == "success" && responseRolPermiso['code'] == "200") {
				this.loading = false;
				this.crearRolPermisoForm.reset();
				swal('¡Bien!!', responseRolPermiso['message'], 'success')
					.then((result) => {
						this.crearRolPermisoForm.reset();
						this.errorCheck = true;
						this.ngOnInit();
						//this.regresar();
					}).catch(swal.noop);
			}
			else if (responseRolPermiso['status'] == "success" && responseRolPermiso['code'] == "300") {
				swal('Advertencia!', responseRolPermiso['message'], 'warning');
				this.loading = false;
			}
			else if (responseRolPermiso["status"] === "error" && responseRolPermiso["code"] === "100") {
				this.loading = false;
				if (this.validarToken) this.expiracionToken(responseRolPermiso['message']);
			}
			else {
				swal('¡Error!', responseRolPermiso['message'], 'error');
			}
		} else {
			this.loading = false;
			//this.errorCheck = true;
			swal('¡Advertencia!!', 'Debe seleccionar al menos un permiso y un rol', 'warning');
		}
	}

	async listarRolesPermisos(page) {
		this.loading = true;

		const responseRolPermisos = await this._permisosService.listarPermisosRol(page);
		//console.log(responseRolPermisos);
		if (responseRolPermisos['status'] == "success" && responseRolPermisos['code'] == "200") {
			this.loading = false;
			this.llenarTabla(responseRolPermisos, page);
		}
		else if (responseRolPermisos['status'] == "success" && responseRolPermisos['code'] == "300") {
			this.loading = false;
			this.dataTable.dataRows = [];
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
			swal('Advertencia!', responseRolPermisos['message'], 'warning');
		}
		else if (responseRolPermisos["status"] === "error" && responseRolPermisos["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseRolPermisos['message']);
		}
		else {
			this.loading = false;
			this.dataTable.dataRows = [];
			swal('¡Error!', responseRolPermisos['message'], 'error');
		}
	}

	llenarTabla(responsePermisosRoles: any, page: any) {
		//console.log(responsePermisosRoles['permisosRoles']);
		this.rolPermisosArray = responsePermisosRoles['permisosRoles'];
		this.dataTable.dataRows = [];
		this.total_item_count = responsePermisosRoles['total_item_count'];
		this.page_actual = page;

		this.total_pages = responsePermisosRoles['total_pages'];

		if (page >= 2) this.page_prev = page - 1;
		else this.page_prev = 1;

		if (page < responsePermisosRoles['total_pages']) this.page_next = page + 1;
		else {
			if (responsePermisosRoles['total_pages'] == 0) this.page_next = 1;
			else this.page_next = responsePermisosRoles['total_pages'];
		}

		let JSONParam;

		this.rolPermisosArray.forEach(element => {
			JSONParam = {
				id: element.id,
				rol: element.rol.nombre,
				permisoId: element.permiso.id,
				permisoTitulo: element.permiso.titulo,
				permisoUrl: element.permiso.url,
				permisoPosicion: element.permiso.posicion,
				permisoSistema: element.permiso.sistema,
				estado: element.estado
			};
			this.dataTable.dataRows.push(JSONParam);
		});

		if (this.total_item_count || this.total_item_count == 0) {
			this.consultaVaciaCargarSpinner = true; // Ocultar spinner de carga de datos
		}
	}

	async cargarInformacionSelect() {
		this.estadoParametros = null;
		this.estadoArray = [];
		this.estadoParametros = await this._parametrizacionService.listarCategoriaParametrizacion('Estado');

		this.estadoParametros.data.forEach((element) => {
			this.estadoArray.push({
				id: element.id,
				nombre: element.nombre,
				valor: element.valor,
				categoria: element.categoria,
				descripcion: element.descripcion
			});
		});
	}

	editarPermisos(permiso) {
		this.cargarInformacionSelect();
		this.idPermiso = permiso.id;
		let rowPermisosRol: ModificarPermisosRol = permiso;

		this.actualizarPermisoRolForm.setValue({
			estado: rowPermisosRol.estado.valor,
		});

		$("#modalActualizar").modal("show");
	}

	async onSubmitActualizarPermisoRol() {
		this.loading = true;
		let estadoPermiso = this.actualizarPermisoRolForm.value.estado;

		const responseModificarPermisosRol = await this._permisosService.modificarPermisosRol(this.idPermiso, estadoPermiso);
		//console.log(responseModificarPermisosRol);
		if (responseModificarPermisosRol["status"] === "success" && responseModificarPermisosRol["code"] === "200") {
			this.loading = false;
			swal('¡Bien!', responseModificarPermisosRol["message"], 'success')
				.then((result) => {
					this.actualizarPermisoRolForm.reset();
					$("#modalActualizar").modal("hide");
					this.listarRolesPermisos(1);
				}).catch(swal.noop);
		}
		else if (responseModificarPermisosRol["status"] === "success" && responseModificarPermisosRol["code"] === "300") {
			this.loading = false;
			this._sweetAlertService.alertGeneral('Advertencia!', responseModificarPermisosRol["message"], 'OK', 'warning');
		}
		else if (responseModificarPermisosRol["status"] === "error" && responseModificarPermisosRol["code"] === "100") {
			this.loading = false;
			if (this.validarToken) this.expiracionToken(responseModificarPermisosRol['message']);
		}
		else {
			this.loading = false;
			this._sweetAlertService.alertGeneral('¡Error!', responseModificarPermisosRol["message"], 'OK', 'error');
		}
	}

	compararDato(e1: any, e2: any): boolean {
		let result = false;
		if (e1 && e2) {
			result = e1 === e2;
		}
		return result;
	}

	regresar() {
		this._router.navigate(["permisos/listar"]);
	}

	expiracionToken(mensaje: any) {
		this.validarToken = false;
		this._sweetAlertService.cerrarSesionVenceToken(mensaje);
	}
}