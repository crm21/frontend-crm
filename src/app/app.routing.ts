import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { LoginGuard } from './services/guards/login.guard';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { RecuperarCuentaComponent } from './pages/recuperar-cuenta/recuperar-cuenta.component';
import { ActualizarPassComponent } from './mi-cuenta/actualizar-pass/actualizar-pass.component';
import { ElegirEmpresasComponent } from './pages/elegir-empresas/elegir-empresas.component';
import { SesionEmpresasComponent } from './pages/sesion-empresas/sesion-empresas.component';
import { SesionUsuariosComponent } from './pages/sesion-usuarios/sesion-usuarios.component';

export const AppRoutes: Routes = [
    {
        path: '',
        redirectTo: 'empresas',
        pathMatch: 'full',
    },
    {
        path: '',
        component: AuthLayoutComponent,
        children: [
            {
                path: '',
                component: ElegirEmpresasComponent
            }, {
                path: '*',
                component: ElegirEmpresasComponent
            }, {
                path: 'empresas',
                component: ElegirEmpresasComponent
            }, {
                path: 'autodenar/registrarse',
                component: RegisterComponent,
                data: {
                    opc: 'A'
                }
            }, {
                path: 'motodenar/registrarse',
                component: RegisterComponent,
                data: {
                    opc: 'M'
                }
            }, {
                path: 'automotora/registrarse',
                component: RegisterComponent,
                data: {
                    opc: 'T'
                }
            }, {
                path: 'recordar-cuenta',
                component: RecuperarCuentaComponent
            }, {
                path: '0081991b3c2227d29d7f8e4f71b8f0867befc79372fe89963ce0c8164056c09d/:idUsuario/:newPass',
                component: ActualizarPassComponent
            }, {
                path: 'autodenar',
                component: SesionEmpresasComponent,
                data: {
                    opc: 'A'
                }
            }, {
                path: 'motodenar',
                component: SesionEmpresasComponent,
                data: {
                    opc: 'M'
                }
            }, {
                path: 'automotora',
                component: SesionEmpresasComponent,
                data: {
                    opc: 'T'
                }
            }, {
                path: 'autodenar/clientes',
                component: SesionUsuariosComponent,
                data: {
                    opc: 1,
                    business: 'A'
                }
            }, {
                path: 'motodenar/clientes',
                component: SesionUsuariosComponent,
                data: {
                    opc: 1,
                    business: 'M'
                }
            }, {
                path: 'automotora/clientes',
                component: SesionUsuariosComponent,
                data: {
                    opc: 1,
                    business: 'T'
                }
            }, {
                path: 'autodenar/colaboradores',
                component: SesionUsuariosComponent,
                data: {
                    opc: 2,
                    business: 'A'
                }
            }, {
                path: 'motodenar/colaboradores',
                component: SesionUsuariosComponent,
                data: {
                    opc: 2,
                    business: 'M'
                }
            }, {
                path: 'automotora/colaboradores',
                component: SesionUsuariosComponent,
                data: {
                    opc: 2,
                    business: 'T'
                }
            }, {
                path: 'autodenar/aseguradora',
                component: SesionUsuariosComponent,
                data: {
                    opc: 3,
                    business: 'A'
                }
            }, {
                path: 'motodenar/aseguradora',
                component: SesionUsuariosComponent,
                data: {
                    opc: 3,
                    business: 'M'
                }
            }, {
                path: 'automotora/aseguradora',
                component: SesionUsuariosComponent,
                data: {
                    opc: 3,
                    business: 'T'
                }
            }
        ]
    },
    {
        path: '',
        component: AdminLayoutComponent,
        canActivate: [LoginGuard],
        children: [
            {
                path: '',
                loadChildren: './dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'asesores',
                loadChildren: './asesores/asesores.module#AsesoresModule'
            },
            {
                path: 'clientes',
                loadChildren: './clientes/clientes.module#ClientesModule'
            },
            {
                path: 'parametrizacion',
                loadChildren: './parametrizacion/parametrizacion.module#ParametrizacionModule'
            },
            {
                path: 'registros-sistema',
                loadChildren: './registros-sistema/registros-sistema.module#RegistrosSistemaModule'
            },
            {
                path: 'permisos',
                loadChildren: './permisos/permisos.module#PermisosModule'
            },
            {
                path: 'usuarios',
                loadChildren: './usuarios/usuarios.module#UsuariosModule'
            },
            {
                path: 'atenciones',
                loadChildren: './tablero-asignacion/tablero-asignacion.module#TableroAsignacionModule'
            },
            {
                path: 'bdc',
                loadChildren: './bdc/bdc.module#BdcModule'
            },
            {
                path: 'mi-cuenta',
                loadChildren: './mi-cuenta/mi-cuenta.module#MiCuentaModule'
            },
            {
                path: 'empleados',
                loadChildren: './empleados/empleados.module#EmpleadosModule'
            },
            {
                path: 'reportes-clientes',
                loadChildren: './reportes-clientes/reportes-clientes.module#ReportesClientesModule'
            },
            {
                path: 'estadisticas',
                loadChildren: './estadisticas/estadisticas.module#EstadisticasModule'
            },
            {
                path: 'control-individual',
                loadChildren: './control-individual/control-individual.module#ControlIndividualModule'
            },
            {
                path: 'agendamiento-posventa',
                loadChildren: './agendamiento-posventa/agendamiento-posventa.module#AgendamientoPosventaModule'
            },
            {
                path: 'tickets',
                loadChildren: './tickets/tickets.module#TicketsModule'
            },
            {
                path: 'eventos-comerciales',
                loadChildren: './eventos-comerciales/eventos-comerciales.module#EventosComercialesModule'
            },
            {
                path: 'seguimiento-del-negocio',
                loadChildren: './seguimiento-negocio/seguimiento-negocio.module#SeguimientoNegocioModule'
            },
            {
                path: 'siembra-y-cosecha',
                loadChildren: './siembra-cosecha/siembra-cosecha.module#SiembraCosechaModule'
            },
            {
                path: 'leads',
                loadChildren: './leads/leads.module#LeadsModule'
            }
        ]
    }
];
