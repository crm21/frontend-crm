import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientesScComponent } from './clientes-sc.component';

describe('ClientesScComponent', () => {
  let component: ClientesScComponent;
  let fixture: ComponentFixture<ClientesScComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientesScComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientesScComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
