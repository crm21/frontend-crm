import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardAdministrativosComponent } from './dashboard-administrativos.component';

describe('DashboardAdministrativosComponent', () => {
  let component: DashboardAdministrativosComponent;
  let fixture: ComponentFixture<DashboardAdministrativosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardAdministrativosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardAdministrativosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
