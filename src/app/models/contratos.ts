export class Contratos {
    constructor(
        public fechaFirmaContrato: string,
        public fechaDesvinculacion: string,
        public descripcionSalario: string,
        public valorSalario: string,
        public fechaIniciacionLabores: string,
        public observacion: string,
        public tipoContrato: string,
        public tipoDesvinculacion: string,
        public cargo: string,
        public persona: string
    ) { }
};