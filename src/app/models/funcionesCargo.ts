export class FuncionesCargos {
    constructor(
        public cargo: string,
        public titulo: string,
        public accionEspecifica: string,
        public resultadosObtenidos: string,
        public tiempoEjecucion: string,
        public recursosUtilizados: string
    ) { }
};